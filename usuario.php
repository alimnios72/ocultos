<?php
include('main.php');
$rutaTemplate = 'usuarios/';
$template = 'userData.tpl.php';
$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css');
$js=array('jquery.js','switcher.js','toggle.js','ui.core.js');
$topmenu['ini'] = 'id="menu-active"';

$data = $_POST;

if(isset($data['updateData']) && $data['updateData'] !== ''){
	if($usuarioDB->updateAdministrador($data))
		$success = "Usuario actualizado!";
	else
		$error = "Error al guardar usuario";
}
if(isset($data['updatePassword']) && $data['updatePassword'] !== ''){
	if($data['password'] !== $data['c_password'])
		$error = "Las contrase�as no coinciden";
	elseif(strlen($data['password']) < 5)
		$error = "La contrase�a debe tener al menos 5 caracteres";
	elseif(preg_match('(^(?=.*[a-zA-Zs])(?=.*\d).+$)', $data['password']) !== 1)
		$error = "La contrase�a de estar formada de letras y al menos un n�mero";
	else{
		if($usuarioDB->updateAdministradorPass($data['id'], $adminLogin->clean($data['password'])))
			$success = "Contrase�a actualizada!";
		else
			$error = "Problema al actualizar contrase�a";
	}
}
$user = $usuarioDB->getAdministradorByID($_SESSION['userID']);

include(RUTA_TPL.'header.tpl.php');
include(RUTA_TPL.'home.tpl.php');
include(RUTA_TPL.'footer.tpl.php');
?>