<?php
$data = $_POST;
$rutaTemplate = 'contabilidad/';
$template = 'gestionCh.tpl.php';
$statusCH = array('Elaborado', 'Cobrado', 'Cancelado');
$tipoCred = array('INDIVIDUAL', 'GRUPAL');

$criterios = array();
if(isset($_POST['update'])){
	if(isset($data['banco']) && $data['banco'] != "")
		$criterios['banco'] = $data['banco'];
	if(isset($data['status']) && $data['status'] != "")
		$criterios['status'] = $data['status'];
	if($data['fecha1'] != "" && $data['fecha2'] != ""){
		$criterios['fechas'][0] = $data['fecha1'];
		$criterios['fechas'][1] = $data['fecha2'];
	}
}
else{
	$criterios['fechas'][0] = subDays(date('Y-m-d'), 3);
	$criterios['fechas'][1] = addDays(date('Y-m-d'), 3);
}
$cheques = $transaccionDB->getChequesByCriterio($criterios);
include(RUTA_TPL.'home.tpl.php');
?>