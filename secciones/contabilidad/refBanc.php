<?php
$rutaTemplate = 'contabilidad/';
$template = 'refBanc.tpl.php';
$data = $_POST;
$seed = "SACREDI";
$path = "uploads/referenciasBancarias/";
$msg[1] = "Archivo subido con �xito!";
$msg[2] = "Error al subir el archivo!";
$msg[3] = "El tama�o del archivo no puede ser mayor a 1MB";
$msg[4] = "Ese archivo ya existe";
$msg[5] = "Debes especificar todos los campos";
$msg[6] = "Error al procesar el archivo";
$msg[7] = "Error al procesar la referencia";

if(isset($data['upload'])){
	if($_FILES["archivo"]["error"] > 0)
		header('location:contabilidad.php?content=refBanc&msg=2');
	elseif($_FILES["archivo"]["size"] > 1048576)
		header('location:contabilidad.php?content=refBanc&msg=3');
	else{
		if(file_exists("uploads/" . $_FILES["archivo"]["name"]))
			header('location:contabilidad.php?content=refBanc&msg=4');
		else
		{
			$fileName = md5($seed.$_FILES["archivo"]["name"]);
			move_uploaded_file($_FILES["archivo"]["tmp_name"], $path.$fileName);
			if(file_exists($path.$fileName) && is_readable ($path.$filename)){
				$handle = @fopen($path.$fileName, "r");
				$contents = fgets($handle);
				if($handle) {
					while (($buffer = fgets($handle, 4096)) !== false) {
						$line = explode('|',$buffer);
						if(is_array($line) && count($line) > 1){
							$depositos[] = array('referencia'=>substr($line[1],-18), 'banco'=>$data['id_cuenta'],
											 'monto'=>number_from_format(substr($line[7],1),2), 
											 'fecha'=>trim($line[14]), 'clave'=>$line[10],
											 'observaciones'=>'Referencia Bancaria Banorte');
						}
					}
					if (!feof($handle))
						header('location:contabilidad.php?content=refBanc&msg=6');
					fclose($handle);
				}
				//Verificamos que las fechas no se hayan procesado
				$fechaIni = $depositos[0]['fecha'];
				$tmp = explode('/', $fechaIni);
				$fechaIni = "{$tmp[2]}-{$tmp[1]}-{$tmp[0]}";
				$fechaFin = $depositos[count($depositos) - 1]['fecha'];
				$tmp = explode('/', $fechaFin);
				$fechaFin = "{$tmp[2]}-{$tmp[1]}-{$tmp[0]}";
				if(!$transaccionDB->existsReferenciaBanc($fechaIni, $fechaFin)){
					//Verifico que haya sido procesado ese archivo con las fechas ini y fin
					//Proceso los dep�sitos obtenidos
					$conciliados = 0;
					foreach($depositos as &$deposito){
						//Guardo el dep�sito en la BD
						$fecha = explode('/', $deposito['fecha']);
						$deposito['fecha'] = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
						$deposito['id'] = $transaccionDB->addDeposito($deposito);
						$credito = getDataFromReferencia($deposito['referencia'], $deposito['monto']);
						if($credito){
							//echo "se va a procesar la referencia {$deposito['referencia']}";
							$credito['idDeposito'] = $deposito['id'];
							aplicaPago($credito, $creditoDB, $transaccionDB, $ubicacionDB);
							$conciliados++;
						}
						else
							echo "error al procesar la referencia {$deposito['referencia']}";
					}
					//Inserto el archivo de referencia bancaria que se proces�
					$transaccionDB->addArchivoReferenciaBancaria($data['id_cuenta'],$fileName,$fechaIni,$fechaFin,$conciliados,$_SESSION['userID']);
					header('location:contabilidad.php?content=refBanc&msg=1');
				}
				else 
					header('location:contabilidad.php?content=refBanc&msg=4');
			}
			else
				header('location:contabilidad.php?content=refBanc&msg=6');
		}
	}
}

$subidos = $transaccionDB->getReferenciasBancSubidas();

include(RUTA_TPL.'home.tpl.php');
?>
