<?php
$data = $_POST;

$conciliado = array('No conciliado','Conciliado');
$rutaTemplate = 'contabilidad/';
$template = 'depositos.tpl.php';

if(isset($data['limpiar']) && $data['limpiar'] != '')
	header('location: contabilidad.php?content=depositos');

if(isset($data['data']) && $data['updateDep'] == 1){
	if(intval($data['tipo_cuenta']) != 4 && intval($data['tipo_cuenta']) != 5)
		$errormsg = "Problema para actualizar flujo de efectivo";
	else{
		$transaccionDB->updateDepositoById($data);
		$successmsg = "Dep�sito actualizado correctamente";
	}
	
}
elseif(isset($data['data'])){
	if(intval($data['tipo_cuenta']) == 4 || intval($data['tipo_cuenta']) == 5){
		if($data['monto'] != "" && $data['fecha'] != "" && $transaccionDB->addDeposito($data))
			$successmsg = "Dep�sito agregado al sistema correctamente";
		else
			$errormsg = "Problema para ingresar el dep�sito";
	}
	else{
		if($data['monto'] != "" && $data['fecha'] != "" ){
			$dep = $transaccionDB->addDeposito($data);
			$data['descripcion'] = $data['observaciones'];
			$data['userID'] = $_SESSION['userID'];
			$data['cta1'] = 2;
			$data['cta2'] = $data['tipo_cuenta'];
			if($transaccionDB->addTransaccionEfectivo($data)){
				$transaccionDB->updateStatusDepositos(array($dep),1);
				$successmsg = "Dep�sito agregado al sistema correctamente";
			}
			else
				$errormsg = "Problema para ingresar el dep�sito";
		}
	}
    
}
if(isset($_GET['idDep']) && $_GET['idDep'] != ''){
	$deposito = $transaccionDB->getDepositosById($_GET['idDep']);
}

if(isset($data['filtrar']) && $data['filtrar'] != ""){
	$criteriaSet = false;
	if($data['fecha1'] != "" && $data['fecha2'] != "")
	{
		$criterio['fechas'][0] = $data['fecha1'];
		$criterio['fechas'][1] = $data['fecha2'];
		$criteriaSet = true;
	}
	if($data['monto1'] != "" && $data['monto2'] != "")
	{
		$criterio['montos'][0] = $data['monto1'];
		$criterio['montos'][1] = $data['monto2'];
		$criteriaSet = true;
	}
	if($data['referencia'])
	{
		$criterio['clave'] = $data['referencia'];
		$criteriaSet = true;
	}
	if($data['folio'])
	{
		$criterio['folio'] = $data['folio'];
		$criteriaSet = true;
	}
	if(!$criteriaSet){
		$day = 1;
		$month = date('m');
		$year = date('Y');
		$date = "{$year}-{$month}-{$day}";
		$criterio['byMes'][0] = $month;
		$criterio['byMes'][1] = $year;
	}
}
else{
	$day = 1;
	$month = date('m');
	$year = date('Y');
	$date = "{$year}-{$month}-{$day}";
	$criterio['byMes'][0] = $month;
	$criterio['byMes'][1] = $year;
}

$depositos = $transaccionDB->getDepositosByCriterio($criterio);
$criterio['tipo_cuenta'] = 1;
$cuentas = $transaccionDB->getCuentasEfectivo($criterio);

include(RUTA_TPL.'home.tpl.php');
?>