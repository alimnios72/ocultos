<?php
$data = $_POST;
$rutaTemplate = 'sistema/';
$template = 'editarCredito.tpl.php';

$statusInactivos = array(0,1,3);
$statusActivos = array(2,4);
$vendedores = $usuarioDB->getVendedores();

if(isset($_GET['idCredito']) && is_numeric($_GET['idCredito'])) {
	$credito = $creditoDB->getCreditoById($_GET['idCredito']);
	$acreditados = $creditoDB->getAcreditadosByCredito($_GET['idCredito']);
	$zonas = $ubicacionDB->getZonas();
	//var_dump($vendedores);
}
if(isset($_GET['borrar']) && intval($_GET['borrar']) == 1){
	deletePagosCredito($transaccionDB, $_GET['idCredito']);
	header("location: sistema.php?content=editar&idCredito={$_GET['idCredito']}");
}
if(isset($_GET['borrarCred']) && intval($_GET['borrarCred']) == 1){
	deletePagosCredito($transaccionDB, $_GET['idCredito']);
	$creditoDB->deleteCredito($_GET);
	header("location: sistema.php?content=buscar");
}
if(isset($data['update'])){
	$cambio = false;
	if($credito['tasa_interes'] != $data['tasaInteres'] || 
	  $credito['plazo'] != $data['plazo'] ||
	  $credito['periodo'] != $data['periodo'] ||
	  $credito['status'] != $data['status']){
		$cambio = true;
	}
	if($cambio){
		if(in_array($data['status'], $statusActivos) && in_array($credito['status'], $statusInactivos)){
			deleteTablaAmortizacion($transaccionDB,$_GET['idCredito']);
			createTablaAmortizacion($creditoDB, $transaccionDB, $_GET['idCredito'], $data);
		}
		elseif(in_array($data['status'], $statusInactivos))
			deleteTablaAmortizacion($transaccionDB,$_GET['idCredito']);
	}
	if($creditoDB->updateCreditoById($data)){
		header("location: sistema.php?content=editar&idCredito={$data['id_credito']}&msg=ok");
	}
	else
		header("location: sistema.php?content=editar&idCredito={$data['id_credito']}&msg=error");
}

include(RUTA_TPL.'home.tpl.php');



/*function compareAmounts($credito, $data){
	$montoTotal = $credito['montoTotal'];
	$montos = $data['cantidad'];
	$sum = 0;
	if(!empty($montos)){
		foreach($montos as $monto)
			$sum += $monto;
	}
	return $montoTotal == $sum;
}*/
?>