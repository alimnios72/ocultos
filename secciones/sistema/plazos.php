<?php
$data = $_POST;
$rutaTemplate = 'sistema/';
$template = 'plazos.tpl.php';

$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$plazos = $sist->plazos;

if(isset($data['nuevo']) && $data['plazo'] != "" && is_numeric($data['plazo'])){
	if(!valueExists($plazos, $data['plazo'])){
			$plazo = $plazos->addChild('plazo');
			$plazo->addChild('meses', $data['plazo']);
			$plazo->addChild('activo', 0);
			$sist->asXML($xml);
			header('location:sistema.php?content=plazos&msg=ok');
	}
	else
		header('location:sistema.php?content=plazos&msg=exists');	
}

function valueExists($plazos, $meses){
	$flag =  false;
	foreach($plazos->plazo as $plazo){
		if($plazo->meses == $meses)
			$flag = true;
	}
	return  $flag;		
}

include(RUTA_TPL.'home.tpl.php');
?>