<?php
$data = $_POST;
$rutaTemplate = 'sistema/';
$template = 'tasasInteres.tpl.php';

$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$intOrd = $sist->interes_ordinario;
$intMor = $sist->interes_moratorio;

if(isset($data['nuevo']) && $data['intOrd'] != "" && is_numeric($data['intOrd'])){
	if(!valueExists($intOrd, $data['intOrd'])){
			$tasa = $intOrd->addChild('tasa');
			$tasa->addChild('monto', $data['intOrd']);
			$tasa->addChild('activo', 0);
			$sist->asXML($xml);
			header('location:sistema.php?content=tasas&msg=ok');
	}
	else
		header('location:sistema.php?content=tasas&msg=exists');	
}
elseif(isset($data['nuevo']) && $data['intMor'] != "" && is_numeric($data['intMor'])){
	if(!valueExists($intMor, $data['intMor'])){
		$tasa = $intMor->addChild('tasa');
		$tasa->addChild('monto', $data['intMor']);
		$tasa->addChild('activo', 0);
		$sist->asXML($xml);
		header('location:sistema.php?content=tasas&msg=ok');
	}
	else
		header('location:sistema.php?content=tasas&msg=exists');
}

function valueExists($tasas, $monto){
	$flag =  false;
	foreach($tasas->tasa as $tasa){
		if($tasa->monto == $monto)
			$flag = true;
	}
	return  $flag;		
}

include(RUTA_TPL.'home.tpl.php');
?>