<?php
$data = $_POST;
$rutaTemplate = 'sistema/';
$template = 'nuevoUsuario.tpl.php';

if(isset($data['add'])){
	if(!validaCampos($data))
		header("location: sistema.php?content=usuarios&msg=campos");
	else{
		if($data['password'] != $data['password_c'])
			header("location: sistema.php?content=usuarios&msg=pass");
		else{
			$personaDB->addUsuario($data);
			header("location: sistema.php?content=usuarios&msg=ok");
		}
	}
}

function validaCampos($fields){
	foreach ($fields as $campo){
		if($campo == "")
			return false;
	}
	return true;
}

include(RUTA_TPL.'home.tpl.php');
?>