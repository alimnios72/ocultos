<?php
$data = $_POST;
$rutaTemplate = 'sistema/';
$template = 'alertapago.tpl.php';

$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$montoAlerta = !empty($sist->montoAlerta) ? $sist->montoAlerta : 0;

if(isset($data['data']) && $data['monto'] != "" && is_numeric($data['monto'])){
	$sist->montoAlerta = $data['monto'];
	$sist->asXML($xml);
	header('location:sistema.php?content=alertapago&msg=ok');	
}


include(RUTA_TPL.'home.tpl.php');
?>