<?php
$data = $_POST;
$rutaTemplate = 'cobranza/';
$template = 'conciliacion.tpl.php';

$criterios = array();
//Para la conciliaci�n escojo s�lo dep�sito NO aplicados
$criterios['aplicado'] = 0;
if(isset($_POST['update'])){
	if(isset($data['banco']) && $data['banco'] != "")
		$criterios['banco'] = $data['banco'];
	if($data['fecha1'] != "" && $data['fecha2'] != ""){
		$criterios['fechas'][0] = $data['fecha1'];
		$criterios['fechas'][1] = $data['fecha2'];
	}
	if($data['monto1'] != "" && $data['monto2'] != ""){
		$criterios['montos'][0] = $data['monto1'];
		$criterios['montos'][1] = $data['monto2'];
	}
	if(isset($data['fechaExacta']) && $data['fechaExacta']!=""){
		$criterios['fecha'] = $data['fechaExacta'];
	}
}
if(!isset($_POST['update']) || empty($criterios)){
	$criterios['fechas'][0] = date('Y-m-d');
	$criterios['fechas'][1] = date('Y-m-d');
}
if(isset($_GET['msg']) && $_GET['msg'] == 'ok')
	$msg = "Pago aplicado correctamente";
$depositos = $transaccionDB->getDepositosByCriterio($criterios);
include(RUTA_TPL.'home.tpl.php');
?>