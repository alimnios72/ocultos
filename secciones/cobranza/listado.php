<?php
$data = $_POST;
$rutaTemplate = 'cobranza/';
$template = 'listaAmortizaciones.tpl.php';

if(!isset($_POST['tipo'])){
    $_POST['tipo'] = 'fecha';
    $criterios['fecha'] = date('Y-m-d');
    $prev = subDays($criterios['fecha'], 1);
    $next = addDays($criterios['fecha'], 1);
}
if($_POST['fecha1'] != '' && $_POST['fecha2'] != ''){
    $criterios['fechas'][] = $_POST['fecha1'];
    $criterios['fechas'][] = $_POST['fecha2'];
}
$criterios['statusAmo'] = 0;
$amortizaciones = $transaccionDB->getAmortizacionesByCriterio($criterios);
//var_dump($amortizaciones);
include(RUTA_TPL.'home.tpl.php');
?>