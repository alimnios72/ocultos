<?php
$data = $_POST;
$rutaTemplate = 'cobranza/';


if(isset($_GET['credito']) && is_numeric($_GET['credito'])){
	$template = 'anticipaCredito.tpl.php';
	$credito = $creditoDB->getCreditoById($_GET['credito']);
	$amort = $transaccionDB->getAmortizacionByCredito($credito['id_credito']);
	$tot = 0; $pag = 0; $int = 0;
	if(is_array($amort) && !empty($amort)){
		foreach($amort as $am){
			if($am['status'] == "1")
				$pag++;
			else
				$int += $am['interes'];
				$int += $am['iva'];
			$tot++;
		}
	}
	//Si pago al menos la mitad de su cr�dito entonces si puede anticipar
	$anticipo = $pag < $tot/2;
	$nopag = $tot - $pag;
	$admins = $usuarioDB->getSuperUsers();
}
else
	$template = 'anticipo.tpl.php';

if(isset($data['buscar'])){
	if(isset($data['expediente']) && $data['expediente'] != ''){
		$criterios['expediente'] = $data['expediente'];
	}
	elseif(isset($data['idPersona']) && $data['idPersona'] != ''){
		$criterios['persona'] = $data['idPersona'];
	}
	elseif(isset($data['grupo']) && $data['grupo'] != ''){
		$criterios['grupo'] = $data['grupo'];
	}
	$creditos = $creditoDB->getCreditoByCriterio($criterios);
}

if(isset($data['idCredito']) && is_numeric($data['idCredito'])){
	$amt = $transaccionDB->getAmortizacionByCredito($data['idCredito']);
	if(is_array($amt) && !empty($amt)){
		foreach($amt as $val){
			if($val['status'] == "0"){
				$movs = explode(',',$val['movimientos']);
				foreach($movs as $mov){
					$data['id_movimiento'] = $mov;
					$data['interes'] = 0;
					$data['iva'] = 0;
					$transaccionDB->updateMovimiento($data);
				}
			}
		}
	}
	header("location: cobranza.php?content=anticipo&msg=ok");	
}
include(RUTA_TPL.'home.tpl.php');
?>