<?php
$data = $_POST;
$rutaTemplate = 'cobranza/';

if(isset($_GET['credito']) && is_numeric($_GET['credito'])){
	$template = 'moraCredito.tpl.php';
	$credito = $creditoDB->getCreditoById($_GET['credito']);
	$criterios = array();
	$data['idCred'] = $credito['id_credito'];
	$criterios['idCred'] = $credito['id_credito'];
	$criterios['tipoAbono'] = 1;
	$criterios['tipoCargo'] = 1;
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
}
else
	$template = 'pagoMora.tpl.php';

if(isset($data['buscar'])){
	if(isset($data['expediente']) && $data['expediente'] != ''){
		$criterios['expediente'] = $data['expediente'];
	}
	elseif(isset($data['idPersona']) && $data['idPersona'] != ''){
		$criterios['persona'] = $data['idPersona'];
	}
	elseif(isset($data['grupo']) && $data['grupo'] != ''){
		$criterios['grupo'] = $data['grupo'];
	}
	$creditos = $creditoDB->getCreditoByCriterio($criterios);
}

if(isset($data['aplicar']) && $data['monto'] != "" && $data['fecha'] != ""){
	$credito = $creditoDB->getCreditoById($data['idCred']);
	$acreditados = $creditoDB->getAcreditadosByCredito($data['idCred']);
	$transaccionDB->addDeposito($data);
	$pagoMora['id_deposito'] = $transaccionDB->lastId;
	$pagoMora['fecha'] = $data['fecha'];
	foreach($acreditados as $acred){
		$pagoMora['pagoFijo'] = $data['monto']* ($acred['cantidad']/$credito['montoTotal']);
		$pagoMora['interes'] = $pagoMora['pagoFijo']/1.16;
		$pagoMora['iva'] = $pagoMora['interes']*0.16;
		$pagoMora['capital'] = 0;
		$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoMora, 1);
	}
	$data['aplicado'] = 1;
	$data['id_deposito'] = $pagoMora['id_deposito'];
	$data['id_banco'] = $data['banco'];
	$data['fecha_deposito'] = $data['fecha'];
	$transaccionDB->updateDeposito($data);
	//Si termina de pagar el credito y no debe mora, el cr�dito queda concluido
	$criterios['idCred'] = $credito['id_credito'];
	$criterios['tipoCargo'] = 0; //amortizacion
	$criterios['tipoAbono'] = 0; //pago al credito
	$cargos = $transaccionDB->getCargosByCriterio($criterios);
	$concluido = true;
	foreach($cargos as $val)
		if(intval($val['status']) == 0)
			$concluido =  false;
	$criterios['tipoCargo'] = 1; //mora
	$criterios['tipoAbono'] = 1; //pago mora
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
	//Si esta concluido y no debe mora, cambio status a concluido
	if($concluido && abs($balanceMora['monto']) < 4/10)
		$creditoDB->updateStatusCredito($criterios['idCred'], 4);
	$successmsg = "Pago de mora aplicado correctamente";
}
include(RUTA_TPL.'home.tpl.php');
?>