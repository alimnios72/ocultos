<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/22/15
 * Time: 1:43 PM
 */

$data = $_POST;
$rutaTemplate = 'cobranza/';
$template = 'asignacion.tpl.php';
$form = 1;

if(!empty($data) && isset($data['codigo']))
{
    $credito = $creditoDB->getCreditoById($data['codigo']);
    $balance = $transaccionDB->getBalanceByCriterio(array(
        'idCred'    => $credito['id_credito'],
        'fecha'     => date('Y-m-d'),
        'tipoCargo' => 0,
        'tipoAbono' => 0
    ));
    $tabla   = $transaccionDB->getAmortizacionByCredito($credito['id_credito']);
    $cuentas = $transaccionDB->getCuentasEfectivo(array('tipo_cuenta' => 1));
    $monto   = $tabla[0]['monto'];
    $form = 2;

    if(is_null($credito) || is_null($tabla))
    {
        $errormsg = 'Cr�dito no encontrado';
        $form = 1;
    }
}
elseif(!empty($data) && isset($data['aplicar']))
{
    $payload = [];
    if($data['monto'] !== '' && is_numeric($data['monto']) && $data['fecha'] != '')
    {
        $deposito = saveDeposito($data, $transaccionDB);
        $payload = [
            'idDeposito' => $deposito,
            'idCredito'  => $data['id_credito']
        ];
        aplicaPago($payload, $creditoDB, $transaccionDB, $ubicacionDB, false);
        $msg = "Pago aplicado correctamente.";
    }
}

include(RUTA_TPL.'home.tpl.php');
?>