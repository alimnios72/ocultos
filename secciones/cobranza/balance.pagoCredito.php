<?php
define('_iva', 1.16);
$data = $_POST;
$rutaTemplate = 'cobranza/';
$template = 'balance.tpl.php';
$precision = 10000;

$criterios['idCred'] = $data['credito'];
$criterios['tipoCargo'] = 0; //amortizacion
$criterios['tipoAbono'] = 0; //pago al credito
if(isset($data['id_deposito']) && isset($data['credito'])){
	$deposito = $transaccionDB->getDepositosById($data['id_deposito']);
	$cargos = $transaccionDB->getCargosByCriterio($criterios);
	//Todos los abonos que hace ya sea mora o al cr�dito
	$abonos = $transaccionDB->getAbonosByCriterio($criterios);
	//El monto total de los pagos al cr�dito
	$sumPagos = $transaccionDB->getSumPagos($data['credito']);
	$credito = $creditoDB->getCreditoById($data['credito']);
	$acreditados = $creditoDB->getAcreditadosByCredito($credito['id_credito']);
	$criterios['fecha'] = $deposito['fecha_deposito'];
	$balance = $transaccionDB->getBalanceByCriterio($criterios);
	$pFijo = $cargos[0]['monto'];
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	$diasMora = MoraTotal($cargos, $criterios['fecha'], $diasInhabiles);
	$mora = $diasMora * $pFijo * ($credito['tasa_mora']/100) * 1.16;
	$deudaTotal = $mora + $balance['interes'] + $balance['iva'] + $balance['capital'];
}
if(isset($_POST['aplicar'])){
	$restante = 0;
	$pago = array();
	$restante = $deposito['monto'];
	
	if($restante > 0){
		$i = 0;
		$cubreAmort = false;
		while($restante > 0){
			$interes = 0;
			$iva = 0;
			$capital = 0;
			//Primero verifico que la amortizaci�n no haya sido pagada
			if(isset($cargos[$i]) && $cargos[$i]['status'] == 0){
				if(strtotime($cargos[$i]['fecha_mov']) <= strtotime($deposito['fecha_deposito'])){
					//Cuando tiene alguna amortizaci�n pagada parcialmente, tiene saldo a su favor de ese pago
					if($sumPagos['monto'] < 0 && $sumPagos['monto'] < -1/10){
						//Si con el pago no alcanza a cubrir el inter�s
						if(($restante - $sumPagos['interes']) < $cargos[$i]['interes']){
							$interes = $restante / _iva;
							$iva = $interes * (_iva-1);
							$capital = 0;
							$cubreAmort = false;
						}						
						else{
							$interes = $cargos[$i]['interes'] + $sumPagos['interes'];
							$interes = ($interes < 1/$precision) ? 0 : $interes;
							$iva = $cargos[$i]['iva'] + $sumPagos['iva'];
							$iva = ($iva < 1/$precision) ? 0 : $iva;
							$dif = $restante - $interes - $iva - $sumPagos['capital'];
							$mayor = $dif > $cargos[$i]['capital'] ? $dif : $cargos[$i]['capital'];
							$menor = $dif < $cargos[$i]['capital'] ? $dif : $cargos[$i]['capital'];
							if($dif < $cargos[$i]['capital'] && ($mayor-$menor) > 1/10){
								$capital = $restante;
								$cubreAmort = false;
							}
							else{
								$capital = $cargos[$i]['capital'] + $sumPagos['capital'];
								$cubreAmort = true;
							}
						}
						$sumPagos['monto'] = 0;
					}
					//Para las amortizaciones que pueden ser pagadas completas 
					elseif($cargos[$i]['monto'] <= $restante){
						$interes = $cargos[$i]['interes'];
						$iva = $cargos[$i]['iva'];
						$capital = $cargos[$i]['capital'];
						$cubreAmort = true;
					}
					//En caso de que no alcance a cubrir la amortizaci�n se ajusta el pago cubriendo primero intereses
					elseif($cargos[$i]['monto'] > $restante){
						//Si ni siquiera alcanza a cubrir el interes e iva de la amortizaci�n hago un ajuste
						if($restante < ($cargos[$i]['interes'] + $cargos[$i]['iva'])){
							$interes = $restante / _iva;
							$iva = $interes * (_iva-1);
							$capital = 0;
						}
						//En caso de que alcance a cubrir interes e iva pero no el capital hago otro ajuste
						else{
							$interes = $cargos[$i]['interes'];
							$iva = $cargos[$i]['iva'];
							$capital = $restante - $interes - $iva;
						}
						$cubreAmort = false;
					}
				}
				///////////////////////////////////////////////////////////////////////////////////////////////
				//En caso de que cubra sus obligaciones de pago, lo que sobre adelantar� amortizaciones o mora
				///////////////////////////////////////////////////////////////////////////////////////////////
				elseif(strtotime($cargos[$i]['fecha_mov']) > strtotime($deposito['fecha_deposito'])){
					//Si ya cubri� sus pagos vencidos y sobra dinero, paga mora en caso de deber
					/*if($mora > 0){
						if($restante > $mora){
							$pagoMora['interes'] = $mora / _iva;
							$pagoMora['iva'] = $pagoMora['interes'] * (_iva - 1);
						}
						else{
							$pagoMora['interes'] = $restante / _iva;
							$pagoMora['iva'] = $pagoMora['interes'] * (_iva - 1);
						}
						$restante -= $pagoMora['interes'] + $pagoMora['iva'];
						$restante = ($restante < 1/$precision) ? 0 : $restante;
					}*/
					//Si adelant� una parcialidad previamente
					if($sumPagos['monto'] < 0){
						//No cubre por completo la parte de capital de la parcialidad
						if(($restante - $sumPagos['capital']) < $cargos[$i]['capital']){
							$capital = $restante;
							$interes = 0;
							$iva = 0;
							$cubreAmort = false;
						}
						//En caso contrario alcanza a pagar todo el capital de la parcialidad y adelante inter�s
						else{
							$capital = $cargos[$i]['capital'] + $sumPagos['capital'];
							$capital = ($capital < 1/$precision) ? 0 : $capital;
							if($restante - $capital - $sumPagos['interes'] - $sumPagos['iva'] < $cargos[$i]['interes'] + $cargos[$i]['iva']){
								$interes = ($restante - $capital) / _iva;
								$iva = $interes * (_iva-1);
								$cubreAmort = false;
							}
							else{
								$interes = $cargos[$i]['interes'] + $sumPagos['interes'];
								$iva = $cargos[$i]['iva'] + $sumPagos['iva'];
								$cubreAmort = true;
							}
							
						}
						$sumPagos['monto'] = 0;
					}
					elseif($cargos[$i]['monto'] <= $restante){
						$interes = $cargos[$i]['interes'];
						$iva = $cargos[$i]['iva'];
						$capital = $cargos[$i]['capital'];
						$cubreAmort = true;
					}
					elseif($cargos[$i]['monto'] > $restante){
						if($restante > $cargos[$i]['capital']){
							$capital = $cargos[$i]['capital'];
							$interes = ($restante - $capital) / _iva;
							$iva = $interes * (_iva-1);
						}
						else{
							$interes = 0;
							$iva = 0;
							$capital = $restante;
						}
						$cubreAmort = false;
					}
				}
				//Acumulo todos lo que voy a juntar en el pago
				$pago['interes'] += $interes;
				$pago['iva'] += $iva;
				$pago['capital'] += $capital;
				if($cubreAmort)
					$pago['cargos_cubiertos'][] = $i;
				//Descuento al sobrante lo que aplique del pago
				$restante -= $interes + $iva + $capital;
				$restante = ($restante < 1/$precision) ? 0 : $restante;
			}
			elseif(isset($cargos[$i]) && $cargos[$i]['status'] == 1){
				$sumPagos['monto'] += $cargos[$i]['monto'];
				$sumPagos['interes'] += $cargos[$i]['interes'];
				$sumPagos['iva'] += $cargos[$i]['iva'];
				$sumPagos['capital'] += $cargos[$i]['capital'];
			}
			//Si ya no hay m�s que pagar y sobra dinero, todo se lo pongo como capital
			else{
				$pago['capital'] += $restante;
				$restante = 0;
			}
			$i++;
		}
	}
	//Aplicamos la parte proporcional del pago a cada integrante del cr�dito
	$pagoAcred['fecha'] = $deposito['fecha_deposito'];
	$pagoAcred['id_deposito'] = $deposito['id_deposito'];
	foreach($acreditados as $acred){
		$pagoAcred['interes'] = $pago['interes'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['iva'] = $pago['iva'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['capital'] = $pago['capital'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['pagoFijo'] = $pagoAcred['interes']+$pagoAcred['iva']+$pagoAcred['capital'];
		$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoAcred, 0);
	}
	//Si cubri� amortizaciones con su pago entonces cambiamos su status a pagado y adem�s
	//metemos la mora al sistema de las amortizaciones que ya pag� (si existe).
	if(!empty($pago['cargos_cubiertos'])){
		foreach ($pago['cargos_cubiertos'] as $num){
			$transaccionDB->updateCargo($cargos[$num]['fecha_mov'], $credito['id_credito'], 1);
			$cargosMora[] = $cargos[$num]; 
		}
	}
	//La mora efectiva es la que se va a guardar en el sistema y esta representa la mora de los 
	//pagos que ya fueron cubiertos, no se cobra mora de pagos todav�a incompletos.
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	$diasMoraEfectiva = MoraTotal($cargosMora, $deposito['fecha_deposito'], $diasInhabiles);
	$moraEfectiva = $diasMoraEfectiva * $pFijo * ($credito['tasa_mora']/100) * 1.16;
	if($moraEfectiva > 0){
		$moraAcred['fecha'] = $deposito['fecha_deposito'];
		foreach($acreditados as $acred){
			$moraAcred['interes'] = ($moraEfectiva/_iva)* ($acred['cantidad']/$credito['montoTotal']);
			$moraAcred['iva'] = $moraAcred['interes'] * (_iva-1);
			$moraAcred['capital'] = 0;
			$moraAcred['pagoFijo'] = $moraAcred['interes']+$moraAcred['iva'];
			$transaccionDB->addMovimiento($acred['id_acreditado'], 0, $moraAcred, 1);
		}
	}
	//Si adelanta y debe mora entonces el sobrante se va a �sta
	/*if($pagoMora['interes'] > 0){
		$pagoMoraAcred['capital'] = 0;
		$pagoMoraAcred['fecha'] = $deposito['fecha_deposito'];
		$pagoMoraAcred['id_deposito'] = $deposito['id_deposito'];
		foreach($acreditados as $acred){
			$pagoMoraAcred['interes'] = $pagoMora['interes'] * ($acred['cantidad']/$credito['montoTotal']);
			$pagoMoraAcred['iva'] = $pagoMora['iva'] * ($acred['cantidad']/$credito['montoTotal']);
			$pagoMoraAcred['pagoFijo'] = $pagoMoraAcred['interes']+$pagoMoraAcred['iva'];
			$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoMoraAcred, 1);
		}
	}*/
	
	//var_dump($moraEfectiva);die();
	//Actualizamos la informaci�n del dep�sito y lo marcamos como 'conciliado'
	$deposito['aplicado'] = 1;
	$transaccionDB->updateDeposito($deposito);
	header('location:cobranza.php?content=conciliacion&msg=ok');
}

include(RUTA_TPL.'home.tpl.php');
?>