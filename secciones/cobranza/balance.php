<?php
$data = $_POST;
$rutaTemplate = 'cobranza/';
$template = 'balance.tpl.php';

if(isset($data['idDeposito']) && isset($data['idCredito']) && isset($_POST['aplicar'])){
	$done = false;
	$deposito = $transaccionDB->getDepositosById($data['idDeposito']);
	$credito = $creditoDB->getCreditoById($data['idCredito']);
	
	if(intval($deposito['id_cuenta']) == 4)
    {
		$dat['descripcion'] = "Pago al cr�dito {$credito['expediente']}";
		if(aplicaPago($data, $creditoDB, $transaccionDB, $ubicacionDB, isset($data['aplicarMora'])))
        {
            $done = true;
        }
	}
	elseif(intval($deposito['id_cuenta']) == 5)
    {
		$acreditados = $creditoDB->getAcreditadosByCredito($data['idCredito']);
		$pagoMora['id_deposito'] = $deposito['id_deposito'];
		$pagoMora['fecha'] = $deposito['fecha_deposito'];
		foreach($acreditados as $acred){
			$pagoMora['pagoFijo'] = $deposito['monto']* ($acred['cantidad']/$credito['montoTotal']);
			$pagoMora['interes'] = $pagoMora['pagoFijo']/1.16;
			$pagoMora['iva'] = $pagoMora['interes']*0.16;
			$pagoMora['capital'] = 0;
			$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoMora, 1);
		}
		$data['aplicado'] = 1;
		$data['id_deposito'] = $pagoMora['id_deposito'];
		$data['id_banco'] = $deposito['id_banco'];
      $data['monto'] = $deposito['monto'];
      $data['clave'] = $deposito['clave'];
		$data['fecha_deposito'] = $deposito['fecha_deposito'];
		$transaccionDB->updateDeposito($data);
		//Si termina de pagar el credito y no debe mora, el cr�dito queda concluido
		$criterios['idCred'] = $credito['id_credito'];
		$criterios['tipoCargo'] = 0; //amortizacion
		$criterios['tipoAbono'] = 0; //pago al credito
		$cargos = $transaccionDB->getCargosByCriterio($criterios);
		$concluido = true;
		foreach($cargos as $val){
			if(intval($val['status']) == 0)
				$concluido =  false;
		}
		$criterios['tipoCargo'] = 1; //mora
		$criterios['tipoAbono'] = 1; //pago mora
		$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
		//Si esta concluido y no debe mora, cambio status a concluido
		if($concluido && abs($balanceMora['monto']) < 4/10)
			$creditoDB->updateStatusCredito($criterios['idCred'], 4);
		$done = true;
		$dat['descripcion'] = "Pago a moratorios {$credito['expediente']}";
	}
	if($done){
		//Movimientos en el flujo de efectivo
		$dat['fecha'] = $deposito['fecha_deposito'];
		$dat['cta1'] = 2;
		$dat['cta2'] = $deposito['id_cuenta'];
		$dat['monto'] = $deposito['monto'];
		$dat['userID'] = $_SESSION['userID'];
		if($transaccionDB->addTransaccionEfectivo($dat))
			header('location:cobranza.php?content=conciliacion&msg=ok');	
	}
}

$criterios['idCred'] = $data['idCredito'];
$criterios['tipoCargo'] = 0; //amortizacion
$criterios['tipoAbono'] = 0; //pago al credito
$deposito = $transaccionDB->getDepositosById($data['idDeposito']);
$credito = $creditoDB->getCreditoById($data['idCredito']);
$acreditados = $creditoDB->getAcreditadosByCredito($credito['id_credito']);
$cargos = $transaccionDB->getCargosByCriterio($criterios);
$abonos = $transaccionDB->getAbonosByCriterio($criterios);


include(RUTA_TPL.'home.tpl.php');
?>
