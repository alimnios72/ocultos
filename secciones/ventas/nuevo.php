<?php
$data = $_POST;
$rutaTemplate = 'ventas/';
$template = 'opcCredito.tpl.php';

$estados = $ubicacionDB->getEstados();
$vendedores = $usuarioDB->getVendedores();
$zonas = $ubicacionDB->getZonas();
//Para las tasas de inter�s
$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$intOrd = xmltoarray($sist->interes_ordinario, 'tasa', 'monto', '%');
array_multisort($intOrd,SORT_ASC,SORT_REGULAR);
$intMor = xmltoarray($sist->interes_moratorio, 'tasa', 'monto', '%');
$plazos = xmltoarray($sist->plazos, 'plazo', 'meses', '');
array_multisort($plazos,SORT_ASC,SORT_REGULAR);
//var_dump($plazos);

if(isset($_GET['reset']))
	borraSesion();

//Se captura la primera p�gina con respecto a los datos del cr�dito
if(isset($data['opcCred'])){
    $_SESSION['opcCred'] = $data;
    $_SESSION['opcCred']['tasa_mora'] = intval($intMor[0]['id']);
    if(isset($data['editar']))
        $template = 'resumen.tpl.php';
    else
        $template = 'solicitud.tpl.php';
}
elseif(isset($data['acreditado'])){ 
    if(isset($data['editar'])){
    	$i = $data['editar'];
    	$_GET['editar'] = $i;
    	$_SESSION['acreditado'][$i]['info_personal'] = $data;
    	$infoEcon = $_SESSION['acreditado'][$i]['info_econom'];
    }
    else
    	$_SESSION['acreditado'][]['info_personal'] = $data;
    $template = 'infoEconomica.tpl.php';
}
elseif(isset($data['info_econom'])){
	if(isset($data['editar'])){
		$i = $data['editar'];
    	$_GET['editar'] = $i;
		$_SESSION['acreditado'][$i]['info_econom'] = $data;
		$_SESSION['acreditado'][$i]['info_econom']['montoPreaprobado'] = estimaMonto($_SESSION["opcCred"], $_SESSION["acreditado"][$i], $creditoDB);
		$referencia = $_SESSION['acreditado'][$i]['referencia'];
    }
	else {
    	$i = lastIndex($_SESSION['acreditado']);
	    $_SESSION['acreditado'][$i]['info_econom'] = $data;
	    $_SESSION['acreditado'][$i]['info_econom']['montoPreaprobado'] = estimaMonto($_SESSION["opcCred"], $_SESSION["acreditado"][$i], $creditoDB);
	}
    $template = 'refPersonal.tpl.php'; 
}
elseif(isset($data['referencia'])){
	if(isset($data['editar'])){
		$i = $data['editar'];
		$_GET['editar'] = $i;
		$_SESSION['acreditado'][$i]['referencia'] = $data;
		if(isset($_SESSION['acreditado'][$i]['info_personal']['id_persona']) && intval($_SESSION["opcCred"]["num_int"]) != $i+1 && !isset($_SESSION['captured'])){
			unset($_GET['editar']);
			$template = 'solicitud.tpl.php';
		}
		else
			$template = 'resumen.tpl.php';
	}
	else{
	    $i = lastIndex($_SESSION['acreditado']);
	    $_SESSION['acreditado'][$i]['referencia'] = $data;
	    if(intval($_SESSION["opcCred"]["num_int"]) == $i+1)
	        $template = 'resumen.tpl.php';
	    elseif(intval($_SESSION["opcCred"]["num_int"]) != $i+1 || $_SESSION["opcCred"]["tipo"] == "0")
	        $template = 'solicitud.tpl.php'; 
	}
}
elseif(isset($data['solidario'])){
    $_SESSION['solidario'] = $data;
    $template = 'resumen.tpl.php';
}
elseif(isset($data['resumen'])){
	if(validaCredito($_SESSION,$errormsg,$creditoDB)){
		//Para creditos nuevos
		$_SESSION['opcCred']['viejo'] = 0;
		try{
			$creditoDB->addCredito($_SESSION, $personaDB);
			borraSesion();
			header('location:ventas.php?content=nuevo&msg=ok');
		}
		catch(Exception $e){
			$errormsg[] = $e->getMessage();
			$template = 'resumen.tpl.php';
		}
	}
	else{ 
		$errormsg;
		$template = 'resumen.tpl.php';
	}
}
elseif(isset($_GET['editar'])){
    switch($_GET['editar']){
        case 'opcCred':
            $template = 'opcCredito.tpl.php';
            break;
        case 'acreditado':
        	$template = 'solicitud.tpl.php';
        	break;
        case 'solidario':
        	$template = 'solicitud.tpl.php';
        	break;
        default:
        	$template = 'resumen.tpl.php';
        	break;
    }
}
elseif(isset($data['incompleto'])){
	$incomp = new xmlTrans('credsIncomp.xml');
	$usuario = $incomp->addNodewAttr('usuario','nombre',$_SESSION['userName']);
	$cred = $incomp->addNode($usuario[0], 'credito');
	$incomp->array2xml($cred, $_SESSION['opcCred'], 'opcCred');
	$incomp->array2xml($cred, $_SESSION['acreditado'], 'acreditados');
	if($_SESSION['opcCred']['tipo'] == "0")
		$incomp->array2xml($cred, $_SESSION['solidario'], 'solidario');
	//var_dump($cred);
	$template = 'resumen.tpl.php';
}
elseif($_SESSION['captured'] == 1)
	$template = 'resumen.tpl.php';

//var_dump($_SESSION);
include(RUTA_TPL.'home.tpl.php');

function borraSesion(){
	unset($_SESSION['captured']);
    unset($_SESSION['acreditado']);
    unset($_SESSION['opcCred']);
    unset($_SESSION['solidario']);
}
function lastIndex($array){
    end($array);
    return key($array);
}
?>
