<?php
$data = $_POST;
$rutaTemplate = 'ventas/renovacion/';
$template = 'renovacion.tpl.php';
$criterios = array();

$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$intOrd = xmltoarray($sist->interes_ordinario, 'tasa', 'monto', '%');
$plazos = xmltoarray($sist->plazos, 'plazo', 'meses', '');
$iva = 1.16;

//unset($_SESSION['solidario']);
//var_dump($_SESSION);

/*unset($_SESSION['credito']);
unset($_SESSION['acreditados']);
die();*/
//unset($data['saveRenov']);
//$data['aproveRenov']=1;

$vendedores = $usuarioDB->getVendedores();
$zonas = $ubicacionDB->getZonas();

if(isset($data['busqExp'])){
	$criterios['expediente'] = $data['expediente'];
	$credito = $creditoDB->getCreditoByCriterio($data);
	$credito = $credito[0];
	$criterios['idCred'] = $credito['id_credito'];
	$criterios['tipoCargo'] = 0;
	$criterios['tipoAbono'] = 0;
	$balance = $transaccionDB->getBalanceByCriterio($criterios);
	$criterios['tipoCargo'] = 1;
	$criterios['tipoAbono'] = 1;
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
	$monto = $balance['interes']+$balance['iva']+$balance['capital'];
	$montoMora = $balanceMora['interes']+$balanceMora['iva']+$balanceMora['capital'];
	$template = 'configCredito.tpl.php';
}
elseif(isset($data['renovar']) || isset($data['regRenov'])){
	$montoBalance = $data['montoBalance'];
	$montoMora = $data['montoMora'];
	//$montoBalance = 0;
	if(!isset($_SESSION['credito']))
		$_SESSION['credito'] = $creditoDB->getCreditoById($data['id_credito']);
	if(!isset($_SESSION['acreditados']))
		$_SESSION['acreditados'] = $creditoDB->getAcreditadosByCredito($data['id_credito']);
	if($_SESSION['credito']['tipo'] == "0" && !isset($_SESSION['solidario'])){
		$_SESSION['solidario'] = $creditoDB->getObligadoSolidarioByCredito($_SESSION['credito']['id_credito']);
	}
	//Paso a la otra nomenclatura de periodos	
	$_SESSION['credito']['periodo'] = array_search($_SESSION['credito']['periodo'], $periodo);
	//$_SESSION['credito']['nombreGrupo'] = getNextName($_SESSION['credito']['nombreGrupo']);
	$credito = $_SESSION['credito'];
	$acreditados = $_SESSION['acreditados'];
	$solidarios = $_SESSION['solidario'];
	$template = 'opcCredito.tpl.php';
	
}
elseif(isset($data['saveRenov'])){
	//Guardo los cambios en la sesi�n
	//$_SESSION['credito']['nombreGrupo'] = !isset($data['grupo']) ? getNextName($_SESSION['credito']['nombreGrupo']) : $data['grupo'];
	$_SESSION['credito']['tipo'] = $data['tipo'];
	$_SESSION['credito']['tasa_interes'] = $data['tasa'];
	$_SESSION['credito']['plazo'] = $data['plazo'];
	$_SESSION['credito']['periodo'] = $data['periodo'];
	$_SESSION['credito']['id_usuario'] = $data['vendedor'];
	$_SESSION['credito']['id_zona'] = $data['zona'];
	//$credito = $_SESSION['credito'];
	//$acreditados = $_SESSION['acreditados'];
	
	//Validaciones
	$noneChecked = true;
	$allMontos = true;
	if(!empty($_SESSION['acreditados'])){
		foreach($_SESSION['acreditados'] as $acred){
			if($acred['checked'] == "true"){
				$noneChecked = false;
				if(!isset($acred['montoAcreditado']) || $acred['montoAcreditado'] == "" || !is_numeric($acred['montoAcreditado'])){
					$allMontos = false;					
				}
			}
		}	
	}
	//Si hay errores 
	if($noneChecked)
		$errores[] = "Debes seleccionar al menos una persona";
	if(!$allMontos)
		$errores[] = "Todas las personas seleccionadas deben llevar un monto";
	if(!$noneChecked && $allMontos){
		//Si todo esta correcto
		$acreditados = array();
		$errores = array();
		$template = 'resumen.tpl.php';
		
		//Traigo informaci�n de creditos anteriores de los acreditados
		foreach($_SESSION['acreditados'] as &$val){
			//Solo considero acreditados seleccionados
			if(isset($val['checked']) && $val['checked'] == "true"){
				if(isset($val['id_persona']) && is_numeric($val['id_persona'])){
					$creditos = $creditoDB->getCreditosByPersona($val['id_persona']);
					if(!empty($creditos)){
						$maxMonto = 0;
						$countCred = 1;
						foreach($creditos as $credito){
							//Cr�ditos concluidos
							if($credito['status'] == 4){
								$maxMonto = $credito['cantidad'] > $maxMonto ? $credito['cantidad'] : $maxMonto;
								$countCred++;
							}
						}
						$lastCred = end($creditos);
						//Calculo los d�as de moratorios del �ltimo cr�dito
						$criterios['idAcred'] = $lastCred['id_acreditado'];
						$criterios['idCred'] = $lastCred['id_credito'];
						$moratorios = $transaccionDB->getMoratoriosByCriterio($criterios,"M.id_acreditado");
						if(!empty($moratorios)){
							//Monto mora
							$mora = $moratorios[0]['monto'];
							//Obtengo el pago fijo
							$amortizaciones = $transaccionDB->getAmortizacionByAcreditado($lastCred['id_acreditado']);
							$pagoFijo = $amortizaciones[0]['monto'];
							$diasMora = round($mora / ($pagoFijo * ($lastCred['tasa_mora']/100) * $iva));
							$aprobado = porcentajeAumento($diasMora,$maxMonto,$countCred);
						}
						else{
							$aprobado = porcentajeAumento(0,$maxMonto,$countCred);
						}
						$val['montoAprobado'] = $val['montoAcreditado'] < $aprobado ? $val['montoAcreditado'] : $aprobado;
					}
					else {
						$val['montoAprobado'] = $val['montoAcreditado'] < 3000 ? $val['montoAcreditado'] : 3000;
					}
					unset($creditos);
				}
			}
			
		}
				
	}
	$credito = $_SESSION['credito'];
	$acreditados = $_SESSION['acreditados'];
	$solidarios = $_SESSION['solidario'];
	//Si encuentro errores no debe dejar continuar
	if(!empty($errores)){
		$template = 'opcCredito.tpl.php';
		$credito = $_SESSION['credito'];
		if($data['tipo'] == "0")
			$solidarios = $_SESSION['solidario'];
		$noContinuar = true;
	}	
}
elseif(isset($data['aproveRenov'])){
	$ids = array();
	$acreditados = array();
	$i = 0;
	foreach($_SESSION['acreditados'] as $acred){
		if($acred['checked'] == "true"){
			$acreditados[] = $acred;
			if(isset($acred['id_persona']))
				$ids[] = array($acred['id_persona'],$data['montos'][$i]);
			$i++;
		}
	}
	//Validaciones 
	if($_SESSION['credito']['tipo'] == 1 && !isset($data['representante']))
		$errores[] = "Debes especificar un representante de grupo";
	if($_SESSION['credito']['tipo'] == 1 && $i < 2)
		$errores[] = "El m�nimo de integrantes es 2";
	if(empty($errores)){
		//Solo inserto los seleccionados
		$repre = $data['representante'];
		$_SESSION['acreditado'] = $acreditados;
		$_SESSION['opcCred'] = $_SESSION['credito'];
		$_SESSION['opcCred']['tasa'] = $_SESSION['credito']['tasa_interes'];
		$_SESSION['opcCred']['zona'] = $_SESSION['credito']['id_zona'];
		$_SESSION['opcCred']['vendedor'] = $_SESSION['credito']['id_usuario'];
		$_SESSION['opcCred']['grupo'] = $_SESSION['credito']['nombreGrupo'];
		$_SESSION['solidario'] = $_SESSION['solidario'][0];
		if($_SESSION['credito']['tipo'] == 1)
			$creditoDB->addCredito($_SESSION, $personaDB, true, $ids, $repre);
		else
			$creditoDB->addCredito($_SESSION, $personaDB, true, $ids, $repre, $_SESSION['solidario']['id_persona']);
		unset($_SESSION['credito']);
		unset($_SESSION['acreditados']);
		unset($_SESSION['acreditado']);
		unset($_SESSION['solidario']);
		//$template = 'renovacion.tpl.php';
		header('location: ventas.php?content=renovacion&msg=ok');
	}
	else{
		$template = 'resumen.tpl.php';
		$credito = $_SESSION['credito'];
		$acreditados = $_SESSION['acreditados'];
	}
}
elseif(isset($data['cancelRenov'])){
	unset($_SESSION['credito']);
	unset($_SESSION['acreditados']);
	unset($_SESSION['solidario']);
	$template = 'renovacion.tpl.php';
}
elseif($_GET['accion'] == 'agregarP'){
	$template = 'solicitud.tpl.php';
	$estados = $ubicacionDB->getEstados();
	if(isset($_GET['editar']) && $_GET['editar'] == 'acreditado'){
		$acred = $_SESSION['acreditado']['']['info_personal'];
		//$_SESSION['acreditados'][] = $_SESSION['acreditado'][""];
		//unset($_SESSION['acreditado']);
	}
	elseif(isset($_GET['editar']) && $_GET['editar'] == 'solidario'){
		unset($_SESSION['solidario'][0]);
		$acred = $_SESSION['solidario'];
	}
	elseif(isset($data['solidario'])){
		//var_dump($_SESSION['solidario']);die();
		$template = 'opcCredito.tpl.php';
		$data['nombre'] = $data['nombres']." ".$data['apellidoP']." ".$data['apellidoM'];
		//Si no existe la persona la agrego
		if(isset($_SESSION['solidario']['id_persona'])){
			$tmp = $_SESSION['solidario'];
			$tmp['nombre'] = $data['nombre'];
			unset($_SESSION['solidario']);
			$_SESSION['solidario'][] = $tmp;
		}
		else{
			$tmp['info_personal'] = $data;
			$personaDB->addPersona($tmp, $idPersona,$repre);
			$_SESSION['solidario'][] = array('id_persona'=>$idPersona[0][0],"nombre"=>$data['nombre']);
		}
		$credito = $_SESSION['credito'];
		$acreditados = $_SESSION['acreditados'];
		$solidarios = $_SESSION['solidario'];
	}
	elseif(isset($data['acreditado'])){
		$repetido = false;
		//Busco si ya est� la persona en la lista de integrantes
		if(!empty($_SESSION['acreditados']) && isset($data['id_persona'])){
			foreach($_SESSION['acreditados'] as $acredi){
				if($acredi['id_persona'] == $data['id_persona'])
					$repetido = true;
			}
		}
		if(!$repetido){
			$template = 'infoEconomica.tpl.php';
			$_SESSION['acreditado']['info_personal'] = $data;
			$infoEcon = $_SESSION['acreditado']['']['info_econom'];
		}
		else{
			$template = 'opcCredito.tpl.php';
			$credito = $_SESSION['credito'];
			$acreditados = $_SESSION['acreditados'];
			$_GET['repetido'] = 'Esa persona ya integra el grupo!';
		}
	}
	elseif(isset($data['info_econom']) && isset($_SESSION['acreditado'])){
		$template = 'opcCredito.tpl.php';
		$_SESSION['acreditado']['info_econom'] = $data;
		$_SESSION['acreditado']['acreditado'] = "{$_SESSION['acreditado']['info_personal']['nombres']} {$_SESSION['acreditado']['info_personal']['apellidoP']} {$_SESSION['acreditado']['info_personal']['apellidoM']}";
		//Si es nuevo lo agrego al sistema, si no lo agrego a los acreditados
		if(isset($_SESSION['acreditado']['info_personal']['id_persona']) && is_numeric($_SESSION['acreditado']['info_personal']['id_persona'])){
			$_SESSION['acreditados'][] = array('id_persona'=>$_SESSION['acreditado']['info_personal']['id_persona'],
											"acreditado"=>$_SESSION['acreditado']['acreditado']);
		}
		else{
			$personaDB->addPersona($_SESSION['acreditado'], $idPersona,$repre);
			$_SESSION['acreditados'][] = array('id_persona'=>$idPersona[0][0],"acreditado"=>$_SESSION['acreditado']['acreditado']);
		}
		$credito = $_SESSION['credito'];
		$acreditados = $_SESSION['acreditados'];
		unset($_SESSION['acreditado']);
	}
	elseif(isset($data['info_econom']) && !isset($_SESSION['acreditado'])){
		$template = 'opcCredito.tpl.php';
		$credito = $_SESSION['credito'];
		$acreditados = $_SESSION['acreditados'];
	}
}
//var_dump($_SESSION);
include(RUTA_TPL.'home.tpl.php');

function porcentajeAumento($diasMora,$montoSol,$numCredito){
	switch($numCredito){
		case 2:
			if($diasMora >= 0 && $diasMora < 6)
				$montoSol *= 1.5;
			elseif($diasMora >= 6 && $diasMora < 11) 
				$montoSol *= 1.3;
			elseif($diasMora >= 11 && $diasMora < 16)
				$montoSol *= 1.25;
			elseif($diasMora >= 16 && $diasMora < 21)
				$montoSol *= 1.2;
			else
				$error = "Caso especial";
			break;
		case 3:
			if($diasMora >= 0 && $diasMora < 6)
				$montoSol *= 1.33333;
			elseif($diasMora >= 6 && $diasMora < 11)
				$montoSol *= 1.2;
			elseif($diasMora >= 11 && $diasMora < 16)
				$montoSol *= 1.25;
			elseif($diasMora >= 16 && $diasMora < 21)
				$montoSol *= 1.1;
			else
				$error = "Caso especial";
			break;
		case 4:
			if($diasMora >= 0 && $diasMora < 6)
				$montoSol *= 1.25;
			elseif($diasMora >= 6 && $diasMora < 11)
				$montoSol *= 1.15;
			elseif($diasMora >= 11 && $diasMora < 16)
				$montoSol *= 1.1;
			elseif($diasMora >= 16 && $diasMora < 21)
				$montoSol *= 1.05;
			else
				$error = "Caso especial";
			break;
		default:
			if($diasMora < 21)
				$montoSol *= 1.15;
			else 
				$error = "Caso especial";
			break;
			
	}
	$montoSol = ceil($montoSol);
	return $montoSol;
}
?>
