<?php
$data = $_POST;
$rutaTemplate = 'ventas/';
$template = 'opcCredito2.tpl.php';

$estados = $ubicacionDB->getEstados();
$vendedores = $usuarioDB->getVendedores();
$zonas = $ubicacionDB->getZonas();
//Para las tasas de inter�s
$xml = RUTA_XML."varsSistema.xml";
$sist = simplexml_load_file($xml);
$intOrd = xmltoarray($sist->interes_ordinario);

if(isset($_GET['reset']))
	borraSesion();

//Se captura la primera p�gina con respecto a los datos del cr�dito
if(isset($data['opcCred'])){
    $_SESSION['opcCred'] = $data;
    $_SESSION['opcCred']['tasa_mora'] = 3;
    if(isset($data['editar']))
        $template = 'resumen2.tpl.php';
    else
        $template = 'solicitud2.tpl.php';
}
elseif(isset($data['acreditado'])){ 
    if(isset($data['editar'])){
    	$i = $data['editar'];
    	$_GET['editar'] = $i;
    	$_SESSION['acreditado'][$i]['info_personal'] = $data;
    	$infoEcon = $_SESSION['acreditado'][$i]['info_econom'];
    }
    else
    	$_SESSION['acreditado'][]['info_personal'] = $data;
    $template = 'infoEconomica2.tpl.php';
}
elseif(isset($data['info_econom'])){
	if(isset($data['editar'])){
		$i = $data['editar'];
    	$_GET['editar'] = $i;
		$_SESSION['acreditado'][$i]['info_econom'] = $data;
		$_SESSION['acreditado'][$i]['info_econom']['montoPreaprobado'] = $_SESSION['acreditado'][$i]['info_econom']['monto_sol'];
		$referencia = $_SESSION['acreditado'][$i]['referencia'];
    }
	else {
    	$i = lastIndex($_SESSION['acreditado']);
	    $_SESSION['acreditado'][$i]['info_econom'] = $data;
	    $_SESSION['acreditado'][$i]['info_econom']['montoPreaprobado'] = $_SESSION['acreditado'][$i]['info_econom']['monto_sol'];
	}
    $template = 'refPersonal2.tpl.php'; 
}
elseif(isset($data['referencia'])){
	if(isset($data['editar'])){
		$i = $data['editar'];
		$_GET['editar'] = $i;
		$_SESSION['acreditado'][$i]['referencia'] = $data;
		if(isset($_SESSION['acreditado'][$i]['info_personal']['id_persona']) && intval($_SESSION["opcCred"]["num_int"]) != $i+1 && !isset($_SESSION['captured'])){
			unset($_GET['editar']);
			$template = 'solicitud2.tpl.php';
		}
		else
			$template = 'resumen2.tpl.php';
	}
	else{
	    $i = lastIndex($_SESSION['acreditado']);
	    $_SESSION['acreditado'][$i]['referencia'] = $data;
	    if(intval($_SESSION["opcCred"]["num_int"]) == $i+1)
	        $template = 'resumen2.tpl.php';
	    elseif(intval($_SESSION["opcCred"]["num_int"]) != $i+1 || $_SESSION["opcCred"]["tipo"] == "0")
	        $template = 'solicitud2.tpl.php'; 
	}
}
elseif(isset($data['solidario'])){
    $_SESSION['solidario'] = $data;
    $template = 'resumen2.tpl.php';
}
elseif(isset($data['resumen'])){
	if(validaCredito2($_SESSION,$errormsg,$creditoDB)){
		//Para creditos viejos
		$_SESSION['opcCred']['viejo'] = 1;
		$creditoDB->addNuevoCredito($_SESSION, $personaDB);
    	borraSesion();
    	header('location:ventas.php?content=viejo&msg=ok');
	}
	else{ 
		$errormsg;
		$template = 'resumen2.tpl.php';
	}
}
elseif(isset($_GET['editar'])){
    switch($_GET['editar']){
        case 'opcCred':
            $template = 'opcCredito2.tpl.php';
            break;
        case 'acreditado':
        	$template = 'solicitud2.tpl.php';
        	break;
        case 'solidario':
        	$template = 'solicitud2.tpl.php';
        	break;
        default:
        	$template = 'resumen2.tpl.php';
        	break;
    }
}
elseif(isset($data['incompleto'])){
	$incomp = new xmlTrans('credsIncomp.xml');
	$usuario = $incomp->addNodewAttr('usuario','nombre',$_SESSION['userName']);
	$cred = $incomp->addNode($usuario[0], 'credito');
	$incomp->array2xml($cred, $_SESSION['opcCred'], 'opcCred');
	$incomp->array2xml($cred, $_SESSION['acreditado'], 'acreditados');
	if($_SESSION['opcCred']['tipo'] == "0")
		$incomp->array2xml($cred, $_SESSION['solidario'], 'solidario');
	//var_dump($cred);
	$template = 'resumen2.tpl.php';
}
elseif($_SESSION['captured'] == 1)
	$template = 'resumen2.tpl.php';

//var_dump($_SESSION);
include(RUTA_TPL.'home.tpl.php');

function borraSesion(){
	unset($_SESSION['captured']);
    unset($_SESSION['acreditado']);
    unset($_SESSION['opcCred']);
    unset($_SESSION['solidario']);
}
function lastIndex($array){
    end($array);
    return key($array);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function validaCredito2($data, &$errors, $creditoDB){
	$acreditado = $data['acreditado'];
	$opcCred = $data['opcCred'];
	$solidario = $data['solidario'];
	if(!validaOpcCredito2($opcCred, $errors, $creditoDB))
		return false;
	if($opcCred['tipo'] == 0 && !validaSolidario2($solidario, $errors, $creditoDB, $acreditado[0]['info_personal']))
		return false;
	if($opcCred['tipo'] == 1 && !validaRepresentante2($acreditado, $errors))
		return false;
	if(!validaAcreditado2($acreditado, $errors, $creditoDB, $opcCred['tipo']))
		return false;
	return true;
}

function validaOpcCredito2($opcCred, &$errors, $creditoDB){
	$flag = true;
	if($opcCred['grupo'] == "" && $opcCred['tipo'] == '1'){
		$errors[] = "Debes especificar un nombre para el grupo";
		$flag = false;
	}
	return $flag;
}
function validaAcreditado2($acreds, &$errors, $creditoDB, $tipo){
	$flag = true;
	$tels = 0;
	$hombres = 0;
	$last_key = end(array_keys($acreds));
	foreach ($acreds as $k=>$per){
		$infoP = $per['info_personal'];
		$infoE = $per['info_econom'];
		$nCompleto = $infoP['nombres']." ".$infoP['apellidoP']." ".$infoP['apellidoM'];
		$nCompleto = trim(strtoupper($nCompleto));
		if($infoP['nombres'] == ''){
			$errors[] = "Debe especificar al menos un nombre. ({$nCompleto})";
			$flag = false;
		}
		elseif($infoP['apellidoP'] == '' && $infoP['apellidoM'] == ''){
			$errors[] = "Debe especificar al menos un apellido. ({$nCompleto})";
			$flag = false;
		}
		if(!isset($infoP['sexo']) || $infoP['sexo'] == ''){
			$errors[] = "Define el sexo de la persona. ({$nCompleto})";
			$flag = false;
		}
		if($infoP['nacimiento'] == "" || !validaFecha2($infoP['nacimiento'])){
			$errors[] = "La fecha de nacimiento no puede estar en blanco o debe ser una fecha v�lida. ({$nCompleto})";
			$flag = false;
		}
		elseif(getEdad($infoP['nacimiento']) < 21 || getEdad($infoP['nacimiento']) > 65){
			$errors[] = "El rango de edades debe ser 21-65 a�os. ({$nCompleto})";
			$flag = false;
		}
		if($infoP['enfermedad_desc'] == '' && ($infoP['enfermedad_solic'] == 1 || $infoP['enfermedad_fam'] == 1)){
			$errors[] = "Debe describir la enfermedad. ({$nCompleto})";
			$flag = false;
		}
		if($infoP['ant_domicilio'] == '' || is_nan($infoP['ant_domicilio'])){
			$errors[] = "Debe especificar correctamente la antig�edad en el domicilio. ({$nCompleto})";
			$flag = false;
		}
		if($infoP['calle'] == '' || $infoP['colonia'] == ''){
			$errors[] = "Debe especificar calle y/o colonia. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['nombre_negocio'] == ''){
			$errors[] = "Especificar nombre del negocio o actividad productiva. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['pag_cred'] == 1 && $infoE['pag_cred_monto'] == ''){
			$errors[] = "Especificar el monto del cr�dito que paga. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['pen_alim'] == 1 && $infoE['pen_alim_monto'] == ''){
			$errors[] = "Especificar el monto de la pensi�n alimentaria. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['gastos_fam'] == '' || $infoE['gastos_fam'] < 0){
			$errors[] = "Definir monto para gastos familiares o marcar 0 (cero) si no hay. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['ing_mensuales'] == '' || $infoE['ing_mensuales'] < 0){
			$errors[] = "Definir monto de los ingresos mensuales o marcar 0 (cero) si no hay. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['inv_semanal'] == '' || $infoE['inv_semanal'] < 0){
			$errors[] = "Definir monto de la inversi�n mensual en el negocio o marcar 0 (cero) si no hay. ({$nCompleto})";
			$flag = false;
		}
		if($infoE['monto_sol'] == '' || $infoE['monto_sol'] < 0){
			$errors[] = "Especificar el monto solicitado. ({$nCompleto})";
			$flag = false;
		}
		$count = 0;
		foreach ($acreds as $a){
			$nCompleto2 = $a['info_personal']['nombres']." ".$a['info_personal']['apellidoP']." ".$a['info_personal']['apellidoM'];
			$nCompleto2 = trim(strtoupper($nCompleto2));
			if($nCompleto == $nCompleto2)
				$count++;
		}
		if($count > 1){
			$errors[] = "{$nCompleto} aparece repetido(a).";
			$flag = false;
		}
	}
	return $flag;
}

function validaSolidario2($sol, &$errors, $creditoDB, $acred){
	$flag = true;
	$nCompleto = $sol['nombres']." ".$sol['apellidoP']." ".$sol['apellidoM'];
	$nCompleto = trim(strtoupper($nCompleto));
	$nCompleto2 = $acred['nombres']." ".$acred['apellidoP']." ".$acred['apellidoM'];
	$nCompleto2 = trim(strtoupper($nCompleto2));
	if($nCompleto == $nCompleto2){
		$errors[] = "Obligado y acreditado son la misma persona.";
		$flag = false;
	}
	if($sol['nombres'] == ''){
		$errors[] = "Debe especificar al menos un nombre. ({$nCompleto})";
		$flag = false;
	}
	elseif($sol['apellidoP'] == '' && $sol['apellidoM'] == ''){
		$errors[] = "Debe especificar al menos un apellido. ({$nCompleto})";
		$flag = false;
	}
	if(!isset($sol['sexo']) || $sol['sexo'] == ''){
		$errors[] = "Define el sexo de la persona. ({$nCompleto})";
		$flag = false;
	}
	if($sol['nacimiento'] == "" || !validaFecha2($sol['nacimiento'])){
		$errors[] = "La fecha de nacimiento no puede estar en blanco o debe ser una fecha v�lida. ({$nCompleto})";
		$flag = false;
	}
	if($sol['calle'] == '' || $sol['colonia'] == ''){
		$errors[] = "Debe especificar calle y/o colonia. ({$nCompleto})";
		$flag = false;
	}
	return $flag;
}

function validaRepresentante2($acreds, &$errors){
	foreach($acreds as $acred){
		if(isset($acred['representante']))
			return true;
	}
	$errors[] = "Especifica un representante para el grupo.";
	return false;
}

function validaFecha2($fecha){
	$fecha = explode('-', $fecha);
	return checkdate($fecha[1], $fecha[2], $fecha[0]);
}
?>