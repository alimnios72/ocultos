<?php
$data = $_POST;
$rutaTemplate = 'administracion/';
$template = 'detallesCredito.tpl.php';
$tipoGestion = array(0=>'Llamada telef�nica', 1=>'Visita extrajudicial', 2=>'Acuerdo de pago');
$cargoStatus = array(0=>'Adeudo',1=>'Pagado');
$representante = "";
$solidario = array();

if(isset($_GET['idCred']) && is_numeric($_GET['idCred'])){
	$criterios = array();
	$criterios['idCred'] = $_GET['idCred'];
	$criterios['tipoAbono'] = 0;
	$criterios['tipoCargo'] = 0;
	$acreditados = $creditoDB->getAcreditadosByCredito($criterios['idCred']);
	$tabla = $transaccionDB->getAmortizacionByCredito($criterios['idCred']);
	$pagos = $transaccionDB->getAbonosByCriterio($criterios);
	$moratorios = $transaccionDB->getMoratoriosByCriterio($criterios);
	$pagoMoratorios = $transaccionDB->getPagoMoratoriosByCriterio($criterios);
	$criterios['fecha'] = date('Y-m-d');
	$balance = $transaccionDB->getBalanceByCriterio($criterios);
	///////////////////////////////////////////////////////////
	$credito = $creditoDB->getCreditoById($criterios['idCred']);
	
	if(intval($credito['tipo']) == 1)
		$representante = $creditoDB->getRepresentanteByCredito($criterios['idCred']);
	else
		$solidario = $creditoDB->getObligadoSolidarioByCredito($criterios['idCred']);
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	$diasMoraHoy = MoraTotal($tabla, date('Y-m-d'), $diasInhabiles);
	$pFijo = $tabla[0]['monto'];
	$criterios['tipoAbono'] = 1;
	$criterios['tipoCargo'] = 1;
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
	$moraDia = $pFijo * ($credito['tasa_mora']/100) * 1.16;
	$moraEfectiva = $diasMoraHoy * $pFijo * ($credito['tasa_mora']/100) * 1.16;
	$moraHoy['interes'] = $moraEfectiva / 1.16;
	$moraHoy['iva'] = $moraHoy['interes'] * 0.16;
	$moraHoy['monto'] = $moraHoy['interes'] + $moraHoy['iva'];
	$moraHoy['fecha'] = strftime("%A %e de %B de %Y", strtotime('today'));
	//Sumo la mora al d�a de hoy con la guardada
	$balanceMora['monto'] += $moraHoy['monto'];
	$balanceMora['interes'] += $moraHoy['interes'];
	$balanceMora['iva'] += $moraHoy['iva'];
	//Para la gesti�n de cobranza.
	$criterios['expediente'] = $credito['expediente'];
	unset($criterios['fecha']);
	$gestiones = $creditoDB->getGestionesByCriterio($criterios);
}
include(RUTA_TPL.'home.tpl.php');
?>