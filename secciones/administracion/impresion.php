<?php
$data = $_POST;
$rutaTemplate = 'administracion/';
$template = 'impresion.tpl.php';

if(isset($_POST['data'])){
    $credito = $creditoDB->getCreditoById($data['idC']);
    $cCredito = $credito;
    $acreditados = $creditoDB->getAcreditadosByCredito($data['idC']);
    $cCredito['tasa'] =  $cCredito['tasa_interes'];
    $cCredito['periodo'] = array_search($cCredito['periodo'], $periodo);
    $cCredito['fecha'] = $cCredito['fecha_entrega'];
    $cCredito['viejo'] = $credito['viejo'];
    foreach ($acreditados as $acreditado){
    	$cCredito['montoTotal'] =  $acreditado['cantidad'];
    	$amortizacion = new Amortizacion($cCredito);
    	$tabla = $amortizacion->getTabla();
    	//var_dump($tabla);die();
    	foreach($tabla as $mov)
    		$transaccionDB->addMovimiento($acreditado['id_acreditado'], 0, $mov, 0);
    	unset($amortizacion);
    }
    //Movimientos en el flujo de efectivo
    $dat['fecha'] = $credito['fecha_entrega'];
    $dat['descripcion'] = "Pr�stamo cr�dito {$credito['expediente']}";
    $dat['cta1'] = 3;
    $dat['cta2'] = 2;
    $dat['monto'] = $credito['montoTotal'];
    $dat['userID'] = $_SESSION['userID'];
    $transaccionDB->addTransaccionEfectivo($dat);
    
    $creditoDB->updateCreditoActivo($data);
    header('location:administracion.php?content=impresion&msg=ok');
}

$creditos = $creditoDB->getCreditosByStatus(1);
include(RUTA_TPL.'home.tpl.php');
?>