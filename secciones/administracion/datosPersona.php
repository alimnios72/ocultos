<?php
$data = $_POST;
$rutaTemplate = 'administracion/';
$template = 'datosPersona.tpl.php';

$readonly = $_SESSION['priv']=='admin' || $_GET['priv'] == 'renovacion' ? "" : "DISABLED";

if(isset($_GET['persona']) && is_numeric($_GET['persona'])) {
	$datos  = $personaDB->getDatosPersona($_GET['persona']);
	$estados = $ubicacionDB->getEstados();
	$deleg = $ubicacionDB->getDelegacionesByEstado($datos['id_estado']);
	$deleg_neg = $ubicacionDB->getDelegacionesByEstado($datos['id_estado_negocio']);
}
if(isset($_POST['update'])){
	//Para saber si actualiz� la fecha
	$fecha = strftime("%d de %B de %Y", strtotime($data['fecha_nac'])) == $data['nacimiento'] ? $data['fecha_nac'] : $data['nacimiento'];
	$info['info_personal']['id_persona'] = $_GET['persona'];
	$info['info_personal']['nombres'] = $data['nombres'];
	$info['info_personal']['apellidoP'] = $data['apellidoP'];
	$info['info_personal']['apellidoM'] = $data['apellidoM'];
	$info['info_personal']['nacimiento'] = $fecha;
	$info['info_personal']['sexo'] = $data['sexo'];
	$info['info_personal']['rfc'] = $data['rfc'];
	$info['info_personal']['curp'] = $data['curp'];
	$info['info_personal']['edocivil'] = $data['edocivil'];
	$info['info_personal']['t_conyuge'] = $data['t_conyuge'];
	$info['info_personal']['dep_econom'] = $data['dep_econom'];
	$info['info_personal']['nombres'] = $data['nombres'];
	$info['info_personal']['calle'] = $data['calle'];
	$info['info_personal']['num_ext'] = $data['num_ext'];
	$info['info_personal']['num_int'] = $data['num_int'];
	$info['info_personal']['dpto'] = $data['dpto'];
	$info['info_personal']['mza'] = $data['mza'];
	$info['info_personal']['lote'] = $data['lote'];
	$info['info_personal']['colonia'] = $data['colonia'];
	$info['info_personal']['delegacion'] = $data['delegacion'];
	$info['info_personal']['cp'] = $data['cp'];
	$info['info_personal']['residencia'] = $data['residencia'];
	$info['info_personal']['tel_casa'] = $data['tel_casa'];
	$info['info_personal']['celular'] = $data['celular'];
	$info['info_personal']['email'] = $data['email'];
	$info['info_personal']['nivel_estudios'] = $data['nivel_estudios'];
	$info['info_personal']['enfermedad_solic'] = $data['enfermedad_solic'];
	$info['info_personal']['enfermedad_fam'] = $data['enfermedad_fam'];
	$info['info_personal']['enfermedad_desc'] = $data['enfermedad_desc'];
	$info['info_personal']['ant_domicilio'] = $data['ant_domicilio'];
	////////////////////////////////////////////////////
	$info['info_econom']['delegacion'] = $data['delegacion_negocio'];
	$info['info_econom']['nombre_negocio'] = $data['nombre_negocio'];
	$info['info_econom']['calle'] = $data['calle_negocio'];
	$info['info_econom']['colonia'] = $data['colonia_negocio'];
	$info['info_econom']['cp'] = $data['cp_negocio'];
	$info['info_econom']['tel_neg'] = $data['telefonos_negocio'];
	$info['info_econom']['ant_negocio'] = $data['ant_negocio'];
	$info['info_econom']['pag_cred_monto'] = ($data['pag_credito'])?$data['pag_credito_monto']:0;
	$info['info_econom']['gastos_fam'] = $data['gastos_fam'];
	$info['info_econom']['pen_alim_monto'] = ($data['pension_alim'])?$data['pension_alim_monto']:0;
	$info['info_econom']['ing_mensuales'] = $data['ing_mensuales'];
	$info['info_econom']['inv_semanal'] = $data['inv_semanal'];
	$ids = array();
	$repre = array();
	$personaDB->updatePersona($info, $ids, $repre);
	header("location:administracion.php?content=datosPersona&persona={$_GET['persona']}&msg=ok");
	ob_flush();
}
include(RUTA_TPL.'home.tpl.php');
?>