<?php
$data = $_POST;
$rutaTemplate = 'reportes/';
$template = 'devengado.tpl.php';
$expediente = false;
$col1 = false;

$criterios = array();
if(isset($data['submit'])){
	if(!empty($data['tipo']) && count($data['tipo']) < 2){
		$criterios['tipo'] = $data['tipo'][0];
	}
	if(isset($data['persona']) && $data['persona'] == "0" && !empty($data['a�o']) && !empty($data['mes'])){
		$criterios['persona'] = true;
		$criterios['a�o'] = implode(',',$data['a�o']);
		$criterios['mes'] = implode(',',$data['mes']);
		$expediente = true;
		$persona = true;
	}
	if(isset($data['expediente']) && $data['expediente'] != ''){
		$criterios['expediente'] = $data['expediente'];
		$expediente = true;
	}
	if(isset($data['status']) && $data['status'] != "0"){
		$criterios['status'] = $data['status'];
	}
}
$tabla = $transaccionDB->getInteresDevengado($criterios);
$pagosNoDevengados = $transaccionDB->getPagosNoDevengados();
if($persona){
	$criterios['saldos'] = true;
	$saldos = $transaccionDB->getInteresDevengado($criterios);
}

include(RUTA_TPL.'home.tpl.php');
?>