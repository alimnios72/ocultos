<?php
$data = $_POST;
$rutaTemplate = 'reportes/';
$template = 'prestado.tpl.php';

$criterios = array();
$criterios['cancelado'] = false;
if(isset($data['buscar'])){
	$criterios['fechas'][0] =  $data['fecha1'];
	$criterios['fechas'][1] =  $data['fecha2'];
}
else{
	$day = 1;
	$month = date('m');
	$year = date('Y');
	$date = "{$year}-{$month}-{$day}";
	$criterios['fechas'][0] =  $date;
	$criterios['fechas'][1] =  subDays(addMonths($date, 1), 1);
}
if(isset($_POST['ordenar'])){
	$orderby[$_POST['ordenar']] = 1;
}
$creditos = $creditoDB->getCreditoByCriterio($criterios,$orderby);

include(RUTA_TPL.'home.tpl.php');
?>