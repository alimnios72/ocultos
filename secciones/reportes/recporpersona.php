<?php
$data = $_POST;
$rutaTemplate = 'reportes/';
$template = 'recporpersona.tpl.php';

if(isset($data['limpiar'])){
	header('location:reportes.php?content=recporpersona');
}

$criterios = array();
if(isset($_POST['fecha1']) && isset($_POST['fecha2']) && $_POST['fecha1']!='' && $_POST['fecha2']!=''){
	$criterios['fechas'][] = $_POST['fecha1'];
	$criterios['fechas'][] = $_POST['fecha2'];
}
else{
	$day = 1;
	$month = date('m');
	$year = date('Y');
	$date = "{$year}-{$month}-{$day}";
	$criterios['fechas'][0] =  $date;
	$criterios['fechas'][1] =  subDays(addMonths($date, 1), 1);
}
if(isset($data['bancos']) && is_array($data['bancos'])){
	$criterios['bancos'] = implode(",",$data['bancos']);
}
if(isset($data['deposito']) && $data['deposito'] != ''){
	$criterios['deposito'] = $data['deposito'];
}
$depositos = $creditoDB->getRecuperadoPorPersona($criterios);

include(RUTA_TPL.'home.tpl.php');
?>