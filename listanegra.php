<?php
require_once 'main.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
require_once 'lib/PHPExcel.php';

$ListaNegra = array('Morosos','Personas Pol�ticamente Expuestas', 'Prevenci�n al terrorismo');
$filename = 'Files/5.xlsx';

$reader = PHPExcel_IOFactory::createReader('Excel2007');
$reader->setReadDataOnly(true);
$excel = $reader->load($filename);
$worksheet = $excel->getActiveSheet();
$firstrow = 2;
$lastrow = $worksheet->getHighestRow();
$data['categoria'] = 1;


//Columnas con informaci�n importante
$nombres = 'J';
$paterno = 'K';
$materno = 'L';
$nacimiento = 'R';
$rfc = 'S';
$sexo = 'M';
$delegacion = 'Q'; 
$entidad = 'E';
$partido = 'H';
$tipo = 'D';
$clase = 'C';
$calidad = 'I';
$distrito = 'G';

?>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>

<?php 
$delegaciones = $ubicacionDB->getAllDelegacionTemp();
echo "Registros : ".$lastrow-$firstrow;//." <br/>";
for($i=$firstrow;$i<=$lastrow;$i++){
	$data['nacimiento'] = $worksheet->getCell($nacimiento.$i)->getValue() == '' ? '1900-01-01' : date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCell($nacimiento.$i)->getValue()));
	$data['nombres'] = utf8_decode($worksheet->getCell($nombres.$i)->getValue());
	$data['paterno'] = utf8_decode($worksheet->getCell($paterno.$i)->getValue());
	$data['materno'] = utf8_decode($worksheet->getCell($materno.$i)->getValue());
	$data['rfc'] = $worksheet->getCell($rfc.$i)->getValue();
	$data['sexo'] = CastSexo($worksheet->getCell($sexo.$i)->getValue());
	$data['entidad'] = $worksheet->getCell($entidad.$i)->getValue();
	$data['partido'] = utf8_decode($worksheet->getCell($partido.$i)->getValue());
	$data['tipo'] = $worksheet->getCell($tipo.$i)->getValue();
	$data['clase'] = $worksheet->getCell($clase.$i)->getValue();
	$data['calidad'] = utf8_decode($worksheet->getCell($calidad.$i)->getValue());
	$data['distrito'] =  $worksheet->getCell($distrito.$i)->getValue();
	if($delegacion == ''){
		$data['delegacion'] = 80;
	}
	else{
		$del = $worksheet->getCell($delegacion.$i)->getValue();
		if(trim($del) == '')
			$data['delegacion'] = 80;
		else
			$data['delegacion'] = CastDelegacion($del,$delegaciones);
	}	
	echo "<b>Nombre:</b> ".$data['nombres']; 
	echo " <b>Paterno:</b> ".$data['paterno']; 
	echo " <b>Materno:</b> ".$data['materno']; 		
	echo " <b>Nacimiento:</b> ". $data['nacimiento'];	
	echo " <b>RFC:</b> ".$data['rfc']; 	
	echo " <b>Sexo:</b> ".$data['sexo']; 
	echo " <b>ID Delegacion:</b> ".$data['delegacion'];	
	echo " <b>Entidad:</b> ".$data['entidad']; 
	echo " <b>Partido:</b> ".$data['partido']; 
	echo " <b>Tipo:</b> ".$data['tipo'];
	echo " <b>Clase:</b> ".$data['clase']; 
	echo " <b>Calidad:</b> ".$data['calidad'];
	echo " <b>Distrito:</b> ".$data['distrito']."<br/>";
	$ubicacionDB->insertaPPE($data);
	
}

function CastSexo($value){
	$sexo = '';
	if($value == "" || $value == " "){
		$sexo = 'H';
	}
	elseif(strtolower(trim($value)) == "mujer"){
		$sexo = 'M';
	}
	else{
		$sexo = 'H';
	}
	return $sexo;
}

function CastDelegacion($value,$delegaciones){
	foreach ($delegaciones as $del){
		if(strtr(strtolower($del['nombre_edo']),'�����','aeiou') == strtolower($value))
			return $del['id_delegacion'];
	}
	return 80;
}

?>
</body>
</html>
