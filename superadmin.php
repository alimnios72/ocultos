<?php
include('main.php');
?>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>SuperAdmin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<style type="text/css">
		#userlist{
			font: normal 13px tahoma, arial, verdana, sans-serif;
		}
		#userlist td{
			height: 26px;
		}
		.online, .away, .offline{
			padding-left: 30px;
		}
		.online{
			background: url(img/superadmin/online.png) no-repeat;
		}
		.away{
			background: url(img/superadmin/away.png) no-repeat;
		}
		.offline{
			background: url(img/superadmin/offline.png) no-repeat;
		}
	</style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js "></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript">
		var refresh = 15000;
		function Start(){
			$("#userdialog").dialog({position: {my: "left top", at: "left top", of: window}});
			getUsers();
			setInterval(getUsers,refresh);
		}
		function getUsers(){
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "procesos/superAdmin.php",true);
			xhr.onload = function(e){
				list = JSON.parse(this.response);
				printList(list);
			}
			xhr.send();
		}
		function printList(list){
			console.log('Refreshed');
			var table = document.getElementById('userlist');
			table.innerHTML = '';
			for(i in list){
				var row = table.insertRow(i);
				var cell = row.insertCell(0);
				cell.innerHTML = list[i].nombres +' '+list[i].apellido_paterno;
				if(list[i].Status == 'Activo')
					cell.className = 'online';
				else if(list[i].Status == 'Ausente')
					cell.className = 'away';
				else
					cell.className = 'offline';
				//row.insertCell(1).innerHTML = list[i].apellido_paterno+' '+list[i].apellido_materno;
				//row.insertCell(2).innerHTML = list[i].Status;
			}
		}
	</script>
</head>
<body onload="Start()">
<div id="userdialog" title="Lista de usuarios">
	<table id="userlist"></table>
</div>
</body>
</html>