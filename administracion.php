<?php
include('main.php');
//Para las hojas de estilo y javascript
$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css','jquery-ui.css','calendar.css');
$js=array('jquery.js','switcher.js','toggle.js','jquery-ui.js','ui.core.js','administracion.js','calendar.js','ui.datepicker-es.js');
$topmenu['admin'] = 'id="menu-active"';
$leftmenu = array('Personas'=>array('Ver perfil'=>'perfil'),
                  'Creditos'=>array('Impresi�n de cr�ditos'=>'impresion', 'Buscar cr�ditos'=>'buscar'));

//Identificamos cual va a ser el contenido a mostrar de la seccion
$content = isset($_GET['content'])? $_GET['content'] : false;
switch($content)
{
    case 'impresion':
        $section = RUTA_SEC.'administracion/impresion.php';
        break;
    case 'perfil':
        $section = RUTA_SEC.'administracion/perfil.php';
        break;
    case 'datosPersona':
        	$section = RUTA_SEC.'administracion/datosPersona.php';
        	break;
    case 'buscar':
        	$section = RUTA_SEC.'administracion/buscar.php';
        	break;
    case 'credito':
        	$section = RUTA_SEC.'administracion/credito.php';
        	break;
    case 'detallesCred':
        	$section = RUTA_SEC.'administracion/detallesCredito.php';
        	break;
    default:
        $section = RUTA_TPL.'home.tpl.php';
        $rutaTemplate = '';
        $template = 'principal.tpl.php';
        break;
}
include(RUTA_TPL.'header.tpl.php');
include($section);
include(RUTA_TPL.'footer.tpl.php');
?>
