<?php
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
setlocale(LC_TIME, 'es_MX');
date_default_timezone_set("America/Mexico_City");
session_start();
ob_start();

define('RUTA_TPL', 'templates/');
define('RUTA_LIB', 'lib/');
define('RUTA_JS', 'js/');
define('RUTA_CSS', 'css/');
define('RUTA_IMG', 'img/');
define('RUTA_SEC', 'secciones/');
define('RUTA_XML', RUTA_LIB.'xml/');

require_once('constantes.php');
require_once(RUTA_LIB.'configDB.php');
require_once(RUTA_LIB.'Math/Finance.php');
require_once(RUTA_LIB.'miscFunctions.php');
require_once(RUTA_LIB.'amortizacion.class.php');
require_once(RUTA_LIB.'DBClass.php');
require_once(RUTA_LIB.'loginDB.php');
///////////////////////////////////////////////////////////////
require_once(RUTA_LIB.'creditosDB.php');
require_once(RUTA_LIB.'personasDB.php');
require_once(RUTA_LIB.'usuariosDB.php');
require_once(RUTA_LIB.'ubicacionesDB.php');
require_once(RUTA_LIB.'transaccionesDB.php');
require_once(RUTA_LIB.'xml.class.php');
require_once(RUTA_LIB.'refBanc.class.php');
require_once(RUTA_LIB.'testDB.php');
//require_once(RUTA_LIB.'PHPExcel.php');
///////////////////////////////////////////////////////////////
//require_once(RUTA_LIB.'cobranzaDB.php');
require_once(RUTA_LIB.'validaciones.php');

$admin = new DB(false, $configDB['dsn'], $configDB['user'], $configDB['password']);
$adminLogin = new LoginDB($admin->dbLink);
$creditoDB = new Credito($admin->dbLink);
$personaDB = new Persona($admin->dbLink);
$usuarioDB = new Usuario($admin->dbLink);
$ubicacionDB = new Ubicacion($admin->dbLink);
$transaccionDB = new Transaccion($admin->dbLink);
$testDB = new Test($admin->dbLink);

if(!isset($loginPage)) $loginPage = false;
if(!$loginPage){
    if(!$adminLogin->isLoggedIn())// || !$adminLogin->Check())
    {
        header("Location:login.php");
        exit;
    }
    else{
    	//Ultima actualizaci�n (para ver si el usuario est� activo)
    	$adminLogin->lastSeen($_SESSION['userID']);
    }
}
?>
