<?php
$producto = array(0=>'CrediMarchante',1=>'CrediN�mina',2=>'CrediHipoteca');
$tipo = array(0=>'Individual',1=>'Grupal');
$periodo = array('s'=>'Semanal','q'=>'Quincenal','m'=>'Mensual','b'=>'Bimestral','t'=>'Trimestral','u'=>'Quinquenal','e'=>'Semestral','a'=>'Anual');
$status = array(0=>'Preaprobado',1=>'Aprobado',2=>'Activo',3=>'Cancelado',4=>'Concluido');
$banco =  array(1=>'AFIRME/BAJIO',3=>'BBVA BANCOMER',6=>'BANORTE');
$diaSemana =  array(1=>'Lunes',2=>'Martes',3=>'Mi�rcoles',4=>'Jueves',5=>'Viernes');
$cargoTipo = array(0=>'Amortizaci�n',1=>'Moratorio');
$cargoStatus = array(0=>'Adeudo',1=>'Pagado');
$conciliado = array('No conciliado','Conciliado');
$abonoTipo = array(0=>'Amortizaci�n',1=>'Moratorio');
$tipoAbono = array(0=>'Al cr�dito', 1=>'A mora');
$ubicaciones = array(
    1 => array(
        "large" => "Av. Revoluci�n, No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000, M�xico, D.F",
        "short" => "M�xico, D.F"
    ),
    2 => array(
        "large" => "Avenida Cuauht�moc, n�mero  7, esquina con calle Plutarco El�as Calles, Colonia Santa B�rbara, Municipio de Ixtapaluca, Estado de M�xico",
        "short" => "Ixtapaluca, Estado de M�xico"
    )
);