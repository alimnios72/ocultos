<?php
define('ROOT','../');
include(ROOT.'main.php');
include(ROOT.RUTA_LIB.'class.customPDF.php');
$data = $_GET;

if($data['ini'] != "")
	$data['rango'][0] = $data['ini'];
else
	$data['rango'][0] = 1;
if($data['fin'] != "")
	$data['rango'][1] = $data['fin'];
else
	$data['rango'][1] = 50;
$cheques = $transaccionDB->getChequesByCriterio($data);

$bancos = array(1 => 'BANCA AFIRME, S.A.', 3 => 'BANCOMER');
$ctas = array(1 => 'CUENTA AFIRME CLASICA, S.A.', 3 => 'CUENTA BANCOMER');
$tipo = array(0 => 'INDIVIDUAL', 1 => 'GRUPO');
$maxCheque = 3;

if($data['tipo'] == 'ch'){
	$name = 'cheques';
	$pdf = new CustomPDF(array(16.5,21));
	$pdf->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));
	if($data['banco'] == 1){
		include(ROOT.RUTA_TPL.'contabilidad/chequeAfirme.tpl.php');
	}
	elseif($data['banco'] == 3){
		include(ROOT.RUTA_TPL.'contabilidad/chequeBancomer.tpl.php');
	}
}
else{
	$name = 'polizas';
	$pdf = new CustomPDF('LETTER');
	$pdf->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));
	include(ROOT.RUTA_TPL.'contabilidad/polizas.tpl.php');
}

$pdfcode = $pdf->ezOutput();
saveFile($pdfcode, $name);
openFile(ROOT.'tmp/'.$name.'.pdf');
?>