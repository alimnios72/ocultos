<?php
$data = $_GET;
$templates = 'documentacion/';
define('ROOT','../');
include(ROOT.'main.php');
include(ROOT.RUTA_LIB.'class.customPDF.php');
include(ROOT.RUTA_LIB.'Barcode/php-barcode.php');

//Generaci�n del c�digo de barras
$x        = 150;  // barcode center
$y        = 100;  // barcode center
$w        = 300;
$h        = 200;
$height   = 80;   // barcode height in 1D ; module size in 2D
$width    = 3;    // barcode height in 1D ; not use in 2D
$angle    = 0;
$type     = 'code128';

$barcode = imagecreatetruecolor($w, $h);
$black   = ImageColorAllocate($barcode,0x00,0x00,0x00);
$white   = ImageColorAllocate($barcode,0xff,0xff,0xff);
$red     = ImageColorAllocate($barcode,0xff,0x00,0x00);
$blue    = ImageColorAllocate($barcode,0x00,0x00,0xff);
imagefilledrectangle($barcode, 0, 0, $w, $h, $white);
//$dataBC = Barcode::gd($barcode, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);


//Inicializamos el objeto pdf del tama�o carta y escogemos el tipo de letra
$pdf = new CustomPDF('LETTER');
$pdf->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));
$pdfAm = new CustomPDF('LETTER','landscape');
$pdfAm->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));

$credito = $creditoDB->getCreditoById($data['idC']);
//$code    = md5($credito['id_credito'].$credito['expediente']);
//$dataBC  = Barcode::gd($barcode, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
$dataBC  = Barcode::gd($barcode, $black, $x, $y, $angle, $type, array('code'=>$credito['id_credito']), $width, $height);
$refBanc = $credito['ref_banc'];
//var_dump($credito['ref_banc']);die();
$fechaEntrega = $credito['fecha_entrega'];
$cCredito = $credito;
$cCredito['tasa'] = $credito['tasa_interes'];
$cCredito['fecha'] = $credito['fecha_entrega'];
$cCredito['periodo'] = array_search($credito['periodo'], $periodo);
$amortizacion = new Amortizacion($cCredito);
$tablaAM = $amortizacion->getTabla();
$tablaAM = formatTablaAM($tablaAM);
$mora = $credito['tasa_mora'];
$moraDiaria = "$".number_format(($amortizacion->pagoFijo * ($credito['tasa_mora']/100) * 1.16),2);
if($credito['tipo'] == 0){
	$credito = $creditoDB->getCreditoIndividual($data['idC']);
	$interesTotal = "$".number_format($amortizacion->montoTotal-$credito['cantidad'],2);
	$credito['acreditado'] = mb_strtoupper($credito['acreditado'],'iso-8859-1');
	$montoTexto = numberToText($credito['cantidad'],true);
	$prestamo = $credito['cantidad'];
	$montoFinal = $amortizacion->montoTotal;
	$credito['cantidad'] = "$".number_format($credito['cantidad'],2);
	$totalTexto = numberToText($amortizacion->montoTotal,true);
	$credito['total'] = "$".number_format($amortizacion->montoTotal,2);
	$pFijoTexto = numberToText($amortizacion->pagoFijo,true);
	$credito['pFijo'] = "$".number_format($amortizacion->pagoFijo,2);
	$plazoTexto = numberToText($credito['plazo']);
	$credito['plazo'] = $amortizacion->plazo;
	$credito['periodo'] = strtoupper($credito['periodo']);
	$CAT = $amortizacion->CAT;
	$firmas['suscriptor']['nombre'] = $credito['acreditado'];
	$firmas['suscriptor']['direccion'] = getDireccionAcreditado($credito);
	
	if($data['doc'] == 'contratos'){
		include(ROOT.RUTA_TPL."{$templates}caratulaIndividual.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}caratulaIndividual.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		$firmas['aval']['nombre'] = $credito['obligado'];
		$firmas['aval']['direccion'] = getDireccionSolidario($credito);
		$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}contratoIndividual.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}contratoIndividual.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}pagareIndividual.php");
		$pdf->ezNewPage();
		$pdfcode = $pdf->ezOutput();
		saveFile($pdfcode, 'contratos');
		openFile(ROOT.'tmp/contratos.pdf');
	}
	else{
		include(ROOT.RUTA_TPL."{$templates}amortizacionIndividual.php");
		$pdfcode = $pdfAm->ezOutput();
		saveFile($pdfcode, 'amortizacion');
		openFile(ROOT.'tmp/amortizacion.pdf');
	}
}
else{
	$credito = $creditoDB->getCreditoGrupal($data['idC']);
	$acreditados = $creditoDB->getAcreditadosByCredito($data['idC']);
	$representante = getRepresentante($acreditados, $credito['id_representante']);
	$acreditados = addTablaAmortizacion($cCredito, $acreditados);
	$listaAcred = getListaAcreditados($acreditados, $acreditadosText);
	$acreditadosText = implode(", ", $acreditadosText);
	foreach($acreditados as $acreditado)
		$firmas[] =  mb_strtoupper($acreditado['acreditado'],'iso-8859-1');
	$firmas['cbi'] = "REPRESENTANTE";
	///////////////////////////////////////////////////////////////////////
	$numInt = numberToText(count($acreditados));
	$pagosText = numberToText($amortizacion->plazo);
	///////////////////////////////////////////////////////////////////////
	//La tabla de amortizaci�n grupal se saca sumando las individuales
	$tablaAM = amortizacionGrupal($acreditados);
	$pagoFijo = $tablaAM[0]['pagoFijo'];
	$montoTotal = $pagoFijo * $amortizacion->plazo;
	$tablaAM = formatTablaAM($tablaAM);
	$moraDiaria = "$".number_format(($pagoFijo * ($credito['tasa_mora']/100) * 1.16),2);
	$pFijo = "$".number_format($pagoFijo,2);
		
	//$interesTotal = "$".number_format($amortizacion->montoTotal-$credito['Total'],2);
	$interesTotal = "$".number_format($montoTotal - $credito['Total'],2);
	$credito['nombre'] = mb_strtoupper($credito['nombre'],'iso-8859-1');
	$montoTexto = numberToText($credito['Total'],true);
	$prestamo = $credito['Total'];
	//$montoFinal = $amortizacion->montoTotal;
	$montoFinal = $montoTotal;
	$credito['cantidad'] = "$".number_format($credito['Total'],2);
	//$totalTexto = numberToText($amortizacion->montoTotal,true);
	$totalTexto = numberToText($montoTotal,true);
	//$credito['total'] = "$".number_format($amortizacion->montoTotal,2);
	$credito['total'] = "$".number_format($montoTotal,2);
	//$pFijoTexto = numberToText($amortizacion->pagoFijo,true);
	$pFijoTexto = numberToText($pagoFijo,true);
	//$credito['pFijo'] = "$".number_format($amortizacion->pagoFijo,2);
	$credito['pFijo'] = "$".number_format($pagoFijo,2);
	$plazoTexto = numberToText($credito['plazo']);
	$credito['plazo'] = $amortizacion->plazo;
	$credito['periodo'] = strtoupper($credito['periodo']);
	$CAT = $amortizacion->CAT;
	if($data['doc'] == 'contratos'){
		include(ROOT.RUTA_TPL."{$templates}controlGrupal.php");
		if(count($acreditados) <= 24)
			$pdf->ezNewPage();
		$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}controlGrupal.php");
		if(count($acreditados) <= 24)
			$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}caratulaGrupal.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}caratulaGrupal.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}contratoGrupal.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}contratoGrupal.php");
		if($pages % 2 != 0)
			$pdf->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}pagareGrupal.php");
		$pdf->ezNewPage();
		$pdfcode = $pdf->ezOutput();
		saveFile($pdfcode, 'contratos');
		openFile(ROOT.'tmp/contratos.pdf');
	}
	else{
		include(ROOT.RUTA_TPL."{$templates}amortizacionGrupal.php");
		$pdfAm->ezNewPage();
		include(ROOT.RUTA_TPL."{$templates}amortizacionGrupal.php");
		foreach($acreditados as $acred){
			$interesTotal = 0;
			$acred['tablaAm'] = formatTablaAM($acred['tablaAm']);
			$interesTotal = $acred['montoInteres'] - $acred['cantidad'];
			include(ROOT.RUTA_TPL."{$templates}amortizacionG-I.php");
		}
		$pdfcode = $pdfAm->ezOutput();
		saveFile($pdfcode, 'amortizacion');
		openFile(ROOT.'tmp/amortizacion.pdf');
	}
}
imagedestroy($barcode);
/***********************************************************
 ******* FUNCIONES AUXILIARES  *******************************
 *************************************************************/
function getRepresentante($acreditados, $idRepre){
	if(!empty($acreditados)){
		foreach($acreditados as $acreditado){
			if($acreditado['id_persona'] == $idRepre)
				return $acreditado;
		}
	}
	return array();
}
function addTablaAmortizacion($config, $acreditados){
	if(!empty($acreditados)){
		foreach($acreditados as &$acreditado){
			$config['montoTotal'] = $acreditado['cantidad'];
			$am = new Amortizacion($config);
			$tabla = $am->getTabla();
			$acreditado['pagoFijo'] = $am->pagoFijo;
			$acreditado['montoInteres'] = $am->montoTotal;
			$acreditado['tablaAm'] = $tabla;
		}
	}
	return $acreditados;
}
function getListaAcreditados($acreditados, &$acreditadosText){
	$j = 1;
	foreach($acreditados as $acreditado){
		$tmp = array();
		$tmp['oneCol'] = $j;
		$tmp['twoCol'] = mb_strtoupper($acreditado['acreditado'],'iso-8859-1');
		$tmp['threeCol'] = "$".number_format($acreditado['cantidad'],2);
		$tmp['fourCol'] = "$".number_format($acreditado['pagoFijo'],2);
        $tmp['fiveCol'] = getDireccionAcreditado($acreditado);
		$info[]=$tmp;
		$acreditadosText[] = $tmp['twoCol'];
		$j++;
	}$interesTotal = $am->montoTotal -  $credito['Total'];
	return $info;
}
function getDireccionAcreditado($acreditado) {
    global $ubicacionDB;
    $direccion = '';
    $fields = ['calle', 'num_ext', 'num_int', 'dpto', 'mza', 'lote', 'colonia', 'codigo_postal'];
    $delegacion = $ubicacionDB->getDelegacionById($acreditado['id_delegacion']);
    $estado = $ubicacionDB->getEstadoByDelegacion($acreditado['id_delegacion']);

    foreach ($fields as $field){
        if ($acreditado[$field] !== '' && !is_null($acreditado[$field])) {
            $direccion .= "{$acreditado[$field]} ";
        }
    }

    $direccion .= " {$delegacion['nombre_del']}, {$estado['nombre_edo']}";

    return $direccion;
}
function getDireccionSolidario($solidario){
	return "{$solidario['calleS']} {$solidario['coloniaS']}";
}
function formatTablaAM($tablaAM){
	foreach ($tablaAM as &$pago){
		$pago['prestamo'] = "$".number_format($pago['prestamo'], 2);
		$pago['pagoFijo'] = "$".number_format($pago['pagoFijo'], 2);
		$pago['interes'] = "$".number_format($pago['interes'], 2);
		$pago['iva'] = "$".number_format($pago['iva'], 2);
		$pago['capital'] = "$".number_format($pago['capital'], 2);
		$pago['saldo'] = "$".number_format($pago['saldo'], 2);
		$pago['fecha'] = strftime("%A %d de %B de %Y", strtotime($pago['fecha']));
	}
	return $tablaAM;
}
function amortizacionGrupal($acreditados){
	$tablaGpo = array();
	foreach($acreditados as $acred){
		$tabla = $acred['tablaAm'];
		foreach ($tabla as $key=>$val){
			if(!array_key_exists($key, $tablaGpo)){
				$tablaGpo[$key]['num'] = $val['num'];
				$tablaGpo[$key]['prestamo'] = $val['prestamo'];
				$tablaGpo[$key]['pagoFijo'] = $val['pagoFijo'];
				$tablaGpo[$key]['interes'] = $val['interes'];
				$tablaGpo[$key]['iva'] = $val['iva'];
				$tablaGpo[$key]['capital'] = $val['capital'];
				$tablaGpo[$key]['saldo'] = $val['saldo'];
				$tablaGpo[$key]['fecha'] = $val['fecha'];
			}
			else{
				$tablaGpo[$key]['prestamo'] += $val['prestamo'];
				$tablaGpo[$key]['pagoFijo'] += $val['pagoFijo'];
				$tablaGpo[$key]['interes'] += $val['interes'];
				$tablaGpo[$key]['iva'] += $val['iva'];
				$tablaGpo[$key]['capital'] += $val['capital'];
				$tablaGpo[$key]['saldo'] += $val['saldo'];
			}
		}
	}
	return $tablaGpo;
}
?>
</body>
</html>
