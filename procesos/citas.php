<?php
define('ROOT','');
$data = $_POST;
//include(ROOT.'main.php');
$sucursales = $ubicacionDB->getSucursales();
if(isset($fecha) && $fecha != '')
	$today = date('Y-m-d', strtotime($fecha));
else	
	$today = date('Y-m-d');
if(!isset($data['idSucursal']))
	$idSucursal = 1;
else
	$idSucursal = $data['idSucursal'];

$prev = subDays($today, 7);
$next = addDays($today, 7);
$dow = date('w');
$daysInWeek = getDaysinWeek($today, $dow);
$ini = $daysInWeek[0];
//Es importante tomar un d�a despu�s de la fecha final puesto que al hacer la comparaci�n
//toma la fecha del d�a con 0 hrs.
$fin = addDays(end($daysInWeek), 1);
$citas = $creditoDB->getCitasCredito($ini,$fin,$idSucursal);
$citas = getTimeTable($citas);
//var_dump($daysInWeek[0]);
$horas = array('09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30',
				'14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30');
include(ROOT.RUTA_TPL.'citas.tpl.php');

function getDaysinWeek($today, $dow){
	for($i=0;$i<7;$i++)
		$days[] = $i > $dow ? addDays($today, $i- $dow) : subDays($today, $dow-$i);
	return $days;
}
?>