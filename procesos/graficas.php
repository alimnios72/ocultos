<?php
header('Content-Type: text/html; charset=windows-1252');
define('ROOT','../');
include(ROOT.'main.php');

$out['meses'] = array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');

$case = $_POST['reporte'];

switch($case)
{
	case 'Totalxtipo':
		$sql = "SELECT YEAR(C.fecha_entrega) ano, MONTH(C.fecha_entrega) mes,
				SUM(A.cantidad) monto, C.tipo
				FROM creditos C
				JOIN acreditados A ON A.id_credito=C.id_credito
				WHERE C.expediente <> 'Indefinido' AND C.expediente <> 'Cancelado'
				AND C.fecha_entrega IS NOT NULL AND YEAR(C.fecha_entrega) = {$_POST['anio']}
				GROUP BY YEAR(C.fecha_entrega), MONTH(C.fecha_entrega), C.tipo";
		$res = $creditoDB->getGraficaReporte($sql);
		$gpos = array(); $indivs = array();
		if(!empty($res)){
			foreach($res as $val){
				if($val['tipo'] == "1")
					$gpos[] = intval($val['monto']);
				else
					$indivs[] = intval($val['monto']);
			}
		}
		$out['grupos'] = $gpos;
		$out['indivs'] = $indivs;
		echo json_encode($out);
		break;
	default:
		break;	
}
?>