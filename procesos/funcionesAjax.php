<?php
header('Content-Type: text/html; charset=windows-1252'); 
define('ROOT','../');
include(ROOT.'main.php');

$accion = isset($_POST['accion']) ? $_POST['accion'] : false;
switch($accion){
    case 'delegaciones':
        $delegaciones = $ubicacionDB->getDelegacionesByEstado($_POST['idEstado']);
        if(!empty($delegaciones)){
        	$nombre = $_POST['nombre']!=''?$_POST['nombre']:'delegacion';
            echo "<label for='{$nombre}'>Delegaciones ";
            getComboBox($delegaciones, $nombre, $nombre, array('value'=>'id_delegacion','text'=>'nombre_del'));
            echo "</label>";
        }
        else
            echo "<p class='msg warning'>No existen delegaciones/municipios para ese estado</p>";
        break;
    case 'datosC':
        $datos = $personaDB->getDatosPersona($_POST['id']);
        include(ROOT.RUTA_TPL.'administracion/datosCliente.tpl.php');
        break;
    case 'historial':
        $hist = $creditoDB->getCreditosByPersona($_POST['id']) ;
        $hist = $hist != NULL ? $hist : array();
        $obg = $creditoDB->getCreditosByObligado($_POST['id'], array(0,1,2,3,4));
        $obg = $obg != NULL ? $obg : array();
        $hist = array_merge($hist,$obg);
        include(ROOT.RUTA_TPL.'administracion/historial.tpl.php');
        break;
    case 'chequesAcred':
        $acreditados = $adminConta->getAcreditadosByCredito($_POST['idsCred']);
        if(empty($acreditados))
             echo "<p class='msg warning'>No existen acreditados para los cr�ditos seleccionados</p>";
        else
            include(ROOT.RUTA_TPL.'contabilidad/chequesAcred.tpl.php');
        break;
    case 'getEdad':
    	$edad = getEdad($_POST['fecha']);
        echo json_encode($edad);
        break;
    //Asigna un representante a un grupo
    case 'repre':
    	foreach($_SESSION['acreditado'] as &$acred){
    		if(isset($acred['representante']))
    			unset($acred['representante']);
    	}
    	$_SESSION['acreditado'][$_POST['num']]['representante'] = 1;
    	break;
    //Muestra el resumen de cr�dito previo a la aprobaci�n de un cr�dito.
    case 'resumenCred':
    	if($_POST['tipo'] == 0){
    		$credito = $creditoDB->getCreditoIndividual($_POST['id']);
    		$cCredito['montoTotal'] = $credito['cantidad'];
    	}
    	else{
    		$credito = $creditoDB->getCreditoGrupal($_POST['id']);
    		$acreditados = $creditoDB->getAcreditadosByCredito($_POST['id']);
    		$cCredito['montoTotal'] = $credito['Total'];
    	}
    	$cCredito['tasa'] = $credito['tasa_interes'];
    	$cCredito['periodo'] = array_search($credito['periodo'], $periodo);
    	$cCredito['plazo'] = $credito['plazo'];
    	$cCredito['fecha'] = $credito['fecha_calculo'];
    	$cCredito['esSemana'] = $credito['esDiaSemana'] == 1 ? 'true' : 'false';
    	$cCredito['esMes'] = $credito['esDiaMes'] == 1 ? 'true' : 'false';
    	$cCredito['diaSemana'] = $credito['diaSemana'];
    	$cCredito['diaMes'] = $credito['diaMes'];
    	$cCredito['viejo'] = $credito['viejo'];
    	$amortizacion = new Amortizacion($cCredito);
    	$tablaAm = $amortizacion->getTabla();
    	include(ROOT.RUTA_TPL.'ventas/resumenAprobados.tpl.php');
    	break;
    //Busca si una persona ya existe en cuanto se intenta capturar una solicitud de cr�dito
    case 'buscaPersona':
    	$_POST['nombres'] = mb_convert_encoding($_POST['nombres'], 'Windows-1252', 'UTF-8');
    	$_POST['apellidoP'] = mb_convert_encoding($_POST['apellidoP'], 'Windows-1252', 'UTF-8');
    	$_POST['apellidoM'] = mb_convert_encoding($_POST['apellidoM'], 'Windows-1252', 'UTF-8');
    	//Para ver si la persona como acreditado
    	$persona = $personaDB->existsPersona($_POST);
    	//var_dump($persona);
    	if($persona != NULL){
    		$acreditado = $creditoDB->getCreditosByAcreditado($persona['id_persona'], array(0,1,2));
    		$obligado = $creditoDB->getCreditosByObligado($persona['id_persona'], array(0,1,2));
    		/*if((!empty($acreditado) || !empty($obligado)) && $_POST['viejo']!=1){
    			$statusCred = !empty($acreditado) ? $status[$acreditado[0]['status']] : $status[$obligado[0]['status']];
    			$tipo = !empty($acreditado) ? 1 : 0;
    			echo json_encode(array('deudor'=>true,'idPersona'=>$persona['id_persona'], 
    									'status'=>$statusCred, 'tipo'=>$tipo));
    		}
    		else{*/
    			if($_POST['tipo'] == 'acreditado')
    				toSessionFormatAcred($_SESSION['acreditado'][$_POST['acred']], $persona);	
    			else
    				toSessionFormatSol($_SESSION['solidario'], $personaDB->existsPersona($_POST));
    			echo json_encode(array('deudor'=>false));
    		//}
    	}
      else{
    		echo "No existe";
    	}
    	break;
    case 'addCita':
    	$creditoDB->addCitaCredito($_POST);
    	break;
    case 'loadCitas':
    	$fecha = $_POST['fecha'];
    	include(ROOT.'procesos/citas.php');
    	break;
    case 'configCred':
    	if(isset($_POST['update']))
    		$creditoDB->updateCreditoAprobado($_POST);
    	$credito = $creditoDB->getCreditoById($_POST['id']);
    	$esDiaMes = $credito['periodo'] == "Mensual" || $credito['periodo'] == "Bimestral" || $credito['periodo'] == "Trimestral" || $credito['periodo'] == "Quinquenal" || $credito['periodo'] == "Semestral" || $credito['periodo'] == "Anual";
    	include(ROOT.RUTA_TPL.'administracion/configCredito.tpl.php');
    	break;
    case 'configCheques':
    	if(isset($_POST['update'])){
    		for ($i=0; $i<count($_POST['ids']); $i++)
    			$transaccionDB->addCheque($_POST['ids'][$i], $_POST['montos'][$i], $_POST['bancos'][$i], $_POST['cheques'][$i]);
    	}
    	$acreditados = $transaccionDB->getChequesByCredito($_POST['id']);
    	include(ROOT.RUTA_TPL.'contabilidad/chequesAcred.tpl.php');
    	break;
    case 'pagDepositos':
    	if($_POST['page'] == 'prev')
    		$_POST['date'] = subMonths($_POST['date'], 1);
    	else
    		$_POST['date'] = addMonths($_POST['date'], 1);
    	$day = 1;
    	$month = date('m', strtotime($_POST['date']));
    	$year = date('Y', strtotime($_POST['date']));
    	$date = "{$year}-{$month}-{$day}";
    	$criterio['byMes'][0] = $month; 
		$criterio['byMes'][1] = $year;
		$depositos = $transaccionDB->getDepositosByCriterio($criterio);
    	include(ROOT.RUTA_TPL.'contabilidad/listaDepositos.tpl.php');
    	break;
    case 'cobraCheques':
   		$data = $_POST;
   		$statusCH = array('Elaborado', 'Cobrado', 'Cancelado');
   		$tipoCred = array('INDIVIDUAL', 'GRUPAL');
    	$criterios = array();
    	if(isset($data['banco']) && $data['banco'] != "")
    		$criterios['banco'] = $data['banco'];
    	if(isset($data['status']) && $data['status'] != "")
    		$criterios['status'] = $data['status'];
    	if($data['fecha1'] != "" && $data['fecha2'] != ""){
    		$criterios['fechas'][0] = $data['fecha1'];
    		$criterios['fechas'][1] = $data['fecha2'];
    	}
    	if(empty($criterios)){
    		$criterios['fechas'][0] = subDays(date('Y-m-d'), 3);
    		$criterios['fechas'][1] = addDays(date('Y-m-d'), 3);
    	}	
    	if($data['action'] == 'update')
    		$transaccionDB->updateChequeCobrado($data['idDisp'], $data['noCheque'], $data['Cstatus'], $data['fechaCobro']);
    	elseif($data['action'] == 'cancel'){
    		$disp = $transaccionDB->getDisposicionCreditoByCheque($data['cNoCh'], $data['cBanco']);
    		if(!empty($disp))
    			$transaccionDB->updateChequeCobrado($disp['id_disposicion'],$data['cNoCh'], 2, date('Y-m-d'), $data['cMotivo']);
    		else
    			$transaccionDB->addDispChequeCobrado($data['cBanco'], $data['cMotivo'], $data['cNoCh']);
    	}    		
    	$cheques = $transaccionDB->getChequesByCriterio($criterios);
    	include(ROOT.RUTA_TPL.'contabilidad/listaCheques.tpl.php');
    	break;
    case 'pagAmortizaciones':
    	$criterios['statusAmo'] = 0;
    	if($_POST['tipo'] == 'fecha'){
	    	$criterios['fecha'] = $_POST['date'];
	    	$prev = subDays($criterios['fecha'], 1);
	    	$next = addDays($criterios['fecha'], 1);
		$criterios['tipo_cargo'] = 0;
    	}
    	else
    		$criterios['expediente'] = $_POST['expediente'];
    	$amortizaciones = $transaccionDB->getAmortizacionesByCriterio($criterios);
    	include(ROOT.RUTA_TPL.'cobranza/listaAmortizaciones.tpl.php');
    	break;
    case 'amortCred':
    	$criterios = array();
    	$criterios['idCred'] = $_POST['id'];
    	$criterios['tipoAbono'] = 0;
    	$criterios['tipoCargo'] = 0;
    	$acreditados = $creditoDB->getAcreditadosByCredito($_POST['id']);
    	$tabla = $transaccionDB->getAmortizacionByCredito($_POST['id']);
    	$pagos = $transaccionDB->getAbonosByCriterio($criterios);
    	$moratorios = $transaccionDB->getMoratoriosByCriterio($criterios);
    	$pagoMoratorios = $transaccionDB->getPagoMoratoriosByCriterio($criterios);
    	$criterios['fecha'] = date('Y-m-d');
    	$balance = $transaccionDB->getBalanceByCriterio($criterios);
    	///////////////////////////////////////////////////////////
    	$credito = $creditoDB->getCreditoById($_POST['id']);
    	if(intval($credito['tipo']) == 1)
    		$representante = $creditoDB->getRepresentanteByCredito($criterios['idCred']);
    	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
    	$diasMoraHoy = MoraTotal($tabla, date('Y-m-d'), $diasInhabiles);
    	$pFijo = $tabla[0]['monto'];
    	$criterios['tipoAbono'] = 1;
    	$criterios['tipoCargo'] = 1;
    	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
    	$moraDia = $pFijo * ($credito['tasa_mora']/100) * 1.16;
    	$moraEfectiva = $diasMoraHoy * $pFijo * ($credito['tasa_mora']/100) * 1.16;
    	$moraHoy['interes'] = $moraEfectiva / 1.16;
    	$moraHoy['iva'] = $moraHoy['interes'] * 0.16;
    	$moraHoy['monto'] = $moraHoy['interes'] + $moraHoy['iva'];
    	$moraHoy['fecha'] = strftime("%A %e de %B de %Y", strtotime('today'));
    	//Sumo la mora al d�a de hoy con la guardada
    	$balanceMora['monto'] += $moraHoy['monto'];
    	$balanceMora['interes'] += $moraHoy['interes'];
    	$balanceMora['iva'] += $moraHoy['iva'];
    	include(ROOT.RUTA_TPL.'administracion/amortizacion.tpl.php');
    	break;
    case 'editDelegaciones':
    	if($_POST['update'] == "true")
    		$ubicacionDB->updateDelegacion($_POST);
    	$delegaciones = $ubicacionDB->getDelegacionesByEstado($_POST['id_edo']);
    	include(ROOT.RUTA_TPL.'sistema/editDelegaciones.tpl.php');
    	break;
    case 'editZonas':
    	if($_POST['update'] == "true")
    		$ubicacionDB->updateZona($_POST);
    	$zonas = $ubicacionDB->getZonasByDelegacion($_POST['idDel']);
    	include(ROOT.RUTA_TPL.'sistema/editZonas.tpl.php');
    	break;
    case 'updateTasa':
    	$xml = ROOT.RUTA_XML."varsSistema.xml";
    	$sist = simplexml_load_file($xml);
    	$intOrd = $sist->interes_ordinario;
    	$intMor = $sist->interes_moratorio;
    	if($_POST['tipo'] == 'tasaOrd'){
	    	foreach($intOrd->tasa as $tasa)
				if($tasa->monto == stripslashes($_POST['tasa']))
					$tasa->activo = $_POST['activo'] == "true" ? 1 : 0;
    	}
    	elseif ($_POST['tipo'] == 'tasaMor'){
    		foreach($intMor->tasa as $tasa){
				if($tasa->monto == $_POST['tasa'])
					$tasa->activo = 1;
				else 
					$tasa->activo = 0;
    		}
    	} 
    	$sist->asXML($xml);
    	break;
    case 'updatePlazo':
    	$xml = ROOT.RUTA_XML."varsSistema.xml";
    	$sist = simplexml_load_file($xml);
    	$plazos = $sist->plazos;
    	foreach($plazos->plazo as $plazo){
    			if($plazo->meses == $_POST['plazo'])
    				$plazo->activo = $_POST['activo'] == "true" ? 1 : 0;
    	}
    	$sist->asXML($xml);
    	break;
    case 'updateAcred':
    	if($_POST['tipo'] == 'monto'){
    		$creditoDB->updateAcreditado($_POST);
    		$credito = $creditoDB->getCreditoById($_POST['idCred']);
    		var_dump($credito);die();
    		$data['tasaInteres'] = $credito['tasa_interes'];
    		$data['plazo'] = $credito['plazo'];
    		$data['periodo'] = $credito['periodo'];
    		if($credito['status'] > 1){
	    		deleteTablaAmortizacion($transaccionDB, $_POST['idCred']);
	    		createTablaAmortizacion($creditoDB, $transaccionDB, $_POST['idCred'], $data);
    		}
    	}
    	else {
    		$creditoDB->deleteAcreditado($_POST);
    		$acreditados = $creditoDB->getAcreditadosByCredito($_POST['idCred']);
    		include(ROOT.RUTA_TPL.'sistema/editAcreditados.tpl.php');
    	}
    	
    	//var_dump($_POST);
    	break;
    case 'borraDep':
		//Borro el dep�sito y los movimientos que pudo haber afectado
    	$movimientos = $transaccionDB->getMovimientosByDeposito($_POST['idDep']);
    	$transaccionDB->deleteDepositoById($_POST['idDep']);
    	if(!empty($movimientos)){
    		foreach($movimientos as $movimiento)
    			$transaccionDB->deleteMovimientoById($movimiento['id_movimiento']);
    		if($movimientos[0]['tipo_abono'] == '0'){//Si el dep�sito era de mora, no hay necesidad de reacomodar
	    		//Reacomodo nuevamente los dep�sitos.
	    		$idCred = $movimientos[0]['id_credito'];
	    		//Cambio el status de cr�dito a activo si es que no esta as�
	    		$creditoDB->updateStatusCredito($idCred,2);
	    		$depositos = $transaccionDB->getDepositoByCredito($idCred,$criterio);
	    		if(!empty($depositos)){
	    			$data['idDeposito'] = $depositos[0]['id_deposito'];
	    			$data['idCredito'] = $idCred;
	    			aplicaPago($data, $creditoDB, $transaccionDB, $ubicacionDB,false, true, true);
	    		}
    		}
    	}
    	$day = 1;
    	$month = date('m', strtotime("now"));
    	$year = date('Y', strtotime("now"));
    	$date = "{$year}-{$month}-{$day}";
    	$criterio['byMes'][0] = $month;
    	$criterio['byMes'][1] = $year;
    	$depositos = $transaccionDB->getDepositosByCriterio($criterio);
    	include(ROOT.RUTA_TPL.'contabilidad/listaDepositos.tpl.php');
    	break;
    case deleteIntegrante:
    	$acred = $_POST['acreditado'];
    	unset($_SESSION['acreditados'][$acred]);
    	$acreditados = $_SESSION['acreditados'];
    	include(ROOT.RUTA_TPL.'ventas/renovacion/integrantes.tpl.php');
    	break;
    case montoIntegrante:
    	$acred = $_POST['acreditado'];
    	$monto = $_POST['monto'];
    	$_SESSION['acreditados'][$acred]['montoAcreditado'] = $monto;
    	$acreditados = $_SESSION['acreditados'];
    	include(ROOT.RUTA_TPL.'ventas/renovacion/integrantes.tpl.php');
    	break;
    case checkboxIntegrante:
    	$acred = $_POST['acreditado'];
    	$checkbox = $_POST['checked'];
    	$_SESSION['acreditados'][$acred]['checked'] = $checkbox;
    	$acreditados = $_SESSION['acreditados'];
    	include(ROOT.RUTA_TPL.'ventas/renovacion/integrantes.tpl.php');
    	break;
    case 'validateUser':
    	$user = $_POST['username'];
    	$pass = $_POST['userpass'];
    	$ans = array('response'=>$adminLogin->doLogin($user, $pass));
    	echo json_encode($ans);
    	break;
    default:
        break;
}

function toSessionFormatSol(&$session, $persona){
	$session['id_persona'] = $persona['idP'];
	$session['nombres'] = $persona['nombres'];
	$session['apellidoP'] = $persona['apellido_paterno'];
	$session['apellidoM'] = $persona['apellido_materno'];
	$session['nacimiento'] = $persona['fecha_nacimiento'];
	$session['edad'] = getEdad($persona['fecha_nacimiento']);
	$session['sexo'] = $persona['sexo'];
	$session['rfc'] = $persona['rfc'];
	$session['curp'] = $persona['curp'];
	$session['edocivil'] = $persona['estado_civil'];
	$session['t_conyuge'] = $persona['t_conyuge'];
	$session['dep_econom'] = $persona['dep_econom'];
	$session['calle'] = $persona['calle'];
	$session['num_ext'] = $persona['num_ext'];
	$session['num_int'] = $persona['num_int'];
	$session['dpto'] = $persona['dpto'];
	$session['mza'] = $persona['mza'];
	$session['lote'] = $persona['lote'];
	$session['colonia'] = $persona['colonia'];
	$session['cp'] = $persona['codigo_postal'];
	$session['estado'] = $persona['id_estado'];
	$session['delegacion'] = $persona['id_delegacion'];
	$session['cp'] = $persona['codigo_postal'];
	$session['residencia'] = $persona['residencia'];
	$session['tel_casa'] = $persona['telefonos'];
	$session['celular'] = $persona['celular'];
	$session['email'] = $persona['email'];
}

function toSessionFormatAcred(&$session, $persona){
	$session['info_personal']['id_persona'] = $persona['idP'];
	$session['info_personal']['nombres'] = $persona['nombres'];
	$session['info_personal']['apellidoP'] = $persona['apellido_paterno'];
	$session['info_personal']['apellidoM'] = $persona['apellido_materno'];
	$session['info_personal']['nacimiento'] = $persona['fecha_nacimiento'];
	$session['info_personal']['edad'] = getEdad($persona['fecha_nacimiento']);
	$session['info_personal']['sexo'] = $persona['sexo'];
	$session['info_personal']['rfc'] = $persona['rfc'];
	$session['info_personal']['curp'] = $persona['curp'];
	$session['info_personal']['edocivil'] = $persona['estado_civil'];
	$session['info_personal']['t_conyuge'] = $persona['t_conyuge'];
	$session['info_personal']['dep_econom'] = $persona['dep_econom'];
	$session['info_personal']['calle'] = $persona['calle'];
	$session['info_personal']['num_ext'] = $persona['num_ext'];
	$session['info_personal']['num_int'] = $persona['num_int'];
	$session['info_personal']['dpto'] = $persona['dpto'];
	$session['info_personal']['mza'] = $persona['mza'];
	$session['info_personal']['lote'] = $persona['lote'];
	$session['info_personal']['colonia'] = $persona['colonia'];
	$session['info_personal']['cp'] = $persona['codigo_postal'];
	$session['info_personal']['estado'] = $persona['id_estado'];
	$session['info_personal']['delegacion'] = $persona['id_delegacion'];
	$session['info_personal']['cp'] = $persona['codigo_postal'];
	$session['info_personal']['residencia'] = $persona['residencia'];
	$session['info_personal']['tel_casa'] = $persona['telefonos'];
	$session['info_personal']['celular'] = $persona['celular'];
	$session['info_personal']['email'] = $persona['email'];
	////////////////////////////////////////////////////////////
	$session['info_personal']['enfermedad_solic'] = $persona['enfermedad_solic'];
	$session['info_personal']['enfermedad_fam'] = $persona['enfermedad_fam'];
	$session['info_personal']['enfermedad_desc'] = $persona['enfermedad_desc'];
	$session['info_personal']['nivel_estudios'] = $persona['nivel_estudios'];
	$session['info_personal']['ant_domicilio'] = $persona['ant_domicilio'];
	////////////////////////////////////////////////////////////
	$session['info_econom']['nombre_negocio'] = $persona['nombre_negocio'];
	$session['info_econom']['calle'] = $persona['calle_negocio'];
	$session['info_econom']['colonia'] = $persona['colonia_negocio'];
	$session['info_econom']['cp'] = $persona['cp_negocio'];
	$session['info_econom']['estado'] = $persona['id_estado_negocio'];
	$session['info_econom']['delegacion'] = $persona['id_delegacion_negocio'];
	$session['info_econom']['tel_neg'] = $persona['telefonos_negocio'];
	$session['info_econom']['ant_negocio'] = $persona['ant_negocio'];
	$session['info_econom']['ing_div'] = $persona['ing_diversos'] > 0 ? 1 : 0;
	$session['info_econom']['ing_div_monto'] = $persona['ing_diversos'];
	$session['info_econom']['pag_cred'] = $persona['pag_credito'] > 0 ? 1 : 0;
	$session['info_econom']['pag_cred_monto'] = $persona['pag_credito'];
	$session['info_econom']['gastos_fam'] = $persona['gastos_fam'];
	$session['info_econom']['pen_alim'] = $persona['pension_alim'] > 0 ? 1 : 0;
	$session['info_econom']['pen_alim_monto'] = $persona['pension_alim'];
	$session['info_econom']['ing_mensuales'] = $persona['ing_mensuales'];
	$session['info_econom']['inv_semanal'] = $persona['inv_semanal'];
	$session['info_econom']['inv_semanal'] = $persona['inv_semanal'];
	$session['info_econom']['pag_diario'] = $persona['pag_diario'];
	$session['info_econom']['ing_diario'] = $persona['ing_diario'];
	$session['info_econom']['monto_sol'] = $persona['monto_sol'];
	////////////////////////////////////////////////////////////
	$session['referencia']['nombres'] = $persona['nombres_ref'];
	$session['referencia']['apellidoP'] = $persona['apellido_paterno_ref'];
	$session['referencia']['apellidoM'] = $persona['apellido_materno_ref'];
	$session['referencia']['parentesco'] = $persona['parentesco'];
	$session['referencia']['calle'] = $persona['calle_ref'];
	$session['referencia']['colonia'] = $persona['colonia_ref'];
	$session['referencia']['cp'] = $persona['codigo_postal_ref'];
	$session['referencia']['tel_casa'] = $persona['telefonos_ref'];
	$session['referencia']['celular'] = $persona['celular_ref'];
}
?>	
