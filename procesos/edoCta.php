<?php
$data = $_GET;
$templates = 'documentacion/';
define('ROOT','../');
include(ROOT.'main.php');
include(ROOT.RUTA_LIB.'class.customPDF.php');

$credito = $creditoDB->getCreditoById($data['idC']);
$cCredito = $credito;
$cCredito['tasa'] = $credito['tasa_interes'];
$cCredito['periodo'] = $periodo[$credito['periodo']];
$am = new Amortizacion($cCredito);
$am->getTabla();

$statusAm =  array('0'=>'Adeuda', '1'=>'Pagada');

$amortizacion = $transaccionDB->getAmortizacionByCredito($data['idC']);
$amortizacion = formatAmortizacion($amortizacion, $statusAm);
//$pagos = $transaccionDB->getAbonosByCriterio(array('idCred'=>$data['idC']));
//$pagos = formatPagos($pagos);
$pagos = $transaccionDB->getAbonosByCredito($data['idC']);
$pagos = formatPagos($pagos);
$criterios['idCred'] = $data['idC'];
$moratorios = $transaccionDB->getMoratoriosByCriterio($criterios);
$criterios['tipoAbono'] = 0;
$criterios['tipoCargo'] = 0;
$balance = $transaccionDB->getBalanceByCriterio($criterios);


//Inicializamos el objeto PDF
$pdf = new CustomPDF('LETTER');
$pdf->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));

include(ROOT.RUTA_TPL."{$templates}edoCta.tpl.php");


$pdfcode = $pdf->ezOutput();
saveFile($pdfcode, 'edoCta');
openFile(ROOT.'tmp/edoCta.pdf');


/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////FUNCIONES////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
function formatAmortizacion($amort, $status){
	$tabla = array();
	foreach ($amort as $val){
		$tmp['Fecha'] = strftime("%A %d %B %Y", strtotime($val['fecha_mov']));
		$tmp['Monto'] = '$'.number_format($val['monto'], 2);
		$tmp['Interes'] = '$'.number_format($val['interes'], 2);
		$tmp['IVA'] = '$'.number_format($val['iva'], 2);
		$tmp['Capital'] = '$'.number_format($val['capital'], 2);
		$tmp['Estado'] =  $status[$val['status']];
		$tabla[] = $tmp;
		unset($tmp);
	}
	return $tabla;
}
function formatPagos($pagos){
	$tabla = array();
	foreach ($pagos as $val){
		if(intval($val['tipo_abono']) == 0){
			$tmp['fecha'] = strftime("%A %d %B %Y", strtotime($val['fecha_deposito']));
			$tmp['monto'] = '$'.number_format($val['monto'], 2);
			$tmp['interes'] = '$'.number_format(-$val['interes'], 2);
			$tmp['iva'] = '$'.number_format(-$val['iva'], 2);
			$tmp['capital'] = '$'.number_format(-$val['capital'], 2);
		}
		else{
			$tmp['intmora'] = '$'.number_format(-$val['interes'], 2);
			$tmp['ivamora'] = '$'.number_format(-$val['iva'], 2);
		}
		$tabla[$val['id_deposito']] = isset($tabla[$val['id_deposito']])?array_merge($tabla[$val['id_deposito']],$tmp):$tmp;
		unset($tmp);
	}
	return $tabla;
}/*
function formatPagos($pagos){
	$tabla = array();
	foreach ($pagos as $val){
		$tmp['Fecha'] = strftime("%A %d %B %Y", strtotime($val['fecha_mov']));
		$tmp['Monto'] = '$'.number_format(-$val['monto'], 2);
		$tmp['Interes'] = '$'.number_format(-$val['interes'], 2);
		$tmp['IVA'] = '$'.number_format(-$val['iva'], 2);
		$tmp['Capital'] = '$'.number_format(-$val['capital'], 2);
		$tabla[] = $tmp;
		unset($tmp);
	}
	return $tabla;
}*/
?>
