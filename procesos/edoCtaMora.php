<?php
$data = $_GET;
$templates = 'documentacion/';
define('ROOT','../');
include(ROOT.'main.php');
include(ROOT.RUTA_LIB.'class.customPDF.php');

$credito = $creditoDB->getCreditoById($data['idC']);
$criterios['idCred'] = $credito['id_credito'];
$criterios['tipoAbono'] = 1;
$criterios['tipoCargo'] = 1;
//$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
$amtz = $transaccionDB->getAmortizacionByCredito($credito['id_credito']);
$criterios['idCred'] = $credito['id_credito'];
$criterios['tipoAbono'] = 0;
$pagos = $transaccionDB->getAbonosByCriterio($criterios);
$pFijo = $amtz[0]['monto'];
$mora = getMoraActiva($amtz, $pagos, $pFijo, $credito['tasa_mora'],$ubicacionDB);
$moratorios = $transaccionDB->getMoratoriosByCredito($credito['id_credito']);
$condonaciones = getCondonaciones($moratorios);
$mora = mergeTables($mora, $moratorios);
$mora = getMoraHoy($mora, $amtz, $pFijo, $credito['tasa_mora'], $ubicacionDB);
$moratorios = formatMora($mora);
//$moratorios = formatMora($moratorios);

//Inicializamos el objeto PDF
$pdf = new CustomPDF('LETTER');
$pdf->selectFont(ROOT.RUTA_LIB.'fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding'));

include(ROOT.RUTA_TPL."{$templates}edoCtaMora.tpl.php");

$pdfcode = $pdf->ezOutput();
saveFile($pdfcode, 'edoCta');
openFile(ROOT.'tmp/edoCta.pdf');


/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////FUNCIONES////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/*function formatMora($moratorios){
	$tabla = array();
	$saldo = 0;
	foreach ($moratorios as $val){
		$tmp['fecha'] = strftime("%A %d %B %Y", strtotime($val['fecha_mov']));
		if(intval($val['tipo_mov']) == 0){
			//$tmp['monto'] = '$'.number_format($val['monto'], 2);
			$tmp['interesC'] = '$'.number_format($val['interes'], 2);
			$tmp['ivaC'] = '$'.number_format($val['iva'], 2);
		}
		else{
			$tmp['interesP'] = '$'.number_format(-$val['interes'], 2);
			$tmp['ivaP'] = '$'.number_format(-$val['iva'], 2);
		}
		$saldo +=  $val['interes'] + $val['iva'];
		$tmp['saldo'] = '$'.number_format($saldo, 2);
		$tabla[$val['fecha_mov']] = isset($tabla[$val['fecha_mov']])?array_merge($tabla[$val['fecha_mov']],$tmp):$tmp;
		unset($tmp);
	}
	return $tabla;
}*/
function formatMora($mora){
	$saldo = 0;
	foreach ($mora as &$m){
		$saldo +=  $m['monto'] + $m['pago'];
		$m['fecha'] = strftime("%A %d %B %Y", strtotime($m['fecha']));
		$m['monto'] = '$'.number_format($m['monto'], 2);
		$m['pago'] = '$'.number_format(-$m['pago'], 2);
		$m['saldo'] = '$'.number_format($saldo, 2);
	}
	return $mora;
}
function mergeTables($mora, $pagos){
	foreach($pagos as $pago){
		if(intval($pago['tipo_mov']) == 1){
			if(isset($mora[$pago['fecha_mov']]))
				$mora[$pago['fecha_mov']]['pago'] = $pago['monto']; 
			else{
				$mora[$pago['fecha_mov']]['no'] = "-";
				$mora[$pago['fecha_mov']]['dias'] = "-";
				$mora[$pago['fecha_mov']]['monto'] = "0";
				$mora[$pago['fecha_mov']]['pago'] = $pago['monto'];
				$mora[$pago['fecha_mov']]['fecha'] = $pago['fecha_mov'];
			}
		}
	}
	//Ordenamos por fecha
	usort($mora, 'comparaFechasMora');
	return $mora;
}
function getMoraHoy($mora, $amtz, $pFijo, $tasa, $ubicacionDB){
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	$tasa = $tasa/100;
	$today = date('Y-m-d');
	foreach ($amtz as $key=>$am){
		if(strtotime($am['fecha_mov']) < strtotime($today) && intval($am['status']) == 0){
			$dias = 0;
			$tmp[] = $am;
			$dias = MoraTotal($tmp, $today, $diasInhabiles);
			$mora[] = array('no'=>$key+1,'dias'=>$dias,'monto'=>$dias*$tasa*1.16*$pFijo,
								'pago'=>0,'fecha'=>$today);
			unset($tmp);
		}
	}
	return $mora;
}

function comparaFechasMora($a, $b){
	$t1 = strtotime($a['fecha']);
	$t2 = strtotime($b['fecha']);
	if($t1 == $t2){
		if($a['no'] == "-")
			$a['no'] = 100;
		if($b['no'] == "-")
			$b['no'] = 100;
		return $a['no'] - $b['no'];
	}
	else
		return $t1 - $t2;
}

function getMoraActiva($amtz, $pagos, $pFijo, $tasa, $ubicacionDB){
	$tasa = $tasa/100;
	//Primero cambio todos los status a no pagado
	foreach ($amtz as &$am)
		$am['status'] = 0;
	//Obtengo todos los cargos de mora de parcialidades pagadas
	$mora = array();
	$restante = 0;
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	if(!empty($pagos)){
		foreach($pagos as $k=>$pago){
			$restante += -$pago['monto'];
			foreach ($amtz as $key=>&$am){
				if($am['status'] == 0){
					if($restante >= $am['monto'] || ($am['monto'] - $restante) < 1/5){
						$tmp[] = $am;
						if(strtotime($am['fecha_mov']) < strtotime($pago['fecha_mov'])){
							$dias = MoraTotal($tmp, $pago['fecha_mov'], $diasInhabiles);
                                                        if($dias > 0){
							        $mora[] = array('no'=>$key+1,'dias'=>$dias,'monto'=>$dias*$tasa*1.16*$pFijo,
									        'pago'=>0, 'fecha'=>$pago['fecha_mov']);
							        /*$mora[$pago['fecha_mov']] = array('no'=>$key+1,'dias'=>$dias,'monto'=>$dias*$tasa*1.16*$pFijo,
									        'pago'=>0, 'fecha'=>$pago['fecha_mov']);*/
                                                        }
						}
						$am['status'] = 1;
						$restante -= $am['monto'];
						unset($tmp);
					}
				}
			}
			
		}
	}
	return $mora;
}

function getCondonaciones($moratorios){
	$condonacion = array();
	$condonacion['condonado']['monto'] = 0;
	$condonacion['pagado']['monto'] = 0;
	if(is_array($moratorios) && !empty($moratorios)){
		foreach($moratorios as $mora){
			if(intval($mora['tipo_mov']) == 1){
				if($mora['banco'] === 'CBI Condonacion'){
					$condonacion['condonado']['monto'] += -($mora['monto']);
					$condonacion['condonado']['fechas'][] = $mora['fecha_mov'];
				}
				else{
					$condonacion['pagado']['monto'] += -($mora['monto']);
					$condonacion['pagado']['fechas'][] = $mora['fecha_mov'];
				}
			}
				
		}
	}
	return $condonacion;
}
?>
