<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Sistema de administraci�n de cr�ditos | Entrar</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<?php echo includeCSS(RUTA_CSS, $css);?>
<?php echo includeJS(RUTA_JS, $js); ?>
<script type="text/javascript">
/* <![CDATA[ */
	$(document).ready(function(){
			$(".block").fadeIn(1000);				   
			$(".idea").fadeIn(1000);
			$('.idea').supersleight();
			$('#username').example('Username');	
			$('#password').example('Password');
	});
/* ]]> */
</script>
</head>

<body>
    <div id="wrap">
            <div class="idea">
            <img src="<?php echo RUTA_IMG; ?>ico-info.gif" alt=""/>
            <p><?php if(isset($msg) && $msg!='') echo $msg; else echo "Introduce tus datos para iniciar sesi�n"; ?></p>
        </div>
        
<div class="block">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <div class="left"></div>
            <div class="right">
                <div class="div-row">
                	<input type="text" id="username" name="username"  onfocus="this.value='';" onblur="if (this.value=='') {this.value='Usuario';}" value="Usuario" />
                                    </div>
                <div class="div-row">
                     <input type="password" id="password" name="password" onfocus="this.value='';" onblur="if (this.value=='') {this.value='************';}" value="************" />
                </div>
                <div class="rm-row">
                    <input type="checkbox" value="c" name="rm" id="remember"/> <label for="remember">Recordarme</label>
                </div>
                <div class="send-row">
                    <button id="login" value="" type="submit" name="login"></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</body>
</html>
