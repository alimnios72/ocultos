<?php
$pdf->ezSetMargins(140, 100, 30, 30);
//Para el logo de la empresa
$imgPath = ROOT.RUTA_IMG.'logo.jpg';


//Para el encabezado del estado de cuenta
$header = $pdf->openObject();
$pdf->addJpegFromFile($imgPath,50,680,120);
$pdf->addText(195,745,11, "<b>CREA BIENESTAR S.A. DE C.V. S.O.F.O.M. E.N.R.</b>");
$pdf->addText(260,725,11, "<b>ESTADO DE CUENTA</b>");
$pdf->addText(250,705,10, "<b>{$producto[$credito['producto']]} - {$tipo[$credito['tipo']]}</b>");
$pdf->addText(240,685,9, "<b>FECHA DE CORTE: ".strftime("%d %B %Y", strtotime('today'))."</b>");
$pdf->closeObject();
$pdf->addObject($header, "all");

//Para el pie de pagina del estado de cuenta
$footer = $pdf->openObject();
$pdf->setLineStyle(1);
$pdf->line(50,60,562,60);
$pdf->addText(70,50,8, "Vito Alessio Robles No. 101, 1er piso, Col. Hacienda de Guadalupe Chimalistac, Delegaci�n �lvaro Obreg�n, C.P. 01050 M�xico D.F.");
$pdf->addText(190,40,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdf->closeObject();
$pdf->addObject($footer, "all");

//Resumen del cr�dito
$pdf->setStrokeColor(1, 0, 0.8);
$pdf->rectangle(40, 540, 520, 105);
$pdf->addText(50,630,10, "<b>Grupo:</b> {$credito['nombreGrupo']}");
$pdf->addText(50,610,10, "<b>Tasa inter�s:</b> {$credito['tasa_interes']}%");
$pdf->addText(50,590,10, "<b>Plazo:</b> {$credito['plazo']} MESES");
$pdf->addText(50,570,10, "<b>Periodo:</b> {$credito['periodo']}");
$pdf->addText(50,550,10, "<b>Monto prestado:</b> $".number_format($credito['montoTotal'], 2));
$pdf->addText(350,630,10, "<b>Inter�s por pagar:</b>");
$pdf->addText(450,630,10, "$".number_format($balance['interes'], 2));
$pdf->addText(350,610,10, "<b>I.V.A. por pagar:</b>");
$pdf->addText(450,610,10, "$".number_format($balance['iva'], 2));
$pdf->addText(350,590,10, "<b>Capital por pagar:</b>");
$pdf->addText(450,590,10, "$".number_format($balance['capital'], 2));
$pdf->addText(350,570,10, "<b>Total:</b>");
$pdf->addText(450,570,11, "<b>$".number_format($balance['monto'], 2)."</b>");


//Tabla de amortizaci�n
$pdf->ezSetY(520);
$cols = array('Fecha'=>'<b>Fecha de adeudo</b>','Monto'=>'<b>Monto</b>','Interes'=>'<b>Inter�s</b>',
			  'IVA'=>'<b>I.V.A.</b>','Capital'=>'<b>Capital</b>','Estado'=>'<b>Estado</b>');
$options = array('xPos'=>'center','maxWidth'=>450, 'xOrientation'=>'center');
$pdf->ezTable($amortizacion, $cols, '<b>Tabla de amortizaci�n</b>', $options);
//Historial de pagos
$pdf->ezSetDy(-20);
$cols = array('Fecha'=>'<b>Fecha de pago</b>','Monto'=>'<b>Monto</b>','Interes'=>'<b>Inter�s</b>',
			  'IVA'=>'<b>I.V.A.</b>','Capital'=>'<b>Capital</b>');
$pdf->ezTable($pagos, $cols, '<b>Historial de pagos</b>', $options);
?>