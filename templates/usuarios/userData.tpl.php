<h1>Usuario</h1>
<?php if(isset($error) && $error !== ''){?>
<p class="msg warning"><?php echo $error; ?></p>
<?php } ?>
<?php if(isset($success) && $succes !== ''){?>
<p class="msg done"><?php echo $success; ?></p>
<?php } ?>
<fieldset>
	<legend>Datos personales</legend>
	<form action="usuario.php" method="post">
	<label for="nombres">Nombres:
		<input class="input-text" type="text" name="nombres" value="<?php echo $user['nombres']; ?>" />
	</label>
	<label for="apellido_paterno">Apellido paterno:
		<input class="input-text" type="text" name="apellido_paterno" value="<?php echo $user['apellido_paterno']; ?>" />
	</label>
	<label for="apellido_materno">Apellido materno:
		<input class="input-text" type="text" name="apellido_materno" value="<?php echo $user['apellido_materno']; ?>" />
	</label>
	<label for="fecha_nacimiento">Fecha de nacimiento:
		<input class="input-text" type="text" name="fecha_nacimiento" value="<?php echo $user['fecha_nacimiento']; ?>" />
	</label>
	<input type="hidden" name="id" value="<?php echo $user['id']; ?>" />
	<input class="input-submit" type="submit" name="updateData" value="Actualizar" />
	</form>
</fieldset>
<fieldset>
	<legend>Cambiar contraseņa</legend>
	<form action="usuario.php" method="post">
	<label for="password">Nueva contraseņa:
		<input class="input-text" type="password" name="password" value="" />
	</label>
	<label for="c_password">Confirmar contraseņa:
		<input class="input-text" type="password" name="c_password" value="" />
	</label>
	<input type="hidden" name="id" value="<?php echo $user['id']; ?>" />
	<input class="input-submit" type="submit" name="updatePassword" value="Cambiar" />
	</form>
</fieldset>