<h1>Gesti�n de cheques</h1>
<form action="contabilidad.php?content=gestionCh" method="post">
<fieldset>
    <legend>Filtrar resultados por</legend>
  	<label for="banco">Banco:
        <select name="banco" id="banco">
        	<option <?php if(!isset($data['banco'])) echo "SELECTED";?> value="">Ninguno</option>
            <option <?php if(isset($data['banco']) && $data['banco']==1) echo "SELECTED";?> value="1">Afirme/Bajio</option>
            <option <?php if(isset($data['banco']) && $data['banco']==3) echo "SELECTED";?> value="3">BBVA Bancomer</option>
        </select> 
    </label>
    <label for="status">Status:
         <select name="status" id="status">
            <option <?php if(isset($data['status']) && $data['status']=="") echo "SELECTED";?> value="">Ninguno</option>
            <option <?php if(isset($data['status']) && $data['status']=="0") echo "SELECTED";?> value="0">Elaborado</option>
            <option <?php if(isset($data['status']) && $data['status']=="1") echo "SELECTED";?> value="1">Cobrado</option>
            <option <?php if(isset($data['status']) && $data['status']=="2") echo "SELECTED";?> value="2">Rechazado</option>
         </select>
    </label>
    <label for="fecha">Entre fechas:
         <input class="input-text" type="text" name="fecha1" id="fecha1" value="<?php if(isset($data['fecha1']) && $data['fecha1']!="") echo $data['fecha1'];?>" size="15" /> y
         <input class="input-text" type="text" name="fecha2" id="fecha2" value="<?php if(isset($data['fecha2']) && $data['fecha2']!="") echo $data['fecha2'];?>" size="15" />
    </label>
   <input type="submit" class="input-submit" name="update" value="Filtrar" />
   <input type="reset" class="input-submit" name="limpiar" id="cleanResults" value="Limpiar" />
</fieldset>
<fieldset>
	<legend>Resultados</legend>
	<div id="gestionCh"><?php include('listaCheques.tpl.php');?></div>
</fieldset>
</form>