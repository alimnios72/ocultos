<?php
$imgPath = RUTA_IMG.'logo.jpg';

//Ponemos el header en todas las p�ginas
$header = $pdf->openObject();
$pdf->addText(40,770,12, "<b>POLIZA DE CHEQUE</b>");
$pdf->closeObject();
$pdf->addObject($header, "all");
//A�adimos la imagen en todas las p�ginas
$img = $pdf->openObject();
$pdf->addJpegFromFile($imgPath,246,715,120);
$pdf->closeObject();
$pdf->addObject($img, "all");
//Cuerpo de la p�liza
$body = $pdf->openObject();
$pdf->addText(40,700,9, "Paguese por este");
$pdf->addText(40,690,9, "cheque a la orden de:");
$pdf->addText(480,685,8, "Moneda Nacional");
$pdf->addText(260,620,9, "<b>BANCO:</b>");
$pdf->addText(450,620,8, "<b>CH No.</b>");
$pdf->addText(40,600,10, "<b>CONCEPTO DE PAGO:</b>");
$pdf->closeObject();
$pdf->addObject($body, "all");
//Firma y fecha de recibo
$bottom = $pdf->openObject();
$pdf->addText(400,315,11, "<b>FIRMA</b>");
$pdf->line(450,315,550,315);
$pdf->addText(400,295,11, "<b>FECHA</b>");
$pdf->line(450,295,550,295);
$pdf->closeObject();
$pdf->addObject($bottom, "all");
//Para la tabla del final
$table = $pdf->openObject();
$pdf->line(40,280,550,280);
$pdf->addText(43,270,10,"<b>CUENTA</b>");
$pdf->addText(95,270,10,"<b>SUBCUENTA</b>");
$pdf->addText(250,270,10,"<b>NOMBRE</b>");
$pdf->addText(390,270,10,"<b>PARCIAL</b>");
$pdf->addText(450,270,10,"<b>DEBE</b>");
$pdf->addText(500,270,10,"<b>HABER</b>");
for($i=0;$i<12;$i++)
    $pdf->line(40,265-($i*15),550,265-($i*15));
$pdf->line(40,280,40,100);
$pdf->line(93,280,93,100);
$pdf->line(165,280,165,100);
$pdf->line(385,280,385,100);
$pdf->line(445,280,445,100);
$pdf->line(495,280,495,100);
$pdf->line(550,280,550,100);
$pdf->closeObject();
$pdf->addObject($table, "all");

$last_key = end(array_keys($cheques));
foreach($cheques as $key => $cheque){
	$pol = $pdf->openObject();
	$pdf->addText(415,718,11, "FECHA: ".strftime("%d %B %Y",strtotime($cheque['fecha_entrega'])));
	$pdf->addText(450,760,12, "<b>".$ctas[$cheque['id_banco']]."</b>");
	$pdf->addText(450,750,10, "<b>No. ".$cheque['no_cuenta']."</b>");
	$pdf->addText(140,695,11, "<b>".mb_strtoupper($cheque['acreditado'],'iso-8859-1')."</b>");
	$pdf->addText(480,695,11, "<b>$".number_format($cheque['montoCH'],2)."</b>");
	$pdf->addText(120,660,11, "<b>".numberToText($cheque['montoCH'],true)."</b>");
	$pdf->addText(300,620,9,"<b>".$bancos[$cheque['id_banco']]."</b>");
	$pdf->addText(485,620,8,"<b>".$cheque['no_cheque']."</b>");
	$gpo = mb_strtoupper($cheque['nombreGpo'],'iso-8859-1');
	$per =  mb_strtoupper($cheque['periodo'],'iso-8859-1');
	$zona = mb_strtoupper($cheque['nombre_zona'],'iso-8859-1');
	$pdf->addText(40,580,10, "PRESTAMO {$tipo[$cheque['tipo']]} {$gpo} ({$zona}) {$cheque['tasa_interes']}% {$cheque['plazo']} MESES/{$per}");
	$pdf->closeObject();
	$pdf->addObject($pol, "add");
	if ($key != $last_key)
		$pdf->ezNewPage();
}
?>