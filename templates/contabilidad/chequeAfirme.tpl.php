<?php
$x = 55;
$y = 580;
$dy = 160;

$first = true;
$j = 1;
$i = 1;

foreach($cheques as $cheque){
	$nombre = mb_strtoupper($cheque['acreditado'], 'iso-8859-1');
	$monto = "$".number_format($cheque['montoCH'], 2);
	$montoText = numberToText($cheque['montoCH'], true);
	$fecha = strftime("%d de %B de %Y",strtotime($cheque['fecha_entrega']));
	printCheque($pdf, $x,$y, $fecha, $nombre, $monto, $montoText);
	$y -= $dy;
	if($first && $j == $data['noCh']){
		$first = false;
		$pdf->ezNewPage();
		$y = 580;
		$j = 1;
	}
	elseif($j == $maxCheque){
		if(($i) < count($cheques))
			$pdf->ezNewPage();
		$y = 580;
		$j = 1;
	}
	else
		$j++;
	$i++;
}

function printCheque(&$pdf, $x, $y, $f, $n, $m, $mT){
    $cheque = $pdf->openObject();
    $pdf->addText($x+220,$y, 9, "<b>$f</b>");
    $y -= 30;
    $pdf->addText($x,$y,9, "<b>$n</b>");
    $pdf->addText($x+260,$y,9, "<b>$m</b>.");
    $y -= 25;
    $pdf->addText($x,$y,9, "<b>$mT</b>");
    $pdf->closeObject();
    $pdf->addObject($cheque, "add");
}

?>