<h1>Impresi�n de cheques</h1>
<fieldset>
    <legend>Cr�ditos aprobados</legend>
    <table>
        <tbody>
            <tr>
                <th></th>
                <th>Producto</th>
                <th>Tipo</th>
                <th>Nombre/Grupo</th>
                <th>Status</th>
                <th>Fecha de aprobaci�n</th>
                <th>Vendedor</th>
                <th>Zona</th>
                <th>Acci�n</th>
            </tr>
            <?php $c = true; ?>
            <?php if(!empty($creditos)){ ?>
            <?php foreach($creditos as $credito){ ?>
                <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                      <td><input type="radio" name="idC" value="<?php echo $credito['id_credito']; ?>" />
                    <td><?php echo $producto[$credito['producto']]; ?></td>
                    <td><?php echo $tipo[$credito['tipo']]; ?></td>
                     <td><?php echo $credito['nombreGrupo']; ?></td>
                    <td><?php echo $status[$credito['status']]; ?></td>
                    <td><?php echo strftime('%a %e de %h de %Y', strtotime($credito['fecha_aprobacion'])); ?></td>
                    <td><?php echo $credito['nombre']; ?></td>
                    <td><?php echo $credito['nombre_zona']; ?></td>
                    <td>
                        <a id="idC_<?php echo $credito['id_credito']; ?>" href="#detalles">Ver</a>
                    </td>
                </tr>
            <?php } }?>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>Disposici�n de cheques</legend>
    <div id="disposciones"></div>
</fieldset>
<form action="contabilidad.php?content=impresionCh" method="post">
<fieldset>
    <legend>Impresi�n</legend>
        <label for="banco">Banco:
            <select id="bancoImp" name="bancoImp">
                <option value="1">Afirme/Bajio</option>
                <option value="3">BBVA Bancomer</option>
            </select>
        </label>
        <label for="noCheques">Cheques en hoja inicial:
        	<select name="noCheques" id="noCheques">
        		<option value="1">1</option>
        		<option value="2">2</option>
        		<option value="3">3</option>
        	</select>
        </label>
         <label for="impresion">Imprimir del cheque 
         	<input type="text" class="input-text" name="chInicial" id="chInicial" size="2" />
         	al
         	<input type="text" class="input-text" name="chFin" id="chFin" size="2" />
        </label>
        <label for="cheques">Cheques:
        	<a href="#" name="ch" class="printChPol">Imprimir</a>
        </label>
        <label for="polizas">P�lizas
        	<a href="#" name="po" class="printChPol">Imprimir</a>
        </label>
</fieldset>
</form>