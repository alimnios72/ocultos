<a name="resultados"></a>
<table>
    <tbody>
        <tr>
        	<th>No.</th>
            <th>Banco</th>
            <th>Descripción</th>
            <th>Monto</th>
            <th>Status</th>
            <th>Fecha elaboración</th>
            <th>Fecha cobro</th>
            <th></th>
        </tr>
        <?php $c = true; ?>
        <?php if(!empty($cheques)){ ?>
        <?php foreach($cheques as $cheque){ ?>
            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
            	<td><input type="text" style="width:40px;" name="noCheque" id="noCheque_<?php echo $cheque['id_disposicion'];?>" value="<?php echo $cheque['no_cheque']; ?>" /></td>
                <td><?php echo $cheque['banco']; ?></td>
                <td><?php if($cheque['statusCH']==2){ echo "<span>{$cheque['observaciones']}</span>"; }else{
	                	if($cheque['tipo']==0) echo "<b>{$cheque['acreditado']}</b> ({$tipoCred[$cheque['tipo']]})";
	                	else echo "<b>{$cheque['acreditado']}</b> (Grupo {$cheque['nombreGpo']})"; } ?>
                </td>
                <td><?php echo "$".number_format($cheque['montoCH'],2); ?></td>
                <td><?php if($cheque['statusCH']==2){ echo "<span>Cancelado</span>"; }else{?>
                	<select name="changeStatus" id="changeStatus_<?php echo $cheque['id_disposicion'];?>">
			            <option <?php if($cheque['statusCH']==0) echo "SELECTED";?> value="0">Elaborado</option>
			            <option <?php if($cheque['statusCH']==1) echo "SELECTED";?> value="1">Cobrado</option>
         			</select>
         			<?php }?>
                </td>
                <td><?php if($cheque['fecha_elaboracion'] != NULL) echo strftime("%d %B %Y", strtotime($cheque['fecha_elaboracion'])); ?></td>
                <td><input type="text" class="fechaCobro" id="fechaCobro_<?php echo $cheque['id_disposicion'];?>" value="<?php if($cheque['fecha_cobro'] != NULL) echo strftime("%d %B %Y", strtotime($cheque['fecha_cobro'])); ?>" size="10" /></td>
                <td><a href="#resultados" id="save_<?php echo $cheque['id_disposicion'];?>" ><img src="<?php echo RUTA_IMG;?>save.png"/></a></td>
            </tr>
        <?php } }?>
        <tr class="bg-red">
        	<td><input type="text" name="cancelNoCh" id="cancelNoCh" size="3" /></td>
        	<td>
        		<select name="cancelBanco" id="cancelBanco">
		            <option value="1">Afirme/Bajio</option>
		            <option value="3">BBVA Bancomer</option>
        		</select> 
        	</td>
        	<td><input type="text" name="cancelMotivo" id="cancelMotivo" value="Motivo de cancelación" size="40" /></td>
        	<td>$0.00</td>
        	<td><b>Cancelado</b></td>
        	<td><?php  echo strftime("%d %B %Y", strtotime('today')); ?></td>
        	<td><?php  echo strftime("%d %B %Y", strtotime('today')); ?></td>
        	<td><a href="#resultados" id="cancelCheque"><img src="<?php echo RUTA_IMG;?>save.png"/></a></td>
        </tr>
    </tbody>
</table>