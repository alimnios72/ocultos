<h1>Detalles del dep�sito</h1>
<fieldset>
	<?php if($deposito != NULL && $deposito != ""){ ?>
		<p><b>Expediente:</b> <?php echo $deposito['expediente'];?></p>
		<p><b>Nombre/grupo:</b> <?php echo $deposito['nombreGrupo'];?></p>
		<p><b>Monto depositado:</b> <?php echo "$".number_format($deposito['monto'],2);?></p>
		<p><b>Inter�s:</b> <?php echo "$".number_format(-$deposito['interes'],2);?></p>
		<p><b>I.V.A.:</b> <?php echo "$".number_format(-$deposito['iva'],2);?></p>
		<p><b>Capital:</b> <?php echo "$".number_format(-$deposito['capital'],2);?></p>
	<?php } else{?>
		<p class="msg warning"><?php echo "Error al cargar informaci�n del dep�sito."?></p>
	<?php }?>
</fieldset>