<a name="lista"></a>
<h3><?php if($date != null) echo strtoupper(strftime("%B %Y", strtotime($date)));?></h3>
<input type="hidden" id="date" name="date" value="<?php echo $date; ?>" />
<table>
    <tbody>
        <tr>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Cuenta</th>
            <th>Conciliado</th>
            <th>Referencia</th>
            <th>Folio</th>
            <th>Observaciones</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
			<td class="footer"><a href="#lista" name="<?php echo $date;?>" class="pagination" id="prev" >Mes anterior</a></td>
			<td class="footer" colspan="5">&nbsp;</td>
			<td class="footer"><a href="#lista" name="<?php echo $date;?>" class="pagination" id="next" >Mes siguiente</a></td>
			<td colspan="2"></td>
		</tr>
        <?php $c = true; ?>
        <?php if(!empty($depositos)){ ?>
        <?php foreach($depositos as $deposito){ ?>
            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                <td><?php echo strftime("%A %d %B %Y", strtotime($deposito['fecha_deposito'])); ?></td>
                <td><?php echo "$".number_format($deposito['monto'],2); ?></td> 
                <td><?php echo $deposito['nombre_cuenta']; ?></td>
                <td><b><?php echo intval($deposito['aplicado']) == 1 ? "<a href='contabilidad.php?content=deposito&id_deposito={$deposito['id_deposito']}' target='_blank'>{$conciliado[$deposito['aplicado']]}</a>": $conciliado[$deposito['aplicado']]; ?></b></td>
                <td><?php echo $deposito['clave_deposito']; ?></td>
                <td><?php echo $deposito['folio']; ?></td>
                <td><?php echo $deposito['observaciones']; ?></td>
                <td><a href="contabilidad.php?content=depositos&idDep=<?php echo $deposito["id_deposito"]; ?>"><img src="<?php echo RUTA_IMG;?>editar.png" title="Modificar"/></a></td>
                <td><a href="#" id="deleteDep_<?php echo $deposito["id_deposito"]; ?>"><img src="<?php echo RUTA_IMG;?>ico-delete.gif" title="Borrar"/></a></td>
            </tr>
        <?php } }?>
        <tr>
			<td class="footer"><a href="#lista" name="<?php echo $date;?>" class="pagination" id="prev" >Mes anterior</a></td>
			<td class="footer" colspan="5">&nbsp;</td>
			<td class="footer"><a href="#lista" name="<?php echo $date;?>" class="pagination" id="next" >Mes siguiente</a></td>
			<td colspan="2"></td>
		</tr>
    </tbody>
</table>