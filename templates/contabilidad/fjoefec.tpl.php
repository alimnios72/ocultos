<h1>Flujo de efectivo</h1>
<form action="contabilidad.php?content=fjoefec" method="post">
<fieldset>
<legend>Filtro</legend>
<label for="fechaIni">Fecha Inicial:
	<input type="text" id="fecha1" class="input-text" name="fecha1"/>
</label>
<label for="fechaFin">Fecha Final:
	<input type="text" id="fecha2" class="input-text" name="fecha2"/>
</label>
<input type="submit" class="input-submit" name='filtro' value="Filtrar" />
</fieldset>
</form>
<fieldset>
<legend>Movimientos</legend>
<table>
    <tbody>
    <tr>
        <th>Fecha</th>
        <th>Cuenta</th>
        <th>Descripci�n</th>
        <th>Monto</th>
        <th>Saldo</th>
        <th>Acci�n</th>
    </tr>
    <?php $c = true; 
    $saldo = is_null($saldoIni) ? 0 : $saldoIni;
    ?>
    <?php if(is_array($movimientos) && !empty($movimientos)){
    	foreach($movimientos as $mov) { 
    		$saldo += $mov["cantidad"]; ?>
    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
        <td><?php echo ucwords(strftime("%A %d %B %Y", strtotime($mov["fecha"]))); ?></td>
        <td><?php echo $mov['nombre_cuenta']; ?></td>
        <td><?php echo $mov['descripcion']; ?></td>
        <td><?php echo "$".number_format($mov["cantidad"], 2); ?></td>
        <td><?php echo "$".number_format($saldo, 2); ?></td>
        <td><img src="img/ico-delete.gif" title="Borrar"></td>
    </tr>
    <?php } }?>
    <tr>
    	<td><a href="contabilidad.php?content=fjoefec&mes=<?php echo $criterio['mes']-1; ?>">Mes anterior</a></td>
    	<td colspan=3></td>
    	<td colspan=2><a href="contabilidad.php?content=fjoefec&mes=<?php echo $criterio['mes']+1; ?>">Mes siguiente</a></td>
    </tr>
</table>
</fieldset>