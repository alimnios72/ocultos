<h1>Egresos</h1>
<form action="contabilidad.php?content=egresos" method="post">
<fieldset>
<legend>Cuenta nueva</legend>
	<label for="nombre_cuenta">Nombre de la cuenta:
		<input type="text" class="input-text" name="nombre_cuenta" />
	</label>
	<label for="descripcion">Descripción de la cuenta:
		<input type="text" class="input-text" name="descripcion_cuenta" />
	</label>
	<input type="submit" class="input-submit" name="cuentaNueva" value="Agregar cuenta" />
</fieldset>
<fieldset>
	<legend>Nuevo egreso</legend>
	<input type="hidden" name="userID" value="<?php echo $_SESSION['userID']; ?>" />
	<div class="col50">
	<label for="id_cuenta">Cuenta:
		<?php getComboBox($cuentas, 'id_cuenta', 'id_cuenta', array('value'=>'id_cuenta','text'=>'nombre_cuenta'));?>
	</label>
	<label for="fecha">Fecha:
		<input type="text" class="input-text" name="fecha" id="fecha" />
	</label>
	</div>
	<div class="col50 f-right">
	<label for="descripcion">Descripción:
		<input type="text" class="input-text" name="descripcion" />
	</label>
	<label for="cantidad">Cantidad:
		$<input type="text" class="input-text" name="monto" />
	</label>
	</div>
	<input type="submit" class="input-submit" name="egresoNuevo" value="Agregar egreso" />
</fieldset>
</form>
<fieldset>
	<legend>Cuentas existentes</legend>
		<?php if(is_array($cuentas) && !empty($cuentas)){
			foreach($cuentas as $cuenta){?>
		<p><?php echo $cuenta['nombre_cuenta'];?></p>
		<?php }}?>
</fieldset>