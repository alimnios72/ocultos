<h1>Dep�sitos bancarios</h1>
<?php if(isset($errormsg)){?>
<p class="msg warning"><?php echo $errormsg; ?></p>
<?php } ?>
<?php if(isset($successmsg)){?>
<p class="msg done"><?php echo $successmsg; ?></p>
<?php } ?>
<form action="contabilidad.php?content=depositos" method="post">
<?php if(isset($deposito) && !empty($deposito))
		$title = "Editar";
	  else 
	  	$title = "Nuevo";
?>
<input type="hidden" name="updateDep" value="<?php if(isset($deposito) && !empty($deposito)) echo 1;?>" />
<input type="hidden" name="id_deposito" value="<?php if(isset($deposito) && !empty($deposito)) echo $deposito['id_deposito'];?>" />
<fieldset>
    <legend><?php echo $title;?> dep�sito</legend>
    <div class="col50">
    	<input type="hidden" name="banco" value="1" />
        <!--<label for="banco">Banco:
        <select name="banco" id="banco">
            <option value="1" <?php if($deposito['id_banco']==1) echo "SELECTED";?>>Efectivo</option>
        </select> 
        </label>-->
        <label for="tipo_cuenta">Cuenta del deposito:
        <?php getComboBox($cuentas, 'tipo_cuenta', 'tipo_cuenta', array('value'=>'id_cuenta','text'=>'nombre_cuenta'), $deposito['id_cuenta']); ?> 
        </label>
        <label for="monto">Monto:
            $<input class="input-text" type="text" name="monto" id="monto" size="10" value="<?php if($deposito['monto']!='') echo $deposito['monto'];?>"/>
        </label>
        <label for="fecha">Fecha:
            <input class="input-text" type="text" name="fecha" id="fecha" size="15" value="<?php if($deposito['fecha_deposito']!='') echo $deposito['fecha_deposito'];?>"/>
        </label>
    </div>
    <div class="col50 f-right">
    	<label for="folio">Folio:
            <input class="input-text" type="text" name="folio" id="folio" size="10" value="<?php if($deposito['folio']!='') echo $deposito['folio'];?>"/>
        </label>
        <label for="clave">Referencia:
            <input class="input-text" type="text" name="clave" id="clave" size="5" value="<?php if($deposito['clave_deposito']!='') echo $deposito['clave_deposito'];?>"/>
        </label>
        <label for="observaciones">Observaciones:
            <input class="input-text" type="text" name="observaciones" id="observaciones" size="30" value="<?php if($deposito['observaciones']!='') echo $deposito['observaciones'];?>"/>
        </label>
    </div>
   
</fieldset>
 <input class="input-submit" type="submit" name="data" value="Guardar">
 </form>
<form action="contabilidad.php?content=depositos" method="post">
 <fieldset>
 	<legend>Filtros</legend>
 	<label for="fechas">Entre fechas: 
 		<input type="text" class="input-text" name="fecha1" id="fecha1" value="<?php if($data['fecha1']!="") echo $data['fecha1'];?>"/> y 
 		<input type="text" class="input-text" name="fecha2" id="fecha2" value="<?php if($data['fecha2']!="") echo $data['fecha2'];?>"/>
 	</label>
 	<label for="montos">Entre montos: 
 		<input type="text" class="input-text" name="monto1" value="<?php if($data['monto1']!="") echo $data['monto1'];?>"/> y 
 		<input type="text" class="input-text" name="monto2" value="<?php if($data['monto2']!="") echo $data['monto2'];?>"/>
 	</label>
 	<label for="referencia">Referencia contiene: 
 		<input type="text" class="input-text" name="referencia" value="<?php if($data['referencia']!="") echo $data['referencia'];?>"/>
 	</label>
    <label for="folio">Folio:
         <input type="text" class="input-text" name="folio" value="<?php if($data['folio']!="") echo $data['folio'];?>"/>
    </label>
 </fieldset>
 <input type="submit" class="input-submit" name="filtrar" value="Aplicar" />
 <input type="submit" class="input-submit" name="limpiar" value="Limpiar" />
 </form>
<fieldset>
    <legend>Lista de d�positos</legend>
	<div id="listaDep"><?php include('listaDepositos.tpl.php');?></div>
</fieldset>
