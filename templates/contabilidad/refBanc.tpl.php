<h1>Referencia Bancaria</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == 1){?>
	<p class="msg done"><?php echo $msg[1]; ?></p>
<?php } elseif(isset($_GET['msg']) && $_GET['msg'] > 1){?>
	<p class="msg warning"><?php echo $msg[$_GET['msg']]; ?></p>
<?php } ?>
<form action="contabilidad.php?content=refBanc" method="post" enctype="multipart/form-data">
<fieldset>
    <legend>Subir archivo</legend>
        <label for="archivo">Archivo:
        	<input type="hidden" name="MAX_FILE_SIZE" value="2048576" />
            <input type="file" name="archivo" id="archivo" />
        </label>
</fieldset>
<input type="hidden" name="id_cuenta" value=6 />
<input class="input-submit" type="submit" name="upload" value="Subir archivo" />	
</form>
<fieldset>
	<legend>Archivos subidos</legend>
	<?php if(!empty($subidos)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Banco</th>
	        <th>Subido por</th>
	        <th>Fecha Inicial</th>
	        <th>Fecha Final</th>
	        <th>Pagos conciliados</th>
	        <th>Subido el</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($subidos as $val) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $val["banco"]; ?></td>
	        <td><?php echo $val["nombre"]; ?></td>
	        <td><?php echo strftime("%d %B %Y", strtotime($val["fecha_inicial"])); ?></td>
	        <td><?php echo strftime("%d %B %Y", strtotime($val["fecha_final"])); ?></td>
	        <td><?php echo $val["pagos_conciliados"]; ?></td>
	        <td><?php echo strftime("%d %B %Y %H:%M", $val["unixtime"]); ?></td>
	    </tr>
	    <?php } ?>
		</table>
	<?php } ?>
</fieldset>