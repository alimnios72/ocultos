<?php if(!empty($acreditados)){?>
<h3>Acreditado(s)</h3>
<?php foreach($acreditados as $acred){?>
	<div class="col50"><p><a href="administracion.php?content=datosPersona&persona=<?php echo $acred['id_persona'];?>" target="_blank"><b><?php echo $acred['acreditado'];?></b></a>
	<?php if(isset($representante) && $representante['id_representante'] == $acred['id_persona']) echo "<span style='color:red;'> (Representante)</span>"?></p></div>
	<div class="col50 f-right"><p>Monto: <b><?php echo "$".number_format($acred['cantidad'],2);?></b></p></div>
<?php } }?>
<p><b>Mora por d�a: </b><?php echo "$".number_format($moraDia, 2); ?></p>

<h2>Cr�dito</h2>
<?php if(!empty($balance)){?>
<h3>Balance al d�a de hoy</h3>
	<table>
    	<tbody>
    	<tr>
    		<th>Inter�s ordinario</th>
    		<th>IVA</th>
    		<th>Capital</th>
    		<th>Total</th>
    	</tr>
    	<tr>
    		<td class="center"><b><?php echo "$".number_format($balance['interes'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['iva'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['capital'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['monto'],2);?></b></td>
    	</tr>
    	</tbody>
    </table>
<?php }?>

<?php if(empty($tabla)) {?>
     <p class="msg warning">No hay tabla de amortizaci�n para este cr�dito.</p>
    <?php }else{ ?>
    <h3>Tabla de amortizaci�n</h3>
    <table>
	    <tbody>
	    <tr>
	    	<th>Num</th>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Capital</th>
	        <th>Fecha</th>
	        <th>Status</th>
	    </tr>
	    <?php $c = true; $i = 0;?>
	    <?php foreach($tabla as $am) { $i++;?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<td><?php echo $i	; ?></td>
	        <td><?php echo "$".number_format($am["monto"],2); ?></td>
	        <td><?php echo "$".number_format($am["interes"],2); ?></td>
	        <td><?php echo "$".number_format($am["iva"],2); ?></td>
	        <td><?php echo "$".number_format($am["capital"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($am["fecha_mov"])); ?></td>
	        <td><b><?php echo $cargoStatus[$am['status']]; ?></b></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
	<?php } ?>
	
<?php if(!empty($pagos)) {?>
    <h3>Pagos realizados</h3>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Capital</th>
	        <th>Fecha</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($pagos as $pago) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format(-$pago["monto"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["interes"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["iva"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["capital"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($pago["fecha_mov"])); ?></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
<?php } ?>

	<h2>Intereses moratorios</h2>
	<?php if(!empty($balanceMora)){?>
		<h3>Balance al d�a de hoy</h3>
			<table>
    		<tbody>
    			<tr>
    				<th>Inter�s moratorio</th>
    				<th>IVA</th>
    				<th>Total</th>
    			</tr>
    			<tr>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['interes'],2);?></b></td>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['iva'],2);?></b></td>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['monto'],2);?></b></td>
    			</tr>
    		</tbody>
   			</table>
	<?php }?>
	
	<h4>Cargos</h4>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Fecha</th>
	    </tr>
<?php if(!empty($moratorios)) {?>
	    <?php $c = true; ?>
	    <?php foreach($moratorios as $mora) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format($mora["monto"],2); ?></td>
	        <td><?php echo "$".number_format($mora["interes"],2); ?></td>
	        <td><?php echo "$".number_format($mora["iva"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($mora["fecha_mov"])); ?></td>
	    </tr>
	    <?php } ?>
	    <?php if($moraEfectiva > 0){?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><b><?php echo "$".number_format($moraHoy["monto"],2); ?></b></td>
	        <td><b><?php echo "$".number_format($moraHoy["interes"],2); ?></b></td>
	        <td><b><?php echo "$".number_format($moraHoy["iva"],2); ?></b></td>
	        <td><b><?php echo $moraHoy["fecha"] ?></b></td>
	    </tr>
	    <?php } ?>
<?php } ?>
		</tbody>
	</table>
	
	<?php if(!empty($pagoMoratorios)) {?>
    <h4>Abonos</h4>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Fecha</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($pagoMoratorios as $pago) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format(-$pago["monto"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["interes"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["iva"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($pago["fecha_mov"])); ?></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
	<?php } ?>
