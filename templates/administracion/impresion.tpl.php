<h1>Impresi�n de cr�ditos aprobados</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == 'ok'){?>
<p class="msg done"><?php echo "Cr�dito activado con �xito."?></p>
<?php } ?>
<fieldset>
    <legend>Citas</legend>
    <div id="weekCalendar"><?php include(	'procesos/citas.php');?></div>
 </fieldset>
<form action="administracion.php?content=impresion" method="post">
<fieldset>
    <legend>Cr�ditos aprobados</legend>
    <table>
        <tbody>
            <tr>
                <th></th>
                <th>Producto</th>
                <th>Tipo</th>
                <th>Nombre/Grupo</th>
                <th>Status</th>
                <th>Fecha de aprobaci�n</th>
                <th>Vendedor</th>
                <th>Zona</th>
                <th>Acci�n</th>
            </tr>
            <?php $c = true; ?>
            <?php if(!empty($creditos)){ ?>
            <?php foreach($creditos as $credito){ ?>
                <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                    <td><input type="radio" name="idC" value="<?php echo $credito['id_credito']; ?>" />
                    <td><?php echo $producto[$credito['producto']]; ?></td>
                    <td><?php echo $tipo[$credito['tipo']]; ?></td>
                     <td><?php echo $credito['nombreGrupo']; ?></td>
                    <td><?php echo $status[$credito['status']]; ?></td>
                    <td><?php echo strftime('%a %e de %h de %Y', strtotime($credito['fecha_aprobacion'])); ?></td>
                    <td><?php echo $credito['nombre']; ?></td>
                    <td><?php echo $credito['nombre_zona']; ?></td>
                    <td>
                        <a id="idC_<?php echo $credito['id_credito']; ?>" href="#detalles">Ver</a>
                    </td>
                </tr>
            <?php } }?>
        </tbody>
    </table>
</fieldset>
<a name="detalles"></a>
<fieldset>
    <legend>Detalles del cr�dito</legend>
    <div id="configCredito"></div>
</fieldset>
<input class="input-submit" type="submit" name="data" value="Activar">
</form>