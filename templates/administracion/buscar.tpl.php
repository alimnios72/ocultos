<h1>Buscar cr�ditos</h1>
<form action="administracion.php?content=buscar" method="post">
<fieldset>
<legend>Criterios de b�squeda</legend>
	<label for="expediente">Buscar por expediente
		<input type="text" class="input-text" name="expediente" id="expediente" size="6" value="<?php if(isset($data['expediente'])) echo $data['expediente']; ?>" />
	</label>
	<label for="acreditado">Buscar por acreditado
		<input type="text" class="input-text" name="buscarPersona" id="buscarPersona" size="30" value="<?php if(isset($data['buscarPersona'])) echo $data['buscarPersona']; ?>" />
		<input type="hidden" id="idPersona" name="idPersona" value="<?php if(isset($data['idPersona'])) echo $data['idPersona']; ?>" />
	</label>
	<label for="grupo">Buscar por nombre del grupo
		<input type="text" class="input-text" name="grupo" id="grupo" size="10" value="<?php if(isset($data['grupo'])) echo $data['grupo']; ?>" />
	</label>
	<input type="submit" class="input-submit" name="buscar" value="Buscar" />
	<input type="button" class="input-submit" name="cleanResults" id="cleanResults" value="Limpiar campos" />
</fieldset>
</form>
<fieldset>
    <legend>Resultados</legend>
    <?php if(empty($creditos)) {?>
     <p class="msg warning">No existen cr�ditos con esos criterios.</p>
    <?php }else{ ?>
    <table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Nombre/Grupo</th>
	        <th>Producto</th>
	        <th>Tipo</th>
	        <th>Status</th>
	        <th>Tasa</th>
	        <th>Periodo</th>
	        <th>Plazo</th>
	        <th>Edo. de cuenta</th>
	        <th>Edo. de cuenta mora</th>
	    </tr>
	    <?php $c = true;?>
	    <?php foreach($creditos as $credito) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><a href="#TablaAmortizacion" id="idCAmt_<?php  echo $credito["id_credito"]; ?>"><?php  echo $credito["expediente"]; ?></a></td>
	        <td><?php echo $credito["nombreGrupo"]; ?></td>
	        <td><?php echo $producto[$credito["producto"]]; ?></td>
	        <td><?php echo $tipo[$credito["tipo"]]; ?></td>
	        <td><?php echo $status[$credito["status"]]; ?></td>
	        <td><?php echo $credito["tasa_interes"].'%' ?></td>
	        <td><?php echo $credito["periodo"] ?></td>
	        <td><?php echo $credito["plazo"].' MESES' ?></td>
	        <td style="text-align:center;"><a href="#" id="edoCta_<?php  echo $credito["id_credito"]; ?>">Imprimir</a></td>
	        <td style="text-align:center;"><a href="#" id="edoCtaMora_<?php  echo $credito["id_credito"]; ?>">Imprimir</a></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
	<?php } ?>
</fieldset>
<a name="TablaAmortizacion"></a>
<fieldset>
    <legend>Detalles del cr�dito</legend>
    <div id="amortCredito"></div>
</fieldset>