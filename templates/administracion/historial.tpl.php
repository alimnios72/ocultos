<?php if(empty($hist)){?>
<p class='msg warning'>No existen cr�ditos asignados para esta persona</p>
<?php }else{?>
	<table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Producto</th>
	        <th>Tipo</th>
	        <th>Status</th>
	        <th>Tasa</th>
	        <th>Plazo</th>
	        <th>Periodo</th>
	        <th>Monto</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($hist as $credito) {?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><a href="administracion.php?content=detallesCred&idCred=<?php echo $credito["id_credito"]; ?>" target="_blank" ><?php echo $credito["expediente"]; ?></a></td>
	        <td><?php echo $producto[$credito["producto"]]; ?></td>
	        <td><?php echo $tipo[$credito["tipo"]]; ?></td>
	        <td><b><?php echo $status[intval($credito["status"])]; ?></b></td>
	        <td><?php echo $credito["tasa_interes"]."%"; ?></td>
	        <td><?php echo $credito["plazo"]." MESES"; ?></td>
	        <td><?php echo $credito["periodo"]; ?></td>
	        <td><?php echo "$".number_format($credito["cantidad"], 2); ?></td>
	    </tr>
	    <?php } ?>
	</table>
<?php }?>