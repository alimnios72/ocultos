<style type="text/css">
.ui-widget-content{
	background: none;
	border: none;
}
.ui-widget-header {
	background: none;
	border: none;
}
.acreditado a{
	color:#0085cc;
	font-weight:bold;
}
.acreditado a:hover{
	color:red;
	text-decoration:underline;
}
</style>
<h1>Detalles del Cr�dito</h1>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Integrantes</a></li>
    <li><a href="#tabs-2">Estado de Cuenta</a></li>
    <li><a href="#tabs-3">Morosidad</a></li>
    <li><a href="#tabs-4">Documentos</a></li>
    <li><a href="#tabs-5">Gesti�n Cobranza</a></li>
  </ul>
  <div id="tabs-1">
    <?php if(!empty($acreditados)){?>
	<h3>Acreditado(s)</h3>
	<?php foreach($acreditados as $acred){?>
		<div class="col50"><p class="acreditado"><a href="administracion.php?content=datosPersona&persona=<?php echo $acred['id_persona'];?>" target="_blank"><?php echo $acred['acreditado'];?></a>
		<?php if(isset($representante) && $representante['id_representante'] == $acred['id_persona']) echo "<span style='color:red;'> (Representante)</span>"?></p></div>
		<div class="col50 f-right"><p>Monto: <b><?php echo "$".number_format($acred['cantidad'],2);?></b></p></div>
	<?php } }?>
	<?php if(isset($solidario) && !empty($solidario)){?>
	<h3>Obligado(s) Solidario(s)</h3>
		<?php foreach($solidario as $val){?>
		<p class="acreditado"><a href="administracion.php?content=datosPersona&persona=<?php echo $val['id_persona'];?>" target="_blank"><b><?php echo $val['nombre']; ?></b></a></p>
	<?php } } ?>
  </div>
  <div id="tabs-2">
<?php if(!empty($balance)){?>
<h3>Balance al d�a de hoy</h3>
	<table>
    	<tbody>
    	<tr>
    		<th>Inter�s ordinario</th>
    		<th>IVA</th>
    		<th>Capital</th>
    		<th>Total</th>
    	</tr>
    	<tr>
    		<td class="center"><b><?php echo "$".number_format($balance['interes'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['iva'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['capital'],2);?></b></td>
    		<td class="center"><b><?php echo "$".number_format($balance['monto'],2);?></b></td>
    	</tr>
    	</tbody>
    </table>
<?php }?>

<?php if(empty($tabla)) {?>
     <p class="msg warning">No hay tabla de amortizaci�n para este cr�dito.</p>
    <?php }else{ ?>
    <h3>Tabla de amortizaci�n</h3>
    <table>
	    <tbody>
	    <tr>
	    	<th>Num</th>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Capital</th>
	        <th>Fecha</th>
	        <th>Status</th>
	    </tr>
	    <?php $c = true; $i = 0;?>
	    <?php foreach($tabla as $am) { $i++;?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<td><?php echo $i	; ?></td>
	        <td><?php echo "$".number_format($am["monto"],2); ?></td>
	        <td><?php echo "$".number_format($am["interes"],2); ?></td>
	        <td><?php echo "$".number_format($am["iva"],2); ?></td>
	        <td><?php echo "$".number_format($am["capital"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($am["fecha_mov"])); ?></td>
	        <td><b><?php echo $cargoStatus[$am['status']]; ?></b></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
	<?php } ?>
	
<?php if(!empty($pagos)) {?>
    <h3>Pagos realizados</h3>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Capital</th>
	        <th>Fecha</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($pagos as $pago) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format(-$pago["monto"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["interes"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["iva"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["capital"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($pago["fecha_mov"])); ?></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
<?php } ?>
  </div>
  <div id="tabs-3">
	<?php if(!empty($balanceMora)){?>
		<h3>Balance al d�a de hoy</h3>
			<table>
    		<tbody>
    			<tr>
    				<th>Inter�s moratorio</th>
    				<th>IVA</th>
    				<th>Total</th>
    			</tr>
    			<tr>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['interes'],2);?></b></td>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['iva'],2);?></b></td>
		    		<td class="center"><b><?php echo "$".number_format($balanceMora['monto'],2);?></b></td>
    			</tr>
    		</tbody>
   			</table>
	<?php }?>
	
	<h4>Cargos</h4>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Fecha</th>
	    </tr>
<?php if(!empty($moratorios)) {?>
	    <?php $c = true; ?>
	    <?php foreach($moratorios as $mora) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format($mora["monto"],2); ?></td>
	        <td><?php echo "$".number_format($mora["interes"],2); ?></td>
	        <td><?php echo "$".number_format($mora["iva"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($mora["fecha_mov"])); ?></td>
	    </tr>
	    <?php } ?>
	    <?php if($moraEfectiva > 0){?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><b><?php echo "$".number_format($moraHoy["monto"],2); ?></b></td>
	        <td><b><?php echo "$".number_format($moraHoy["interes"],2); ?></b></td>
	        <td><b><?php echo "$".number_format($moraHoy["iva"],2); ?></b></td>
	        <td><b><?php echo $moraHoy["fecha"] ?></b></td>
	    </tr>
	    <?php } ?>
<?php } ?>
		</tbody>
	</table>
	
	<?php if(!empty($pagoMoratorios)) {?>
    <h4>Abonos</h4>
    <table>
	    <tbody>
	    <tr>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Fecha</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($pagoMoratorios as $pago) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo "$".number_format(-$pago["monto"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["interes"],2); ?></td>
	        <td><?php echo "$".number_format(-$pago["iva"],2); ?></td>
	        <td><?php echo strftime("%A %e de %B de %Y", strtotime($pago["fecha_mov"])); ?></td>
	    </tr>
	    </tbody>
	    <?php } ?>
	</table>
	<?php } ?>
  </div>
  <div id="tabs-4">
  	<p><a href="#void" id="edoCta_<?php echo $credito['id_credito'];?>"><img src="<?php echo RUTA_IMG."page_white_acrobat.png";?>" /> Estado de Cuenta</a></p>
  	<p><a href="#void" id="edoCtaMora_<?php echo $credito['id_credito'];?>"><img src="<?php echo RUTA_IMG."page_white_acrobat.png";?>" /> Estado de Cuenta de mora</a></p>
  </div>
  <div id="tabs-5">
  	<?php if(!empty($gestiones)){
	  	foreach($gestiones as $gestion){
		$fecha = strftime("%a %e de %b de %Y",strtotime($gestion['fecha_gestion']));?>
	  	<div style="border:1px solid #303030; padding-left:20px; margin-bottom:5px;">
			<h4><?php echo $gestion['gestionado']; ?></h4>
			<p><?php echo $fecha; ?> - <b><?php echo $tipoGestion[$gestion['tipo_gestion']]; ?></b></p>
			<p><?php echo $gestion['descripcion']; ?></p>
			<p><b>Realizado por:</b> <?php echo $gestion['usuario']; ?></p>
		</div>
	<?php } }else{ ?>
		<p class="msg warning">No existen gestiones para este cr�dito.</p>
  	<?php } ?>
  </div>
  </div>
