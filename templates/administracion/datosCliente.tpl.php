<div class="col50">
        <label for="nombres">Nombres
            <input class="input-text" type="text" name="nombres" id="nombres" value="<?php echo $datos['nombres'];?>" size="40" READONLY/>
        </label>
        <label for="apellidoP">Apellido Paterno
            <input class="input-text" type="text" name="apellidoP" id="apellidoP" value="<?php echo $datos['apellido_paterno'];?>" size="30" READONLY/>
        </label>
        <label for="apellidoM">Apellido Materno
            <input class="input-text" type="text" name="apellidoM" id="apellidoM" value="<?php echo $datos['apellido_materno'];?>" size="30" READONLY/>
        </label>
        <label for="nacimiento">Fecha de nacimiento
            <input class="input-text" type="text" name="nacimiento" id="nacimiento" value="<?php echo strftime("%d de %B de %Y", strtotime($datos['fecha_nacimiento']));?>" size="20" />
        </label>
         <label for="sexo">Sexo
        <?php if($datos['sexo'] == 'H') $hombre='CHECKED'; else $mujer='CHECKED';?>
            <input type="radio" name="hombre" id="hombre" value="H" <?php echo isset($hombre)?$hombre:'';?> READONLY />Hombre
            <input type="radio" name="mujer" id="mujer" value="M" <?php echo isset($mujer)?$mujer:'';?>  READONLY />Mujer
        </label>
         <label for="calle">Calle y n�mero
            <input class="input-text" type="text" name="calle" id="calle" value="<?php echo $datos['calle'];?>" size="40" READONLY/>
        </label>
</div>
<div class="col50 f-right">
        <label for="colonia">Colonia
            <input class="input-text" type="text" name="colonia" id="colonia" value="<?php echo $datos['colonia'];?>" size="30" READONLY />
        </label>
        <label for="estado">Estado
             <input class="input-text" type="text" name="estado" id="estado" value="<?php echo $datos['nombre_edo'];?>" size="30" READONLY />
        </label>
        <label for="delegacion">Delegaci�n
            <input class="input-text" type="text" name="delegacion" id="delegacion" value="<?php echo $datos['nombre_del'];?>" size="30" READONLY />
        </label>
        <label for="cp">C�digo Postal
            <input class="input-text" type="text" name="cp" id="cp" value="<?php echo $datos['codigo_postal'];?>" size="15" READONLY />
        </label>
        <label for="tel_casa">Tel�fono de casa
            <input class="input-text" type="text" name="tel_casa" id="tel_casa" value="<?php echo $datos['telefonos'];?>" size="20" READONLY />
        </label>
        <label for="celular">Celular
            <input class="input-text" type="text" name="celular" id="celular" value="<?php echo $datos['celular'];?>" size="20" READONLY/>
        </label>	
</div>
<div class="col50">
	<p><a href="administracion.php?content=datosPersona&persona=<?php echo $datos['idP'];?>">Ver m�s informaci�n</a></p>
</div>