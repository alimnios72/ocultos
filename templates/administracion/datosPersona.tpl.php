<h1>Informaci�n completa del cliente</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == "ok"){?>
	<p class="msg done"><?php echo "Datos guardados con �xito." ?></p>
<?php } ?>
<form action="administracion.php?content=datosPersona&persona=<?php echo $_GET['persona']; ?>" method="post">
<fieldset>
	<legend>Informaci�n personal</legend>
	<div class="col50">
        <label for="nombres">Nombres
            <input class="input-text" type="text" name="nombres" id="nombres" value="<?php echo $datos['nombres'];?>" size="40" <?php echo $readonly;?>/>
        </label>
        <label for="apellidoP">Apellido Paterno
            <input class="input-text" type="text" name="apellidoP" id="apellidoP" value="<?php echo $datos['apellido_paterno'];?>" size="30" <?php echo $readonly;?>/>
        </label>
        <label for="apellidoM">Apellido Materno
            <input class="input-text" type="text" name="apellidoM" id="apellidoM" value="<?php echo $datos['apellido_materno'];?>" size="30" <?php echo $readonly;?>/>
        </label>
        <label for="nacimiento">Fecha de nacimiento
            <input class="input-text" type="text" name="nacimiento" id="nacimiento" value="<?php echo strftime("%d de %B de %Y", strtotime($datos['fecha_nacimiento']));?>" size="20" <?php echo $readonly;?> />
        </label>
        <input type="hidden" name="fecha_nac" value="<?php echo $datos['fecha_nacimiento']; ?>" />
         <label for="sexo">Sexo
        <?php if($datos['sexo'] == 'H') $hombre='CHECKED'; else $mujer='CHECKED';?>
            <input type="radio" name="sexo" id="hombre" value="H" <?php echo isset($hombre)?$hombre:'';?> <?php echo $readonly;?> />Hombre
            <input type="radio" name="sexo" id="mujer" value="M" <?php echo isset($mujer)?$mujer:'';?>  <?php echo $readonly;?> />Mujer
        </label>
	</div>
	<div class="col50 f-right">
	     <label for="rfc">RFC
            <input class="input-text" type="text" name="rfc" id="rfc" value="<?php echo $datos['rfc'];?>" size="30" <?php echo $readonly;?> />
        </label>
          <label for="curp">CURP
            <input class="input-text" type="text" name="curp" id="curp" value="<?php echo $datos['curp'];?>" size="30" <?php echo $readonly;?> />
        </label>
        <label for="edocivil">Estado Civil
            <select name="edocivil" id="edocivil" <?php echo $readonly;?>>
                <option <?php if($datos['estado_civil']== "Soltero") echo 'selected="selected"';?> value="Soltero">Soltero</option>
                <option <?php if($datos['estado_civil']== "Casado") echo 'selected="selected"';?> value="Casado">Casado</option>
                <option <?php if($datos['estado_civil']== "Union Libre") echo 'selected="selected"';?> value="Union Libre" >Uni�n Libre</option>
                <option <?php if($datos['estado_civil']== "Divorciado") echo 'selected="selected"';?> value="Divorciado">Divorciado</option>
                <option <?php if($datos['estado_civil']== "Viudo") echo 'selected="selected"';?> value="Viudo">Viudo</option>
            </select>
        </label>
        <div id="conyuge">
	        <label for="t_conyuge">�Trabaja su conyuge?
	            <input type="radio" name="t_conyuge" id="siC" value="1" <?php if($datos['t_conyuge']==1) echo 'CHECKED';?> <?php echo $readonly;?> />S�
	            <input type="radio" name="t_conyuge" id="noC" value="0" <?php if($datos['t_conyuge']==0) echo 'CHECKED';?> <?php echo $readonly;?> />No
	        </label>
        </div>
        <label for="dep_econom">Dependientes econ�micos
            <select name="dep_econom" id="dep_econom" <?php echo $readonly;?>>
                <option <?php if($datos['dep_econom']== "0") echo 'selected="selected"';?> value="0">0</option>
                <option <?php if($datos['dep_econom']== "1") echo 'selected="selected"';?> value="1">1</option>
                <option <?php if($datos['dep_econom']== "2") echo 'selected="selected"';?> value="2">2</option>
                <option <?php if($datos['dep_econom']== "3") echo 'selected="selected"';?> value="3">3</option>
                <option <?php if($datos['dep_econom']== "4") echo 'selected="selected"';?> value="4">4</option>
                <option <?php if($datos['dep_econom']== "5") echo 'selected="selected"';?> value="5">5</option>
                <option <?php if($datos['dep_econom']== "6") echo 'selected="selected"';?> value="6">6</option>
                <option <?php if($datos['dep_econom']== "7") echo 'selected="selected"';?> value="7">7</option>
                <option <?php if($datos['dep_econom']== "8") echo 'selected="selected"';?> value="8">8</option>
                <option <?php if($datos['dep_econom']== "9") echo 'selected="selected"';?> value="9">9</option>
                <option <?php if($datos['dep_econom']== "10") echo 'selected="selected"';?> value="10">10</option>
            </select>
        </label>
	</div>
</fieldset>
<fieldset>
	<legend>Domicilio</legend>
	<div class="col50">
	  	<label for="calle">Calle
            <input class="input-text" type="text" name="calle" id="calle" value="<?php echo $datos['calle'];?>" size="40" <?php echo $readonly;?>/>
        </label>
         <label for="numeros">
            Ext: <input class="input-text" type="text" name="num_ext"  size="3" value="<?php echo $datos['num_ext'];?>" <?php echo $readonly;?> />
            Int: <input class="input-text" type="text" name="num_int"  size="3" value="<?php echo $datos['num_int'];?>" <?php echo $readonly;?> />
            Dpto: <input class="input-text" type="text" name="dpto"  size="3" value="<?php echo $datos['dpto'];?>" <?php echo $readonly;?> />
            Mza: <input class="input-text" type="text" name="mza"  size="3" value="<?php echo $datos['mza'];?>" <?php echo $readonly;?> />
            Lt: <input class="input-text" type="text" name="lote"  size="3" value="<?php echo $datos['lote'];?>" <?php echo $readonly;?> 	/>
        </label>
        <label for="colonia">Colonia
            <input class="input-text" type="text" name="colonia" id="colonia" value="<?php echo $datos['colonia'];?>" size="30" <?php echo $readonly;?> />
        </label>
        <label for="estado">Estado
             <?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'),$datos['id_estado'], $readonly); ?>
        </label>
     </div>
     <div class="col50 f-right">
    	<div id="deleg">
	        <label for="delegacion">Delegaci�n
	           <?php getComboBox($deleg, 'delegacion', 'delegacion', array('value'=>'id_delegacion','text'=>'nombre_del'),$datos['id_delegacion'],$readonly); ?>
	        </label>
        </div>
        <label for="cp">C�digo Postal
            <input class="input-text" type="text" name="cp" id="cp" value="<?php echo $datos['codigo_postal'];?>" size="15" <?php echo $readonly;?> />
        </label>
        <label for="residencia">Residencia
            <select name="residencia" id="residencia" <?php echo $readonly;?>>
                <option <?php if($datos['residencia']=='Propia') echo 'selected="selected"';?> value="Propia">Propia</option>
                <option <?php if($datos['residencia']=='Rentada') echo 'selected="selected"';?> value="Rentada">Rentada</option>
                <option <?php if($datos['residencia']=='Pagandose') echo 'selected="selected"';?> value="Pagandose">Pag�ndose</option>
                <option <?php if($datos['residencia']=='Prestada') echo 'selected="selected"';?> value="Prestada">Prestada</option>
                <option <?php if($datos['residencia']=='Vive con familiares') echo 'selected="selected"';?> value="Vive con familiares">Vive con familiares</option>
            </select>
        </label>
        <label for="ant_domicilio">Antig�edad en el domiclio
           <input class="input-text" type="text" name="ant_domicilio" id="ant_domicilio" value="<?php echo $datos['ant_domicilio'];?>" size="2" <?php echo $readonly;?>/>a�os
        </label>
     </div>
</fieldset>
<fieldset>
	<legend>Contacto</legend>
	     <label for="tel_casa">Tel�fono de casa
            <input class="input-text" type="text" name="tel_casa" id="tel_casa" value="<?php echo $datos['telefonos'];?>" size="20" <?php echo $readonly;?> />
        </label>
        <label for="celular">Celular
            <input class="input-text" type="text" name="celular" id="celular" value="<?php echo $datos['celular'];?>" size="20" <?php echo $readonly;?>/>
        </label>
        <label for="email">Email
            <input class="input-text" type="text" name="email" id="email" value="<?php echo $datos['email'];?>" size="20" <?php echo $readonly;?> />
        </label>
</fieldset>
<fieldset>
	<legend>Informaci�n adicional</legend>
	<div class="col50">
	    <label for="nivel_estudios">Nivel de estudios
            <select name="nivel_estudios" id="nivel_estudios" <?php echo $readonly;?>>
                <option <?php if($datos['nivel_estudios']=='Ninguno') echo 'selected="selected"';?>  value="Ninguno">Ninguno</option>
                <option <?php if($datos['nivel_estudios']=='Primaria') echo 'selected="selected"';?>  value="Primaria">Primaria</option>
                <option <?php if($datos['nivel_estudios']=='Secundaria') echo 'selected="selected"';?>  value="Secundaria">Secundaria</option>
                <option <?php if($datos['nivel_estudios']=='Preparatoria') echo 'selected="selected"';?>  value="Preparatoria">Preparatoria</option>
                <option <?php if($datos['nivel_estudios']=='Licenciatura') echo 'selected="selected"';?>  value="Licenciatura">Licenciatura</option>
                <option <?php if($datos['nivel_estudios']=='Posgrado') echo 'selected="selected"';?>  value="Posgrado">Posgrado</option>
            </select>
        </label>
        <label for="enfermedad_solic">�Usted tiene alguna enfermedad cr�nico degenerativa?<br />
         	<input type="radio" name="enfermedad_solic" id="siES" value="1" <?php if($datos['enfermedad_solic']==1) echo 'CHECKED';?> <?php echo $readonly;?> />S�
            <input type="radio" name="enfermedad_solic" id="noES" value="0" <?php if($datos['enfermedad_solic']==0) echo 'CHECKED';?> <?php echo $readonly;?> />No
        </label>
    </div>
    <div class="col50 f-right">
        <label for="enfermedad_fam">�Su conyuge o sus dependientes econ�micos tienen alguna enfermedad cr�nico degenerativa?<br />
         	<input type="radio" name="enfermedad_fam" id="siEF" value="1" <?php if($datos['enfermedad_fam']==1) echo 'CHECKED';?> <?php echo $readonly;?> />S�
            <input type="radio" name="enfermedad_fam" id="noEF" value="0" <?php if($datos['enfermedad_fam']==0) echo 'CHECKED';?> <?php echo $readonly;?> />No
        </label>
        <label for="enfermedad_desc">Describa la enfermedad
            <input class="input-text" type="text" name="enfermedad_desc" id="enfermedad_desc" value="<?php echo $datos['enfermedad_desc'];?>" size="20" <?php echo $readonly;?> />
        </label>
     </div>
</fieldset>
<fieldset>
	<legend>Informaci�n del negocio</legend>
	<div class="col50">
		<label for="nombre_negocio">Nombre del negocio, oficio o actividad productiva<br />
	            <input class="input-text" type="text" name="nombre_negocio" id="nombre_negocio" value="<?php echo $datos['nombre_negocio'];?>" size="40" <?php echo $readonly;?> />
	    </label>
	    <label for="calle_negocio">Calle y n�mero
	            <input class="input-text" type="text" name="calle_negocio" id="calle_negocio" value="<?php echo $datos['calle_negocio'];?>" size="40" <?php echo $readonly;?> />
	    </label>
	    <label for="colonia_negocio">Colonia
	            <input class="input-text" type="text" name="colonia_negocio" id="colonia_negocio" value="<?php echo $datos['colonia_negocio'];?>" size="40" <?php echo $readonly;?> />
	    </label>
	    <label for="estado_negocio">Estado
	             <?php getComboBox($estados, 'estado_negocio', 'estado_negocio', array('value'=>'id_estado','text'=>'nombre_edo'),$datos['id_estado_negocio'], $readonly); ?>
	    </label>
	</div>
	<div class="col50 f-right">
		<div id="deleg_neg">
		    <label for="delegacion_negocio">Delegaci�n
		           <?php getComboBox($deleg_neg, 'delegacion_negocio', 'delegacion_negocio', array('value'=>'id_delegacion','text'=>'nombre_del'),$datos['id_delegacion_negocio'], $readonly); ?>
		    </label>
		</div>
	    <label for="cp_negocio">C�digo Postal
	            <input class="input-text" type="text" name="cp_negocio" id="cp_negocio" value="<?php echo $datos['cp_negocio'];?>" size="20" <?php echo $readonly;?> />
	    </label>
	    <label for="telefonos_negocio">Tel�fono(s) del negocio
	            <input class="input-text" type="text" name="telefonos_negocio" id="telefonos_negocio" value="<?php echo $datos['telefonos_negocio'];?>" size="20" <?php echo $readonly;?> />
	    </label>
	    <label for="ant_negocio">Antig�edad en el negocio
	            <input class="input-text" type="text" name="ant_negocio" id="ant_negocio" value="<?php echo $datos['ant_negocio'];?>" size="2" <?php echo $readonly;?> />a�os
	    </label>
	 </div>
</fieldset>
<fieldset>
	<legend>Informaci�n econ�mica</legend>
	<div class="col50">
		 <label for="pag_credito">�Est� pagando alg�n cr�dito?
	         	<input type="radio" name="pag_credito" id="siPC" value="1" <?php if($datos['pag_credito']>0) echo 'CHECKED';?> <?php echo $readonly;?> />S�
	            <input type="radio" name="pag_credito" id="noPC" value="0" <?php if($datos['pag_credito']<=0) echo 'CHECKED';?> <?php echo $readonly;?> />No
	     </label>
	     <div id="pagando_cred">
	     	 <label for="pag_credito_monto">�Cu�nto? (mensual)
	         	 <input class="input-text" type="text" name="pag_credito_monto" id="pag_credito_monto" value="<?php echo $datos['pag_credito'];?>" size="10" <?php echo $readonly;?> />
	     	</label>
	     </div>
	     <label for="gastos_fam">�A cu�nto ascienden sus gastos familiares?
	         	<input class="input-text" type="text" name="gastos_fam" id="gastos_fam" value="<?php echo $datos['gastos_fam'];?>" size="10" <?php echo $readonly;?> />
	     </label>
	      <label for="pension_alim">�Est� pagando alguna pensi�n alimentaria?
	         	<input type="radio" name="pension_alim" id="siPA" value="1" <?php if($datos['pension_alim']>0) echo 'CHECKED';?> <?php echo $readonly;?> />S�
	            <input type="radio" name="pension_alim" id="noPA" value="0" <?php if($datos['pension_alim']<=0) echo 'CHECKED';?> <?php echo $readonly;?> />No
	     </label>
	     <div id="pension_alim">
	     	 <label for="pension_alim_monto">�Cu�nto? (mensual)
	         	 <input class="input-text" type="text" name="pension_alim_monto" id="pag_credito_monto" value="<?php echo $datos['pension_alim'];?>" size="10" <?php echo $readonly;?> />
	     	 </label>
	     </div>
	 </div>
     <div class="col50 f-right">
	     <label for="ing_mensuales">�A cu�nto ascienden sus ingresos mensuales?
	         	<input class="input-text" type="text" name="ing_mensuales" id="ing_mensuales" value="<?php echo $datos['ing_mensuales'];?>" size="10" <?php echo $readonly;?> />
	     </label>
	     <label for="inv_semanal">�A cu�nto asciende la inversi�n semanal en su negocio?
	         	<input class="input-text" type="text" name="inv_semanal" id="inv_semanal" value="<?php echo $datos['inv_semanal'];?>" size="10" <?php echo $readonly;?> />
     	</label>
     </div>
</fieldset>
<?php if(empty($readonly)){?>
	<input type="submit" class="input-submit" name="update" value="Guardar cambios" />
<?php } ?>
</form>