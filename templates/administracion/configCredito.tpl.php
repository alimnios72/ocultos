<?php
$contratos = "doc=contratos&idC={$credito['id_credito']}";
$tablas = "doc=amortizaciones&idC={$credito['id_credito']}"; 
?>
<input type="hidden" name="id_credito" id="id_credito" value="<?php echo $credito['id_credito'];?>" />
	<label for="expediente" style="border:dotted #FF99FF 2px;">Expediente:
		<input type="text" name="expediente" id="expediente" size="8" value="<?php echo $credito['expediente'];?>" />
	</label>
<div class="col50">
	<label for="periodo">Periodo:
		<input type="text" name="periodo" id="periodo" size="8" value="<?php echo $credito['periodo'];?>" READONLY />
	</label>
	<label for="plazo">Plazo:
		<input type="text" name="plazo" id="plazo" size="6" value="<?php echo $credito['plazo']." MESES";?>" READONLY />
	</label>
	<label for="fechaInicio">Fecha de entrega:
		<input type="text" name="fecha_inicio" id="fecha_inicio" value="<?php if(isset($credito['fecha_entrega']) && $credito['fecha_entrega'] != NULL) echo $credito['fecha_entrega'];?>" />
	</label>
	<label for="contratos">Contratos:
		<a href="#detalles" onclick="popitup('procesos/documentacion.php?<?php echo $contratos;?>')">Imprimir</a>
	</label>
</div>
<div class="col50 f-right">
	<label for="diaSemana">D�a de la semana:
	     <input type="checkbox" name="esSemana" id="esSemana" <?php if(isset($credito['esDiaSemana']) && $credito['esDiaSemana'] == 1) echo 'checked';?> <?php if($esDiaMes) echo "DISABLED";?>/>
	     <select name="diaSemana" id="diaSemana" <?php if($esDiaMes) echo "DISABLED";?>>
		 	<option <?php if($credito['diaSemana']== "1") echo 'selected="selected"';?> value="1">Lunes</option>
	        <option <?php if($credito['diaSemana']== "2") echo 'selected="selected"';?> value="2">Martes</option>
	        <option <?php if($credito['diaSemana']== "3") echo 'selected="selected"';?> value="3">Mi�rcoles</option>
	        <option <?php if($credito['diaSemana']== "4") echo 'selected="selected"';?> value="4">Jueves</option>
	        <option <?php if($credito['diaSemana']== "5") echo 'selected="selected"';?> value="5">Viernes</option>
	        <option <?php if($credito['diaSemana']== "6") echo 'selected="selected"';?> value="6">S�bado</option>
	     </select> 
	</label>
	<label for="diaMes">D�a del mes:
	      <input type="checkbox" name="esMes" id="esMes" <?php if(isset($credito['esDiaMes']) && $credito['esDiaMes'] == 1) echo 'checked';?> <?php if(!$esDiaMes) echo "DISABLED";?>/>
	      <select name="diaMes" id="diaMes" <?php if(!$esDiaMes) echo "DISABLED";?>>
	                <?php for($i=1;$i<=31;$i++) {
	                    if($credito['diaMes'] == $i)
	                        echo "<option selected='selected' value='{$i}'>{$i}</option>";
	                    else
	                        echo "<option value='{$i}'>{$i}</option>";
	                } ?>
	      </select> 
	</label>
	<label for="bancoD">Banco a depositar:
		<select name="bancoD" id="bancoD">
			<option <?php if($credito['cta_deposito']== "3") echo 'selected="selected"';?> value="3">BBVA Bancomer</option>
			<option <?php if($credito['cta_deposito']== "1") echo 'selected="selected"';?> value="1">Afirme/Bajio</option>
		</select> 
	</label>
	<label for="amortizaciones">Tablas de amortizaci�n:
		<a href="#detalles" onclick="popitup('procesos/documentacion.php?<?php echo $tablas;?>')">Imprimir</a>
	</label>
</div>
<input type="button" name="Guardar" id="updateCred" value="Guardar" />