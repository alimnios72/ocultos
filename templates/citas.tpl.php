<a name="calendario"></a>
<label for="sucursal">Sucursal:
	<?php echo getComboBox($sucursales, 'sucursal', 'sucursal', array('value'=>'id_sucursal', 'text'=>'nombre_sucursal'), $data['idSucursal'])?>
</label>
<table class="calendar">
<colgroup>
  <col />
  <col id="Sun" />
  <col id="Mon" />
  <col id="Tue" />
  <col id="Wed" />
  <col id="Thu" />
  <col id="Fri" />
  <col id="Sat" />
</colgroup>
<thead>
  <tr>
    <th> </th>
    <th scope="col">Domingo<br /><?php echo strftime("%e %b", strtotime($daysInWeek[0]));?></th>
    <th scope="col">Lunes <br /><?php echo strftime("%e %b", strtotime($daysInWeek[1]));?></th>
    <th scope="col">Martes <br /><?php echo strftime("%e %b", strtotime($daysInWeek[2]));?></th>
    <th scope="col">Mi�rcoles <br /><?php echo strftime("%e %b", strtotime($daysInWeek[3]));?></th>
    <th scope="col">Jueves <br /><?php echo strftime("%e %b", strtotime($daysInWeek[4]));?></th>
    <th scope="col">Viernes <br /><?php echo strftime("%e %b", strtotime($daysInWeek[5]));?></th>
    <th scope="col">S�bado <br /><?php echo strftime("%e %b", strtotime($daysInWeek[6]));?></th>
  </tr>
</thead>
<tbody>
	<?php foreach ($horas as $hora){
		$tmp = explode(':',$hora);
		$h = $tmp[0];
		$m = $tmp[1];
		$domingo="";$lunes="";$martes="";$miercoles="";$jueves="";$viernes="";$sabado="";
	?>
		<tr <?php echo $h%2==0?'':'class="odd"'?>>
			<td class="hour" scope="row"><?php echo $hora;?></td>
			<?php if(isset($citas[$hora])){
						foreach($citas[$hora] as $cita){
							if($cita['fecha'] == $daysInWeek[0])
								$domingo = "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[1])
								$lunes =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[2])
								$martes =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[3])
								$miercoles =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[4])
								$jueves =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[5])
								$viernes =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
							elseif($cita['fecha'] == $daysInWeek[6])
								$sabado =  "{$cita['nombre']}  ({$tipo[$cita['tipo']]})";
						}?>
					<td <?php if($domingo!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[0]}_{$hora}'";?>>
						<?php echo ($domingo!="") ? $domingo : '&nbsp;'?>
					</td>	
					<td <?php if($lunes!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[1]}_{$hora}'";?>>
						<?php echo ($lunes!="") ? $lunes : '&nbsp;'?>
					</td>
					<td <?php if($martes!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[2]}_{$hora}'";?>>
						<?php echo ($martes!="") ? $martes : '&nbsp;'?>
					</td>
					<td <?php if($miercoles!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[3]}_{$hora}'";?>>
						<?php echo ($miercoles!="") ? $miercoles : '&nbsp;'?>
					</td>
					<td <?php if($jueves!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[4]}_{$hora}'";?>>
						<?php echo ($jueves!="") ? $jueves : '&nbsp;'?>
					</td>
					<td <?php if($viernes!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[5]}_{$hora}'";?>>
						<?php echo ($viernes!="") ? $viernes : '&nbsp;'?>
					</td>
					<td <?php if($sabado!="") echo 'class="busy"'; else echo "id='cell_{$daysInWeek[6]}_{$hora}'";?>>
						<?php echo ($sabado!="") ? $sabado : '&nbsp;'?>
					</td>					
						
				<?php } else{?>
					<td id="<?php echo "cell_{$daysInWeek[0]}_{$hora}";?>">&nbsp;</td>	
					<td id="<?php echo "cell_{$daysInWeek[1]}_{$hora}";?>">&nbsp;</td>
					<td id="<?php echo "cell_{$daysInWeek[2]}_{$hora}";?>">&nbsp;</td>
					<td id="<?php echo "cell_{$daysInWeek[3]}_{$hora}";?>">&nbsp;</td>
					<td id="<?php echo "cell_{$daysInWeek[4]}_{$hora}";?>">&nbsp;</td>
					<td id="<?php echo "cell_{$daysInWeek[5]}_{$hora}";?>">&nbsp;</td>
					<td id="<?php echo "cell_{$daysInWeek[6]}_{$hora}";?>">&nbsp;</td>
				<?php } ?>
		</tr>
	<?php } ?>
	<tr>
		<td class="footer"><a href="#calendario" class="pagination" id="<?php echo $prev;?>" >Anterior</a></td>
		<td class="footer" colspan="6">&nbsp;</td>
		<td class="footer"><a href="#calendario" class="pagination" id="<?php echo $next;?>" >Siguiente</a></td>
	</tr>
</tbody>
</table>