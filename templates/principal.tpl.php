<h1>BIENVENIDO A SACREDI</h1>
<?php if($_SESSION['priv'] == 'admin'){?>
<h2>Actualizaciones del sistema</h2>
<ul>
	<li>[06-11-2013] <span class='new'>(Nuevo): </span>Alerta para dep�sitos sospechosos, se configura la cantidad desde el m�dulo Sistema</li>
	<li>[06-11-2013] <span class='new'>(Nuevo): </span>Reporte para buscar personas que hacen dep�sitos mayores a cierta cantidad.</li>
	<li>[26-08-2013] <span class='error'>(Error): </span>El problema al buscar cr�ditos por obligado solidarios en "Ver perfil" qued� resuelto</li>
	<li>[22-08-2013] <span class='error'>(Error): </span>Se solucion� el problema de tablas de amortizaci�n repetidas al cambiar plazo, 
	periodo, tasa de inter�s y monto</li>
	<li>[19-08-2013] <span class='new'>(Nuevo): </span>Nuevos periodos "Semestral" y "Anual" para nuevos cr�ditos y renovaciones</li>
</ul>
<?php } ?>