<h1>Inter�s por cobrar/cobrado por fecha</h1>
<form action="reportes.php?content=totinteres" method="post">
<fieldset>
	<legend>Filtro</legend>
	<label for="fecha1">Fecha inicial
		<input type="text" class="input-text" name="fecha1" id="fecha1" value=""/>
	</label>
	<label for="fecha2">Fecha final
		<input type="text" class="input-text" name="fecha2" id="fecha2" value=""/>
	</label>
	<input type="submit" class="input-submit" name="buscar" value="Buscar"/>
</fieldset>
</form>

<fieldset>
	<legend>Resultados</legend>
	<p><a href="#cobrado">Cobrado</a></p>
	<p><a href="#balance">Balance</a></p>
	<h2>Por cobrar</h2>
	<?php if(!empty($porCobrar)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Nombre/Grupo</th>
	        <th>Status</th>
	        <th>Fecha</th>
	        <th>Inter�s</th>
	        <th>I.V.A.</th>
	        <th>Capital</th>
	    </tr>
	    <?php $c = true; $sumI = 0; $sumV = 0; $sumC = 0;?>
	    <?php foreach($porCobrar as $val) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $val["expediente"]; ?></td>
	        <td><?php echo $val["nombreGrupo"]; ?></td>
	        <td><?php echo $status[$val["status"]]; ?></td>
	        <td><?php echo ucwords(strftime("%A %d %B %Y", strtotime($val["fecha_mov"]))); ?> </td>
	        <td><?php echo "$".number_format($val["interes"], 2); ?></td>
	        <td><?php echo "$".number_format($val["iva"], 2); ?></td>
	        <td><?php echo "$".number_format($val["capital"], 2);?></td>
	    </tr>
	    <?php
	    	$sumI += $val['interes']; 
	    	$sumV += $val['iva'];
	    	$sumC += $val['capital'];
		} ?>
		<tr>
			<td colspan=3></td>
			<td style="text-align:right;"><b>Total</b></td>
			<td><b><?php echo "$".number_format($sumI, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumV, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumC, 2); ?></b></td>
		</tr>
		</table>
	<?php }else{ ?>
		<p class="msg warning">No hay amortizaciones para estas fechas</p>
	<?php } ?>
	<!-- ///////////////////////////////////////////////////////////////////// -->
	<h2><a name="cobrado">Cobrado</a></h2>
	<?php if(!empty($cobrado)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Nombre/Grupo</th>
	        <th>Status</th>
	        <th>Fecha</th>
	        <th>Inter�s</th>
	        <th>I.V.A.</th>
	        <th>Capital</th>
	    </tr>
	    <?php $c = true; $sumI2 = 0; $sumV2 = 0; $sumC2 = 0;?>
	    <?php foreach($cobrado as $val) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $val["expediente"]; ?></td>
	        <td><?php echo $val["nombreGrupo"]; ?></td>
	        <td><?php echo $status[$val["status"]]; ?></td>
	        <td><?php echo ucwords(strftime("%A %d %B %Y", strtotime($val["fecha_mov"]))); ?> </td>
	        <td><?php echo "$".number_format(-$val["interes"], 2); ?></td>
	        <td><?php echo "$".number_format(-$val["iva"], 2); ?></td>
	        <td><?php echo "$".number_format(-$val["capital"], 2);?></td>
	    </tr>
	    <?php
	    	$sumI2 += -$val['interes']; 
	    	$sumV2 += -$val['iva'];
	    	$sumC2 += -$val['capital'];
		} ?>
		<tr>
			<td colspan=3></td>
			<td style="text-align:right;"><b>Total</b></td>
			<td><b><?php echo "$".number_format($sumI2, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumV2, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumC2, 2); ?></b></td>
		</tr>
		</table>
	<?php }else{ ?>
		<p class="msg warning">No hay pagos para estas fechas</p>
	<?php } ?>
	<h2><a name="balance">Balance</a></h2>
	<table>
		<tr>
	        <th>Inter�s</th>
	        <th>I.V.A.</th>
	        <th>Capital</th>
	    </tr>
		<tr>
			<td><b><?php echo "$".number_format($sumI-$sumI2, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumV-$sumV2, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumC-$sumC2, 2); ?></b></td>
		</tr>
	
	</table>
</fieldset>