<h1>Desglose por dep�sito bancario</h1>
<form action="reportes.php?content=pordeposito" method="post">
<fieldset>
	<legend>Filtro</legend>
	<label for="fecha1">Fecha inicial
		<input type="text" class="input-text" name="fecha1" id="fecha1" value="<?php if(isset($_POST['fecha1'])) echo $_POST['fecha1'];?>"/>
	</label>
	<label for="fecha2">Fecha final
		<input type="text" class="input-text" name="fecha2" id="fecha2" value="<?php if(isset($_POST['fecha2'])) echo $_POST['fecha2'];?>"/>
	</label>
	<label for="bancos">Bancos:<br/>
		<input type="checkbox" name="bancos[]" value="1" <?php if(isset($data['bancos']) && in_array("1",$data['bancos'])) echo "CHECKED"; ?>/>AFIRME/BAJIO<br/>
		<input type="checkbox" name="bancos[]" value="3" <?php if(isset($data['bancos']) && in_array("3",$data['bancos'])) echo "CHECKED"; ?>/>BBVA/BANCOMER<br/>
		<input type="checkbox" name="bancos[]" value="6" <?php if(isset($data['bancos']) && in_array("6",$data['bancos'])) echo "CHECKED"; ?>/>BANORTE
	</label>
	<!--<input type="hidden" name="criterios" id="criterios" value="<?php echo json_encode($data);?>"/>-->
	<input type="submit" class="input-submit" name="filtrar" value="Filtrar"/>
	<input type="submit" class="input-submit" name="limpiar" value="Limpiar"/>
</fieldset>
</form>
<fieldset>
<legend>Resultado</legend>
	<?php if(!empty($depositos)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Banco</th>
	        <th>Fecha</th>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>IVA</th>
	        <th>Capital</th>
	        <th>Expediente</th>
	        <th>Grupo/Nombre</th>
	        <th>Referencia</th>
	    </tr>
	    <?php $c = true; $sumMonto = 0;$sumInt = 0;$sumIVA = 0;$sumCap = 0;?>
	    <?php foreach($depositos as $dep) {
	    	$sumMonto += $dep["monto"];
	    	$sumInt += $dep["interes"];
	    	$sumIVA += $dep["iva"];
	    	$sumCap += $dep["capital"];
	    ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $dep["banco"]; ?></td>
	        <td><?php echo ucwords(strftime("%d %b %Y", strtotime($dep["fecha_mov"]))); ?></td>
	        <td><?php echo "$".number_format($dep["monto"], 2); ?></td>
	        <td><?php echo "$".number_format($dep["interes"], 2); ?></td>
	        <td><?php echo "$".number_format($dep["iva"], 2); ?></td>
	        <td><?php echo "$".number_format($dep["capital"], 2); ?></td>
	        <td><?php echo $dep["expediente"]; ?></td>
	        <td><?php echo $dep["nombreGrupo"]; ?></td>
	        <td><?php echo $dep["clave_deposito"]; ?></td>
	    </tr>
	    <?php
	    	$sum += $cred['montoTotal']; 
		} ?>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:right;"><b>Totales</b></td>
			<td><b><?php echo "$".number_format($sumMonto, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumInt, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumIVA, 2); ?></b></td>
			<td><b><?php echo "$".number_format($sumCap, 2); ?></b></td>
			<td colspan=3></td>
		</tr>
		</table>
	<?php }else{ ?>
		<p class="msg warning">No hay cr�ditos para esas fechas.</p>
	<?php } ?>
	<!--<a href="#">Exportar a excel</a>-->
</fieldset>
