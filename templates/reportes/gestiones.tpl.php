<style type="text/css">
#tabs .ui-widget-content{
	background: none;
	border: none;
}
#tabs .ui-widget-header {
	background: none;
	border: none;
}
</style>
<h1>Reporte de gestiones</h1>
<form action="reportes.php?content=gestiones" method="post">
<fieldset>
	<legend><a href="#void" id="showSearch">Buscar</a></legend>
	<div id="buscarDiv" style="display:none;">
	<label for="fecha">Fecha:
		<input type="text" class="input-text" id="fecha1" name="fecha" />
	</label>
	<label for="usuario">Usuario:
		<input type="text" class="input-text" id="usuario" name="usuario" />
	</label>
	<input type="hidden" id="userID" name="userID" />
	<input type="submit" class="input-submit" value="Filtrar"/>
	</div>
</fieldset>
</form>

<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Por fecha</a></li>
    <li><a href="#tabs-2">Por usuario</a></li>
  </ul>
  <div id="tabs-1">
<?php
$lastMonth = "";
$lastDate = "";
if(!empty($gestiones)){
	foreach($gestiones as $gestion){
		if(date('n', strtotime($gestion['fecha_gestion'])) != $lastMonth){
			echo '<h2>'.ucfirst(strftime("%B, %Y", strtotime($gestion['fecha_gestion']))).'</h2>';
			$lastMonth = date('n', strtotime($gestion['fecha_gestion']));
		}
		if(date('d', strtotime($gestion['fecha_gestion'])) != $lastDate){
			echo '<h3>'.ucfirst(strftime("%A %e", strtotime($gestion['fecha_gestion']))).'</h3>';
			$lastDate = date('d', strtotime($gestion['fecha_gestion']));
		}?>
		<p>Expediente: <b><a href="#void" style="color:#0085cc;text-decoration:underline;" id="a_<?php echo $gestion['id_gestion'];?>"><?php echo $gestion['expediente'];?></a></b></p>
		<div class="gestion" id="div_<?php echo $gestion['id_gestion'];?>">
			<p class="gestionado">Gestionado: <?php echo $gestion['gestionado'];?></p>
			<span class="tipogestion"><?php echo $tipoGestion[$gestion['tipo_gestion']];?></span>
			<p><?php echo $gestion['descripcion'];?></p>
			<p><span class="usuario"><?php echo $gestion['usuario'];?></span></p>
		</div>
<?php 	}
}
?>
</div>
 <div id="tabs-2">
<?php
$lastUser = "";
$lastDate = "";
if(!empty($gestionesU)){
	foreach($gestionesU as $gestion){
		if($gestion['usuario'] != $lastUser){
			echo '<h2>'.$gestion['usuario'].'</h2>';
			$lastUser = $gestion['usuario'];
		}
		if(date('d', strtotime($gestion['fecha_gestion'])) != $lastDate){
			echo '<h3>'.ucfirst(strftime("%A %e %b %Y", strtotime($gestion['fecha_gestion']))).'</h3>';
			$lastDate = date('d', strtotime($gestion['fecha_gestion']));
		}?>
		<p>Expediente: <b><a href="#void" style="color:#0085cc;text-decoration:underline;" id="aU_<?php echo $gestion['id_gestion'];?>"><?php echo $gestion['expediente'];?></a></b></p>
		<div class="gestion" id="divU_<?php echo $gestion['id_gestion'];?>">
			<p class="gestionado">Gestionado: <?php echo $gestion['gestionado'];?></p>
			<span class="tipogestion"><?php echo $tipoGestion[$gestion['tipo_gestion']];?></span>
			<p><?php echo $gestion['descripcion'];?></p>
			<p><span class="usuario"><?php echo $gestion['usuario'];?></span></p>
		</div>
<?php 	}
}
?>
</div>
</div>