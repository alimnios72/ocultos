<h1>Totales Por Mes</h1>
<?php if(!empty($creditos)){
$acumPrestado = 0;
$acumRecuperado = 0;
foreach($creditos as $credito){
	$mes = ucfirst(strftime("%B", mktime(0, 0, 0, $credito['mes'], 10)));
	$acumPrestado += $credito['prestado'];
	$acumRecuperado += $credito['monto']; 
	?>
<h2><?php echo "{$mes}, {$credito['a�o']}";?></h2>
	<table>
	<thead>
    <tr>
        <th>Total Prestado</th>
        <th>Total Recuperado</th>
        <th>Total Prestado Acumulado</th>
        <th>Total Recuperado Acumulado</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php echo "$".number_format($credito["prestado"], 2); ?></td>
        <td><?php echo "$".number_format($credito["monto"], 2); ?></td>
        <td><?php echo "$".number_format($acumPrestado, 2); ?></td>
        <td><?php echo "$".number_format($acumRecuperado, 2); ?></td>
    </tr>
    </tbody>
</table>
<?php }?>
<?php }?>