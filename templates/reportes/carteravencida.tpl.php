<?php if($tipoCartera == 'parcialidades'){?><h1>Reporte por parcialidades vencidas</h1><?php }?>
<?php if($tipoCartera == 'dias'){?><h1>Reporte por d�as vencidos</h1><?php }?>
<form action="reportes.php?content=<?php echo $content;?>" method="post">
<fieldset>
	<legend>Filtrar</legend>
	<!-- <label for="buscarGpo">Buscar grupo:
		<input type="text" class="input-text" name="grupo" size="8"/>
	</label> -->
	<label for="filtrado">
		<select name="filtro">
			<option value="monto" <?php if($data['filtro'] == 'monto') echo 'SELECTED';?>>Monto</option>
			<?php if($tipoCartera == 'parcialidades'){?>
			<option value="parcialidad" <?php if($data['filtro'] == 'parcialidad') echo 'SELECTED';?>>Parcialidad</option>
			<?php }?>
			<?php if($tipoCartera == 'dias'){?>
			<option value="dias" <?php if($data['filtro'] == 'dias') echo 'SELECTED';?>>D�as</option>
			<?php }?>
		</select>
		<select name="signo">
			<option value="menor" <?php if($data['signo'] == 'menor') echo 'SELECTED';?>>Menor que</option>
			<option value="igual" <?php if($data['signo'] == 'igual') echo 'SELECTED';?>>Igual</option>
			<option value="mayor" <?php if($data['signo'] == 'mayor') echo 'SELECTED';?>>Mayor que</option>
		</select>
		<input type="text" class="input-text" name="valor" value="<?php if(isset($data['valor'])) echo $data['valor'];?>" size="6"/>
	</label>
</fieldset>
<input type="submit" class="input-submit" name="done" value="Aplicar" />
</form>
<fieldset>
	<legend>Resultados</legend>
	<?php if(!empty($creditos)){?>
		<table id="carteraVencida" class="tablesorter">
	    <thead>
	    <tr>
	        <th style="width:90px;">Expediente</th>
	        <th>Nombre/Grupo</th>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>Iva</th>
	        <th>Capital</th>
	        <?php if($tipoCartera == 'parcialidades'){?><th style="width:110px;">Parcialidades</th><?php }?>
	        <?php if($tipoCartera == 'dias'){?>
	        <th style="width:60px;">D�as sin pago</th>
	        <th style="width:60px;">D�as acumulados</th>
	        <?php }?>
	        <th>Zona</th>
	    </tr>
	    </thead>
	    <tbody>
	    <?php $c = true; $sumInt=0; $sumIva=0; $sumCap=0;?>
	    <?php foreach($creditos as $credito) { 
	    $sumInt += $credito["interes"];
	    $sumIva += $credito["iva"];
	    $sumCap += $credito["capital"];
	    ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><a href="administracion.php?content=detallesCred&idCred=<?php echo $credito['id_credito'];?>" target="_blank"><?php echo $credito["expediente"]; ?></a></td>
	        <td><?php echo $credito["nombreGrupo"]; ?></td>
	        <td><?php echo "$".number_format($credito["monto"], 2); ?></td>
	        <td><?php echo "$".number_format($credito["interes"], 2); ?></td>
	        <td><?php echo "$".number_format($credito["iva"], 2); ?></td>
	        <td><?php echo "$".number_format($credito["capital"], 2); ?></td>
	        <?php if($tipoCartera == 'parcialidades'){?><td><?php echo $credito["pagos"]; ?></td><?php }?>
	        <?php if($tipoCartera == 'dias'){?>
	        <td><?php echo $credito["dias"]; ?></td>
	        <td><?php echo $credito["diasacum"]; ?></td>
	        <?php }?>
	        <td><?php echo $credito['nombre_zona']; ?></td>
	    </tr>
	    <?php } ?>
	    <tr>
	    	<td colspan=3>&nbsp;</td>
	    	<td><b><?php echo "$".number_format($sumInt, 2); ?></b></td>
	    	<td><b><?php echo "$".number_format($sumIva, 2); ?></b></td>
	    	<td><b><?php echo "$".number_format($sumCap, 2); ?></b></td>
	    	<td colspan=<?php echo $tipoCartera =='dias'? 3: 2;?>>&nbsp;</td>
	    </tr>
	    </tbody>
    <?php } ?>
</table>
</fieldset>