<h1>Reporte Inter�s devengado</h1>
<form action="reportes.php?content=devengado" method="post">
<fieldset>
	<legend>Filtros</legend>
	<label for="grupos">S�lo los grupos:
		 <input type="checkbox" name="tipo[]" value="1" <?php if(isset($data['tipo']) && in_array("1", $data['tipo'])) echo "checked";?>/>
	</label>
	<label for="individuales">S�lo los individuales:
		<input type="checkbox" name="tipo[]" value="0" <?php if(isset($data['tipo']) && in_array("0", $data['tipo'])) echo "checked";?>/>
	</label>
	<label for="persona">Por persona:
		<input type="checkbox" name="persona" id="persona" value="0" <?php if(isset($data['persona'])) echo "checked";?>/>
		
	</label>
	<div id="tiempo" <?php if(!isset($data['persona'])) echo 'style="display:none;"';?>>
		<select multiple name="a�o[]">
			<option value="2011" <?php if(is_array($data['a�o']) && in_array("2011", $data['a�o'])) echo "selected";?>>2011</option>
			<option value="2012" <?php if(is_array($data['a�o']) && in_array("2012", $data['a�o'])) echo "selected";?>>2012</option>
			<option value="2013" <?php if(is_array($data['a�o']) && in_array("2013", $data['a�o'])) echo "selected";?>>2013</option>
			<option value="2014" <?php if(is_array($data['a�o']) && in_array("2014", $data['a�o'])) echo "selected";?>>2014</option>
			<option value="2015" <?php if(is_array($data['a�o']) && in_array("2015", $data['a�o'])) echo "selected";?>>2015</option>
		</select>
		<select multiple name="mes[]">
			<option value="1" <?php if(is_array($data['mes']) && in_array("1",$data['mes'])) echo "selected";?>>Enero</option>
			<option value="2" <?php if(is_array($data['mes']) && in_array("2",$data['mes'])) echo "selected";?>>Febrero</option>
			<option value="3" <?php if(is_array($data['mes']) && in_array("3",$data['mes'])) echo "selected";?>>Marzo</option>
			<option value="4" <?php if(is_array($data['mes']) && in_array("4",$data['mes'])) echo "selected";?>>Abril</option>
			<option value="5" <?php if(is_array($data['mes']) && in_array("5",$data['mes'])) echo "selected";?>>Mayo</option>
			<option value="6" <?php if(is_array($data['mes']) && in_array("6",$data['mes'])) echo "selected";?>>Junio</option>
			<option value="7" <?php if(is_array($data['mes']) && in_array("7",$data['mes'])) echo "selected";?>>Julio</option>
			<option value="8" <?php if(is_array($data['mes']) && in_array("8",$data['mes'])) echo "selected";?>>Agosto</option>
			<option value="9" <?php if(is_array($data['mes']) && in_array("9",$data['mes'])) echo "selected";?>>Septiembre</option>
			<option value="10" <?php if(is_array($data['mes']) && in_array("10",$data['mes'])) echo "selected";?>>Octubre</option>
			<option value="11" <?php if(is_array($data['mes']) && in_array("11",$data['mes'])) echo "selected";?>>Noviembre</option>
			<option value="12" <?php if(is_array($data['mes']) && in_array("12",$data['mes'])) echo "selected";?>>Diciembre</option>
		</select>
		</div>
	<label for="expediente"> Por expediente:
		<input type="text" class="input-text" name="expediente" value="<?php if(isset($data['expediente'])) echo $data['expediente'];?>"/>
	</label>
	<label for="status">Status:
		<select name="status">
			<option value="0" <?php if($data['status'] == "0") echo "selected";?>>Todos</option>
			<option value="2" <?php if($data['status'] == "2") echo "selected";?>>Activo</option>
			<option value="4" <?php if($data['status'] == "4") echo "selected";?>>Concluido</option>
		</select>
	</label>
	<input type="submit" class="input-submit" name="submit" value="Filtrar" />
</fieldset>
</form>
<fieldset>
<legend>Total devengado</legend>
	<?php if($persona){
		if(!empty($data['mes'])){
			foreach ($data['mes'] as $mes)
				$dates[] = ucfirst(strftime("%B", mktime(0, 0, 0, $mes, 10)));
		}
	?>
		<h3><?php echo implode(', ',$dates)." ".implode(',',$data['a�o']);?></h3>
	<?php } ?>
	<?php if(!empty($tabla)){?>
		<table>
	    <thead>
	    <tr>
	    	<?php if($persona){?>
	    		<th>Expediente</th>
	    		<th>Grupo</th>
	    	<?php }?>
	    	<?php if($expediente){?>
	    		<th>Nombre</th>
	    	<?php }else{?>
		    	<th>Mes</th>
		        <th>A�o</th>
	    	<?php }?>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>Iva</th>
	        <th>Capital</th>
	    </tr>
	    </thead>
	    <tbody>
	    <?php $c = true; $sumI1 = 0; $sumV1 = 0; $sumC1 = 0;?>
	    <?php foreach($tabla as $val) { 
	    if($val['tipo_mov'] == 0){ 
		$sumI1 += $val["interes"]; $sumV1 += $val["iva"]; $sumC1 += $val["capital"];
		$mes = ucfirst(strftime("%B", mktime(0, 0, 0, $val['mes'], 10)));?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<?php if($persona){?>
	    		<td><?php echo $val["expediente"]; ?></td>
	    		<td><?php echo $val["grupo"]; ?></td>
	    	<?php }?>
	    	<?php if($expediente){?>
	    		<td><?php echo $val["nombres"]; ?></td>
	    	<?php }else{?>
		        <td><?php echo $mes; ?></td>
		        <td><?php echo $val["a�o"]; ?></td>
	        <?php }?>
	        <td><?php echo "$".number_format($val["monto"], 2); ?></td>
	        <td><?php echo "$".number_format($val["interes"], 2); ?></td>
	        <td><?php echo "$".number_format($val["iva"], 2); ?></td>
	        <td><?php echo "$".number_format($val["capital"], 2); ?></td>
	    </tr>
	    <?php } } ?>
	    <tr>
	    	<td colspan="2">No devengadas pero pagadas</td>
	    	<td><?php echo "$".number_format($pagosNoDevengados['monto'] ,2); ?></td>
	    	<td><?php echo "$".number_format($pagosNoDevengados['interes'] ,2); ?></td>
	    	<td><?php echo "$".number_format($pagosNoDevengados['iva'], 2); ?></td>
	    	<td><?php echo "$".number_format($pagosNoDevengados['capital'], 2); ?></td>
	    </tr>
	    <?php 
	    	$sumI1 += $pagosNoDevengados['interes'];
	    	$sumV1 += $pagosNoDevengados['iva'];
	    	$sumC1 += $pagosNoDevengados['capital'];
	    ?>
	    <tr>
	    	<td colspan="<?php echo $persona ? '4' : ($expediente ? '2': '3');?>">&nbsp;</td>
	    	<td><b><?php echo "$".number_format($sumI1, 2);?></b></td>
	    	<td><b><?php echo "$".number_format($sumV1, 2);?></b></td>
	    	<td><b><?php echo "$".number_format($sumC1, 2);?></b></td>
	    </tr>
	    </tbody>
    <?php } ?>
</table>
</fieldset>

<fieldset>
<legend>Total cobrado</legend>
	<?php if(!empty($tabla)){?>
		<table>
	    <thead>
	    <tr>
	    	<?php if($persona){?>
	    		<th>Expediente</th>
	    		<th>Grupo</th>
	    	<?php }?>
	        <?php if($expediente){?>
	    		<th>Nombre</th>
	    	<?php }else{?>
		    	<th>Mes</th>
		        <th>A�o</th>
	    	<?php }?>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>Iva</th>
	        <th>Capital</th>
	    </tr>
	    </thead>
	    <tbody>
	    <?php $c = true; $sumI2 = 0; $sumV2 = 0; $sumC2 = 0;?>
	    <?php foreach($tabla as $val) { 
	    if($val['tipo_mov'] == 1){ 
		$sumI2 += $val["interes"]; $sumV2 += $val["iva"]; $sumC2 += $val["capital"];
		$mes = ucfirst(strftime("%B", mktime(0, 0, 0, $val['mes'], 10)));?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<?php if($persona){?>
	    		<td><?php echo $val["expediente"]; ?></td>
	    		<td><?php echo $val["grupo"]; ?></td>
	    	<?php }?>
	        <?php if($expediente){?>
	    		<td><?php echo $val["nombres"]; ?></td>
	    	<?php }else{?>
		        <td><?php echo $mes; ?></td>
		        <td><?php echo $val["a�o"]; ?></td>
	        <?php }?>
	        <td><?php echo "$".number_format(-$val["monto"], 2); ?></td>
	        <td><?php echo "$".number_format(-$val["interes"], 2); ?></td>
	        <td><?php echo "$".number_format(-$val["iva"], 2); ?></td>
	        <td><?php echo "$".number_format(-$val["capital"], 2); ?></td>
	    </tr>
	    <?php } } ?>
	    <tr>
	    	<td colspan="<?php echo $persona ? '4' : ($expediente ? '2': '3');?>">&nbsp;</td>
	    	<td><b><?php echo "$".number_format(-$sumI2, 2);?></b></td>
	    	<td><b><?php echo "$".number_format(-$sumV2, 2);?></b></td>
	    	<td><b><?php echo "$".number_format(-$sumC2, 2);?></b></td>
	    </tr>
	    </tbody>
    <?php } ?>
</table>
</fieldset>

<fieldset>
<legend>Total NO cobrado</legend>
	<?php if(!empty($tabla)){?>
		<table>
	    <thead>
	    <tr>
	        <th>Inter�s</th>
	        <th>Iva</th>
	        <th>Capital</th>
	    </tr>
	    </thead>
	    <tbody>
	    <tr>
	        <td><b><?php echo "$".number_format($sumI1+$sumI2, 2); ?></b></td>
	        <td><b><?php echo "$".number_format($sumV1+$sumV2, 2); ?></b></td>
	        <td><b><?php echo "$".number_format($sumC1+$sumC2, 2); ?></b></td>
	    </tr>
	    </tbody>
    <?php } ?>
</table>
</fieldset>

<?php if(!empty($saldos)){ ?>
<fieldset>
	<legend>Saldos</legend>
	<table>
	    <thead>
	    <tr>
	        <th>Expediente</th>
	        <th>Grupo</th>
	        <th>Nombre</th>
	        <th>Monto</th>
	        <th>Inter�s</th>
	        <th>Iva</th>
	        <th>Capital</th>
	    </tr>
	    </thead>
	    <tbody>
	    <?php $c = true; ?>
	    <?php foreach($saldos as $val) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<td><?php echo $val["expediente"]; ?></td>
	    	<td><?php echo $val["grupo"]; ?></td>
	    	<td><?php echo $val["nombres"]; ?></td>
	        <td><?php echo "$".number_format($val["monto"], 2); ?></td>
	        <td><?php echo "$".number_format($val["interes"], 2); ?></td>
	        <td><?php echo "$".number_format($val["iva"], 2); ?></td>
	        <td><?php echo "$".number_format($val["capital"], 2); ?></td>
	    </tr>
	    <?php } ?>
	    </tbody>
	 </table>
</fieldset>
<?php }?>