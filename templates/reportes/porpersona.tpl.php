<h1>Total prestado por persona</h1>
<form action="reportes.php?content=porpersona" method="post">
<fieldset>
	<legend>Filtro</legend>
	<label for="fecha1">Fecha inicial
		<input type="text" class="input-text" name="fecha1" id="fecha1" value="<?php if(isset($_POST['fecha1'])) echo $_POST['fecha1'];?>"/>
	</label>
	<label for="fecha2">Fecha final
		<input type="text" class="input-text" name="fecha2" id="fecha2" value="<?php if(isset($_POST['fecha2'])) echo $_POST['fecha2'];?>"/>
	</label>
	<label for="ordenar">Ordenar por:
		<select name="ordenar">
			<option value="fecha" <?php if($_POST['ordenar'] == "fecha") echo "SELECTED";?>>Fecha</option>
			<option value="expediente" <?php if($_POST['ordenar'] == "expediente") echo "SELECTED";?>>Expediente</option>
			<option value="nombre" <?php if($_POST['ordenar'] == "nombre") echo "SELECTED";?>>Nombre</option>
		</select>
	</label>
	<?php if($_POST['ordenar'] == "expediente") echo "<p class='msg warning'> Al ordenar por expediente no se consideran los cr�ditos a los cuales no se ha asignado expediente, es decir, aquellos marcados como 'INDEFINIDO' </p>"; ?>
	<input type="submit" class="input-submit" name="buscar" value="Buscar"/>
</fieldset>
</form>

<fieldset>
	<legend>Resultados</legend>
	<?php if(!empty($personas)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Nombre</th>
	        <th>Monto</th>
	        <th>Fecha entrega</th>
	        <th>Zona</th>
	    </tr>
	    <?php $c = true; $sum = 0;?>
	    <?php foreach($personas as $persona) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $persona["expediente"]; ?></td>
	        <td><?php echo $persona["nombre"]; ?></td>
	        <td><?php echo "$".number_format($persona["cantidad"], 2); ?></td>
	        <td><?php echo ucwords(strftime("%A %d %B %Y", strtotime($persona["fecha_entrega"]))); ?></td>
	        <td><?php echo $persona["nombre_zona"]; ?></td>
	        
	        
	    </tr>
	    <?php
	    	$sum += $persona["cantidad"]; 
		} ?>
		<tr>
			<td colspan=2></td>
			<td><b><?php echo "$".number_format($sum, 2); ?></b></td>
			<td colspan=2></td>
		</tr>
		</table>
	<?php }else{ ?>
		<p class="msg warning">No hay pr�stamos para esas fechas.</p>
	<?php } ?>
</fieldset>