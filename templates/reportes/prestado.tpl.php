<h1>Cr�ditos entregados por fecha</h1>
<form action="reportes.php?content=prestado" method="post">
<fieldset>
	<legend>Filtro</legend>
	<label for="fecha1">Fecha inicial
		<input type="text" class="input-text" name="fecha1" id="fecha1" value="<?php if(isset($_POST['fecha1'])) echo $_POST['fecha1'];?>"/>
	</label>
	<label for="fecha2">Fecha final
		<input type="text" class="input-text" name="fecha2" id="fecha2" value="<?php if(isset($_POST['fecha2'])) echo $_POST['fecha2'];?>"/>
	</label>
	<label for="ordenar">Ordenar por:
		<select name="ordenar">
			<option value="fecha" <?php if($_POST['ordenar'] == "fecha") echo "SELECTED";?>>Fecha</option>
			<option value="status" <?php if($_POST['ordenar'] == "status") echo "SELECTED";?>>Status</option>
			<option value="tasa" <?php if($_POST['ordenar'] == "tasa") echo "SELECTED";?>>Tasa</option>
			<option value="expediente" <?php if($_POST['ordenar'] == "expediente") echo "SELECTED";?>>Expediente</option>
			<option value="nombre" <?php if($_POST['ordenar'] == "nombre") echo "SELECTED";?>>Nombre</option>
		</select>
	</label>
	<?php if($_POST['ordenar'] == "expediente") echo "<p class='msg warning'> Al ordenar por expediente no se consideran los cr�ditos a los cuales no se ha asignado expediente, es decir, aquellos marcados como 'INDEFINIDO' </p>"; ?>
	<input type="submit" class="input-submit" name="buscar" value="Buscar"/>
</fieldset>
</form>

<fieldset>
	<legend>Resultados</legend>
	<?php if(!empty($creditos)){?>
		<table>
	    <tbody>
	    <tr>
	        <th>Expediente</th>
	        <th>Nombre/Grupo</th>
	        <th>Tasa</th>
	        <th>Plazo</th>
	        <th>Periodo</th>
	        <th>Status</th>
	        <th>Fecha entrega</th>
	        <th>Monto</th>
	    </tr>
	    <?php $c = true; $sum = 0;?>
	    <?php foreach($creditos as $cred) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo $cred["expediente"]; ?></td>
	        <td><?php echo $cred["nombreGrupo"]; ?></td>
	        <td><?php echo $cred["tasa_interes"]; ?>%</td>
	        <td><?php echo $cred["plazo"]; ?> MESES</td>
	        <td><?php echo $cred["periodo"]; ?></td>
	        <td><?php echo $status[$cred["status"]]; ?></td>
	        <td><?php echo ucwords(strftime("%A %d %B %Y", strtotime($cred["fecha_entrega"]))); ?></td>
	        <td><?php echo "$".number_format($cred["montoTotal"], 2); ?></td>
	    </tr>
	    <?php
	    	$sum += $cred['montoTotal']; 
		} ?>
		<tr>
			<td colspan=6></td>
			<td style="text-align:right;"><b>Total</b></td>
			<td><b><?php echo "$".number_format($sum, 2); ?></b></td>
		</tr>
		</table>
	<?php }else{ ?>
		<p class="msg warning">No hay cr�ditos para esas fechas.</p>
	<?php } ?>
</fieldset>