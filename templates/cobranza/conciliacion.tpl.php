<h1>Lista de dep�sitos</h1>
<?php if(isset($msg)){?>
<p class="msg done"><?php echo $msg?></p>
<?php } ?>
<form action="cobranza.php?content=conciliacion" method="post">
<fieldset>
    <legend>Filtrar resultados por</legend>
  	<label for="banco">Banco:
        <select name="banco" id="banco">
        	<option <?php if(!isset($data['banco'])) echo "SELECTED";?> value="">Ninguno</option>
            <option <?php if(isset($data['banco']) && $data['banco']==1) echo "SELECTED";?> value="1">Afirme/Bajio</option>
            <option <?php if(isset($data['banco']) && $data['banco']==3) echo "SELECTED";?> value="3">BBVA Bancomer</option>
            <option <?php if(isset($data['banco']) && $data['banco']==3) echo "SELECTED";?> value="6">BANORTE</option>
        </select> 
    </label>
    <label for="fechaExacta">Fecha exacta:
    	<input class="input-text" type="text" name="fechaExacta" id="fechaExacta" value="<?php if(isset($data['fechaExacta']) && $data['fechaExacta']!="") echo $data['fechaExacta'];?>" size="15" /> 
    </label>
    <label for="fecha">Entre fechas:
         <input class="input-text" type="text" name="fecha1" id="fecha1" value="<?php if(isset($data['fecha1']) && $data['fecha1']!="") echo $data['fecha1'];?>" size="15" /> y
         <input class="input-text" type="text" name="fecha2" id="fecha2" value="<?php if(isset($data['fecha2']) && $data['fecha2']!="") echo $data['fecha2'];?>" size="15" />
    </label>
    <label for="monto">Entre montos:
         <input class="input-text" type="text" name="monto1" id="monto1" value="<?php if(isset($data['monto1']) && $data['monto1']!="") echo $data['monto1'];?>" size="15" /> y
         <input class="input-text" type="text" name="monto2" id="monto2" value="<?php if(isset($data['monto2']) && $data['monto2']!="") echo $data['monto2'];?>" size="15" />
    </label>
   <input type="submit" class="input-submit" name="update" value="Filtrar" />
   <input type="reset" class="input-submit" name="limpiar" id="cleanResults" value="Limpiar" />
</fieldset>
<fieldset>
	<legend>Resultados</legend>
	<table>
    <tbody>
        <tr>
            <th>Banco</th>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Referencia</th>
            <th>Observaciones</th>
            <th>Acci�n</th>
        </tr>
        <?php $c = true; ?>
        <?php if(!empty($depositos)){ ?>
        <?php foreach($depositos as $deposito){ ?>
            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                <td><?php echo $deposito['banco']; ?></td>
                <td><?php echo strftime("%A %d %B %Y", strtotime($deposito['fecha_deposito'])); ?></td>
                <td><?php echo "$".number_format($deposito['monto'],2); ?></td>
                <td><?php echo $deposito['clave_deposito']; ?></td>
                <td><?php echo $deposito['observaciones']; ?></td>
                <td><a href="?content=aplicacion&id=<?php echo $deposito['id_deposito'];?>">Aplicar</a></td>
            </tr>
        <?php } }?>
    </tbody>
</table>
</fieldset>
</form>
