<h1>Alerta de pagos</h1>
<fieldset>
	<legend>Pagos</legend>
	 <?php if(!empty($pagos)){ ?>
    <table>
    <tbody>
        <tr>
            <th>Banco</th>
            <th>Mes de pago</th>
            <th>Monto</th>
            <th>Inter�s</th>
            <th>IVA</th>
            <th>Persona</th>
            <th>Expediente</th>
            
        </tr>
        <?php $c = true; ?>
        <?php foreach($pagos as $pago){ ?>
            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                <td><?php echo $pago['banco']; ?></td>
                <td><?php echo strftime("%B %Y", strtotime($pago["fecha_mov"])); ?></td>
                <td><?php echo "$".number_format($pago['monto'],2); ?></td>
                <td><?php echo "$".number_format($pago['interes'],2); ?></td>
                <td><?php echo "$".number_format($pago['iva'],2); ?></td>
                <td><?php echo $pago['nombre']; ?></td>
                <td><a href="administracion.php?content=detallesCred&idCred=<?php echo $pago['id_credito']; ?>" title="<?php echo $pago['nombreGrupo']; ?>"><?php echo $pago['expediente']; ?></a></td> 
            </tr>
        <?php } ?>
    </tbody>
	</table>
    <?php } ?>
</fieldset>