<h1>Aplicar deposito</h1>
<fieldset>
    <legend>Datos del dep�sito</legend>
    <div class="col50">
        <p>Banco: <b><?php echo $deposito['banco'];?></b></p>
        <p>Monto: <b><?php echo "$".number_format($deposito['monto'],2);?></b></p>
        <p>Fecha: <b><?php echo ucfirst(strftime("%A %d %B %Y", strtotime($deposito["fecha_deposito"])));?></b></p>
    </div>
    <div class="col50 f-right">
        <p>Referencia: <b><?php echo $deposito['clave_deposito'];?></b></p>
        <p>Observaciones: <b><?php echo $deposito['observaciones'];?></b></p>
    </div>
</fieldset>
<form id="aplica" action="cobranza.php?content=balance" method="post">
<input type="hidden" name="idDeposito" value="<?php echo $deposito['id_deposito'];?>" />
<fieldset>
	<legend>Buscar</legend>
	<label for="expediente">Buscar por expediente
    	<input type="text" class="input-text noEnterSubmit" name="expediente" id="expediente" value="" />
    	<a href="#" id="searchExp"><img style="vertical-align:middle;" src="<?php echo RUTA_IMG;?>search.png" /></a>
    </label>
</fieldset>
<fieldset>
    <legend>Aplicar a:</legend>
   	<div id="listaAmort"><?php include('listaAmortizaciones.tpl.php');?></div>
    <br />
    <input class="input-submit" type="button" name="data" id="elegirPago" value="Elegir">
</fieldset>
</form>