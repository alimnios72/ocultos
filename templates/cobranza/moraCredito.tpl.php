<h1>Detalles del cr�dito</h1>
<fieldset>
	<legend>Informaci�n de mora</legend>
	<p>Nombre/Grupo: <b><?php echo $credito['nombreGrupo'];?></b></p>
	<p>Monto: <b><?php echo "$".number_format($credito['montoTotal'],2);?></b></p>
	<p>Zona: <b><?php echo $credito['nombre_zona'];?></b></p>
	
	<table>
    	<tbody>
    		<tr>
    			<th>Inter�s moratorio</th>
    			<th>IVA</th>
    			<th>Total</th>
    		</tr>
    		<tr>
		    	<td class="center"><b><?php echo "$".number_format($balanceMora['interes'],2);?></b></td>
		    	<td class="center"><b><?php echo "$".number_format($balanceMora['iva'],2);?></b></td>
		    	<td class="center"><b><?php echo "$".number_format($balanceMora['monto'],2);?></b></td>
    		</tr>
    	</tbody>
   	</table>
</fieldset>
<form action="cobranza.php?content=pagoMora" method="post">
	<fieldset>
		<legend>Pago de mora</legend>
		<label for="banco">Banco/Cuenta:
			<select name="banco">
				<option value=2>Crea Bienestar Condonaci�n</option>
			</select>
		</label>
		<label for="monto">Monto:
			<input type="text" class="input-text" name="monto" value="" size="5"/>
		</label>
		<label for="fecha">Fecha de pago:
			<input type="text" class="input-text" name="fecha" id="fecha" value="" size="8">
		</label>
		<input type="hidden" name="idCred" value="<?php echo $data['idCred'];?>" />
		<input type="submit" class="input-submit" name="aplicar" value="Aplicar"/>
	</fieldset>
</form>
