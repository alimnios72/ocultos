<h1>Balance de cuenta</h1>
<form action="cobranza.php?content=balance" method="post">
<fieldset>
    <legend>Datos del dep�sito</legend>
    <div class="col50">
        <p>Banco: <b><?php echo $deposito['banco'];?></b></p>
        <p>Monto: <b><?php echo "$".number_format($deposito['monto'],2);?></b></p>
        <p>Fecha: <b><?php echo ucfirst(strftime("%A %d %B %Y", strtotime($deposito["fecha_deposito"])));?></b></p>
    </div>
    <div class="col50 f-right">
        <p>Referencia: <b><?php echo $deposito['clave_deposito'];?></b></p>
        <p>Observaciones: <b><?php echo $deposito['observaciones'];?></b></p>
    </div>
</fieldset>
<fieldset>
    <legend>Balance</legend>
    <input type="hidden" name="idDeposito" value="<?php echo $data['idDeposito'];?>" />
    <input type="hidden" name="idCredito" value="<?php echo $data['idCredito'];?>" />
    <p>Nombre/Grupo: <b><?php echo $credito['nombreGrupo'];?></b></p>
    <p>Expediente: <b><?php echo $credito['expediente'];?></b></p>
    <p>Monto del pr�stamo: <b><?php echo "$".number_format($credito['montoTotal'],2);?></b></p>
    <table>
    	<caption>Adeudo al d�a del dep�sito</caption>
    	<tr>
    		<th>Inter�s moratorio</th>
    		<th>Inter�s ordinario</th>
    		<th>IVA</th>
    		<th>Capital</th>
    		<th>Total</th>
    	</tr>
    	<?php if($deudaTotal<=0) $red = true; else $red = false;?>
    	<tr>
    		<td class="center"><b><?php echo "$".number_format($mora,2);?></b></td>
    		<td class="center"><b><?php if($red) echo "<span>";?><?php echo "$".number_format($balance['interes'],2);?><?php if($red) echo "</span>";?></b></td>
    		<td class="center"><b><?php if($red) echo "<span>";?><?php echo "$".number_format($balance['iva'],2);?><?php if($red) echo "</span>";?></b></td>
    		<td class="center"><b><?php if($red) echo "<span>";?><?php echo "$".number_format($balance['capital'],2);?><?php if($red) echo "</span>";?></b></td>
    		<td class="center"><b><?php if($red) echo "<span>";?><?php echo "$".number_format($deudaTotal,2);?><?php if($red) echo "</span>";?></b></td>
    	</tr>
    </table>
    <div class="col50">
	    <h3>Tabla de amortizaci�n</h3>
	    <table>
	    	<tr>
	    		<th>Num.</th>
	    		<th>Fecha</th>
	    		<th>Monto</th>
	    		<th>Inter�s</th>
	    		<th>IVA</th>
	    		<th>Capital</th>
	    		<th>Status</th>
	    	</tr>
	    	<?php $c = true; $i = 0;?>
	        <?php if(!empty($cargos)) {
	        	foreach($cargos as $cargo){ ?>
	            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	            	<td><?php $i++; echo $i;?></td>
	            	<td><?php echo ucfirst(strftime("%A %d %B %Y", strtotime($cargo["fecha_mov"]))); ?></td>
	                <td><?php echo "$".number_format($cargo['monto'],2); ?></td>
	                <td><?php echo "$".number_format($cargo['interes'],2); ?></td>
	                <td><?php echo "$".number_format($cargo['iva'],2); ?></td>
	                <td><?php echo "$".number_format($cargo['capital'],2); ?></td>
	                <td><b><?php echo $cargoStatus[$cargo['status']]; ?></b></td>
	            </tr>
	        <?php } }?>
	    </table>
	  </div>
	  <div class="col50 f-right">
	    <h3>Pagos realizados</h3>
	    <table>
	    	<tr>
	    		<th>Num.</th>
	    		<th>Fecha</th>
	    		<th>Monto</th>
	    		<th>Tipo</th>
	    	</tr>
	    	<?php $c = true; $i = 0;?>
	        <?php if(!empty($abonos)) {
	        	foreach($abonos as $abono){ ?>
	            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	            	<td><?php $i++; echo $i;?></td>
	            	<td><?php echo ucfirst(strftime("%A %d %B %Y", strtotime($abono["fecha_mov"]))); ?></td>
	                <td><?php echo "$".number_format($abono['monto']*-1,2); ?></td>
	                <td><b><?php echo $tipoAbono[$abono['tipo_abono']]; ?></b></td>
	            </tr>
	        <?php } }?>
	    </table>
	  </div>
</fieldset>
<label for="aplicarMora">Considerar mora
<input type="checkbox" name="aplicarMora" id="aplicarMora" value="1" />
</label>
<input type="submit" class="input-submit" name="aplicar" value="Aplicar dep�sito" />
</form>