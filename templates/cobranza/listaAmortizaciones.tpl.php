<div id="listaAmort">
    <form action="cobranza.php?content=listado" method="post">
        <fieldset>
            <legend>Filtrar resultados por</legend>
            <label for="fecha">Entre fechas:
                <input class="input-text" type="text" name="fecha1" id="fecha1" value="<?php if(isset($data['fecha1']) && $data['fecha1']!="") echo $data['fecha1'];?>" size="15" /> y
                <input class="input-text" type="text" name="fecha2" id="fecha2" value="<?php if(isset($data['fecha2']) && $data['fecha2']!="") echo $data['fecha2'];?>" size="15" />
            </label>
            <input type="hidden" name="tipo" value="rango" />
            <input type="submit" class="input-submit" name="update" value="Filtrar" />
        </fieldset>
    </form>

    <?php if($_POST['tipo'] == 'fecha'){?>
        <table>
            <tr>
                <td><a href="#" class="pagination" id="<?php echo $prev;?>">Anterior</a></td>
                <td><b><?php echo ucfirst(strftime("%A %d de %B de %Y", strtotime($criterios['fecha'])));?></b></td>
                <td><a href="#" class="pagination" id="<?php echo $next;?>">Siguiente</a></td>
            </tr>
        </table>
        <br />
    <?php } ?>
    <?php if(!empty($amortizaciones)){ ?>
        <table>
            <tbody>
            <tr>
                <th>&nbsp;</th>
                <th>Expediente</th>
                <th>Persona/Grupo</th>
                <th>Monto</th>
                <th>Inter�s</th>
                <th>I.V.A.</th>
                <th>Capital</th>
                <th>Fecha de pago</th>
                <th>Zona</th>
            </tr>
            <?php $c = true; ?>
            <?php foreach($amortizaciones as $amortizacion){ ?>
                <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
                    <td><input type="radio" name="idCredito" value="<?php echo $amortizacion['id_credito']; ?>" /></td>
                    <td><a href="administracion.php?content=credito&id=<?php echo $amortizacion['id_credito']; ?>" target="_blank"><?php echo $amortizacion['expediente']; ?></a></td>
                    <td><b><?php echo $amortizacion['nombreGrupo']; ?></b></td>
                    <td><?php echo "$".number_format($amortizacion['monto'],2); ?></td>
                    <td><?php echo "$".number_format($amortizacion['interes'],2); ?></td>
                    <td><?php echo "$".number_format($amortizacion['iva'],2); ?></td>
                    <td><?php echo "$".number_format($amortizacion['capital'],2); ?></td>
                    <td><?php echo strftime("%A %d %B %Y", strtotime($amortizacion["fecha_mov"])); ?></td>
                    <td><b><?php echo $amortizacion['nombre_zona']; ?></b></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php }else{?>
        <p class="msg warning">No existen pagos para �sta fecha</p>
    <?php } ?>
</div>