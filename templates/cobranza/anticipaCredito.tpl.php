<h1>Detalles del cr�dito</h1>
<?php if($nopag > 0){?>
<fieldset>
	<legend>Cr�dito a anticipar</legend>
	<p>Nombre/Grupo: <b><?php echo $credito['nombreGrupo'];?></b></p>
	<p>Monto: <b><?php echo "$".number_format($credito['montoTotal'],2);?></b></p>
	<p>Amortizaciones pagadas: <b><?php echo $pag; ?></b></p>
	<p>Amortizaciones NO pagadas: <b><?php echo $nopag; ?></b></p>
	<p>Se perder�n <b><?php echo "$".number_format($int,2);?></b> de intereses, �desea continuar?</p>
	 <form action="cobranza.php?content=anticipo" method="post" id="formAnticipo">
	 	<input type="hidden" name="idCredito" value="<?php echo $credito['id_credito'];?>" />
	 	<input type="hidden" id="mitadCred" name="mitadCred" value="<?php echo $anticipo; ?>" />
	 	<input class="input-submit" type="button" name="afirmativo" id="anticiparCred" value="S�" />
	 	&nbsp;
	 	<input class="input-submit" type="button" id="negativo" name="negativo" value="No" />
	 </form>
	 <?php if($anticipo){?>
	 	<p style="color:red;">Nota: la mitad del cr�dito no ha sido cubierta.</p>
	 <?php }?>
</fieldset>
<?php }else{?>
<h2>Imposible de anticipar el cr�dito. El cr�dito ya est� completamente pagado.</h2>
<?php } ?>
<div id="dialog-confirm" title="Anticipar cr�dito">
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
	Utilice cuenta de administrador para confirmar la acci�n</p>
	<p>Administrador <select id="username" name="username">
	<?php if(is_array($admins) && !empty($admins)){
		foreach($admins as $admin){?>
	<option value="<?php echo $admin['usuario'];?>"><?php echo $admin['usuario'];?></option>
	<?php } }?>
	</select></p>
	<p>Contrase�a <input type="password" id="userpass" name="userpass"/></p>
</div>
