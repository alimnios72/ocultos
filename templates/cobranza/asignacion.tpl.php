<?php if(isset($errormsg)){ ?>
    <p class="msg warning"><?php echo $errormsg; ?></p>
<?php } ?>
<?php if(isset($msg)){?>
    <p class="msg done"><?php echo $msg?></p>
<?php } ?>
<h1>Asignaci�n de pagos</h1>
<?php if($form === 1) { ?>
<form action="cobranza.php?content=asignacion" method="post">
    <fieldset>
        <legend>B�squeda</legend>
            <label for="codigo">C�digo de barras:
                <input class="input-text" type="text" name="codigo" id="codigo" autofocus size="15" />
            </label>
            <input type="submit" class="input-submit" name="busqExp" value="Buscar" />
    </fieldset>
</form>
<?php } ?>
<?php if($form === 2) { ?>
<fieldset>
    <legend>Deuda a la fecha</legend>
    <div class="col50">
        <p>Nombre: <b><?= $credito['nombreGrupo'] ?></b></p>
        <p>Monto: <b><?= '$'.number_format($balance['monto'],2) ?></b></p>
        <p>Inter�s: <b><?= '$'.number_format($balance['interes'],2) ?></b></p>
    </div>
    <div class="col50 f-right">
        <p>I.V.A.: <b><?= '$'.number_format($balance['iva'],2) ?></b></p>
        <p>Capital: <b><?= '$'.number_format($balance['capital'],2) ?></b></p>
    </div>
</fieldset>
<fieldset>
    <legend>Datos del pago</legend>
    <form action="cobranza.php?content=asignacion" method="post">
    <input type="hidden" name="id_credito" value="<?= $credito['id_credito'] ?>" />
    <div class="col50">
        <input type="hidden" name="banco" value="1" />
        <label for="tipo_cuenta">Cuenta del deposito:
            <select id="tipo_cuenta" name="tipo_cuenta">
                <option value="4">Parcialidades</option>
                <option value="5">Moratorios</option>
            </select>
        </label>
        <label for="monto">Monto:
            $<input class="input-text" type="text" name="monto" id="monto" size="10" value="<?= $monto ?>"/>
        </label>
        <label for="fecha">Fecha:
            <input class="input-text" type="text" name="fecha" id="fecha" size="15" value="<?= date('Y-m-d') ?>"/>
        </label>
    </div>
    <div class="col50 f-right">
        <label for="folio">Folio:
            <input class="input-text" type="text" name="folio" id="folio" size="10" value=""/>
        </label>
        <label for="clave">Referencia:
            <input class="input-text" type="text" name="clave" id="clave" size="5" value=""/>
        </label>
        <label for="observaciones">Observaciones:
            <input class="input-text" type="text" name="observaciones" id="observaciones" size="30" value=""/>
        </label>
    </div>
    <input type="submit" name="aplicar" value="Aplicar">
    </form>
</fieldset>
<?php } ?>