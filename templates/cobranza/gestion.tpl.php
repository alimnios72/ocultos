<h1>Gesti�n de cobranza</h1>
<?php if(isset($successmsg)){?>
<p class="msg done"><?php echo $successmsg?></p>
<?php }?>
<?php if(!isset($data['expediente']) || $data['expediente'] == ""){?>
<form action="cobranza.php?content=gestion" method="post">
<fieldset>
    <legend>B�squeda</legend>
  	<label for="expediente">Expediente:
       <input class="input-text" type="text" name="expediente" id="expediente" size="15" />
    </label>
   <input type="submit" class="input-submit" name="busqExp" value="Buscar" />
</fieldset>
</form>
<?php } else{?>
<form action="" method="post">
<fieldset>
	<legend>Gestionar cr�dito</legend>
	<label for="acreditado">Acreditado:
	 	<?php getComboBox($acreditados, 'acreditado', 'acreditado', array('value'=>'id_acreditado','text'=>'acreditado')); ?>
	 </label>
	 <label for="tipo_gestion">Tipo de gesti�n:
	 	<select name="tipo_gestion" id="tipo_gestion">
	 		<?php foreach($tipoGestion as $key=>$value){?>
	 		<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
	 		<?php }?>
	 	</select>
	 </label>
	 <label for="fecha">Fecha de gesti�n: 
	 	<input type="text" class="input-text" name="fecha_gestion" id="fecha" />
	 </label>
	 <label for="descripcion">Descripci�n: <br/>
	 	<textarea class="input-text" name="descripcion" id="descripcion" rows="4" cols="60"></textarea>
	 </label>
	 <input type="hidden" name="userID" value="<?php echo $_SESSION['userID']; ?>" />
	 <input type="hidden" name="expediente" value="<?php echo $data['expediente']; ?>" />
	 <input type="submit" class="input-submit" name="guardar" value="Guardar gesti�n" />
</fieldset>
</form>
<fieldset>
	<legend>Historial del gestiones</legend>
	<?php if(!empty($gestiones)){
		foreach($gestiones as $gestion){
			$fecha = strftime("%a %e de %b de %Y",strtotime($gestion['fecha_gestion']));?>
		<div style="border:1px solid #303030; padding-left:20px; margin-bottom:5px; position:relative;">
			<?php if($_SESSION['userID'] == $gestion['id_usuario']) {?>
				<a style="position:absolute;top:5px;right:5px;" 
					href="cobranza.php?content=gestion&action=delete&id=<?php echo $gestion['id_gestion'];?>">
					<img src="<?php echo RUTA_IMG."cancel-icon.png"?>" />
				</a>
			<?php } ?>
			<h4><?php echo $gestion['gestionado'];?></h4>
			<p><?php echo "{$fecha} - <b>({$tipoGestion[$gestion['tipo_gestion']]})";?></b></p>
			<p><?php echo $gestion['descripcion'];?></p>
			<p><b>Realizado por:</b> <?php echo $gestion['usuario'];?></p>
			
		</div>
	<?php } }?>
</fieldset>
<?php }?>