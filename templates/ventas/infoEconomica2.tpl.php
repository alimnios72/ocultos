<h1>Informaci�n econ�mica</h1>
<form action="ventas.php?content=viejo" method="post">
<?php if(isset($_GET['editar'])) { ?>
    <input type="hidden" name="editar" value="<?php echo $_GET['editar'];?>" />
<?php } ?>
<fieldset>
    <legend>Informaci�n del negocio</legend>
    <div class="col50">
        <label for="nombre_negocio">Nombre del negocio, oficio o actividad productiva<br />
            <input class="input-text" type="text" name="nombre_negocio" id="nombre_negocio" size="40" value="<?php echo $infoEcon['nombre_negocio'];?>"/>
        </label>
        <label for="calle">Calle y n�mero
            <input class="input-text" type="text" name="calle" id="calle" size="40" value="<?php echo $infoEcon['calle'];?>" />
        </label>
        <label for="colonia">Colonia
            <input class="input-text" type="text" name="colonia" id="colonia" size="30" value="<?php echo $infoEcon['colonia'];?>" />
        </label>
        <label for="estado">Estado
            <?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'), $infoEcon['estado']); ?>
        </label>
    </div>
    <div class="col50 f-right">
        <div id="deleg">
        <?php if(isset($infoEcon['estado']) && isset($infoEcon['delegacion'])){
        	$delegaciones = $ubicacionDB->getDelegacionByEstado($infoEcon['estado']);
        	echo "<label for='delegacion'>Delegaciones ";
        	getComboBox($delegaciones, 'delegacion', 'delegacion', array('value'=>'id_delegacion','text'=>'nombre_del'),$infoEcon['delegacion']);
        	echo "</label>";
        }
        ?>
        </div>
        <label for="cp">C�digo Postal
            <input class="input-text" type="text" name="cp" id="cp" size="15" value="<?php echo $infoEcon['cp'];?>" />
        </label>
        <label for="tel_neg">Tel�fono del negocio
            <input class="input-text" type="text" name="tel_neg" id="tel_neg" size="20" value="<?php echo $infoEcon['tel_neg'];?>" />
        </label>
        <label for="ant_negocio">Antig�edad en su negocio
            <input class="input-text" type="text" name="ant_negocio" id="ant_negocio" size="2" maxlength="2" value="<?php echo $infoEcon['ant_negocio'];?>"/> a�os
        </label>
    </div>
</fieldset>
<fieldset>
    <legend>Ingresos/Egresos</legend>
    <div class="col50">
    <!-- 
        <label for="ing_div">�Cuenta con ingresos diversos a su negocio?
            <input type="radio" name="ing_div" id="siID" value="1" <?php if($infoEcon['ing_div']=='1') echo "CHECKED";?>/>Si
            <input type="radio" name="ing_div" id="noID" value="0" <?php if($infoEcon['ing_div']=='0' || !isset($infoEcon)) echo "CHECKED";?>/>No
        </label>
        <div id="ingresos_div">
            <label for="ing_div_monto">�A cu�nto ascienden?
                $<input class="input-text" type="text" name="ing_div_monto" id="ing_div_monto" size="3" maxlength="10"  value="<?php echo $infoEcon['ing_div_monto'];?>" /> (mensualmente)
            </label>
        </div>
       -->
        <label for="pag_cred">�Est� pagando alg�n cr�dito?
            <input type="radio" name="pag_cred" id="siPC" value="1" <?php if($infoEcon['pag_cred']=='1') echo "CHECKED";?>/>Si
            <input type="radio" name="pag_cred" id="noPC" value="0" <?php if($infoEcon['pag_cred']=='0' || !isset($infoEcon)) echo "CHECKED";?>/>No
        </label>
        <div id="pagando_cred">
            <label for="pag_cred_monto">�Cu�nto?
                $<input class="input-text" type="text" name="pag_cred_monto" id="pag_cred_monto" size="3" maxlength="10" value="<?php echo $infoEcon['pag_cred_monto'];?>" /> (mensualmente)
            </label>
        </div>
        <label for="gastos_fam">�A cu�nto ascienden sus gastos familiares?
                $<input class="input-text" type="text" name="gastos_fam" id="gastos_fam" size="4" maxlength="10" value="<?php echo $infoEcon['gastos_fam'];?>" /> (mensualmente)
        </label>
        <label for="pen_alim">�Est� pagando alguna pensi�n alimentaria?
            <input type="radio" name="pen_alim" id="siPA" value="1" <?php if($infoEcon['pen_alim']=='1') echo "CHECKED";?>/>Si
            <input type="radio" name="pen_alim" id="noPA" value="0" <?php if($infoEcon['pen_alim']=='0' || !isset($infoEcon)) echo "CHECKED";?>/>No
        </label>
        <div id="pension_alim">
            <label for="pen_alim_monto">�Cu�nto?
                $<input class="input-text" type="text" name="pen_alim_monto" id="pen_alim_monto" size="3" maxlength="10" value="<?php echo $infoEcon['pen_alim_monto'];?>" /> (mensualmente)
            </label>
        </div>
    </div>
    <div class="col50 f-right">
        <label for="ing_mensuales">�A cu�nto ascienden sus ingresos mensuales?
            $<input class="input-text" type="text" name="ing_mensuales" id="ing_mensuales" size="4" maxlength="10" value="<?php echo $infoEcon['ing_mensuales'];?>" />
        </label>
        <label for="inv_semanal">�A cu�nto asciende la inversi�n <b>mensual</b> en su negocio?
            $<input class="input-text" type="text" name="inv_semanal" id="inv_semanal" size="4" maxlength="10" value="<?php echo $infoEcon['inv_semanal'];?>" />
        </label>
    </div>
</fieldset>
<fieldset>
    <legend>Datos del cr�dito</legend>
    <label for="monto_sol">Monto solicitado
        $<input class="input-text" type="text" name="monto_sol" id="monto_sol" size="6" maxlength="10" value="<?php echo $infoEcon['monto_sol'];?>" />
    </label>
</fieldset>
<input class="input-submit" type="submit" name="info_econom" value="Guardar y continuar">
</form>