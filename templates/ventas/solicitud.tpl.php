<?php
if(isset($_SESSION['acreditado'][0]) && $_SESSION['opcCred']['tipo'] == "0" && !isset($_GET['editar'])){
    $acreditado = false;
    $submit = "solidario";
    $title = "Datos del Obligado Solidario";
}
else{
    $acreditado = true;
    $submit = "acreditado";
    $title = "Datos del Acreditado";
}

if(isset($_GET['editar']) && $_GET['editar'] =='acreditado')
	$acred = $_SESSION['acreditado'][$_GET['num']]['info_personal'];
elseif(isset($_GET['editar']) && $_GET['editar'] =='solidario'){
	$acreditado = false;
	$submit = "solidario";
	$title = "Datos del Obligado Solidario";
	$acred = $_SESSION['solidario'];
}
//Tomamos el numero del ultimo acreditado para el caso de que ya exista saber cual voy a cargar
if(!isset($_SESSION['acreditado']))
	$last = 0;
else{
	if(isset($_GET['editar']))
		$last = $_GET['num'];
	else
		$last = lastIndex($_SESSION['acreditado']) + 1;
}
?>
<h1><?php echo $title; ?></h1>
<?php if($_SESSION['opcCred']['tipo'] == "1"){?>
	<h4>Acreditado <?php  echo ($last + 1)." de ".$_SESSION['opcCred']['num_int'];?></h4>
<?php }?>
<form id="solicitud" action="ventas.php?content=nuevo" method="post">
<input type="hidden" name="num" id="num" value="<?php  echo $last;?>">

<?php if(isset($_GET['editar']) && $_GET['editar'] =='acreditado') { ?>
    <input type="hidden" name="editar" value="<?php echo $_GET['num'];?>" />
<?php } ?>
<fieldset>
    <legend>Informaci�n personal</legend>
    <?php if(isset($acred['id_persona'])) {?>
    	<input type="hidden" name="id_persona" value="<?php echo $acred['id_persona']; ?>" />
    <?php }?>
    <div class="col50">
        <label for="apellidoP">Apellido Paterno
            <input class="input-text solicitud" type="text" name="apellidoP" id="apellidoP" size="30" value="<?php echo $acred['apellidoP'];?>" <?php if(isset($acred['id_persona'])) echo "READONLY"; ?>/>
        </label>
        <label for="apellidoM">Apellido Materno
            <input class="input-text solicitud" type="text" name="apellidoM" id="apellidoM" size="30" value="<?php echo $acred['apellidoM'];?>" <?php if(isset($acred['id_persona'])) echo "READONLY"; ?>/>
        </label>
        <label for="nombres">Nombres
            <input class="input-text solicitud" type="text" name="nombres" id="nombres" size="40" value="<?php echo $acred['nombres'];?>" <?php if(isset($acred['id_persona'])) echo "READONLY"; ?>/>
        </label>
        <label for="nacimiento">Fecha de nacimiento
            <input class="input-text solicitud" type="text" name="nacimiento" id="nacimiento" size="30" value="<?php echo $acred['nacimiento'];?>" <?php if(isset($acred['id_persona'])) echo "READONLY"; ?>/>
        </label>
        <label for="edad">Edad:
            <input class="input-text" type="text" name="edad" id="edad" size="2" value="<?php echo $acred['edad'];?>" READONLY/>
        </label>
    </div>
    <div class="col50 f-right">
        <label for="sexo">Sexo
            <input type="radio" name="sexo" id="hombre" value="H" <?php if($acred['sexo']=='H') echo "CHECKED";?>/>Hombre
            <input type="radio" name="sexo" id="mujer" value="M"  <?php if($acred['sexo']=='M') echo "CHECKED";?>/>Mujer
        </label>
        <label for="rfc">RFC
            <input class="input-text" type="text" name="rfc" id="rfc" size="30" value="<?php echo $acred['rfc'];?>"/>
        </label>
          <label for="curp">CURP
            <input class="input-text" type="text" name="curp" id="curp" size="30" value="<?php echo $acred['curp'];?>"/>
        </label>
        <label for="edocivil">Estado Civil
            <select name="edocivil" id="edocivil">
                <option <?php if($acred['edocivil']== "Soltero") echo 'selected="selected"';?> value="Soltero">Soltero</option>
                <option <?php if($acred['edocivil']== "Casado") echo 'selected="selected"';?> value="Casado">Casado</option>
                <option <?php if($acred['edocivil']== "Union Libre") echo 'selected="selected"';?> value="Union Libre" >Uni�n Libre</option>
                <option <?php if($acred['edocivil']== "Divorciado") echo 'selected="selected"';?> value="Divorciado">Divorciado</option>
                <option <?php if($acred['edocivil']== "Viudo") echo 'selected="selected"';?> value="Viudo">Viudo</option>
            </select>
        </label>
        <div id="conyuge">
            <label for="t_conyuge">�Trabaja su conyuge?
                <input type="radio" name="t_conyuge" id="siC" value="1" <?php if($acred['t_conyuge']=='1') echo "CHECKED";?>/>Si
                <input type="radio" name="t_conyuge" id="noC" value="0" <?php if($acred['t_conyuge']=='0' || !isset($acred)) echo "CHECKED";?>/>No
            </label>
        </div>
        <label for="dep_econom">Dependientes econ�micos
            <select name="dep_econom" id="dep_econom">
                <option <?php if($acred['dep_econom']== "0") echo 'selected="selected"';?> value="0">0</option>
                <option <?php if($acred['dep_econom']== "1") echo 'selected="selected"';?> value="1">1</option>
                <option <?php if($acred['dep_econom']== "2") echo 'selected="selected"';?> value="2">2</option>
                <option <?php if($acred['dep_econom']== "3") echo 'selected="selected"';?> value="3">3</option>
                <option <?php if($acred['dep_econom']== "4") echo 'selected="selected"';?> value="4">4</option>
                <option <?php if($acred['dep_econom']== "5") echo 'selected="selected"';?> value="5">5</option>
                <option <?php if($acred['dep_econom']== "6") echo 'selected="selected"';?> value="6">6</option>
                <option <?php if($acred['dep_econom']== "7") echo 'selected="selected"';?> value="7">7</option>
                <option <?php if($acred['dep_econom']== "8") echo 'selected="selected"';?> value="8">8</option>
                <option <?php if($acred['dep_econom']== "9") echo 'selected="selected"';?> value="9">9</option>
                <option <?php if($acred['dep_econom']== "10") echo 'selected="selected"';?> value="10">10</option>
            </select>
        </label>
        <!-- <input id="listaNegra" type="button" value="Lista negra" />-->
    </div>
</fieldset>
<fieldset id="listaNegraView" style="display:none;">
<legend>Lista Negra</legend>
<?php include('listanegra.tpl.php'); ?>
</fieldset>
<?php if($acreditado) { ?>
<fieldset>
    <legend>Informaci�n adicional</legend>
    <div class="col50">
        <label for="enfermedad_solic">�Usted tiene alguna enfermedad cr�nico degenerativa?<br />
            <input type="radio" name="enfermedad_solic" id="siES" value="1" <?php if($acred['enfermedad_solic']=='1') echo "CHECKED";?>/>Si
            <input type="radio" name="enfermedad_solic" id="noES" value="0" <?php if($acred['enfermedad_solic']=='0' || !isset($acred)) echo "CHECKED";?>/>No
        </label>
        <label for="enfermedad_fam">�Su conyuge o sus dependientes econ�micos tienen alguna enfermedad cr�nico degenerativa?<br />
            <input type="radio" name="enfermedad_fam" id="siEF" value="1" <?php if($acred['enfermedad_fam']=='1') echo "CHECKED";?>/>Si
            <input type="radio" name="enfermedad_fam" id="noEF" value="0" <?php if($acred['enfermedad_fam']=='0' || !isset($acred)) echo "CHECKED";?>/>No
        </label>
        <label for="enfermedad_desc">Describa la enfermedad
            <input class="input-text" type="text" name="enfermedad_desc" id="enfermedad_desc" size="30" value="<?php echo $acred['enfermedad_desc'];?>" />
        </label>
    </div>
    <div class="col50 f-right">
        <label for="nivel_estudios">�Cu�l es su nivel de estudios?
            <select name="nivel_estudios" id="nivel_estudios">
                <option <?php if($acred['nivel_estudios']=='Ninguno') echo 'selected="selected"';?>  value="Ninguno">Ninguno</option>
                <option <?php if($acred['nivel_estudios']=='Primaria') echo 'selected="selected"';?>  value="Primaria">Primaria</option>
                <option <?php if($acred['nivel_estudios']=='Secundaria') echo 'selected="selected"';?>  value="Secundaria">Secundaria</option>
                <option <?php if($acred['nivel_estudios']=='Preparatoria') echo 'selected="selected"';?>  value="Preparatoria">Preparatoria</option>
                <option <?php if($acred['nivel_estudios']=='Licenciatura') echo 'selected="selected"';?>  value="Licenciatura">Licenciatura</option>
                <option <?php if($acred['nivel_estudios']=='Posgrado') echo 'selected="selected"';?>  value="Posgrado">Posgrado</option>
            </select>
        </label>
        <label for="ant_domicilio">Antig�edad en su domicilio
            <input class="input-text" type="text" name="ant_domicilio" id="ant_domicilio" size="2" maxlength="2" value="<?php echo $acred['ant_domicilio'];?>" /> a�os
        </label>
    </div>
</fieldset>
<?php } ?>
<fieldset>
    <legend>Domicilio</legend>
    <div class="col50">
        <label for="calle">Calle
            <input class="input-text" type="text" name="calle" id="calle" size="40" value="<?php echo $acred['calle'];?>" />
        </label>
         <label for="numeros">
            Ext: <input class="input-text" type="text" name="num_ext"  size="3" value="<?php echo $acred['num_ext'];?>" />
            Int: <input class="input-text" type="text" name="num_int"  size="3" value="<?php echo $acred['num_int'];?>" />
            Dpto: <input class="input-text" type="text" name="dpto"  size="3" value="<?php echo $acred['dpto'];?>" />
            Mza: <input class="input-text" type="text" name="mza"  size="3" value="<?php echo $acred['mza'];?>" />
            Lt: <input class="input-text" type="text" name="lote"  size="3" value="<?php echo $acred['lote'];?>" />
        </label>
        <label for="colonia">Colonia
            <input class="input-text" type="text" name="colonia" id="colonia" size="30" value="<?php echo $acred['colonia'];?>" />
        </label>
        <label for="estado">Estado
            <?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'),$acred['estado']); ?>
        </label>
    </div>
    <div class="col50 f-right">
        <div id="deleg">
        <?php if(isset($acred['estado']) && isset($acred['delegacion'])){
        	$delegaciones = $ubicacionDB->getDelegacionesByEstado($acred['estado']);
        	echo "<label for='delegacion'>Delegaciones ";
        	getComboBox($delegaciones, 'delegacion', 'delegacion', array('value'=>'id_delegacion','text'=>'nombre_del'),$acred['delegacion']);
        	echo "</label>";
        }
        ?>
        </div>
        <label for="cp">C�digo Postal
            <input class="input-text" type="text" name="cp" id="cp" size="15" value="<?php echo $acred['cp'];?>" />
        </label>
        <label for="residencia">Residencia
            <select name="residencia" id="residencia">
                <option <?php if($acred['residencia']=='Propia') echo 'selected="selected"';?> value="Propia">Propia</option>
                <option <?php if($acred['residencia']=='Rentada') echo 'selected="selected"';?> value="Rentada">Rentada</option>
                <option <?php if($acred['residencia']=='Pagandose') echo 'selected="selected"';?> value="Pagandose">Pag�ndose</option>
                <option <?php if($acred['residencia']=='Prestada') echo 'selected="selected"';?> value="Prestada">Prestada</option>
                <option <?php if($acred['residencia']=='Vive con familiares') echo 'selected="selected"';?> value="Vive con familiares">Vive con familiares</option>
            </select>
        </label>
        <p>*Si no demuestra con predial a su nombre marcar como Prestada</p>
    </div>
</fieldset>
<fieldset>
    <legend>Contacto</legend>
    <label for="tel_casa">Tel�fono de casa
        <input class="input-text" type="text" name="tel_casa" id="tel_casa" size="20" value="<?php echo $acred['tel_casa'];?>" />
    </label>
    <label for="celular">Celular
        <input class="input-text" type="text" name="celular" id="celular" size="20" value="<?php echo $acred['celular'];?>" />
    </label>
    <label for="email">Email
        <input class="input-text" type="text" name="email" id="email" size="20" value="<?php echo $acred['email'];?>" />
    </label>
</fieldset>
<input class="input-submit" type="submit" name="<?php echo $submit; ?>" id="button" value="Guardar y continuar">
</form>
<div id="exists"></div>