<h1>Referencias personales</h1>
<form action="ventas.php?content=nuevo" method="post">
<?php if(isset($_GET['editar'])) { ?>
    <input type="hidden" name="editar" value="<?php echo $_GET['editar'];?>" />
<?php } ?>
<fieldset>
    <legend>Informaci�n de la referencia</legend>
    <div class="col50">
        <label for="nombres">Nombres
            <input class="input-text" type="text" name="nombres" id="nombres" size="40" value="<?php echo $referencia['nombres'];?>"/>
        </label>
        <label for="apellidoP">Apellido Paterno
            <input class="input-text" type="text" name="apellidoP" id="apellidoP" size="30" value="<?php echo $referencia['apellidoP'];?>"/>
        </label>
        <label for="apellidoM">Apellido Materno
            <input class="input-text" type="text" name="apellidoM" id="apellidoM" size="30" value="<?php echo $referencia['apellidoM'];?>"/>
        </label>
    </div>
    <div class="col50 f-right">
        <label for="parentesco">Parentesco
            <input class="input-text" type="text" name="parentesco" id="parentesco" size="15" value="<?php echo $referencia['parentesco'];?>"/>
        </label>
    </div>
</fieldset>
<fieldset>
    <legend>Domicilio</legend>
    <div class="col50">
        <label for="calle">Calle y n�mero
            <input class="input-text" type="text" name="calle" id="calle" size="40" value="<?php echo $referencia['calle'];?>"/>
        </label>
        <label for="colonia">Colonia
            <input class="input-text" type="text" name="colonia" id="colonia" size="30" value="<?php echo $referencia['colonia'];?>"/>
        </label>
        <label for="estado">Estado
            <?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'),$referencia['estado']); ?>
        </label>
    </div>
    <div class="col50 f-right">
        <div id="deleg">
        <?php if(isset($referencia['estado']) && isset($referencia['delegacion'])){
        	$delegaciones = $ubicacionDB->getDelegacionByEstado($referencia['estado']);
        	echo "<label for='delegacion'>Delegaciones ";
        	getComboBox($delegaciones, 'delegacion', 'delegacion', array('value'=>'id_delegacion','text'=>'nombre_del'),$referencia['delegacion']);
        	echo "</label>";
        }
        ?>
        </div>
        <label for="cp">C�digo Postal
            <input class="input-text" type="text" name="cp" id="cp" size="15" value="<?php echo $referencia['cp'];?>"/>
        </label>
    </div>
</fieldset>
<fieldset>
    <legend>Contacto</legend>
    <label for="tel_casa">Tel�fono de casa y/o trabajo
        <input class="input-text" type="text" name="tel_casa" id="tel_casa" size="20" value="<?php echo $referencia['tel_casa'];?>"/>
    </label>
    <label for="celular">Celular
        <input class="input-text" type="text" name="celular" id="celular" size="20" value="<?php echo $referencia['celular'];?>"/>
    </label>
</fieldset>
<input class="input-submit" type="submit" name="referencia" value="Guardar y continuar">
</form>