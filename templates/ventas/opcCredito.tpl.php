<h1>Datos del cr�dito</h1>
<?php if(isset($_GET['msg'])){?>
<p class="msg done"><?php echo "Cr�dito guardado con �xito."?></p>
<?php } ?>
<form action="ventas.php?content=nuevo" method="post">
<fieldset>
    <legend>Informaci�n del cr�dito</legend>
    <div class="col50">
        <label for="producto">Producto:
            <select name="producto" id="producto">
                <option <?php if($_SESSION['opcCred']['producto']== "0") echo 'selected="selected"';?> value="0">CrediMarchante</option>
                <option <?php if($_SESSION['opcCred']['producto']== "1") echo 'selected="selected"';?> value="1">CrediN�mina</option>
                <option <?php if($_SESSION['opcCred']['producto']== "2") echo 'selected="selected"';?>value="2">CrediHipoteca</option>
            </select> 
        </label>
        <label for="tipo">Tipo:
            <select name="tipo" id="tipo">
                <option <?php if($_SESSION['opcCred']['tipo']== "0") echo 'selected="selected"';?> value="0">Individual</option>
                <option <?php if($_SESSION['opcCred']['tipo']== "1") echo 'selected="selected"';?> value="1">Grupal</option>
            </select> 
        </label>
        <label for="tasa">Tasa de inter�s:
        	<?php getComboBox($intOrd, 'tasa', 'tasa', array('value'=>'id','text'=>'tasa'), !isset($_SESSION['opcCred']['tasa'])?"70":$_SESSION['opcCred']['tasa']); ?>
        </label>
        <label for="periodo">Periodo:
            <select name="periodo" id="periodo">
                <option <?php if($_SESSION['opcCred']['periodo']== "s") echo 'selected="selected"';?> value="s">Semanal</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "q") echo 'selected="selected"';?> value="q">Quincenal</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "m") echo 'selected="selected"';?> value="m">Mensual</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "b") echo 'selected="selected"';?> value="b">Bimestral</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "t") echo 'selected="selected"';?> value="t">Trimestral</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "u") echo 'selected="selected"';?> value="u">Quinquenal</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "e") echo 'selected="selected"';?> value="e">Semestral</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "a") echo 'selected="selected"';?> value="a">Anual</option>
            </select> 
        </label>
    </div>
    <div class="col50 f-right">
       <label for="plazo">Plazo mensual:
       	<?php getComboBox($plazos, 'plazo', 'plazo', array('value'=>'id','text'=>'plazo'), $_SESSION['opcCred']['plazo']); ?>
        </label>
        <label for="vendedor">Vendedor
            <?php getComboBox($vendedores, 'vendedor', 'vendedor', array('value'=>'id','text'=>'nombre')); ?>
        </label>
        <label for="zona">Zona
            <?php getComboBox($zonas, 'zona', 'zona', array('value'=>'id_zona','text'=>'zona')); ?>
        </label>
    </div>
</fieldset>
<div id="opcGpo">
    <fieldset>
        <legend>Datos del grupo</legend>
        <div class="col50">
            <label for="grupo">Nombre del grupo:
                <input class="input-text" type="text" name="grupo" id="grupo" size="30" value="<?php echo $_SESSION['opcCred']['grupo'];?>"/>
            </label>
        </div>
        <div class="col50 f-right">
            <label for="integrantes">Numero de integrantes del grupo
                <select name="num_int" id="num_int">
                <?php for($i=2;$i<=40;$i++) {
                    if($_SESSION['opcCred']['num_int'] == $i)
                        echo "<option selected='selected' value='{$i}'>{$i}</option>";
                    else
                        echo "<option value='{$i}'>{$i}</option>";
                } ?>
            </select> 
            </label>
        </div>
    </fieldset>
</div>
<?php if(isset($_GET['editar'])) { ?>
    <input type="hidden" name="editar" value="1" />
<?php } ?>
<input class="input-submit" type="submit" name="opcCred" value="Guardar y continuar">
</form>
