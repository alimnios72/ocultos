<h1>An�lisis de cr�ditos</h1>
<?php if(isset($errormsg)){?>
<p class="msg warning"><?php echo $errormsg; ?></p>
<?php } ?>
<?php if(isset($_GET['msg'])){?>
<p class="msg done"><?php echo $successmsg; ?></p>
<?php } ?>
<form action="ventas.php?content=analisis" method="post">
<fieldset>
    <legend>Cr�ditos pendientes</legend>
    <?php if(!empty($creditos)) {?>
        <table>
        <tbody>
            <tr>
            	<th></th>
                <th>Producto</th>
                <th>Tipo</th>
                <th>Nombre/Grupo</th>
                <th>Status</th>
                <th>Fecha de captura</th>
                <th>Vendedor</th>
                <th>Zona</th>
                <th>Acci�n</th>
            </tr>
            <?php $c = true; ?>
            <?php foreach($creditos as $credito) { ?>
            <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
            	<td><input type="radio" name="creditos" value="<?php echo $credito["id_credito"]; ?>" /></td>
                <td><?php echo $producto[$credito["producto"]]; ?></td>
                <td><?php echo $tipo[$credito["tipo"]]; ?></td>
                <td><?php echo $credito["nombreGrupo"]; ?></td>
                <td><?php echo $status[$credito["status"]]; ?></td>
                <td><?php echo strftime("%a %e de %b de %Y", strtotime($credito["fecha_captura"])); ?></td>
                <td><?php echo $credito["nombre"]; ?></td>
                <td><?php echo $credito["nombre_zona"]; ?></td>
                <td><a href="#" id="credito_<?php echo "{$credito["id_credito"]}_{$credito["tipo"]}"; ?>">Ver</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php }else {?>
        <p class="msg info">No existen solicitudes de cr�dito pendientes.</p>
    <?php } ?>
</fieldset>
<fieldset>
	<legend>Detalles</legend>
	<div id="resumenCred"></div>
</fieldset>
<fieldset>
	<legend>Citas</legend>
	<div id="dialog-form" title="Nueva Cita">
	<fieldset>
		<input type="hidden" name="id_credito" id="id_credito" value="" />
		<input type="hidden" name="fecha" id="fecha" value="" />
		<input type="hidden" name="citaI" id="citaI" value="" />
		<input type="hidden" name="citaF" id="citaF" value="" />
		<p>La cita se agendar� el <span id="fechaText"></span>,  de <span id="citaInicio"></span> a <span id="citaFin"></span></p>
		<p>�Est� de acuerdo?</p>
		<input type="submit" id="accept" name="aceptar" value="Aceptar" />
		<input type="submit" id="cancel" name="cancelar" value="Cancelar">
	</fieldset>
	</div>
	<div id="weekCalendar"><?php include('procesos/citas.php');?></div>
</fieldset>
<input class="input-submit" type="submit" name="aprob" value="Aprobar">
<input class="input-submit" type="submit" name="rech" value="Rechazar">
</form>