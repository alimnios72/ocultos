<h3>Datos del cr�dito</h3>
 <div class="col50">
        <p>Producto: <b><?php echo $producto[$credito['producto']]; ?></b></p>
        <p>Tipo: <b><?php echo $tipo[$credito['tipo']]; ?></b></p>
        <p>Tasa de Inter�s: <b><?php echo $credito['tasa_interes']; ?>%</b></p>
        <p>Periodo: <b><?php echo $credito['periodo']; ?></b></p>
</div>
<div class="col50 f-right">
        <p>Plazo: <b><?php echo $credito['plazo']; ?> MESES</b></p>
        <p>Banco: <b><?php echo $credito['banco']; ?></b></p>
        <p>Vendedor: <b><?php echo $credito['vendedor']; ?></b></p>
        <p>Zona: <b><?php echo $credito['nombre_zona'] ?></b></p>
</div>
<?php if($credito['tipo'] == 1){?>
	<h3>Datos del grupo</h3>
	<p>Nombre del grupo: <b><?php echo $credito['nombre']; ?> </b></p>
	<p>N�mero de integrantes: <b><?php echo $credito['num_int']; ?> </b></p>
	<p>Monto total: <b><?php echo "$".number_format($credito['Total']); ?></b></p>
	<h3>Integrantes</h3>
	<?php if(!empty($acreditados)){ foreach($acreditados as $acreditado){
			$repre = ($credito['id_representante'] == $acreditado['id_persona']) ? "<span style='color:red;'> (Representante)</span>" : "";
		?>
		<div class="col50">
	        <p>Nombre: <a href="administracion.php?content=datosPersona&persona=<?php echo $acreditado['id_persona']; ?>" target="_blank"><b><?php echo $acreditado['acreditado'];?></b></a><?php echo $repre; ?></p>
		</div>
		<div class="col50 f-right">
		      <p>Monto preaprobado: <b><?php echo "$".number_format($acreditado['cantidad']); ?></b></p>
		</div>
	<?php } }?>
<?php }?>
<?php if($credito['tipo'] == 0){?>
	 <h3>Acreditado</h3>
	 <div class="col50">
	        <p>Nombre: <a href="administracion.php?content=datosPersona&persona=<?php echo $credito['id_persona']; ?>" target="_blank"><b><?php echo $credito['acreditado']; ?></b></a></p>
	</div>
	<div class="col50 f-right">
	       <p>Monto preaprobado: <b><?php echo "$".number_format($credito['cantidad']); ?></b></p>
	</div>
	<h3>Solidario</h3>
	<p>Nombre: <a href="administracion.php?content=datosPersona&persona=<?php echo $credito['id_solidario']; ?>" target="_blank"><b><?php echo $credito['obligado']; ?></b></a></p>
<?php }?>
<h3>Tabla de amortizaci�n</h3>
<?php  include(ROOT.RUTA_TPL.'amortizacion.tpl.php'); ?>