<h1>Datos del cr�dito</h1>
<?php if(isset($_GET['msg'])){?>
<p class="msg done"><?php echo "Cr�dito guardado con �xito."?></p>
<?php } ?>
<form action="ventas.php?content=viejo" method="post">
<fieldset>
    <legend>Informaci�n del cr�dito</legend>
    <div class="col50">
        <label for="producto">Producto:
            <select name="producto" id="producto">
                <option <?php if($_SESSION['opcCred']['producto']== "0") echo 'selected="selected"';?> value="0">CrediMarchante</option>
                <option <?php if($_SESSION['opcCred']['producto']== "1") echo 'selected="selected"';?> value="1">CrediN�mina</option>
                <option <?php if($_SESSION['opcCred']['producto']== "2") echo 'selected="selected"';?>value="2">CrediHipoteca</option>
            </select> 
        </label>
        <label for="tipo">Tipo:
            <select name="tipo" id="tipo">
                <option <?php if($_SESSION['opcCred']['tipo']== "0") echo 'selected="selected"';?> value="0">Individual</option>
                <option <?php if($_SESSION['opcCred']['tipo']== "1") echo 'selected="selected"';?> value="1">Grupal</option>
            </select> 
        </label>
        <label for="tasa">Tasa de inter�s:
        	<?php getComboBox($intOrd, 'tasa', 'tasa', array('value'=>'id','text'=>'tasa'), !isset($_SESSION['opcCred']['tasa'])?"70":$_SESSION['opcCred']['tasa']); ?>
        </label>
        <label for="periodo">Periodo:
            <select name="periodo" id="periodo">
                <option <?php if($_SESSION['opcCred']['periodo']== "s") echo 'selected="selected"';?> value="s">Semanal</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "q") echo 'selected="selected"';?> value="q">Quincenal</option>
                <option <?php if($_SESSION['opcCred']['periodo']== "m") echo 'selected="selected"';?> value="m">Mensual</option>
            </select> 
        </label>
    </div>
    <div class="col50 f-right">
       <label for="plazo">Plazo mensual:
            <select name="plazo" id="plazo">
                <option <?php if($_SESSION['opcCred']['plazo']== "4") echo 'selected="selected"';?> value="4">4</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "6") echo 'selected="selected"';?> value="6">6</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "8") echo 'selected="selected"';?> value="8">8</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "10") echo 'selected="selected"';?> value="10">10</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "12") echo 'selected="selected"';?> value="12">12</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "18") echo 'selected="selected"';?> value="18">18</option>
                <option <?php if($_SESSION['opcCred']['plazo']== "36") echo 'selected="selected"';?> value="36">36</option>
            </select> 
        </label>
        <label for="vendedor">Vendedor
            <?php getComboBox($vendedores, 'vendedor', 'vendedor', array('value'=>'id','text'=>'nombre')); ?>
        </label>
        <label for="zona">Zona
            <?php getComboBox($zonas, 'zona', 'zona', array('value'=>'id_zona','text'=>'zona')); ?>
        </label>
    </div>
</fieldset>
<div id="opcGpo">
    <fieldset>
        <legend>Datos del grupo</legend>
        <div class="col50">
            <label for="grupo">Nombre del grupo:
                <input class="input-text" type="text" name="grupo" id="grupo" size="30" value="<?php echo $_SESSION['opcCred']['grupo'];?>"/>
            </label>
        </div>
        <div class="col50 f-right">
            <label for="integrantes">Numero de integrantes del grupo
                <select name="num_int" id="num_int">
                <?php for($i=2;$i<=50;$i++) {
                    if($_SESSION['opcCred']['num_int'] == $i)
                        echo "<option selected='selected' value='{$i}'>{$i}</option>";
                    else
                        echo "<option value='{$i}'>{$i}</option>";
                } ?>
            </select> 
            </label>
        </div>
    </fieldset>
</div>
<?php if(isset($_GET['editar'])) { ?>
    <input type="hidden" name="editar" value="1" />
<?php } ?>
<input class="input-submit" type="submit" name="opcCred" value="Guardar y continuar">
</form>