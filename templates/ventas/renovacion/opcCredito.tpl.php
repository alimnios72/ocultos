<h1>Opciones del cr�dito</h1>
<?php if(isset($_GET['repetido'])){?>
	<p class="msg error"><?php echo $_GET['repetido'];?></p>
<?php }?>
<?php if(!empty($errores)){
	foreach($errores as $error){?>
	<p class="msg error"><?php echo $error;?></p>
<?php } }?>
<form action="ventas.php?content=renovacion" method="post">
<fieldset>
	<legend>Informaci�n del cr�dito</legend>
	<?php if($credito['tipo'] == "1"){?>
	<label for="grupo" style="border:dotted #FF99FF 2px;">Grupo:
		<input type="text" class="input-text" name="grupo" value="<?php echo $credito['nombreGrupo'];?>" DISABLED/>
	</label>
	<?php } ?>
	<div class="col50">
		<label for="producto">Producto:
			<input type="text" class="input-text" name="producto" value="<?php echo $producto[$credito['producto']];?>" size="13" DISABLED/>
		</label>
		<label for="tipo">Tipo:
			<select name="tipo">
				<option value="0" <?php if($credito['tipo'] == "0") echo 'selected';?>>Individual</option>
				<option value="1" <?php if($credito['tipo'] == "1") echo 'selected';?>>Grupal</option>
			</select>
			<!-- </select><input type="text" class="input-text" name="grupo" value="<?php echo $tipo[$credito['tipo']];?>" size="10" DISABLED/> -->
		</label>
		<label for="tipo">Tasa de inter�s:
			<?php echo getComboBox($intOrd,'tasa','tasa',array('value'=>'id','text'=>'tasa'),$credito['tasa_interes']); ?>
		</label>
		<label for="periodo">Periodo:
	        <select name="periodo" id="periodo">
	            <option value="s" <?php if($credito['periodo'] == "s") echo 'selected';?>>Semanal</option>
	            <option value="q" <?php if($credito['periodo'] == "q") echo 'selected';?>>Quincenal</option>
	            <option value="m" <?php if($credito['periodo'] == "m") echo 'selected';?>>Mensual</option>
               <option value="b" <?php if($credito['periodo'] == "b") echo 'selected';?>>Bimestral</option>
	            <option value="t" <?php if($credito['periodo'] == "t") echo 'selected';?>>Trimestral</option>
               <option value="u" <?php if($credito['periodo'] == "u") echo 'selected';?>>Quinquenal</option>
	            <option value="e" <?php if($credito['periodo'] == "e") echo 'selected';?>>Semestral</option>
	            <option value="a" <?php if($credito['periodo'] == "a") echo 'selected';?>>Anual</option>
	        </select> 
	    </label>
    </div>
    <div class="col50 f-right">
	    <label for="plazo">Plazo mensual:
	        <?php getComboBox($plazos,'plazo','plazo',array('value'=>'id','text'=>'plazo'),$credito['plazo']); ?>
	    </label>
	    <label for="vendedor">Vendedor
	           <?php getComboBox($vendedores, 'vendedor', 'vendedor', array('value'=>'id','text'=>'nombre'), $credito['id_usuario']); ?>
	    </label>
	    <label for="zona">Zona
	            <?php getComboBox($zonas, 'zona', 'zona', array('value'=>'id_zona','text'=>'zona'), $credito['id_zona']); ?>
	    </label>
	</div>
</fieldset>
<fieldset id="integrantes_table">
	<legend>Integrantes</legend>
	<?php include('integrantes.tpl.php');?>
</fieldset>
<?php if($credito['tipo'] == "0"){?>
	<fieldset>
		<legend>Solidario</legend>
		<table>
			<tr>
				<th>Nombre</th>
				<th>Acci�n</th>
			</tr>
			<?php $c = true; ?>
	    	<?php foreach($solidarios as $solidario) { ?>
	    	<tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
				<td><?php echo $solidario['nombre'];?></td>
				<td><a href="ventas.php?content=renovacion&accion=agregarP&editar=solidario">Cambiar</a></td>
			</tr>
			<?php } ?>
		</table>
	</fieldset>
<?php } ?>
<input type="submit" name="saveRenov" id="saveRenov" class="buttonsRenov" value="Guardar" />
<input type="submit" name="cancelRenov" value="Cancelar" id="cancelRenov" class="buttonsRenov" />
</form>
