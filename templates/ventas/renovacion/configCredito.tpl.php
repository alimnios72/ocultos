<h1>Cr�dito a renovar</h1>
<?php if(empty($credito)){?>
	<p class="msg error"><?php echo "No existe cr�dito con ese expediente"?></p>
<?php }else{?>
<form action="ventas.php?content=renovacion" method="post">
	<fieldset>
		<legend>Detalles del cr�dito</legend>
		<div class="col50">
			<p>Expediente: <b><?php echo $credito['expediente'];?></b></p>
			<p>Nombre/Grupo: <b><?php echo $credito['nombreGrupo'];?></b></p>
			<p>Monto del cr�dito: <b><?php echo "$".number_format($credito['montoTotal'],2);?></b></p>
		</div>
		<div class="col50 f-right">
			<p>Tasa de inter�s: <b><?php echo $credito['tasa_interes']."%";?></b></p>
			<p>Plazo: <b><?php echo $credito['plazo']." MESES";?></b></p>
			<p>Periodo: <b><?php echo $credito['periodo'];?></b></p>
		</div>
	</fieldset>
	<fieldset>
		<legend>Balances</legend>
		<div class="col50">
			<h3>Balance del cr�dito</h3>
			<p>Monto: <b><?php echo "$".number_format($monto,2);?></b></p>
			<p>Inter�s: <b><?php echo "$".number_format($balance['interes'],2);?></b></p>
			<p>IVA: <b><?php echo "$".number_format($balance['iva'],2);?></b></p>
			<p>Capital: <b><?php echo "$".number_format($balance['capital'],2);?></b></p>
		</div>
		<div class="col50 f-right">
			<h3>Balance de mora</h3>
			<p>Monto: <b><?php echo "$".number_format($montoMora,2);?></b></p>
			<p>Inter�s: <b><?php echo "$".number_format($balanceMora['interes'],2);?></b></p>
			<p>IVA: <b><?php echo "$".number_format($balanceMora['iva'],2);?></b></p>
		</div>
	</fieldset>
	<input type="hidden" name="id_credito" value="<?php echo $credito['id_credito']; ?>" />
	<input type="hidden" name="montoBalance" value="<?php echo $monto; ?>" />
	<input type="hidden" name="montoMora" value="<?php echo $montoMora; ?>" />
	<input type="submit" class="input-submit" name="renovar" value="Renovar" />
</form>
<?php }?>