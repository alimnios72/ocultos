<h1>Resumen de la renovación</h1>
<?php if(!empty($errores)){
	foreach($errores as $error){?>
	<p class="msg error"><?php echo $error;?></p>
<?php } } ?>
<form action="ventas.php?content=renovacion" method="post">
<div class="col50">
	<?php if($credito['tipo'] == "1"){?><p><b>Grupo:</b> <?php echo $credito['nombreGrupo'];?></p><?php }?>
	<p><b>Producto:</b> <?php echo $producto[$credito['producto']];?></p>
	<p><b>Tipo:</b> <?php echo $tipo[$credito['tipo']];?></p>
	<p><b>Tasa de interés:</b> <?php echo $credito['tasa_interes'];?>%</p>
</div>
<div class="col50 f-right"> 
	<p><b>Periodo:</b> <?php echo $periodo[$credito['periodo']];?></p>
	<p><b>Plazo mensual:</b> <?php echo $credito['plazo'];?></p>
	<p><b>Vendedor:</b> <?php echo $credito['nombre'];?></p>
	<p><b>Zona:</b> <?php echo $credito['nombre_zona'];?></p>
</div>
<table>
    <tbody>
	    <tr>
	    	<?php if($credito['tipo'] == "1"){?><th>Repre</th><?php }?>
	        <th>Nombre</th>
	        <th>Monto aprobado</th>
	    </tr>
	    <?php $c = true; ?>
	    <?php foreach($acreditados as $acreditado) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	    	<?php if($acreditado['checked'] == "true"){?>
	    		<?php if($credito['tipo'] == "1"){?><td><input type="radio" name="representante" value="<?php echo $acreditado['id_persona'];?>" /></td><?php }?>
		    	<td><?php echo strtoupper($acreditado['acreditado']); ?></td>
		    	<td>$<input type="text" class="input-text" name="montos[]" value="<?php echo $acreditado['montoAprobado']; ?>" /></td>
		    <?php } ?>
	    </tr>
    </tbody>
    <?php } ?>
</table>
<?php if($credito['tipo'] == 0){
	foreach($solidarios as $solidario){
		echo "<p><b>Obligado Solidario:</b> {$solidario['nombre']}</p>";		
	}
} ?>

	<input type="submit" name="regRenov" value="Regresar" id="regRenov" class="buttonsRenov" />
	<input type="submit" name="cancelRenov" value="Cancelar" id="cancelRenov" class="buttonsRenov" />
	<input type="submit" name="aproveRenov" value="Aprobar" id="aproveRenov" class="buttonsRenov" />
</form>