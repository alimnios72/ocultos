<?php
    if($_SESSION["opcCred"]["tipo"] == 0)
        $grupo = false;
    else
        $grupo = true;

    $sum = 0;
    $cCredito = $_SESSION['opcCred'];
    $cCredito['esSemana'] = isset($_SESSION['opcCred']['esSemana']) ? 'true' : 'false';
    $cCredito['esMes'] = isset($_SESSION['opcCred']['esMes']) ? 'true' : 'false';
    foreach($_SESSION['acreditado'] as $int){
        $sum += $int['info_econom']['montoPreaprobado'];
        $cCredito['montoTotal'] = $sum;
    }
    $amortizacion = new Amortizacion($cCredito);
    $tablaAm = $amortizacion->getTabla();
    
    $_SESSION['captured'] = 1;
?>
<h1>Resumen del cr�dito</h1>
<h2>Verifica la informaci�n que se muestra a continuaci�n</h2>
<?php if(!empty($errormsg)){
	foreach ($errormsg as $msg){ 
?>
	<p class="msg warning"><?php echo $msg; ?></p>
<?php }  } ?>
<form action="ventas.php?content=nuevo" method="post">
<fieldset>
    <legend>
        <a href="ventas.php?content=nuevo&editar=opcCred">
            <img src="<?php echo RUTA_IMG.'editar.png'; ?>" title="Modificar informaci�n" alt="Editar" />
            <span>Informaci�n del cr�dito</span>
        </a>
    </legend>
    <div class="col50">
        <p>Producto: <b><?php echo $producto[$_SESSION['opcCred']['producto']]; ?></b></p>
        <p>Tipo: <b><?php echo $tipo[$_SESSION['opcCred']['tipo']]; ?></b></p>
        <p>Tasa de Inter�s: <b><?php echo $_SESSION['opcCred']['tasa']; ?>%</b></p>
        <p>Periodo: <b><?php echo $periodo[$_SESSION['opcCred']['periodo']]; ?></b></p>
    </div>
    <div class="col50 f-right">
    	<p>Plazo: <b><?php echo $_SESSION['opcCred']['plazo']; ?> MESES</b></p>
        <p>Vendedor: <b><?php echo searchAssocArray($vendedores, $_SESSION['opcCred']['vendedor'], 'id','nombre'); ?></b></p>
        <p>Zona: <b><?php echo searchAssocArray($zonas, $_SESSION['opcCred']['zona'], 'id_zona','zona'); ?></b></p>
    </div>
</fieldset>
<?php if($grupo){ ?>
    <fieldset>
        <legend>
        	 <a href="ventas.php?content=nuevo&editar=opcCred">
        	 	<img src="<?php echo RUTA_IMG.'editar.png'; ?>" title="Modificar informaci�n" alt="Editar" />
        	 <span>Informaci�n del grupo</span>
        	</a>
        </legend>
        <p>Nombre del Grupo: <b><?php echo $_SESSION['opcCred']['grupo']; ?></b></p>
        <p>N�mero de integrantes: <b><?php echo $_SESSION['opcCred']['num_int']; ?></b></p>
    </fieldset>
<?php } ?>
<fieldset>
    <legend>Acreditado(s)</legend>
    <?php foreach($_SESSION['acreditado'] as $key=>$int) {?>
        <div class="col50">
            <p>
            	<?php if($grupo) {?>
            		Repre: <input type="radio" name="repre" value="<?php echo $key; ?>" <?php if($int['representante']==1) echo "CHECKED";?>/>
            	<?php }?>
             	<a href="ventas.php?content=nuevo&editar=acreditado&num=<?php echo $key; ?>">Nombre completo: </a><b><?php echo "{$int['info_personal']['nombres']} {$int['info_personal']['apellidoP']} {$int['info_personal']['apellidoM']}"; ?></b>
            </p>
        </div>
        <div class="col50 f-right">
            <p>Monto preaprobado: <b><?php echo "$".number_format($int['info_econom']['montoPreaprobado'],2); ?></b></p>
        </div>
    <?php } if($grupo) { ?>
    <div class="col50">
        <p>Monto Total: <b><?php echo "$".number_format($sum,2); ?></b></p>
    </div>
    <?php } ?>
</fieldset>
<?php if(!$grupo){ ?>
    <fieldset>
        <legend>Obligado Solidario</legend>
            <p>
            	<a href="ventas.php?content=nuevo&editar=solidario">Nombre completo: </a>
            	<b><?php echo "{$_SESSION['solidario']['nombres']} {$_SESSION['solidario']['apellidoP']} {$_SESSION['solidario']['apellidoM']}"; ?></b></p>
    </fieldset>
<?php } ?>
<fieldset>
    <legend>Tabla de amortizaci�n</legend>
    <?php  include(RUTA_TPL.'amortizacion.tpl.php'); ?>
</fieldset>
<input class="input-submit" type="submit" name="resumen" value="Guardar cr�dito">
<input class="input-submit" type="button" id="reset" value="Borrar informaci�n">
<!--<input class="input-submit" type="submit" name="incompleto" value="Guardar cr�dito incompleto">-->
</form>