<?php
//Iniciamos la numeraci�n de las p�ginas
$i = $pdf->ezStartPageNumbers(550,20,10,'','{PAGENUM} de {TOTALPAGENUM}',1);

//Creamos un encabezado de p�gina para los datos de RECA y de la empresa
$header = $pdf->openObject();
$pdf->setColor(0.50,0.50,0);
$pdf->addText(510,755,11, "CreaBienestar");
$pdf->addText(400,745,8, "CREA BIENESTAR, S.A. de C.V., SOFOM, E.N.R.");
$pdf->setColor(0.67,1,0.18);
$pdf->addText(460,735,8, "Cr�dito y Bienestar para tu familia");
$pdf->closeObject();
$pdf->addObject($header, "add");
//Creamos un pie de p�gina donde vengan los datos de la empresa.
$footer = $pdf->openObject();
$pdf->setColor(0,0,0);
$pdf->setStrokeColor(0,0,0);
$pdf->setLineStyle(1);
$pdf->line(50,60,562,60);
$pdf->addText(70,50,8, "Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F.");
$pdf->addText(190,40,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdf->closeObject();
if(count($acreditados) < 24)
	$pdf->addObject($footer, "add");
else
	$pdf->addObject($footer, "next");
//Para el t�tulo del pagar�
$title = $pdf->openObject();
$pdf->setColor(0.41,0.55,0.13);
$pdf->filledRectangle(30,709,300,16);
$pdf->setColor(1,1,1);
$pdf->addText(47,713,12, "<b>HOJA DE CONTROL GRUPAL - CrediMarchante</b>");
$pdf->closeObject();
$pdf->addObject($title, "add");

$pdf->ezSetY(690);
//Empezamos a con la informaci�n correspondiente al contrato a 2 columnas
$pdf->ezText("M�xico, Distrito Federal a ".strftime("%d de %B de %Y", strtotime($fechaEntrega))."\n\n",10,array('justification'=>'right'));
$text = "<b>Nombre del \"Grupo\": {$credito['nombre']}</b>\n";
$text .= "<b>N�mero de integrantes: {$numInt}</b>\n";
$text .= "<b>Representante: {$representante['acreditado']}</b>\n";
$text .= "<b>Domicilio: {$representante['calle']}, {$representante['colonia']}</b>\n";
$text .= "<b>Plazo: {$plazoTexto} MESES</b>\n";
$text .= "<b>N�mero de pagos: {$pagosText}</b>\n";
$pdf->ezText($text, 13);

$cols = array('oneCol'=>"No.",'twoCol'=>"Integrante del Grupo",'threeCol'=>"Monto del Cr�dito",'fourCol'=>"Pago ".ucfirst(strtolower($credito['periodo'])));

$options = array('width'=>550,
                 'showHeadings' => 1,
                 'shaded'=>0,
                 'fontSize'=>11,
                 'rowGap'=>3,
                 'showLines'=>2,
                 'protectRows'=>15,
                 /*'cols'=>array( 
                        "oneCol" => array('justification'=>'center','width' => '195'), 
                        "twoCol" => array('justification'=>'center', 'width' => '195'), 
                        "threeCol" => array('justification'=>'center', 'width' => '160'))*/
                );
$pdf->ezTable($listaAcred,$cols,'',$options);
unset($cols);
unset($info);
unset($options);

$cols = array('oneCol'=>"");
$info = array(array('oneCol'=>"DEPOSITO SOLIDARIO {$credito['periodo']} <b>{$pFijo}</b>"));
$options = array('width'=>550,
                 'showHeadings' => 0,
                 'shaded'=>0,
                 'fontSize'=>11,
                 'rowGap'=>8,
                 'cols'=>array( 
                        "oneCol" => array('justification'=>'center')),
                );
$pdf->ezTable($info,$cols,'',$options);
$pdf->ezStopPageNumbers(1,1,$i);
$pdf->setColor(0,0,0);
//Detenemos la inserci�n de encabezado y pie de p�gina para las p�ginas subsecuentes.
$pdf->stopObject($header);
$pdf->stopObject($footer);
$pdf->stopObject($title);
?>