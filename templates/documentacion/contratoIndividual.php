<?php
$pages = 1;
/*$obligados = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($obligados)), 0);
$obligados = array_map('mb_strtoupper',$obligados);
$obgsText = implode(', ',$obligados);*/

//Iniciamos la numeraci�n de las p�ginas
$i = $pdf->ezStartPageNumbers(550,20,10,'','{PAGENUM} de {TOTALPAGENUM}',1);

//Creamos un encabezado de p�gina la primera hoja del contrato
$header = $pdf->openObject();
$pdf->setColor(0.50,0.50,0);
$pdf->addText(50,765,9, "<b>CREA BIENESTAR, S.A. DE C.V. SOFOM E.N.R.</b>");
$pdf->addText(450,765,9, "<b>CREDIMARCHANTE</b>");
$pdf->setColor(0,0,0);
$pdf->addText(400,755,9, "<b>RECA No. 4296-439-008394/01-13161-0711</b>");
$pdf->closeObject();
$pdf->addObject($header, "add");

$pdf->ezSetY(730);
//Empezamos a con la informaci�n correspondiente al contrato a 2 columnas
$pdf->ezColumnsStart(array('gap'=>40,'num'=>2));
$text = "<b>CONTRATO DE APERTURA DE CR�DITO SIMPLE PARA EL PRODUCTO DENOMINADO \"CREDIMARCHANTE\" QUE CELEBRAN, POR UNA PARTE ".
        "CREA BIENESTAR, S.A. de C.V., SOFOM, E.N.R., EN SU CAR�CTER DE ACREDITANTE A QUIEN EN LO SUCESIVO SE LE DENOMINAR� COMO ".
        "\"CreaBienestar\" Y POR OTRA PARTE {$credito['acreditado']}, EN SU CAR�CTER DE ACREDITADO, A QUIEN EN LO SUCESIVO SE LE ".
        "DENOMINAR� COMO EL \"ACREDITADO\", QUE EN SU CONJUNTO SE LES DENOMINAR� COMO \"LAS PARTES\", QUIENES DE CONFORMIDAD HAN ".
        "MANIFESTADO SU PLENA VOLUNTAD EN SUJETARSE AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CLAUSULAS:</b> \n\n";
$pdf->ezText($text, 10, array('justification'=>'left'));
$text = "<b><u>DECLARACIONES:</u></b>\n\n";
$pdf->ezText($text, 10, array('justification'=>'centre'));

$text = "<b>I.-Declara CreaBienestar, por conducto de su representante legal, que:</b> \n\n";
$text .= "a.	Es una sociedad an�nima constituida y en operaci�n de conformidad con las leyes de los Estados Unidos  Mexicanos, ".
        "seg�n lo acredita en los t�rminos de la Escritura P�blica No. 44781 otorgada el 27 de Julio de 2010, ante la fe del Lic. ".
        "Gabriel Benjam�n D�az Soto, Notario P�blico No. 131 del Distrito Federal, y cuyo primer testimonio se encuentra debidamente ".
        "inscrito en el Registro P�blico de la propiedad y del Comercio del Distrito Federal, bajo el folio mercantil No.  422503-1. \n";
$text .= "b.	Tiene su domicilio social en Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 ".
		"M�xico, D.F. y cuenta con la siguiente direcci�n en internet: www.creabienestar.com.mx. \n";
$text .= "c.	Opera como SOFOM, E.N.R., por lo que no requiere de autorizaci�n por parte de la Secretar�a de Hacienda y Cr�dito P�blico ".
        "para constituirse y operar, y no est� sujeta a la supervisi�n y vigilancia de la Comisi�n Nacional Bancaria y de Valores.  \n";
$text .= "d.	Sus representantes cuentan con las facultades suficientes para acudir en su nombre y representaci�n  a la celebraci�n ".
        "y ejecuci�n del presente Contrato de apertura de Cr�dito Simple (en lo sucesivo el \"Contrato\"), facultades que a la fecha no ".
        "les han sido revocadas ni modificadas en forma alguna.   \n";
$text .= "e.	Cuenta con la capacidad jur�dica y econ�mica suficiente para hacer frente a las obligaciones que asume por virtud de este Contrato. \n";
$text .= "f.	Previa revisi�n y an�lisis de la Solicitud de Cr�dito (en lo sucesivo la \"Solicitud\"), as� como de la dem�s informaci�n y ".
        "documentaci�n que fuera proporcionada y presentada por el Acreditado, autoriz� el Cr�dito simple, en moneda nacional (en lo sucesivo ".
        "el \"Cr�dito\") al  Acreditado. \n\n";
        
$text .= "<b>II.-Declara el Acreditado, por su propio derecho que:</b> \n\n";
$text .= "a.	Es una persona f�sica de nacionalidad mexicana, con pleno ejercicio y goce de sus facultades para la celebraci�n del presente Contrato. \n";
$text .= "b.	Sus datos generales son los que han quedado asentados en la Solicitud (Anexo \"A\" del presente Contrato), en la que precisa su ".
        "deseo de obtener un Cr�dito, oblig�ndose bajo los t�rminos y condiciones aqu� estipulados. \n";
$text .= "c.	Que cuenta con el apoyo de un Obligado Solidario, mismo que se obliga conforme al presente CONTRATO, en forma solidaria e ".
        "ilimitadamente a favor de CreaBienestar en todas y cada una de las  obligaciones a cargo del Acreditado consignadas en este instrumento, ".
        "renunciando expresamente a los beneficios de orden, excusi�n y divisi�n de deuda, renunciando al efecto en cuanto pueda favorecerle. \n";
$text .= "d.	Con la firma de este contrato, se obliga a mantener una actividad econ�mica productiva, l�cita y rentable, que provea su subsistencia ".
        "personal y familiar as� como su capacidad de pago y elegibilidad crediticia. \n";
$text .= "e.	La celebraci�n del presente Contrato no es contraria a disposici�n alguna aplicable al Acreditado, ni contraviene o viola Contrato o ".
        "Convenio alguno del que sea parte y no est� en mora en cualquier otra obligaci�n a su cargo.  \n";
$text .= "f.	Que CreaBienestar, previamente a la celebraci�n de este Contrato, le dio a conocer los t�rminos y condiciones aplicables al mismo y ".
        "de los dem�s documentos a suscribir, as� como el <b>Costo Anual Total (CAT)</b> correspondiente al Cr�dito que se le otorga, para fines ".
        "informativos y de comparaci�n exclusivamente.  \n";
$text .= "g.	Toda la informaci�n y documentaci�n que proporcion� a CreaBienestar, a la fecha de firma del presente Contrato y con la Solicitud  es ver�dica. \n\n";
$text .= "Expuestas las anteriores declaraciones, las Partes que suscriben el presente Contrato manifiestan su voluntad de otorgar y sujetarse al tenor de las siguientes: \n\n";
$pdf->ezText($text, 10, array('justification'=>'full'));

$text = "<b><u>CLAUSULAS</u></b>\n\n";
$pdf->ezText($text, 10, array('justification'=>'centre'));

$text = "<b>PRIMERA.- OBJETO DEL CONTRATO.</b> CreaBienestar otorga el Cr�dito a favor del Acreditado por la cantidad que aparece en la Car�tula (Anexo \"B\" del ".
        "presente Contrato) se�alada como Monto del Cr�dito. El \"Acreditado\" esta de acuerdo en el monto se�alado y se obliga a cumplir en la forma, t�rminos ".
        "y condiciones convenidas en el Contrato, a restituir a CreaBienestar el monto del Cr�dito, m�s los intereses e impuestos que se estipulen y generen ".
        "hasta el d�a de la liquidaci�n total del Cr�dito. \n\n";
$text .= "<b>SEGUNDA.- REGISTRO DEL PRESENTE CONTRATO ANTE LA COMISI�N NACIONAL PARA LA PROTECCI�N Y DEFENSA DE LOS USUARIOS DE SERVICIOS FINANCIEROS (CONDUSEF).</b> ".
        "El presente Contrato se encuentra debidamente inscrito en el Registro de Contratos de Adhesi�n de la Comisi�n para la Protecci�n y Defensa de los Usuarios ".
        "de Servicios Financieros (en lo sucesivo, CONDUSEF), con la clave <b>4296-439-008394/01-13161-0711.</b>\n\n";
$text .= "<b>TERCERA.- DESTINO DEL CR�DITO.</b> El Acreditado se obliga a destinar el monto del Cr�dito para aquella actividad econ�mica establecida en el documento ".
        "denominado Solicitud (Anexo \"A\").\n\n";
$text .= "<b>CUARTA.- VIGENCIA.</b> La vigencia del Contrato iniciar� a partir de la fecha de su celebraci�n, la cual aparece en la Car�tula del mismo, y ".
        "terminar� en la fecha de pago de la �ltima parcialidad o cuando sean cumplimentadas, en su totalidad, las obligaciones que en t�rminos del presente ".
        "Contrato contrae el Acreditado. El contrato no puede ser prorrogable.\n\n";
$text .= "<b>QUINTA.- DISPOSICI�N DEL CR�DITO.</b> CreaBienestar depositar� el monto total del Cr�dito en la cuenta bancaria que le indique el Acreditado en ".
        "la Solicitud (Anexo \"A\"), dentro de los dos d�as h�biles bancarios siguientes a la firma del presente contrato e  informar� al Acreditado dentro del ".
        "ese mismo plazo el n�mero de folio o referencia del dep�sito correspondiente, en la direcci�n de correo electr�nico o tel�fono que el Acreditado indique ".
        "en la Solicitud.\n";
$text .= "Por virtud de este Contrato, el Acreditado extiende el recibo m�s amplio y suficiente que en derecho proceda respecto de la disposici�n del Cr�dito ".
        "otorgado por CreaBienestar.\n";
$text .= "En defecto de lo se�alado en el primer p�rrafo de la presente cl�usula y en caso de que el Acreditado as� lo solicitara, CreaBienestar expedir� a ".
        "nombre del Acreditado un cheque (en lo sucesivo el Cheque) con cargo a la cuenta bancaria que CreaBienestar disponga para este efecto, el cual le ser� ".
        "entregado al Acreditado al momento de la firma del presente Contrato. Al momento de la entrega del Cheque, el Acreditado se compromete a firmar en ".
        "beneficio de CreaBienestar una P�liza de Cheque como comprobante de disposici�n del Cr�dito. El Cheque podr� ser cobrado en cualquiera de las sucursales ".
        "de la instituci�n de cr�dito se�alada por CreaBienestar  establecidas en los Estados Unidos Mexicanos.  \n\n";
$text .= "Una vez que el Acreditado dispuso del cr�dito, trat�ndose de transferencia bancaria en el momento de realizar la misma; y en el caso de cheque, al ".
        "hacer efectivo su cobro o al ser depositado por el Acreditado, este queda obligado a realizar el pago de las parcialidades referidas en la Car�tula del ".
        "Contrato (Anexo \"B\2), en las fechas convenidas.  \n\n";
$text .= "En caso de que el Acreditado no presente el cheque para su cobro a la instituci�n de cr�dito que corresponda dentro de los 10 d�as naturales siguientes ".
        "a la fecha de su recepci�n, el presente contrato quedar� sin efectos.   \n\n";
$text .= "En caso de robo o extrav�o del cheque, el Acreditado deber� dar aviso a CreaBienestar v�a telef�nica o  personalmente a la Unidad Especializada de ".
        "Atenci�n al Cliente, cuyos datos se encuentran referidos en la cl�usula D�cima Sexta de este Contrato, cesando la responsabilidad del acreditado respecto ".
        "del cobro del cheque a partir del momento en que  CreaBienestar confirme la recepci�n de dicho aviso, proporcion�ndole un n�mero de reporte.  \n\n";
$text .= "De actualizarse el supuesto a que se refiere el p�rrafo anterior, CreaBienestar llevar� a cabo la revocaci�n del cheque emitido al Acreditado y emitir� un ".
        "nuevo cheque a nombre del Acreditado, en sustituci�n del robado o extraviado. \n\n";
$text .= "El robo o extrav�o del cheque no dar�n lugar a la terminaci�n del contrato. \n\n";
$text .= "Como consecuencia de la disposici�n del Cr�dito que realiza el Acreditado, �ste estar� obligado a pagar a CreaBienestar, a trav�s de parcialidades iguales ".
        "y consecutivas y en el periodo que aparece en la Car�tula, el monto del principal del cr�dito simple, el inter�s ordinario correspondiente y el I.V.A. ".
        "sobre el inter�s ordinario (en lo sucesivo y en conjunto, el \"Monto Total del Cr�dito\"). \n\n";
$text .= "Para el caso de la Clausula D�cima Segunda, primer p�rrafo, del presente contrato, se entiende que el Acreditado dispuso del cr�dito, trat�ndose de ".
        "transferencia bancaria en el momento de realizar la misma; y en el caso de cheque, al hacer efectivo su cobro o al ser depositado. \n\n";
$text .= "<b>SEXTA. PAGAR�.</b> El \"Acreditado\", en este acto, para garantizar la obligaci�n de pago y conjuntamente con el presente Contrato, suscribe ".
        "a su cargo y a favor de CreaBienestar, un Pagar� (Anexo \"C\" del presente Contrato) cuyo importe equivale al Monto Total del Cr�dito y, el que a ".
        "su vez, documenta la disposici�n que realiza el Acreditado sobre el monto del Cr�dito (en lo sucesivo el \"Pagar�\"). CreaBienestar por conducto ".
        "del Contrato, acusa la recepci�n del Pagar� a su entera satisfacci�n. \n\n";
$text .= "CreaBienestar se obliga a devolver el Pagar� al Acreditado, una vez terminado el Contrato y realizado el pago total del Cr�dito a entera satisfacci�n ".
        "de CreaBienestar, y una vez que las Partes firmen la Constancia de Terminaci�n de Contrato.  \n\n";
$text .= "Los pagar�s tambi�n deber�n ser suscritos por uno o dos avales conforme a la pol�tica de CreaBienestar.  \n\n";
$text .= "<b>SEXTA BIS. OBLIGACI�N SOLIDARIA.</b> En este acto, el Acreditado se�ala a {$credito['obligado']} como Obligado Solidario, cuyos datos se se�alan en ".
        "la solicitud de cr�dito, mismo que voluntariamente acepta obligarse conforme al presente CONTRATO, en forma solidaria e ilimitadamente a favor de ".
        "CreaBienestar en todas y cada una de las  obligaciones a cargo del Acreditado consignadas en este instrumento, renunciando expresamente a los beneficios ".
        "de orden, excusi�n y divisi�n de deuda, renunciando al efecto en cuanto pueda favorecerle.  \n\n";
$text .= "El Obligado Solidario conviene en obligarse solidariamente con el \"Acreditado\" tanto para el cumplimiento exacto, completo y en tiempo de las ".
        "obligaciones que contraen en este contrato, as� como para el pago puntual del principal, intereses y de cualquier otra cantidad que deba pagarse ".
        "a CreaBienestar incluyendo accesorios, gastos y costas en caso de juicio, por lo que se obliga a asumir el pago del monto que el \"Acreditado\" no ".
        "cubra, cualquiera que sea la raz�n de dicho incumplimiento.  \n\n";
$text .= "El Obligado Solidario comprende el alcance y consecuencias jur�dicas de conformidad con lo dispuesto por los art�culos 1987, 1988, 1989, 1995, ".
        "1998 y dem�s relativos y aplicables del C�digo Civil Federal vigente u sus correlativos en el Distrito Federal y los Estados de la Rep�blica Mexicana, ".
        "incluyendo la suscripci�n como Avale del pagare a que se refiere la Clausula Sexta de este contrato.   \n\n";
$text .= "<b>S�PTIMA.- FORMA DE PAGO Y TASAS DE INTER�S.</b> El Acreditado se obliga a pagar a CreaBienestar, sin necesidad de previo requerimiento, notificaci�n, ".
        "protesto o aviso alguno:   \n\n";
$text .= "a.	El monto principal del Cr�dito conforme a la cl�usula Primera del presente Contrato, mediante las parcialidades que corresponden al plazo del Cr�dito ".
        "se�alado en la Car�tula.\n";
$text .= "b.	Los intereses ordinarios a raz�n de una tasa fija anual que se se�ala en la Car�tula, sobre el saldo insoluto del Cr�dito. El pago de los intereses no ".
        "podr� ser exigido por adelantado, sino �nicamente por periodos vencidos. CreaBienestar, durante la vigencia de este Contrato, no cobrar� ni modificar� las tasas ".
        "de inter�s del Cr�dito.  El c�lculo de inter�s se realizar� multiplicando la Tasa de Inter�s Anual por el monto del Cr�dito, entre 360 d�as, y el resultado se ".
        "multiplica por los d�as del plazo contratado. \n";
$text .= "c.	El Impuesto al Valor Agregado (IVA) o cualquier otro impuesto vigente que en su caso se genere sobre intereses. \n";
$text .= "d.	El Acreditado,  se obliga a realizar cualquier pago de capital, intereses ordinarios y/o moratorios y de cualquier otro gasto que se genere por ".
        "virtud de este contrato, mediante dep�sitos en la cuenta bancaria se�alada en la solicitud que forma parte integrante de este contrato, o bien mediante ".
        "transferencia electr�nica a dicha cuenta con el n�mero de Clabe se�alada en la solicitud a m�s tardar dentro de los <u>dos d�as h�biles</u> posteriores a la ".
        "fecha en que hubiera correspondido el pago, seg�n lo se�alado en la Tabla de Amortizaci�n (Anexo \"D\" del presente Contrato). De no realizarse el pago ".
        "total de la parcialidad de que se trate en el plazo anteriormente se�alado, el Acreditado cubrir� a CreaBienestar por concepto de intereses moratorios, ".
        "la cantidad que resulten de aplicar  la tasa se�alada en la Caratula (anexo \"B\"), sobre el monto de la parcialidad vencida y no pagada, por cada uno de ".
        "los d�as naturales transcurridos desde la fecha en que el pago debi� realizarse, hasta la fecha  en que efectivamente se realiz�. \n\n";
$text .= "Lo anterior, sin perjuicio del derecho que tendr� CreaBienestar de dar por vencido el presente Contrato en t�rminos de lo dispuesto en la cl�usula D�cima ".
        "Tercera del Contrato.  Los intereses moratorios no podr�n ser exigidos por adelantado, sino �nicamente por periodos vencidos y causar�n el Impuesto al Valor ".
        "Agregado correspondiente.  \n\n";
$text .= "En caso de que alguna de las fechas en que el Acreditado deba realizar un pago ocurra en un d�a inh�bil, el pago respectivo deber� efectuarse el d�a h�bil ".
        "inmediato siguiente.  \n\n";
$text .= "El Acreditado deber� conservar los recibos de las operaciones bancarias que realice en beneficio de CreaBienestar, toda vez que los mismos ser�n el �nico ".
        "medio para demostrar los pagos que el Acreditado ha realizado a favor de CreaBienestar.    \n\n";
$text .= "<b>OCTAVA.- CASO FORTUITO O FUERZA MAYOR.</b> El Acreditado se obliga ante CreaBienestar al debido cumplimiento de las obligaciones que a su cargo se ".
        "encuentran establecidas en el presente Contrato, incluso en presencia de hechos o eventos de caso fortuito y/o de fuerza mayor.    \n\n";
$text .= "<b>NOVENA.- COSTO ANUAL TOTAL DE FINANCIAMIENTO (CAT).</b> El Acreditado manifiesta que se le ha explicado y ha entendido el significado del <b>CAT</b> que para ".
        "este Contrato es equivalente al porcentaje indicado en la Car�tula. Este <b>CAT</b> s�lo es comparable con el <b>CAT</b> de otros cr�ditos denominados en moneda nacional ".
        "a tasa fija y es <b>\"para fines informativos y de comparaci�n exclusivamente\".</b> \n\n";
$text .= "<b>D�CIMA.- ESTADOS DE CUENTA.</b> El Acreditado en este acto exime a CreaBienestar de la obligaci�n de enviarle los Estados de Cuenta a su domicilio, oblig�ndose ".
        "CreaBienestar a que el Acreditado en todo momento tenga acceso a su Estado de Cuenta, en forma gratuita a trav�s de los siguientes medios, a elecci�n del propio Acreditado:\n\n";
$text .= "a.	V�a telef�nica o fax a la Unidad Especializada de Atenci�n al Cliente cuyos datos de atenci�n y contacto se establecen en la cl�usula D�cima Sexta de este ".
        "Contrato, para que le sea entregado el Estado de Cuenta dentro de los ocho d�as h�biles siguientes a la fecha del cierre del periodo mensual de que se trate, en la ".
        "direcci�n de correo electr�nico que el Acreditado indique.\n";
$text .= "b.	En la direcci�n de Internet www.creabienestar.com.mx, mediante la clave confidencial que para tal efecto CreaBienestar le proporcione al Acreditado a trav�s ".
        "de la Unidad Especializada de Atenci�n al Cliente. El Acreditado podr� consultar su estado de cuenta en todo momento; sin embargo, las actualizaciones al estado de ".
        "cuenta se reflejar�n despu�s de ocho d�as h�biles contados a partir del d�a siguiente a la fecha del cierre del periodo mensual de que se trate.\n\n";
$text .= "El Estado de Cuenta indicar�, entre otros aspectos: las cantidades cargadas y abonadas, el saldo insoluto del Cr�dito, as� como los datos necesarios para realizar ".
        "el c�lculo de los intereses a pagar.\n\n";
$text .= "El Acreditado contar� con un periodo de noventa d�as naturales, contando a partir de la fecha de corte de pago en que se presenta el cargo, para formular por ".
        "escrito, cualquier aclaraci�n, inconformidad, reclamaci�n o queja con respecto a la informaci�n contenida en el mismo, ante la Unidad Especializada de Atenci�n ".
        "al Cliente cuyos datos de atenci�n y contacto se establecen en la cl�usula D�cima Sexta de este Contrato; en caso contrario, se entender� que dicha informaci�n ".
        "es aceptada en los t�rminos en los que se refleja en el Estado de Cuenta. La aclaraci�n, inconformidad, reclamaci�n o queja ser� atendida y dictaminada por ".
        "escrito, dentro de los quince d�as h�biles siguientes a la fecha en que sea presentada.\n\n";
$text .= "CreaBienestar se obliga a enviar por escrito la respuesta a  la solicitud de aclaraci�n, inconformidad, reclamaci�n o queja, a la cuenta de correo electr�nico ".
        "proporcionada por el Acreditado en la Solicitud de Cr�dito.\n\n";
$text .= "<b>D�CIMA PRIMERA.- APLICACI�N DE LOS PAGOS.</b> Cualquier pago que en t�rminos del Contrato sea realizado por el Acreditado, ser� aplicado por CreaBienestar ".
        "en el orden siguiente:\n\n";
$text .= "a.	Al pago de los intereses moratorios correspondientes al adeudo vencido m�s el IVA, en su caso.\n";
$text .= "b.	El remanente, despu�s de pagar el concepto anterior, al pago de los intereses ordinarios vencidos m�s el IVA conforme a la cl�usula S�ptima inciso d) ".
        "del presente Contrato.\n";
$text .= "c.	El remanente, despu�s de pagar el concepto anterior, al pago del capital vencido.\n";
$text .= "d.	El remanente, despu�s de pagar el concepto anterior, al pago de los intereses ordinarios corrientes, conforme a la cl�usula S�ptima inciso ".
        "b) del presente Contrato.\n";
$text .= "e.	El remanente, despu�s de pagar el concepto anterior, se aplicar� al pago de capital vigente.\n\n";
$text .= "<b>D�CIMA SEGUNDA. TERMINACI�N ANTICIPADA.</b> El Acreditado podr� cancelar el presente Contrato en un periodo de diez d�as h�biles posteriores a la ".
        "firma del mismo, sin responsabilidad alguna para el Acreditado. Lo anterior, siempre que el Acreditado no haya dispuesto total o parcialmente del monto del ".
        "cr�dito, conforme a la Clausula Quinta del presente Contrato.\n\n";
$text .= "Sin prejuicio de lo anterior, el Acreditado podr� dar por terminado el Contrato de  manera anticipada, siempre y cuando haya transcurrido la mitad del plazo ".
        "del Cr�dito y el Acreditado est� al corriente en el pago del mismo.\n\n";
$text .= "En cualquiera de los casos referidos en los p�rrafos primero y segundo de esta Cl�usula se seguir� el siguiente procedimiento: i) el Acreditado deber� solicitar ".
        "la cancelaci�n previamente a CreaBienestar mediante escrito debidamente firmado y presentado ante la Unidad Especializada de Atenci�n al Cliente cuyos datos de ".
        "atenci�n y contacto se establecen en la cl�usula D�cima Sexta del presente Contrato o en alguna de las sucursales de CreaBienestar; ii) Crea Bienestar se ".
        "cerciorar� de la autenticidad y veracidad de la identidad del Acreditado, confirmando los datos del Acreditado por v�a telef�nica o por medio de correo ".
        "electr�nico, al n�mero telef�nico y/o cuenta de correo electr�nico proporcionados por el Acreditado en la Solicitud (Anexo \"A\");  iii) CreaBienestar ".
        "proporcionar� un n�mero de reporte que identifique la solicitud de cancelaci�n y har� del conocimiento del Acreditado, a m�s tardar dentro de los diez d�as ".
        "naturales siguientes a la presentaci�n de dicha solicitud, la procedencia o improcedencia de la misma, y, en su caso, el importe del saldo insoluto del ".
        "principal del Cr�dito pendiente por cubrir; iv) el Acreditado deber� depositar dicha cantidad directamente en la cuenta bancaria de CreaBienestar se�alada ".
        "en la Solicitud (anexo \"A\"); v) el Acreditado deber� notificar y comprobar a CreaBienestar el pago se�alado en el inciso anterior, para que surta efectos ".
        "la cancelaci�n del Contrato, y vi) una vez cancelado el Contrato CreaBienestar pondr� a disposici�n del Acreditado la Constancia de Terminaci�n del Contrato, ".
        "as� como el Pagar� una vez cumplidos treinta d�as naturales contados a partir de que surta efectos la cancelaci�n.\n".
        "Trat�ndose del supuesto de cancelaci�n contenido en el  primer p�rrafo de la esta Cl�usula, la cancelaci�n surtir� efectos a partir de la fecha en que ".
        "CreaBienestar le notifique al Acreditado la procedencia de la solicitud de la cancelaci�n. \n\n";
$text .= "En caso de muerte del Acreditado, la deuda se condonara, para lo cual se deber� entregar a CreaBienestar original de Acta de Defunci�n salvo las ".
        "siguientes excepciones: \n\n";
$text .= "1.- En caso de Suicidio\n";
$text .= "2.- Si la muerte ocurri� debido a alguna enfermedad cr�nica degenerativa que el Acreditado no haya reportado en su Solicitud de Cr�dito.\n\n";
$text .= "En estos casos, la deuda deber� ser cubierta por el o los obligados solidarios.\n\n";
$text .= "<b>D�CIMA TERCERA. VENCIMIENTO ANTICIPADO.</b> CreaBienestar podr� dar por vencido anticipadamente este Contrato y sus anexos, sin responsabilidad ".
        "y sin necesidad de declaraci�n judicial previa, exigiendo al Acreditado el pago anticipado de todas las cantidades que le adeude a CreaBienestar a la ".
        "fecha de la resoluci�n, en los siguientes casos:\n\n";
$text .= "a.	El Cr�dito no sea pagado en los t�rminos del presente Contrato.\n";
$text .= "b.	La falta de pago por parte del Acreditado, en las fechas de pago, de cualquiera de las parcialidades, as� como de cualquier otro importe que ".
        "el Acreditado adeude a CreaBienestar conforme al Contrato.\n";
$text .= "c.	Si las declaraciones, informaci�n y/o documentaci�n proporcionada por el Acreditado a CreaBienestar resulta ser falsa o incorrecta.\n";
$text .= "d.	El estado de insolvencia del Acreditado.\n";
$text .= "e.	Si el Acreditado cambia de domicilio y no da aviso a CreaBienestar dentro de los 15 d�as naturales a la fecha efectiva de haberlo realizado.\n";
$text .= "f.	Si el Cr�dito acordado fuera destinado a la consecuci�n de cualquier acto il�cito o si el Acreditado es acusado de la comisi�n de alg�n delito.\n";
$text .= "g.	Si el Acreditado transmite de cualquier forma sus derechos u obligaciones derivados del presente Contrato, sin la autorizaci�n previa de CreaBienestar.\n";
$text .= "h.	En los dem�s casos en que, conforme a la ley o a este Contrato, sea exigible anticipadamente el cumplimiento de la obligaci�n a plazo.\n";
$text .= "Como consecuencia de lo anterior, el Acreditado pagar� a favor de CreaBienestar: i) el saldo insoluto del Cr�dito, as� como los intereses ordinarios vencidos; ".
        "ii) cualquier otro importe que el Acreditado adeude a CreaBienestar conforme al Contrato, y, iii) los da�os y/o perjuicios que, en su caso, le sean ocasionados a ".
        "CreaBienestar, los cuales ser�n determinados por la autoridad judicial competente.\n\n";
$text .= "<b>D�CIMA CUARTA.- PAGOS ANTICIPADOS.</b> El Acreditado podr� realizar pagos anticipados del Cr�dito a su cargo, siempre que el monto m�nimo a pagar sea el ".
        "equivalente a una parcialidad del Cr�dito y el Acreditado se encuentre al corriente en los pagos exigibles conforme a lo acordado en el presente Contrato.  ".
        "Dichos prepagos �nicamente se podr�n realizar previo aviso por escrito a CreaBienestar con 5 (cinco) d�as h�biles de anticipaci�n a la fecha en que se ".
        "pretendan realizar. En caso de que el Acreditado efect�e el pago sin la notificaci�n debida, CreaBienestar aplicar� los recursos recibidos al d�a h�bil ".
        "siguiente a aqu�l en  que los reciba, seg�n la prelaci�n establecida en este Contrato.\n\n";
$text .= "Con los pagos anticipados, CreaBienestar reducir� el n�mero de las parcialidades pactadas, aplicando el pago parcial anticipado a las amortizaciones consecutivas.\n\n";
$text .= "En caso de que el Acreditado pretenda realizar pagos anticipados por un importe igual al saldo insoluto del Cr�dito, deber� observar lo dispuesto en el la ".
        "cl�usula D�cima Segunda del presente Contrato.\n\n";
$text .= "En cualquier caso, CreaBienestar entregar� al Acreditado el comprobante del pago anticipado, as� como la Tabla de Amortizaci�n (Anexo \"D\" del presente Contrato) ".
        "actualizada, a trav�s del correo electr�nico que el Acreditado indique en la Solicitud  o bien, en la oficinas de CreaBienestar.\n\n";
$text .= "<b>D�CIMA QUINTA.- CESI�N DE DERECHOS Y OBLIGACIONES. </b>El Acreditado no podr� ceder o transmitir los derechos u obligaciones que para �l se deriven del ".
        "presente Contrato, sin la previa autorizaci�n por escrito de CreaBienestar. Cualquier transmisi�n en t�rminos distintos a los aqu� previstos ser� nula y no ".
        "ser� reconocida por CreaBienestar.\n\n";
$text .= "CreaBienestar podr� transmitir, descontar y/o ceder, en cualquier tiempo y sin previa notificaci�n ni autorizaci�n del Acreditado, los derechos que se ".
        "derivan del presente Contrato y/o del Pagar� a favor de cualquier tercero (en adelante \"Cesionario\"), por lo que en caso de que CreaBienestar realice dicha ".
        "transmisi�n o cesi�n, el Acreditado desde este momento autoriza y faculta a CreaBienestar de manera expresa e irrevocable, a presentar a la empresa/dependencia ".
        "donde el Acreditado est� laborando, una notificaci�n por escrito de la cesi�n correspondiente, en la cual se le indique a la empresa/dependencia que a partir ".
        "de la fecha de dicha notificaci�n, todos y cada uno de los pagos se realicen a la cuenta bancaria del Cesionario respectivo que se indique en la propia notificaci�n .\n\n";
$text .= "<b>D�CIMA SEXTA. SERVICIOS DE ATENCI�N AL CLIENTE.</b> En caso de que el Acreditado tuviere alguna inconformidad, queja, solicitud, aclaraci�n o consulta relacionada ".
        "con el Cr�dito contratado, podr� presentarla  directamente por escrito ante la Unidad Especializada de Atenci�n al Cliente, ubicada en Av. Revoluci�n  No. ".
        "1653, Col. San �ngel, Del. �lvaro Obreg�n C.P. 01000, M�xico D.F.; cuyo titular es la Lic. Marcela Soto Alarc�n.\n\n";
$text .= "Asimismo, el Acreditado podr� dirigir cualquier inconformidad, queja, solicitud o aclaraci�n a la citada Unidad, trav�s de cualquiera de las opciones que se ".
        "mencionan a continuaci�n:\n\n";
$text .= "-	L�nea de tel�fono: 56-61-30-19 y fax: 56-61-30-19\n";
$text .= "-     Correo electr�nico: attncliente@creabienestar.com.mx\n";
$text .= "-	P�gina de Internet: www.creabienestar.com.mx\n\n";
$text .= "CreaBienestar har� del conocimiento del Acreditado, en un plazo m�ximo de cuarenta y cinco d�as naturales, el dictamen correspondiente a la inconformidad, ".
        "queja, solicitud, aclaraci�n o consulta, a la cuenta de correo electr�nico que para dichos efectos indique el Acreditado en la Solicitud.\n".
        "El Acreditado tambi�n podr� dirigir sus aclaraciones, dudas, consultas o inconformidades a la <b>Comisi�n Nacional para la Protecci�n y Defensa de los ".
        "Usuarios de Servicios Financieros (CONDUSEF)</b>, cuyos datos se proporcionan a continuaci�n:\n\n";
$text .= "-	N�mero telef�nico de atenci�n a usuarios: 53.40.09.99 o LADA sin costo al:  01800.999.80.80\n";
$text .= "-	Direcci�n en Internet: www.condusef.gob.mx\n";
$text .= "-	Correo electr�nico: opinion@condusef.gob.mx\n\n";
$text .= "<b>D�CIMA S�PTIMA.- GASTOS Y COSTAS.</b> En el supuesto de que CreaBienestar tuviera que demandar al Acreditado por incumplimiento, los gastos y costas ".
        "del juicio (incluidos aquellos por concepto de honorarios de abogados y fedatarios contratados por CreaBienestar) ser�n reintegrados a CreaBienestar, ".
        "en su totalidad, por el Acreditado. \n\n";
$text .= "<b>D�CIMA OCTAVA.- MODIFICACI�N DEL CONTRATO.</b> CreaBienestar dar� aviso al Acreditado de cualquier modificaci�n al presente Contrato con treinta d�as naturales ".
        "de anticipaci�n a la modificaci�n del mismo. El Acreditado podr� dar por terminada la relaci�n contractual en caso de no estar de acuerdo con las modificaciones, ".
        "dentro de los sesenta d�as naturales posteriores al aviso de modificaci�n, siempre y cuando haya liquidado totalmente el monto del Cr�dito y los intereses que se ".
        "generen en virtud de este Contrato, de acuerdo con el procedimiento se�alado en la cl�usula D�cima Segunda y sin que se originen  gastos administrativos a su cargo. ".
        "Una vez terminado el Contrato y liquidado el Cr�dito, CreaBienestar pondr� a disposici�n del Acreditado la Constancia de  Terminaci�n del Contrato, as� como el ".
        "Pagar� una vez cumplidos treinta d�as naturales contados a partir de la terminaci�n del Contrato. \n\n";
$text .= "<b>D�CIMA NOVENA.- ANEXOS.</b> Los siguientes anexos forman parte integrante del presente Contrato, por lo que ambas partes los reconocen plenamente desde este momento: \n\n";
$text .= "Anexo \"A\" Solicitud de Cr�dito\n";
$text .= "Anexo \"B\" Car�tula\n";
$text .= "Anexo \"C\" Pagar�\n";
$text .= "Anexo \"D\" Tabla de Amortizaci�n \n\n";
$text .= "<b>VIG�SIMA.- DOMICILIOS.</b> Las partes contratantes convienen que en tanto no se comunique por escrito el cambio de domicilio de cualquiera de �stas, cualquier ".
        "diligencia, actuaci�n, notificaci�n, emplazamiento, requerimiento o comunicaci�n, surtir� efectos legales en los domicilios se�alados a continuaci�n: \n\n";
$text .= "-	CreaBienestar: Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F. \n";
$text .= "-	El Acreditado: el que se indica en la Solicitud. \n";
$text .= "Las partes se obligan a comunicar a la contraparte cualquier cambio de domicilio dentro de los 15 d�as naturales siguientes a la fecha efectiva de haberlo realizado. \n\n";
$text .= "<b>VIG�SIMA PRIMERA.-  DECLARACI�N.</b> El Acreditado en este acto manifiesta expresamente que CreaBienestar le hizo saber, previo a la firma de este Contrato ".
        "su contenido y alcance, as� como que al momento de la firma del Contrato le es entregado al Acreditado una copia del Contrato y de cada uno de los Anexos ".
        "relacionados en la cl�usula D�cima Novena del mismo. \n\n";
$text .= "<b>VIG�SIMA SEGUNDA.- T�TULOS.</b> Los t�tulos de las cl�usulas del Contrato, son exclusivamente por conveniencia de las partes, para una referencia y lectura ".
        "m�s simple, por lo que no regir�n la interpretaci�n del Contrato. \n\n";
$text .= "<b>VIG�SIMA TERCERA. LEYES APLICABLES.</b>  El Contrato se rige por lo dispuesto en sus cl�usulas y, de manera supletoria y en lo que resulte aplicable, ".
        "por la Ley General de T�tulos y Operaciones de Cr�dito, <i>Ley para la Transparencia y Ordenamiento de los Servicios Financieros,  Disposiciones de car�cter ".
        "general a que se refieren los art�culos 11, 12, 13 y 23 de la Ley para la Transparencia y Ordenamiento de los Servicios Financieros aplicables a las ".
        "instituciones de cr�dito, sociedades financieras de objeto limitado, sociedades financieras de objeto m�ltiple reguladas y las entidades financieras ".
        "que act�en como fiduciarias en fideicomisos que otorguen cr�dito, pr�stamo o financiamiento al p�blico</i>, en tanto se emitan por parte de la Comisi�n ".
        "Nacional para la Protecci�n y Defensa de los Usuarios de Servicios Financieros las disposiciones referidas en la citada Ley para la Transparencia y ".
        "Ordenamiento de los Servicios Financieros en materia de Contratos de Adhesi�n, Publicidad, Estados de Cuenta y Comprobantes de Operaci�n,  <i>C�digo de ".
        "Comercio, C�digo Civil Federal</i>, y dem�s leyes y disposiciones que resulten aplicables.\n\n";
$text .= "<b>VIG�SIMA CUARTA.- CONTROVERSIAS.</b> La COMISI�N NACIONAL PARA LA PROTECCI�N Y DEFENSA DE LOS USUARIOS DE SERVICIOS FINANCIEROS (CONDUSEF) ser� ".
        "competente en la v�a administrativa para resolver cualquier controversia que se suscite por la interpretaci�n, cumplimiento y ejecuci�n de este Contrato. ".
        "Sin perjuicio de lo anterior, toda controversia previa a la intervenci�n del �rgano jurisdiccional derivado de este Contrato, se resolver� en arbitraje de ".
        "conformidad con el Reglamento de Arbitraje de la Confederaci�n de C�maras Industriales de los Estados Unidos Mexicanos vigente, en la Ciudad de M�xico, ".
        "Distrito Federal o, en su caso, a elecci�n de CreaBienestar, en el domicilio del Acreditado.\n\n";
$text .= "<b>VIG�SIMA QUINTA.- COMPETENCIA.</b> En caso de que la controversia subsistiera y no se lograra acuerdo en concreto sobre el cumplimiento, ".
        "contenido, interpretaci�n y alcance de los derechos y las obligaciones contenidas en este Contrato, las partes se someten a lo se�alado en la ".
        "legislaci�n vigente para el Distrito Federal y a la jurisdicci�n y competencia de los Tribunales de la Ciudad de M�xico, o bien, a elecci�n de ".
        "CreaBienestar, a la del domicilio del Acreditado. \n\n";
$text .= "<b>Las partes reconocen el alcance y efectos del Contrato, oblig�ndose solidariamente en todos y cada uno de los t�rminos asentados en el mismo ".
        "y, por lo tanto, lo firman en un s�lo acto, en dos ejemplares aut�grafos, quedando un tanto en poder de CreaBienestar y el otro en poder del Acreditado, ".
        "en la Ciudad y el d�a indicados en la Car�tula; lugar y fecha que se considerar�n para todos los efectos legales, como lugar y fecha de firma del Contrato.</b> \n\n";
$pdf->ezText($text, 10, array('justification'=>'full'));
$pdf->ezColumnsStop();
//$firmas = array_merge($obligados, $firmas);
$pages = 6;
$pdf->addSignatures($firmas, 350, 350, 600, 30,$pages);
$pdf->ezStopPageNumbers(1,1,$i);
?>