<?php
$imgPath = ROOT.RUTA_IMG.'logo.jpg';
$prestamo = $acred['cantidad'];
$montoFinal = $acred['montoInteres'];
/*for($i = 0; $i < $credito['plazo']; $i++){
    if($i > 0)
        $prestamo -= preg_replace("/([^0-9\\.])/i", "", $amortizacion[$i-1]['capital']);
    $montoFinal -= $amortizacion[$i]['monto'];
    $amortizacion[$i]['pago'] = $i+1;
    $amortizacion[$i]['prestamo'] = "$".number_format($prestamo, 2);
    $amortizacion[$i]['monto'] = "$".number_format($amortizacion[$i]['monto'], 2);
    $amortizacion[$i]['interes'] = "$".number_format($amortizacion[$i]['interes'], 2);
    $amortizacion[$i]['iva'] = "$".number_format($amortizacion[$i]['iva'], 2);
    $amortizacion[$i]['capital'] = "$".number_format($amortizacion[$i]['capital'], 2);
    $amortizacion[$i]['saldo_final'] = "$".number_format($montoFinal, 2);
    $amortizacion[$i]['fecha_mov'] = strftime("%A %d de %B de %Y", strtotime($amortizacion[$i]['fecha_mov']));
}*/
//var_dump($amortizacion);die();
//Para el t�tulo de la tabla de amortizaci�n
$pdfAm->ezNewPage();
$title = $pdfAm->openObject();
$pdfAm->setColor(0.41,0.55,0.13);
$pdfAm->filledRectangle(40,579,300,16);
$pdfAm->setColor(1,1,1);
$pdfAm->addText(47,583,12, "<b>TABLA DE AMORTIZACION - CREDIMARCHANTE</b>");
$pdfAm->closeObject();
$pdfAm->addObject($title, "all");
//Creamos un pie de p�gina donde vengan los datos de la empresa.
$footer = $pdf->openObject();
$pdfAm->setColor(0,0,0);
$pdfAm->setStrokeColor(0,0,0);
$pdfAm->setLineStyle(1);
$pdfAm->line(30,50,762,50);
$pdfAm->addText(170,40,8, "Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F.");
$pdfAm->addText(290,30,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdfAm->closeObject();
$pdfAm->addObject($footer, "add");
//Para el logo de la empresa
$pdfAm->addJpegFromFile($imgPath,610,520,120);
//Barcode
$pdfAm->addImage($barcode,420,460,150,100,100);

$pdfAm->ezSetY(580);
$cols = array('col1'=>"",
              'col2'=>"",);

$info = array(
            array('col1'=>"<b>GRUPO</b>",'col2'=>$credito['nombre']),
            array('col1'=>"<b>ACREDITADO</b>",'col2'=>mb_strtoupper($acred['acreditado'],'iso-8859-1')),
            array('col1'=>"<b>CREDITO</b>",'col2'=>"$".number_format($acred['cantidad'], 2)),
            array('col1'=>"<b>TASA INTERES ANUAL</b>",'col2'=>"{$credito['tasa_interes']}%"),
            array('col1'=>"<b>PLAZO</b>",'col2'=>$credito['plazo']),
            array('col1'=>"<b>INTERES AL FINAL DEL CREDITO</b>",'col2'=>"$".number_format($interesTotal, 2)),
            array('col1'=>"<b>MONTO AL FINAL DEL CREDITO</b>",'col2'=>"$".number_format($montoFinal, 2)),
            array('col1'=>"<b>DESCUENTOS</b>",'col2'=>$credito['periodo']),
    );
$options = array('maxWidth'=>600,
                 'showHeadings'=>0,
                 'shaded'=>0,
                 'fontSize'=>10,
                 'showLines'=> 0,
                 //'xPos'=>'center',
                 //'xOrientation'=>'left',
                 'rowGap'=>1,
                 'cols'=>array( 
                        "col1" => array('justification'=>'left','width' => '200'), 
                        "col2" => array('justification'=>'left', 'width' => '400'),),
                );
$pdfAm->ezTable($info,$cols,'',$options);
$pdfAm->ezText("\n",5);
unset($cols);
unset($info);
unset($options);
$cols = array('num'=>"<b>No. Periodo</b>",
              'prestamo'=>"<b>Pr�stamo</b>",
              'pagoFijo'=>"<b>Pagos Fijos</b>",
              'interes'=>"<b>Inter�s</b>",
              'iva'=>"<b>IVA</b>",
              'capital'=>"<b>Capital</b>",
              'saldo'=>"<b>Saldo Final</b>",
              'fecha'=>"<b>Fecha de pago</b>",
              );
$info = $acred['tablaAm'];
$options = array('shaded'=>0,
                 'fontSize'=>9,
                 'rowGap'=>1,
                 'showLines'=> 2,
                 //'shadeCol2'=>array(0.8,0.8,0.8),
                 'xPos'=>'center',
                 'xOrientation'=>'center',
                );
$pdfAm->ezTable($info,$cols,'',$options);
$pdfAm->ezText("\n",5);
unset($cols);
unset($info);
unset($options);
$text = "Los pagos se realizar�n en las fechas establecidas en esta Tabla y de acuerdo a la Car�tula del Contrato de Cr�dito Simple (Anexo \"B\")\n";
$text .= "Los montos expresados en la columna de \"Pagos Fijos\" de esta Tabla incluyen el I.V.A. Sin embargo, mientras el referido Contrato de ".
        "Cr�dito tenga vigencia, los montos referidos en esta Tabla estar�n sujetos, en su caso, a las variaciones que sufra el I.V.A., conforme ".
        "a las reformas fiscales que se autoricen por las autoridades competentes.\n";
$text .= "Para el caso de que los pagos no se efect�en o se realicen de manera incompleta, se aplicar� la tasa de mora conforme al contrato.\n";
$text .= "Le recordamos que en caso de no depositar en su fecha de pago, deber� de adicionar <b>{$moraDiaria}</b> por cada d�a de retraso correspondiente ".
        "a intereses moratorios; asimismo, conforme a la Cl�usula D�cima Tercera del Contrato, en caso de tener  dos parcialidades vencidas, se le ".
        "exigir� el pago total de su cr�dito. Es por lo anterior que lo invitamos a no retrasarse en sus pagos.\n\n";
$text .= "<b>Notas:</b>\n\n";
$text .= "- Recuerde que debe realizar sus pagos completos en la fecha estipulada.\n";
$text .= "- En caso de que la fecha de su pago sea en d�a inh�bil bancario, podr� realizar su pago en el d�a h�bil bancario siguiente sin cargo de comisi�n moratoria.\n";
$text .= "- Cualquier duda o comentario favor de comunicarse a la Unidad Especializada de Atenci�n al Cliente se�alada en el contrato.\n\n";
$pdfAm->ezText($text,10);
?>