<?php
$pdf->ezSetMargins(140, 100, 30, 30);
//Para el logo de la empresa
$imgPath = ROOT.RUTA_IMG.'logo.jpg';


//Para el encabezado del estado de cuenta
$header = $pdf->openObject();
$pdf->addJpegFromFile($imgPath,50,680,120);
$pdf->addText(195,745,11, "<b>CREA BIENESTAR S.A. DE C.V. S.O.F.O.M. E.N.R.</b>");
$pdf->addText(230,725,11, "<b>ESTADO DE CUENTA DE MORA</b>");
$pdf->addText(250,705,10, "<b>{$producto[$credito['producto']]} - {$tipo[$credito['tipo']]}</b>");
$pdf->addText(230,685,9, "<b>FECHA DE CORTE: ".strftime("%d %B %Y", strtotime('today'))."</b>");
$pdf->closeObject();
$pdf->addObject($header, "all");

//Para el pie de pagina del estado de cuenta
$footer = $pdf->openObject();
$pdf->setLineStyle(1);
$pdf->line(50,60,562,60);
$pdf->addText(70,50,8, "Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F.");
$pdf->addText(190,40,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdf->closeObject();
$pdf->addObject($footer, "all");

//Resumen mora
/*$pdf->setStrokeColor(1, 0, 0.8);
$pdf->rectangle(40, 540, 520, 105);
$pdf->addText(50,630,10, "<b>Int:</b> {$credito['nombreGrupo']}");
$pdf->addText(50,610,10, "<b>Tasa inter�s:</b> {$credito['tasa_interes']}%");
$pdf->addText(50,590,10, "<b>Plazo:</b> {$credito['plazo']} MESES");
$pdf->addText(50,570,10, "<b>Periodo:</b> {$credito['periodo']}");
$pdf->addText(50,550,10, "<b>Monto prestado:</b> $".number_format($credito['montoTotal'], 2));
$pdf->addText(350,630,10, "<b>Inter�s por pagar:</b>");
$pdf->addText(450,630,10, "$".number_format($balance['interes'], 2));
$pdf->addText(350,610,10, "<b>I.V.A. por pagar:</b>");
$pdf->addText(450,610,10, "$".number_format($balance['iva'], 2));
$pdf->addText(350,590,10, "<b>Capital por pagar:</b>");
$pdf->addText(450,590,10, "$".number_format($balance['capital'], 2));
$pdf->addText(350,570,10, "<b>Total:</b>");
$pdf->addText(450,570,11, "<b>$".number_format($balance['monto'], 2)."</b>"); */

//Historial de mora
//$pdf->ezSetDy(-20);
$cols = array('fecha'=>'<b>Fecha</b>','no'=>'No. Parcialidad', 'dias'=>'D�as atraso',  
			 'monto'=>'<b>Cargo Mora</b>','pago'=>'<b>Pago Mora</b>',
			  'saldo'=>'<b>Saldo</b>');
$pdf->ezTable($moratorios, $cols, '<b>Tabla de mora</b>', $options);

$pdf->ezSetDy(-20);
if($condonaciones['condonado']['monto'] > 0){
	$pagado = "$".number_format($condonaciones['pagado']['monto'],2);
	$condonado = "$".number_format($condonaciones['condonado']['monto'],2);
	$options = array("justification"=>'center',"spacing"=>2);
	$pdf->ezText("<b>Mora pagada:</b> {$pagado}",12,$options);
	$pdf->ezText("<b>Mora condonada:</b> {$condonado}",12,$options);
}
?>