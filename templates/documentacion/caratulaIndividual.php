<?php
$pages = 1;

//Iniciamos la numeraci�n de las p�ginas
$i = $pdf->ezStartPageNumbers(550,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
//Creamos un encabezado de p�gina para los datos de RECA y de la empresa
$header = $pdf->openObject();
$pdf->setColor(0.52,0.52,0.52);
$pdf->addText(400,765,9, "<b>RECA No. 4296-439-008394/01-13161-0711</b>");
$pdf->setColor(0.50,0.50,0);
$pdf->addText(510,755,11, "CreaBienestar");
$pdf->addText(400,745,8, "CREA BIENESTAR, S.A. de C.V., SOFOM, E.N.R.");
$pdf->setColor(0.67,1,0.18);
$pdf->addText(460,735,8, "Cr�dito y Bienestar para tu familia");
$pdf->closeObject();
$pdf->addObject($header, "all");
//Creamos un pie de p�gina donde vengan los datos de la empresa.
$footer = $pdf->openObject();
$pdf->setColor(0,0,0);
$pdf->setStrokeColor(0,0,0);
$pdf->setLineStyle(1);
$pdf->line(50,60,562,60);
$pdf->addText(70,50,8, "Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F.");
$pdf->addText(190,40,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdf->closeObject();
$pdf->addObject($footer, "all");
//Para el t�tulo de la car�tula
$title = $pdf->openObject();
$pdf->setColor(0.41,0.55,0.13);
$pdf->filledRectangle(40,709,300,16);
$pdf->setColor(1,1,1);
$pdf->addText(47,713,12, "<b>CAR�TULA DE CONTRATO - CREDIMARCHANTE</b>");
$pdf->closeObject();
$pdf->addObject($title, "add");

//Anexo
$pdf->addText(500,713,12, "ANEXO \"B\"");

$pdf->ezSetY(708);
$pdf->ezText("M�xico, Distrito Federal a ".strftime("%d de %B de %Y", strtotime($fechaEntrega))."\n\n",10,array('justification'=>'right'));
$pdf->ezText("<b>Esta Car�tula forma parte del Contrato respectivo que celebran CREA BIENESTAR S.A. de C.V. SOFOM E.N.R., en su car�cter de \"Acreditante\" ".
             "y {$credito['acreditado']} en su car�cter de \"Acreditado\".\"Los rubros precisados en este resumen, se entender�n".
             "referidos a las cl�usulas contenidas en el Contrato de Cr�dito Simple del que se desprenden\".</b>\n\n",12);

$cols = array('cat'=>"<b>C.A.T (Costo Anual Total)</b>",
              'int'=>"<b>TASA DE INTER�S</b>",
              'monto'=>"<b>MONTO DEL CR�DITO SIMPLE</b>",
              'total'=>"<b>MONTO TOTAL A PAGAR</b>",
              'com'=>"<b>COMISIONES \n (Montos y Cl�usulas)</b>");
$info = array(
                array('cat'=>"Para fines informativos y de comparaci�n {$CAT}% \n Fija �nica Sin IVA",
                      'int'=>"Ordinario: {$credito['tasa_interes']}% \n Fija �nica \n Tasa Moratoria {$mora}% +IVA \n Fija (Diaria)",
                      'monto'=>"\n {$credito['cantidad']} \n\n {$montoTexto}",
                      'total'=>"\n {$credito['total']} \n\n {$totalTexto}",
                      'com'=>"\nPor apertura $0 \n\n Por prepago $0"),
            );
$options = array('maxWidth'=>550,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'rowGap'=>6,
                 'outerLineThickness' =>3,
                 'innerLineThickness'=>1,
                 'fontSize'=>10,
                 'xPos' => 'center',
                 'cols'=>array( 
                        "cat" => array('justification'=>'center','width' => '80'), 
                        "int" => array('justification'=>'center', 'width' => '70'), 
                        "monto" => array('justification'=>'center', 'width' => '140'), 
                        "total" => array('justification'=>'center', 'width' => '170'),
                        "com" => array('justification'=>'center', 'width' => '90'),), 
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
////////////////////////////////////////////////////
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"<b>Metodolog�a de c�lculo de inter�s ordinario:</b> <i>se multiplica la Tasa de Inter�s Anual por el monto ".
                      "del Cr�dito, entre 360 d�as, y el resultado se multiplica por los d�as del plazo contratado</i>\n\n".
                      "<b>Metodolog�a de c�lculo de inter�s moratorio:</b> <i> se multiplica la tasa de inter�s moratoria (m�s IVA) por el monto de la ".
                      "parcialidad vencida y no pagada, por cada uno de los d�as naturales transcurridos desde la fecha en que el pago debi� ".
                      "realizarse, hasta la fecha en que efectivamente se realiz�</i>"));
$options = array('width'=>550,
                 'rowGap'=>6,
                 'showHeadings' => 0,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
////////////////////////////////////////////////////
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"<b>Plazo del cr�dito: {$plazoTexto} MESES \n".
                      "Datos del Banco donde deber� depositar cada una de las parcialidades: \n".
                      "BANORTE\n".
                      "Empresa: 122043 Referencia: {$refBanc}\n".
                      "Los pagos ser�n como sigue:</b>"));
$options = array('width'=>550,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
$cols = array('oneCol'=>"",'twoCol'=>"",'threeCol'=>"",);
$info = array(
                array('oneCol'=>"<b>N�mero de parcialidades</b> \n\n {$credito['plazo']} \n\n (Las parcialidades son iguales, fijas y consecutivas)",
                      'twoCol'=>"<b>Monto de cada una de la parcialidades</b> \n\n {$credito['pFijo']} \n\n {$pFijoTexto}",
                      'threeCol'=>"<b>Periodo</b> \n (Cuando se realizar�n los pagos) \n {$credito['periodo']}",
                      ));
$options = array('maxWidth'=>550,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                 'cols'=>array( 
                        "oneCol" => array('justification'=>'center','width' => '195'), 
                        "twoCol" => array('justification'=>'center', 'width' => '195'), 
                        "threeCol" => array('justification'=>'center', 'width' => '160'))
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"El pago oportuno que usted realice de sus cr�ditos en las fechas pactadas, le permitir� mantener un historial crediticio satisfactorio. \n".
                      "Contratar cr�ditos en exceso de su capacidad de pago puede afectar su patrimonio y su historial crediticio. \n".
                      "Incumplir sus obligaciones le puede generar intereses moratorios."));
$options = array('width'=>550,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"<b>Autorizaci�n:</b>Los datos personales pueden utilizarse para mercadotecnia:  <b>SI___</b>    <b>NO___</b>"));
$options = array('width'=>550,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
$pdf->ezNewPage();
$pages++;
$pdf->ezSetY(730);
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"<b>Dudas, aclaraciones y reclamaciones:</b> \n".
                      "Deber� seguirse el procedimiento referido en la cl�usula Decima \n".
                      "Sexta del Contrato, ante la Unidad Especializada de Atenci�n al Cliente, cuyos datos son los siguientes: \n".
                      "Domcilio: Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel \n".
                      "Delegaci�n �lvaro Obreg�n C.P. 01000. M�xico, D.F.\n".
                      "Tel�fono: 56-61-30-19 Fax: 56-61-30-19 \n".
                      "Correo electr�nico: attncliente@creabienestar.com.mx \n".
                      "P�gina de internet: www.creabienestar.com.mx \n\n".
                      "<b>Tambi�n puede dirigirse a:</b>\n".
                      "CONDUSEF: Tel�fono: 01800 999 8080. P�gina de internet: www.condusef.gob.mx"));
$options = array('width'=>550,
                 'protectRows'=>1,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
$cols = array('oneLine'=>"",);
$info = array(
                array('oneLine'=>"<b>ESTADO DE CUENTA/CONSULTA DE MOVIMIENTOS</b> \n\n".
                      "a. Mediante solicitud v�a telef�nica, fax o por correo electr�nico a la Unidad Especializada ".
                      "de Atenci�n al Cliente, para \nque le sea enviado a la direcci�n de correo elect�nico que indique, o bien, \n".
                      "b. Consulta v�a Internet a la direcci�n: www.creabienestar.com.mx, mediante la clave confidencial ".
                      "que para tal efecto le proporciones CREA BIENESTAR S.A. de C.V. SOFOM E.N.R. \n\n".
                      "El contrato de adhesi�n de cr�dito simple ha quedado inscrito en el Registro de Contratos de Adhesi�n ".
                      "de la Comisi�n Nacional para la Protecci�n y Defensa de los Usuarios de los Servicios Financieros, bajo ".
                      "el folio no. <b>4296-439-008394/01-13161-0711</b> en cumplimiento a lo se�alado en el art�culo 11 de la ".
                      "Ley para la Transparencia y Ordenamiento de los Servicios Financieros."));
$options = array('maxWidth'=>550,
                 'showHeadings' => 0,
                 'rowGap'=>6,
                 'lineCol'=>array(0.41,0.55,0.13),
                 'shaded'=>0,
                 'outerLineThickness' =>3,
                 'fontSize'=>10,
                );
$pdf->ezTable($info,$cols,'',$options);
unset($options,$cols,$info);
/////////////////////////////////////////////
//Para la secci�n de firmas de los acreditados
$pdf->addSignatures($firmas, 50, 380, 600, 80, $pages);
$pdf->ezStopPageNumbers(1,1,$i);
//Detenemos la inserci�n de encabezado y pie de p�gina para las p�ginas subsecuentes.
$pdf->stopObject($header);
$pdf->stopObject($footer);
?>