<?php
//Iniciamos la numeraci�n de las p�ginas
$i = $pdf->ezStartPageNumbers(550,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);

//Creamos un encabezado de p�gina para los datos de RECA y de la empresa
$header = $pdf->openObject();
$pdf->setColor(0.50,0.50,0);
$pdf->addText(510,755,11, "CreaBienestar");
$pdf->addText(400,735,8, "CREA BIENESTAR, S.A. de C.V., SOFOM, E.N.R.");
$pdf->closeObject();
$pdf->addObject($header, "all");
//Creamos un pie de p�gina donde vengan los datos de la empresa.
$footer = $pdf->openObject();
$pdf->setColor(0,0,0);
$pdf->setStrokeColor(0,0,0);
$pdf->setLineStyle(1);
$pdf->line(50,60,562,60);
$pdf->addText(70,50,8, "Av. Revoluci�n No. 1653, 1er piso, Col. San �ngel, Delegaci�n �lvaro Obreg�n, C.P. 01000 M�xico D.F.");
$pdf->addText(190,40,8, "Tel�fono: 56-61-30-19. P�gina web: www.creabienestar.com.mx");
$pdf->closeObject();
$pdf->addObject($footer, "all");
//Para el t�tulo del pagar�
$title = $pdf->openObject();
$pdf->setColor(0.41,0.55,0.13);
$pdf->filledRectangle(460,709,120,16);
$pdf->setColor(1,1,1);
$pdf->addText(490,713,12, "<b>PAGAR�</b>");
$pdf->closeObject();
$pdf->addObject($title, "add");

$pdf->ezSetY(680);
$important = "<b>BUENO POR {$credito['total']} ({$totalTexto})</b>";
$pdf->ezText($important, 11, array('justification'=>'center'));
$pdf->ezText("\n", 9);

$periodo = strtolower($credito['periodo']);
$text = "Por el presente pagar�, el suscriptor y los avales, se obligan a pagar incondicionalmente a la ".
    "orden de <b>CREA BIENESTAR, S.A. DE C.V., SOCIEDAD FINANCIERA DE OBJETO M�LTIPLE, ENTIDAD NO REGULADA</b>, ".
    "mediante dep�sito en cualquiera de las sucursales del Banco Mercantil del Norte, S.A de C.V., ".
    "Instituci�n de Banca M�ltiple,  a nombre de CREA BIENESTAR, S.A. DE C.V., SOFOM, ENR, Empresa 122043, ".
    "Referencia {$refBanc} o en las oficinas de la citada sociedad ubicadas en: {$ubicaciones[$credito['id_zona']]['large']}, la ".
    "cantidad de <b>{$credito['total']} ({$totalTexto})</b>, misma que ser� pagada a trav�s de {$credito['plazo']} exhibiciones ".
    "{$periodo} y consecutivas, precisamente en los d�as y por las cantidades establecidas en la tabla de amortizaci�n siguiente:";
$pdf->ezText($text, 9, array('justification'=>'left'));

$pdf->ezText("\n",5);
$cols = array('num'=>"<b>Exhibici�n</b>",
    'fecha'=>"<b>Fecha</b>",
    'pagoFijo'=>"<b>Importe</b>"
);
$options = array('shaded'=>0,
    'fontSize'=>9,
    'rowGap'=>1,
    'showLines'=> 2,
    'xPos'=>'center',
    'xOrientation'=>'center',
);
$pdf->ezTable($tablaAM, $cols,'',$options);
$pdf->ezText("\n",5);

$text = "El suscriptor y los avales reconocen y aceptan que la falta de pago oportuno de una o m�s de las amortizaciones ".
    "estipuladas en el presente PAGAR�, generar� el vencimiento anticipado de los pagos que faltaren por realizar, siendo exigible ".
    "por <b>CREA BIENESTAR, S.A. DE C.V., SOCIEDAD FINANCIERA DE OBJETO M�LTIPLE, ENTIDAD NO REGULADA</b>, el pago ".
    "inmediato de todas las amortizaciones no pagadas, m�s los intereses moratorios respectivos a raz�n de una tasa del ".
    "{$mora}% (m�s IVA) sobre el monto de la parcialidad vencida y no pagada, por cada uno de los d�as naturales ".
    "transcurridos desde la fecha en que el pago debi� realizarse y hasta la fecha en que efectivamente se realice.";
$pdf->ezText($text, 9, array('justification'=>'left'));
$pdf->ezText("\n",9);

$text = "LUGAR Y FECHA DE SUSCRIPCI�N: {$ubicaciones[$credito['id_zona']]['short']} a " . strftime("%d de %B de %Y", strtotime($fechaEntrega));
$pdf->ezText($text, 9, array('justification'=>'left'));
$pdf->ezText("\n\n",11);

$pdf->ezText("<b>EL SUSCRIPTOR</b>\n", 9, array('justification'=>'center'));
$pdf->ezText($firmas['suscriptor']['nombre'], 9, array('justification'=>'center'));
$pdf->ezText($firmas['suscriptor']['direccion']."\n\n\n", 8, array('justification'=>'center'));

$pdf->ezText("<b>AVALES</b>\n", 9, array('justification'=>'center'));
$pdf->ezText($firmas['aval']['nombre'], 9, array('justification'=>'center'));
$pdf->ezText($firmas['aval']['direccion'], 8, array('justification'=>'center'));

$pdf->ezStopPageNumbers(1,1,$i);
//Detenemos la inserci�n de encabezado y pie de p�gina para las p�ginas subsecuentes.
$pdf->stopObject($header);
$pdf->stopObject($title);
$pdf->stopObject($footer);
?>