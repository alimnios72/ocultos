<!-- Tray -->
<div id="tray" class="box">
    <p class="f-left box">
    <!-- Switcher -->
	<span class="f-left" id="switcher">
	    <a href="#" rel="1col" class="styleswitch ico-col1" title="Display one column"><img src="<?php echo RUTA_IMG ;?>switcher-1col.gif" alt="1 Column" /></a>
	    <a href="#" rel="2col" class="styleswitch ico-col2" title="Display two columns"><img src="<?php echo RUTA_IMG ;?>switcher-2col.gif" alt="2 Columns" /></a>
	</span>
        Esconder barra lateral
    </p>
    <p class="f-right">Usuario: <strong><a href="usuario.php"><?php echo $_SESSION['userName']; ?></a></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><a href="logout.php" id="logout">SALIR</a></strong></p>
</div> <!--  /tray -->
<hr class="noscreen" />

<!-- Menu -->
    <div id="menu" class="box">
	<ul class="box">
	    <li <?php if(isset($topmenu['ini'])) {echo $topmenu['ini'];}?>><a href="index.php"><span>Inicio</span></a></li>
        <li <?php if(isset($topmenu['admin'])) {echo $topmenu['admin'];}?>><a href="administracion.php"><span>Administración</span></a></li>
        <?php if($_SESSION['priv'] != 'juridico') {?>
	        <li <?php if(isset($topmenu['conta'])) {echo $topmenu['conta'];}?>><a href="contabilidad.php"><span>Contabilidad</span></a></li>
		    <li <?php if(isset($topmenu['venta'])) {echo $topmenu['venta'];}?>><a href="ventas.php"><span>Ventas</span></a></li>
		<?php } ?>
        <li <?php if(isset($topmenu['cobra'])) {echo $topmenu['cobra'];}?>><a href="cobranza.php"><span>Jurídico</span></a></li>
        <?php if($_SESSION['priv'] == 'admin') {?>
        	<li <?php if(isset($topmenu['sist'])) {echo $topmenu['sist'];}?>><a href="sistema.php"><span>Sistema</span></a></li>
        	<li <?php if(isset($topmenu['repor'])) {echo $topmenu['repor'];}?>><a href="reportes.php"><span>Reportes</span></a></li>
        <?php } ?>
	</ul> 
    </div> <!-- /header -->