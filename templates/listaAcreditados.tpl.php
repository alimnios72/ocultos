<?php $montoTotal = 0; ?>
<table>
    <tbody>
    <tr>
        <th>Nombre</th>
        <th>Monto</th>
    </tr>
    <?php $c = true; ?>
    <?php foreach($acreditados as $acreditado) { ?>
    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
        <td>
            <input type='hidden' name="acreditados[]" value="<?php echo $acreditado['id_acreditado'];?>" />
            <?php echo $acreditado['nombreCompleto']; ?>
        </td>
        <td>
            $<input type="text" name="montos[]" value="<?php echo $acreditado['cantidad']; ?>" />
        </td>
    </tr>
    <?php
        $montoTotal += $acreditado['cantidad'];
        }
    ?>
</table>
 <label for="montoTotal">Monto total:
        $<input class="input-text" type="text" name="montoTotal" id="montoTotal" size="5" READONLY value="<?php echo $montoTotal; ?>"/>
        <img id="updateMonto" src="<?php echo RUTA_IMG;?>Knob Refresh.png" />
</label>