<?php if(empty($delegaciones)){?>
	<p class='msg warning'>No existen delegaciones/municipios para este estado.</p>
<?php }else{?>
<table>
	<tbody>
	<tr>
		<th>Delegación/Municipio</th>
		<th>&nbsp;</th>
	</tr>
	<?php $c = true; ?>
    <?php foreach($delegaciones as $del) { ?>
    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
        <td><input type="text" class="input-text" id="del_<?php echo $del["id_delegacion"]; ?>" value="<?php echo $del["nombre_del"]; ?>" /></td>
        <td><a href="#" id="saveDel_<?php echo $del["id_delegacion"]; ?>"><img src="<?php echo RUTA_IMG;?>save.png" /></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>
<?php } ?>