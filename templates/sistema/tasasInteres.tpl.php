<h1>Configurar tasas de inter�s</h1>
<?php if(isset($_GET['msg'])){
		if($_GET['msg'] == "ok"){ ?>
			<p class="msg done"><?php echo "Tasa guardada con �xito."?></p>
		<?php }else { ?>
			<p class="msg warning"><?php echo "Esa tasa ya existe!"?></p>
<?php } }?>
<form action="sistema.php?content=tasas" method="post">
<fieldset>
	<legend>Nueva tasa</legend>
	<label for="interesOrdinario">Tasa de inter�s ordinario
		<input type="text" class="input-text" name="intOrd" size="2" />%
	</label>
	<label for="interesMoratorio">Tasa de inter�s moratorio
		<input type="text" class="input-text" name="intMor" size="2" />%
	</label>
	<input type="submit" class="input-submit" name="nuevo" value="Agregar" />
</fieldset>
<fieldset>
	<legend>Tasas de inter�s</legend>
	<div class="col50">
	<?php if(empty($intOrd)){?>
		<p class='msg warning'>No existe ninguna tasa registrada en el sistema</p>
	<?php }else{?>
		<table>
			<caption>Inter�s ordinario</caption>
			<tbody>
			<tr>
				<th>Tasa</th>
				<th>Activa</th>
				<th>&nbsp;</th>
			</tr>
			<?php $c = true; ?>
		    <?php foreach($intOrd->tasa as $tasa) { ?>
		    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
		        <td><?php echo $tasa->monto; ?>%</td>
		        <td><input type="checkbox" name="tasasOrd[]" id="tasaOrd_<?php echo $tasa->monto; ?>" <?php if(intval($tasa->activo)) echo "CHECKED"; ?>/></td>
		        <td><a href="#" id="saveTasaOrd_<?php echo $tasa->monto; ?>"><img src="<?php echo RUTA_IMG;?>save.png" /></a></td>
		    </tr>
		    <?php } ?>
		    </tbody>
		</table>
	<?php }?>
	</div>
	<div class="col50 f-right">
	<?php if(empty($intMor)){?>
		<p class='msg warning'>No existe ninguna tasa registrada en el sistema</p>
	<?php }else{?>
		<table>
			<caption>Inter�s moratorio</caption>
			<tbody>
			<tr>
				<th>Tasa</th>
				<th>Activa</th>
				<th>&nbsp;</th>
			</tr>
			<?php $c = true; ?>
		    <?php foreach($intMor->tasa as $tasa) { ?>
		    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
		        <td><?php echo $tasa->monto; ?>%</td>
		        <td><input type="radio" name="activoMora" id="tasaMor_<?php echo $tasa->monto; ?>" <?php if(intval($tasa->activo)) echo "CHECKED"; ?>/></td>
		        <td><a href="#" id="saveTasaMor_<?php echo $tasa->monto; ?>"><img src="<?php echo RUTA_IMG;?>save.png" /></a></td>
		    </tr>
		    <?php } ?>
		    </tbody>
		</table>
	<?php }?>
	</div>
	<br />
</fieldset>
</form>