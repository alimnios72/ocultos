<h1>Configurar zonas</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == 'ok'){?>
	<p class='msg done'>Guardado con �xito!</p>
<?php }?>
<form action="sistema.php?content=zonas" method="post">
<fieldset>
	<legend>Agregar nueva</legend>
	<label for="estado">Escoge un estado para la zona:
		<?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'));?>
	</label>
	<div id="deleg"></div>
	<label for="zona">Especifica el nombre de la zona:
		<input type="text" class="input-text" name="zona" id="zona" />
	</label>
	<input type="submit" class="input-submit" name="nuevo" value="Agregar" />
</fieldset>
<fieldset>
	<legend>Modificar</legend>
	<label for="estado_z">Estado:
		<?php getComboBox($estados, 'estado_z', 'estado_z', array('value'=>'id_estado','text'=>'nombre_edo'));?>
	</label>
	<div id="delegZ"></div>
	<div id="zonas"></div>
</fieldset>
</form>