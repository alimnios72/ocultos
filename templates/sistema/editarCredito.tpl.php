<h1>Editar cr�dito</h1>
<?php if(isset($_GET['msg'])){
	if($_GET['msg']=='ok'){?>
	<p class="msg done">Datos guardados con �xito.</p>
	<?php }else{?>
	<p class="msg error">Error al guardar cr�dito.</p>
<?php } }?>
<form id="forma" action="sistema.php?content=editar&idCredito=<?php echo $_GET['idCredito']; ?>" method="post">
<input type="hidden" name="id_credito" id="id_credito" value="<?php echo $_GET['idCredito']; ?>" />
<fieldset>
<legend>Datos cr�dito</legend>
	<h4>Cr�dito: <?php echo $credito['nombreGrupo']; ?></h4>
	<div class="col50">
		<label for="expediente">Expediente
			<input type="text" class="input-text" name="expediente" value="<?php echo $credito['expediente'];?>" size=3 />
		</label>
		<label for="producto">Producto
			<?php getComboBox($productoCred, 'producto', 'producto', array('value'=>'id','text'=>'txt'), $credito['producto']); ?>
		</label>
		<label for="status">Status
			<?php getComboBox($statusCred, 'status', 'status', array('value'=>'id','text'=>'txt'), $credito['status']); ?>
		</label>
		<label for="tasaInteres">Tasa de inter�s
			<input type="text" class="input-text" name="tasaInteres" value="<?php echo $credito['tasa_interes'];?>" size=2 />
		</label>
	</div>
	<div class="col50 f-right">
		<label for="plazo">Plazo
			<input type="text" class="input-text" name="plazo" value="<?php echo $credito['plazo'];?>" size=2 /> MESES
		</label>
		<label for="periodo">Periodo
			<?php getComboBox($periodoCred, 'periodo', 'periodo', array('value'=>'val','text'=>'val'), $credito['periodo']); ?>
		</label>
		<label for="zona">Zona
			<?php getComboBox($zonas, 'zona', 'zona', array('value'=>'id_zona','text'=>'zona'), $credito['id_zona']); ?>
		</label>
		<label for="vendedor">Vendedor
			<?php getComboBox($vendedores, 'vendedor', 'vendedor', array('value'=>'id','text'=>'nombre'), $credito['id_usuario']); ?>
		</label>
		<label for="banco">Depositar en
			<select name="banco" id="banco">
				<option <?php if($credito['cta_deposito']== "3") echo 'selected="selected"';?> value="3">BBVA Bancomer</option>
				<option <?php if($credito['cta_deposito']== "1") echo 'selected="selected"';?> value="1">Afirme/Bajio</option>
			</select> 
		</label>
	</div>
</fieldset>
<div id="dialog-confirm" title="Borrar pagos">
    <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Esta acci�n borrar� todos los pagos. �Est� seguro?</p>
</div>
<div id="dialog-confirm-cred" title="Borrar cr�dito" style="width=250px;height=160px;">
    <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Esta acci�n borrar� toda la informaci�n del cr�dito . �Est� seguro?</p>
</div>
<input type="submit" class="input-submit" name="update" value="Guardar" />
<input type="submit" class="input-submit" name="borrarPagos" id="borrarPagos" value="Borrar pagos" />
<input type="submit" class="input-submit" name="borrarCredito" id="borrarCredito" value="Borrar cr�dito" />
<fieldset>
    <legend>Acreditados</legend>
    <div id="acreditados"><?php include('editAcreditados.tpl.php');?></div>
</fieldset>
</form>