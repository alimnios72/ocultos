<?php if(empty($zonas)){?>
	<p class='msg warning'>No existen zonas para esta delegación/municipio.</p>
<?php }else{?>
<table>
	<tbody>
	<tr>
		<th>Zonas</th>
		<th>&nbsp;</th>
	</tr>
	<?php $c = true; ?>
    <?php foreach($zonas as $zona) { ?>
    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
        <td><input type="text" class="input-text" id="zona_<?php echo $zona["id_zona"]; ?>" value="<?php echo $zona["nombre_zona"]; ?>" /></td>
        <td><a href="#" id="saveZona_<?php echo $zona["id_zona"]; ?>"><img src="<?php echo RUTA_IMG;?>save.png" /></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>
<?php } ?>