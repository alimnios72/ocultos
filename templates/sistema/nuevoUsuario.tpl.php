<h1>Nuevo usuario del sistema</h1>
<?php if(isset($_GET['msg'])){
	if($_GET['msg']=='ok'){?>
	<p class="msg done">Usuario agregado con �xito.</p>
	<?php }elseif($_GET['msg']=='campos'){?>
	<p class="msg error">No puedes dejar campos vacios</p>
	<?php }elseif($_GET['msg']=='pass'){?>
	<p class="msg error">Las contrase�as no coinciden</p>
<?php } }?>
<form action="sistema.php?content=usuarios" method="post">
<fieldset>
<legend>Datos del usuario</legend>
	<label for="nombres">Nombres
		<input type="text" class="input-text" name="nombres" id="nombres" />
	</label>
	<label for="apellidoP">Apellido Paterno
		<input type="text" class="input-text" name="apellidoP" id="apellidoP" />
	</label>
	<label for="apellidoM">Apellido Materno
		<input type="text" class="input-text" name="apellidoM" id="apellidoM" />
	</label>
	<label for="usuario">Nombre de usuario
		<input type="text" class="input-text" name="usuario" id="usuario" />
	</label>
	<label for="nacimiento">Fecha de nacimiento
		<input type="text" class="input-text" name="nacimiento" id="nacimiento" />
	</label>
	<label for="password">Contrase�a
		<input type="password" class="input-text" name="password" id="password"  />
	</label>
	<label for="password_c">Confirmar contrase�a
		<input type="password" class="input-text" name="password_c" id="password_c" />
	</label>
	<label for="privilegios">Privilegios
		<select name="privilegios" id="privilegios">
			<option value='ventas'>Vendedor</option>
			<option value='captura'>Capturista</option>
		</select>
	</label>
	<input type="submit" class="input-submit" name="add" value="Agregar" />
</fieldset>
</form>