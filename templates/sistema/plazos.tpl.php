<h1>Configurar plazos</h1>
<?php if(isset($_GET['msg'])){
		if($_GET['msg'] == "ok"){ ?>
			<p class="msg done"><?php echo "Plazo guardado con �xito."?></p>
		<?php }else { ?>
			<p class="msg warning"><?php echo "Ese plazo ya existe!"?></p>
<?php } }?>
<form action="sistema.php?content=plazos" method="post">
<fieldset>
	<legend>Nueva plazo</legend>
	<label for="plazo">Plazo
		<input type="text" class="input-text" name="plazo" size="2" /> MESES
	</label>
	<input type="submit" class="input-submit" name="nuevo" value="Agregar" />
</fieldset>
<fieldset>
	<legend>Plazos</legend>
	<div>
	<?php if(empty($plazos)){?>
		<p class='msg warning'>No existe ning�n plazo registrado en el sistema</p>
	<?php }else{?>
		<table>
			<tbody>
			<tr>
				<th>Plazo</th>
				<th>Activo</th>
				<th>&nbsp;</th>
			</tr>
			<?php $c = true; ?>
		    <?php foreach($plazos->plazo as $plazo) { ?>
		    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
		        <td><?php echo $plazo->meses; ?> MESES</td>
		        <td><input type="checkbox" name="plazos" id="plazo_<?php echo $plazo->meses; ?>" <?php if(intval($plazo->activo)) echo "CHECKED"; ?>/></td>
		        <td><a href="#" id="savePlazo_<?php echo $plazo->meses; ?>"><img src="<?php echo RUTA_IMG;?>save.png" /></a></td>
		    </tr>
		    <?php } ?>
		    </tbody>
		</table>
	<?php }?>
	</div>
	<br />
</fieldset>
</form>