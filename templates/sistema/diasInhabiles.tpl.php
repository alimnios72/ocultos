<h1>Administrar d�as inhabiles bancarios</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == 'ok'){?>
	<p class='msg done'>Guardado con �xito!</p>
<?php }?>
<form action="sistema.php?content=diasInhabiles" method="post">
<fieldset>
	<legend>Nuevo d�a inh�bil</legend>
	<label for="diainhabil">Fecha del d�a inh�bil:
		<input type="text" class="input-text" name="diainhabil" id="diainhabil" />
	</label>
	<label for="descripcion">Descripci�n:
		<input type="text" class="input-text" name="descripcion" id="descripcion" />
	</label>
	<input type="submit" class="input-submit" name="nuevo" value="Agregar" />
</fieldset>
<fieldset>
	<legend>Lista de d�as inh�biles</legend>
	<label for="a�o">Seleccione el a�o:
		<select name="anio">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
		</select>
	</label>
	<table>
	<tbody>
	<tr>
		<th>D�a Inh�bil</th>
		<th>Descripci�n</th>
	</tr>
	<?php if(!empty($diasInh)){?>
		<?php $c = true; ?>
	    <?php foreach($diasInh as $dia) { ?>
	    <tr <?php echo (($c = !$c)?' class="bg"':''); ?>>
	        <td><?php echo ucfirst(strftime("%A %d %B %Y", strtotime($dia["fecha"]))); ?></td>
	        <td><?php echo $dia["descripcion"]; ?></td>
	    </tr>
	    <?php } ?>
    <?php } ?>
    </tbody>
</table>
</fieldset>
</form>