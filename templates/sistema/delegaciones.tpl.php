<h1>Configurar delegaciones/municipios</h1>
<?php if(isset($_GET['msg']) && $_GET['msg'] == 'ok'){?>
	<p class='msg done'>Guardado con �xito!</p>
<?php }?>
<form action="sistema.php?content=delegaciones" method="post">
<fieldset>
	<legend>Agregar nuevo</legend>
	<label for="estado">Selecciona el estado al que deseas agregar una nueva delegaci�n/municipio:
		<?php getComboBox($estados, 'estado', 'estado', array('value'=>'id_estado','text'=>'nombre_edo'));?>
	</label>
	<label for="delegacion">Especifica el nombre de la delegaci�n/municipio:
		<input type="text" class="input-text" name="delegacion" id="delegacion" />
	</label>
	<input type="submit" class="input-submit" name="nuevo" value="Agregar" />
</fieldset>
<fieldset>
	<legend>Modificar</legend>
	<label for="estado">Selecciona un estado para modificar sus delegaciones/municipios:
		<?php getComboBox($estados, 'estado_edit', 'estado_edit', array('value'=>'id_estado','text'=>'nombre_edo'));?>
	</label>
	<div id="delegaciones"></div>
</fieldset>
</form>