<?php
require_once('DBClass.php');
class Transaccion extends DB{
	
	/********************************************************************/
	function updateAmortizacion($data,$mov){
		$values = array(':in'=>array('data'=>$data['interes'],'type'=>'i'),
						':iv'=>array('data'=>$data['iva'],'type'=>'i'),
						':ca'=>array('data'=>$data['capital'],'type'=>'i'),
						':id'=>array('data'=>$mov,'type'=>'i'),);
		$sql = "UPDATE movimientos SET interes=:in,iva=:iv,capital=:ca,monto=interes+iva+capital 
				WHERE id_movimiento = :id";
		var_dump($this->setDataByQuery($sql, $values));
	}
	/********************************************************************/
	
	function addDeposito($info){
		$values = array(':b'=>array('data'=>$info['banco'], 'type'=>'i'),
						':m'=>array('data'=>$info['monto'], 'type'=>'s'),
						':f'=>array('data'=>$info['fecha'], 'type'=>'s'),
						':r'=>array('data'=>$info['clave'], 'type'=>'s'),
						':o'=>array('data'=>$info['observaciones'], 'type'=>'s'),
						':l'=>array('data'=>$info['folio'], 'type'=>'s'),
						':c'=>array('data'=>$info['tipo_cuenta'], 'type'=>'i'));
		$sql = "INSERT INTO depositos_bancarios (id_banco, monto, fecha_deposito, clave_deposito, 
				aplicado, observaciones, folio, id_cuenta)
				VALUES (:b,:m,:f,:r,0,:o,:l,:c)";
		if($this->setDataByQuery($sql, $values))
			return $this->lastId;
		else 
			return false;
	}
	
	function addDispChequeCobrado($idBanco, $obsv, $noCheque){
		$values = array(':b'=>array('data'=>$idBanco,'type'=>'i'),
						':o'=>array('data'=>utf8_decode($obsv),'type'=>'s'),
						);
		$sql = "INSERT INTO disposiciones_credito (id_banco, tipo_disp, cantidad, observaciones)
				VALUES (:b, 0, 0, :o)";
		if($this->setDataByQuery($sql, $values)){
			$idDisp = $this->lastId;
			$this->addChequeCobrado($idDisp, $noCheque);
		}
	}
	
	private function addChequeCobrado($idDisp, $noCheque){
		$values = array(':id'=>array('data'=>$idDisp,'type'=>'i'),
						':n'=>array('data'=>$noCheque,'type'=>'i'),
		);
		$sql = "INSERT INTO cheques (id_disposicion, no_cheque, status, fecha_elaboracion, fecha_cobro)
				VALUES (:id, :n, 2, NOW(), NOW())";
		$this->setDataByQuery($sql, $values);
	}
	
	function addCuentaEfectivo($data){
		$values = array(':n'=>array('data'=>$data['nombre_cuenta'], 'type'=>'s'),
						':t'=>array('data'=>$data['tipo_cuenta'], 'type'=>'i'),
						':d'=>array('data'=>$data['descripcion'], 'type'=>'s'),
				);
		$sql = "INSERT INTO cuentas_efectivo (nombre_cuenta,tipo_cuenta,descripcion)
				VALUES (:n,:t,:d)";
		return $this->setDataByQuery($sql, $values) ? true: false;
	}
	
	function addTransaccionEfectivo($data){
		$values = array(':f'=>array('data'=>$data['fecha'],'type'=>'s'),
						':d'=>array('data'=>$data['descripcion'],'type'=>'s'),
						':u'=>array('data'=>$data['userID'],'type'=>'i'),
		);
		$sql = "INSERT INTO transacciones_efectivo (fecha, fecha_realizada, descripcion,administrador)
				VALUES (:f,NOW(),:d,:u)";
		if($this->setDataByQuery($sql, $values)){
			$id = $this->lastId;
			$this->addPartidaEfectivo($id, $data['cta1'], $data['monto']);
			$this->addPartidaEfectivo($id, $data['cta2'], -1*$data['monto']);
			return true;
		}
		return false;
		
	}
	
	function addPartidaEfectivo($idtrans, $idcta, $monto){
		$values = array(':t'=>array('data'=>$idtrans,'type'=>'i'),
						':c'=>array('data'=>$idcta,'type'=>'i'),
						':m'=>array('data'=>$monto,'type'=>'s'),
		);
		$sql = "INSERT INTO partidas_efectivo (id_transaccion, id_cuenta, cantidad)
				VALUES (:t,:c,:m)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addCheque($idAcred, $monto, $banco, $noCheque){
		$exists = $this->existsCheque($idAcred);
		if(empty($exists)){
			$values = array(':b'=>array('data'=>$banco,'type'=>'i'),
							':c'=>array('data'=>$monto,'type'=>'s'),
						);
			$sql = "INSERT INTO disposiciones_credito (id_banco, tipo_disp, cantidad)
					VALUES (:b, 0, :c)";
			if($this->setDataByQuery($sql, $values)){
				$idDisp = $this->lastId;
				$this->addDisposicionAcreditado($idDisp, $idAcred);
				$this->addDisposicionCheque($idDisp, $noCheque);
			}
		}
		else 
			$this->updateCheque($exists[0]['id_disposicion'], $monto, $banco, $noCheque);
	}
	
	private function addDisposicionAcreditado($idDisp, $idAcred){
		$values = array(':d'=>array('data'=>$idDisp,'type'=>'i'),
						':a'=>array('data'=>$idAcred,'type'=>'i'),
						);
		$sql = "INSERT INTO disposiciones_acreditados (id_disposicion, id_acreditado)
				VALUES (:d, :a)";
		$this->setDataByQuery($sql, $values);
	}
	
	private function addDisposicionCheque($idDisp, $noCheque){
		$values = array(':d'=>array('data'=>$idDisp,'type'=>'i'),
						':n'=>array('data'=>$noCheque,'type'=>'i'),
		);
		$sql = "INSERT INTO cheques (id_disposicion, no_cheque, status, fecha_elaboracion)
				VALUES (:d, :n, 0, NOW())";
		$this->setDataByQuery($sql, $values);
	}
	
	function addMovimiento($idAcred, $tipo, $data, $tipoCargoAbono){
		$values = array(':id'=>array('data'=>$idAcred,'type'=>'i'),
						':t'=>array('data'=>$tipo,'type'=>'i'),
						':m'=>array('data'=>($tipo==0)?$data['pagoFijo']:-$data['pagoFijo'],'type'=>'s'),
						':in'=>array('data'=>($tipo==0)?$data['interes']:-$data['interes'],'type'=>'s'),
						':iv'=>array('data'=>($tipo==0)?$data['iva']:-$data['iva'],'type'=>'s'),
						':c'=>array('data'=>($tipo==0)?$data['capital']:-$data['capital'],'type'=>'s'),
						':f'=>array('data'=>$data['fecha'],'type'=>'s'),
						);
		$sql = "INSERT INTO movimientos (id_acreditado, tipo_mov, monto, interes, iva, capital, fecha_mov)
				VALUES (:id, :t, :m, :in, :iv, :c, :f)";
		if($this->setDataByQuery($sql, $values)){
			$idMov = $this->lastId;
			if($tipo == 0)
				$this->addCargo($idMov, $tipoCargoAbono);
			else
				$this->addPago($idMov, $data['id_deposito'], $tipoCargoAbono);
		}
	}
	
	private function addPago($idMov, $deposito, $tipoAbono){
		$values = array(':id'=>array('data'=>$idMov,'type'=>'i'),
						':dep'=>array('data'=>$deposito,'type'=>'i'),
						':t'=>array('data'=>$tipoAbono,'type'=>'i'),
						);
		$sql = "INSERT INTO abonos (id_movimiento, id_deposito, tipo_abono)
				VALUES (:id,:dep, :t)";
		$this->setDataByQuery($sql, $values);
	}
	
	private function addCargo($idMov, $tipoCargo){
		$values = array(':id'=>array('data'=>$idMov,'type'=>'i'),
						':t'=>array('data'=>$tipoCargo,'type'=>'i'),
						);
		$sql = "INSERT INTO cargos (id_movimiento, tipo_cargo, status)
				VALUES (:id, :t, 0)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addArchivoReferenciaBancaria($idCta,$fileName,$fechaIni,$fechaFin,$pagos,$idUser){
		$values = array(':idC'=>array('data'=>$idCta,'type'=>'i'),
						':a'=>array('data'=>$fileName,'type'=>'s'),
						':fi'=>array('data'=>$fechaIni,'type'=>'s'),
						':ff'=>array('data'=>$fechaFin,'type'=>'s'),
						':p'=>array('data'=>$pagos,'type'=>'i'),
						':idU'=>array('data'=>$idUser,'type'=>'i'),
		);
		$sql = "INSERT INTO referencias_bancarias (id_cuenta, archivo, fecha_inicial, fecha_final, 
				pagos_conciliados, id_usuario)
				VALUES (:idC, :a, :fi, :ff, :p, :idU)";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateCheque($idDisp, $monto, $banco, $noCheque){
		$values = array(':id'=>array('data'=>$idDisp,'type'=>'i'),
				':b'=>array('data'=>$banco,'type'=>'i'),
				':c'=>array('data'=>$monto,'type'=>'s'),
				':n'=>array('data'=>$noCheque, 'type'=>'i'),
		);
		$sql = "UPDATE disposiciones_credito DC, cheques CH 
				SET DC.id_banco = :b, DC.cantidad = :c, CH.no_cheque = :n
				WHERE DC.id_disposicion = :id AND CH.id_disposicion=DC.id_disposicion";
		return $this->setDataByQuery($sql, $values); 
	}
	
	function updateChequeCobrado($idDisp, $noCheque, $status, $fecha, $obs=""){
		$criterio = array();
		if($idDisp != ''){
			$values[':id'] = array('data'=>$idDisp,'type'=>'i');
		}
		if($noCheque != ''){
			$criterio[] = 'no_cheque = :n';
			$values[':n'] = array('data'=>$noCheque,'type'=>'i');
		}
		if($status != ''){
			$criterio[] = 'status = :s';
			$values[':s'] = array('data'=>$status,'type'=>'i');
		}
		if($fecha != ''){
			$criterio[] = 'fecha_cobro = :f';
			$values[':f'] = array('data'=>$fecha,'type'=>'s');
		}
		$criterio = implode(' , ',$criterio);
		
		$sql = "UPDATE cheques SET {$criterio} 
				WHERE id_disposicion = :id";
		if($this->setDataByQuery($sql, $values)){
			$this->updateDisposicionCheque($idDisp, $obs);
		}
	}
	
	function updateStatusAmortizacionesByCredito($idCred, $status){
		$values = array(':id'=>array('data'=>$idCred,'type'=>'i'),
						':s'=>array('data'=>$status,'type'=>'i'),
		);
		$sql = "UPDATE cargos CA 
				JOIN movimientos M ON M.id_movimiento=CA.id_movimiento 
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado 
				JOIN creditos C ON C.id_credito=A.id_credito
				SET CA.status=:s
				WHERE C.id_credito=:id";
		$this->setDataByQuery($sql, $values);
	}
	
	private function updateDisposicionCheque($idDisp, $obs){
		$values = array(':id'=>array('data'=>$idDisp,'type'=>'i'),
						':o'=>array('data'=>utf8_decode($obs),'type'=>'s'),
						);
		$sql = "UPDATE disposiciones_credito SET observaciones = :o
				WHERE id_disposicion = :id";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateDeposito($data){
		$values = array(':id'=>array('data'=>$data['id_deposito'],'type'=>'i'),
						':b'=>array('data'=>$data['id_banco'],'type'=>'i'),
						':m'=>array('data'=>$data['monto'],'type'=>'s'),
						':f'=>array('data'=>$data['fecha_deposito'],'type'=>'s'),
						':c'=>array('data'=>$data['clave_deposito'],'type'=>'s'),
						':a'=>array('data'=>$data['aplicado'],'type'=>'i'),
						':o'=>array('data'=>$data['observaciones'],'type'=>'s'),
					);
		$sql = "UPDATE depositos_bancarios SET id_banco=:b, monto=:m, fecha_deposito=:f,
				clave_deposito=:c, aplicado = :a, observaciones = :o
				WHERE id_deposito = :id";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateCargo($fecha, $idCred, $status){
		$values = array(':f'=>array('data'=>$fecha,'type'=>'s'),
						':c'=>array('data'=>$idCred, 'type'=>'i'),
						':s'=>array('data'=>$status,'type'=>'i'),
					);
		$sql = "UPDATE cargos CA
				JOIN movimientos M ON M.id_movimiento = CA.id_movimiento
				JOIN acreditados A ON A.id_acreditado = M.id_acreditado
				JOIN creditos C ON C.id_credito = A.id_credito
				SET CA.status=:s
				WHERE M.fecha_mov = :f AND C.id_credito = :c";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateDepositoById($data){
		$values = array(':id'=>array('data'=>$data['id_deposito'],'type'=>'i'),
						':d'=>array('data'=>$data['banco'],'type'=>'i'),
						':m'=>array('data'=>$data['monto'], 'type'=>'s'),
						':f'=>array('data'=>$data['fecha'],'type'=>'s'),	
						':c'=>array('data'=>$data['clave'],'type'=>'s'),
						':o'=>array('data'=>$data['observaciones'],'type'=>'s'),
						':l'=>array('data'=>$data['folio'], 'type'=>'s'),
						':u'=>array('data'=>$data['tipo_cuenta'], 'type'=>'i'),
						);
		$sql = "UPDATE depositos_bancarios
				SET id_banco=:d, monto=:m, fecha_deposito=:f, clave_deposito=:c,
				observaciones=:o, folio=:l, id_cuenta=:u
				WHERE id_deposito=:id";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateMovimiento($data){
		$sql = "UPDATE movimientos SET {0} monto=interes+iva+capital WHERE id_movimiento=:id";
		$values = array(':id'=>array('data'=>$data['id_movimiento'],'type'=>'i'));
		$columns = array('id_acreditado'=>array(':a','i'),
						 'tipo_mov'=>array(':t','i'),
						 'interes'=>array(':i','s'),
						 'iva'=>array(':v','s'),
						 'capital'=>array(':c','s'),
						 'fecha_mov'=>array(':f','s'),
		);
		$tmp = "";
		foreach($columns as $key=>$val){
			if(isset($data[$key]) && $data[$key] !== ''){
				$tmp .= "{$key}={$val[0]},";
				$values[$val[0]] = array('data'=>$data[$key],'type'=>$val[1]);
			}
			else 
				$tmp .= "{$key}={$key},";
		}
		$sql = str_replace("{0}", $tmp, $sql);
		$this->setDataByQuery($sql, $values);
	}
	
	function updateStatusDepositos($depositos, $status){
		$depositos = implode(', ', $depositos);
		$values = array(':s'=>array('data'=>$status,'type'=>'i'),
						);
		$sql = "UPDATE depositos_bancarios SET aplicado=:s
				WHERE id_deposito IN ({$depositos})";
		$this->setDataByQuery($sql, $values);
	}
	
	function getChequesByCredito($id){
		$values = array(':id'=>$id);
		$sql = "SELECT A.*, A.cantidad AS prestamo, DC.*, DC.cantidad AS montoCheque, CH.*,
				CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) as acreditado, B.*
				FROM acreditados AS A
				LEFT JOIN disposiciones_acreditados AS DA ON DA.id_acreditado = A.id_acreditado
				LEFT JOIN disposiciones_credito AS DC ON DC.id_disposicion = DA.id_disposicion
				LEFT JOIN cheques AS CH ON CH.id_disposicion = DC.id_disposicion
				LEFT JOIN cuentas_bancarias AS B ON B.id_banco = DC.id_banco
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN personas AS P ON P.id_persona = A.id_persona
				WHERE C.id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCuentasEfectivo($data){
		$values = array();
		$criterio = array();
		
		if(isset($data['tipo_cuenta'])){
			$criterio[] = 'tipo_cuenta = :t';
			$values[':t'] = $data['tipo_cuenta'];
		}
		
		$criterio = empty($data) ? "1=1" : implode(' AND ',$criterio);
		$sql = "SELECT * 
				FROM cuentas_efectivo
				WHERE {$criterio}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDisposicionCreditoByCheque($noCheque,$banco){
		$values = array(':ch'=>$noCheque, ':b'=>$banco);
		$sql = "SELECT * FROM cheques CH
			JOIN disposiciones_credito DC ON DC.id_disposicion=CH.id_disposicion
			WHERE CH.no_cheque=:ch AND DC.id_banco=:b";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getChequesByCriterio($data){
		$values = array();
		$criterio =  array();
		$left = "LEFT";
		//Criterio por defecto es que son cheques
		$criterio[] = 'DC.tipo_disp = 0';
		if(isset($data['banco'])){
			$criterio[] = 'DC.id_banco = :b';
			$values[':b'] = $data['banco'];
		}
		if(isset($data['status'])){
			$criterio[] = 'CH.status = :s';
			$values[':s'] = $data['status'];
		}
		if(isset($data['fechas'])){
			$criterio[] = '(CH.fecha_elaboracion BETWEEN :f1 AND :f2)';
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['rango'])){
			$left = "";
			$criterio[] = '(CH.no_cheque BETWEEN :ini  AND :fin)';
			$values[':ini'] = $data['rango'][0];
			$values[':fin'] = $data['rango'][1];
		}
		//De los criterios seleccionados formamos una cadena para la consulta SQL
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT DC.id_banco, B.banco, B.no_cuenta, CH.fecha_elaboracion, CH.fecha_cobro, DC.cantidad as montoCH, CH.no_cheque, 
				C.tasa_interes, C.plazo, C.periodo, C.tipo, CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) as acreditado, 
				G.nombre AS nombreGpo, Z.nombre_zona, CH.status AS statusCH, DC.id_disposicion, DC.observaciones, C.fecha_entrega
				FROM disposiciones_credito AS DC
				JOIN cheques AS CH ON CH.id_disposicion = DC.id_disposicion
				JOIN cuentas_bancarias AS B ON B.id_banco = DC.id_banco
				{$left} JOIN disposiciones_acreditados AS DA ON DA.id_disposicion = DC.id_disposicion
				{$left} JOIN acreditados AS A ON A.id_acreditado = DA.id_acreditado
				{$left} JOIN creditos AS C ON C.id_credito = A.id_credito
				{$left} JOIN personas AS P ON P.id_persona = A.id_persona
				LEFT JOIN zonas AS Z ON Z.id_zona = C.id_zona
				LEFT JOIN representantes R ON R.id_credito=C.id_credito
				LEFT JOIN agrupaciones G ON G.id_grupo=R.id_grupo
				WHERE {$criterio}
				ORDER BY CH.no_cheque";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	private function existsCheque($idAcred){
		$values = array(':id'=>$idAcred);
		$sql = "SELECT A.*, DC.*
				FROM acreditados AS A
				JOIN disposiciones_acreditados AS DA ON DA.id_acreditado = A.id_acreditado
				JOIN disposiciones_credito AS DC ON DC.id_disposicion = DA.id_disposicion
				JOIN cheques AS CH ON CH.id_disposicion = DC.id_disposicion
				WHERE A.id_acreditado = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDepositosById($id){
		$values = array(':id'=>$id);
		$sql = "SELECT D.*, B.banco, SUM(M.interes) interes, SUM(M.iva) iva, SUM(M.capital) capital, COALESCE(tg.nombre, tp.NombComp) nombreGrupo,
				C.expediente, C.id_credito, AB.tipo_abono
				FROM depositos_bancarios AS D
				JOIN cuentas_bancarias AS B ON B.id_banco =  D.id_banco
				LEFT JOIN abonos AB ON AB.id_deposito = D.id_deposito
				LEFT JOIN movimientos M ON M.id_movimiento = AB.id_movimiento
				LEFT JOIN acreditados A ON A.id_acreditado = M.id_acreditado
				LEFT JOIN creditos C ON C.id_credito = A.id_credito
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE D.id_deposito = :id
				GROUP BY D.id_deposito, D.id_banco, D.clave_deposito, D.aplicado, D.observaciones, D.monto, D.fecha_deposito, B.banco, C.expediente";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getDepositoByCredito($id,$criterios=array()){
		if(isset($criterios['fecha']) && $criterios['fecha'] != '')
			$criterio[] = "M.fecha_mov >= '{$criterios['fecha']}'";
		$criterio[] = "C.id_credito = :id";
		
		$criterio = implode(" AND ",$criterio);
		
		$values = array(':id'=>$id);
		$sql = "SELECT DISTINCT(D.id_deposito), D.id_banco, D.monto, D.fecha_deposito, D.clave_deposito, 
				D.aplicado, D.observaciones, AB.tipo_abono, C.id_credito 
				FROM depositos_bancarios D
				JOIN abonos AB ON AB.id_deposito = D.id_deposito
				JOIN movimientos M ON M.id_movimiento = AB.id_movimiento
				JOIN acreditados A ON A.id_acreditado = M.id_acreditado
				JOIN creditos C ON C.id_credito = A.id_credito
				WHERE {$criterio}
				ORDER BY D.fecha_deposito";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDepositosGreaterThan($id, $date, $equal=false){
		$sign = $equal ? ">=" : ">";
		$values = array(':id'=>$id,':d'=>$date);
		$sql = "SELECT fecha_deposito 
				FROM depositosporcredito 
				WHERE id_credito = :id AND fecha_deposito {$sign} :d";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDepositosByCriterio($data){
		$values = array();
		$criterio =  array();
		
		if(isset($data['aplicado'])){
			$criterio[] = 'D.aplicado = :a';
			$values[':a'] = $data['aplicado'];
		}
		if(isset($data['banco'])){
			$criterio[] = 'D.id_banco = :b';
			$values[':b'] = $data['banco'];
		}
		if(isset($data['fechas'])){
			$criterio[] = '(D.fecha_deposito BETWEEN :f1 AND :f2)';
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['montos'])){
			$criterio[] = '(D.monto BETWEEN :m1  AND :m2)';
			$values[':m1'] = $data['montos'][0];
			$values[':m2'] = $data['montos'][1];
		}
		if(isset($data['byMes'])){
			$criterio[] = 'MONTH(D.fecha_deposito) = :m AND YEAR(D.fecha_deposito) = :y';
			$values[':m'] = $data['byMes'][0];
			$values[':y'] = $data['byMes'][1];
		}
		if(isset($data['fecha'])){
			$criterio[] = 'D.fecha_deposito = :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['clave'])){
			$criterio[] = 'D.clave_deposito LIKE :cd';
			$values[':cd'] = '%'.$data['clave'].'%';
		}
		if(isset($data['folio'])){
			$criterio[] = 'D.folio = :fo';
			$values[':fo'] = $data['folio'];
		}
		//De los criterios seleccionados formamos una cadena para la consulta SQL
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT D.*, B.banco, C.nombre_cuenta
				FROM depositos_bancarios AS D
				JOIN cuentas_bancarias AS B ON B.id_banco =  D.id_banco
				LEFT JOIN cuentas_efectivo C ON C.id_cuenta=D.id_cuenta 
				WHERE {$criterio}
				ORDER BY D.id_banco, D.fecha_deposito";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAmortizacionesByCriterio($data){
		$values = array();
		$criterio =  array();
		
		//Criterio por defecto es un cargo
		$criterio[] =  'M.tipo_mov = 0';
		//Criterio por defecto es cargo de amortizaci�n
		//$criterio[] = 'CA.tipo_cargo = 0';
		//Criterio por defecto debe ser un cr�dito activo
		if(!isset($data['noActivo']))
			$criterio[] = 'C.status = 2';
		if(isset($data['statusAmo'])){
			$criterio[] = 'CA.status = :s';
			$values[':s'] = $data['statusAmo'];
		}
		if(isset($data['fecha'])){
			$criterio[] = 'M.fecha_mov = :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['expediente'])){
			$criterio[] = 'C.expediente = :e';
			$values[':e'] = $data['expediente'];
		}
		if(isset($data['fechas'])){
			$criterio[] = '(M.fecha_mov BETWEEN :f1 AND :f2)';
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['tipo_cargo'])){
			$criterio[] = 'CA.tipo_cargo = :tc';
			$values[':tc'] = $data['tipo_cargo'];
		}
		if(isset($data['cancelado'])){
			if(!$data['cancelado']){
				$criterio[] = "(C.expediente <> 'Cancelado')";
			}
		}
		
		$criterio = implode(' AND ',$criterio);

		
		$sql = "SELECT C.id_credito, SUM(M.monto) AS monto, SUM(M.interes) AS interes, SUM(M.iva) AS iva, SUM(M.capital) AS capital,
				M.fecha_mov, COALESCE(tg.nombre, tp.NombComp) AS nombreGrupo, C.expediente, C.status, CA.tipo_cargo, Z.nombre_zona
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN cargos AS CA ON CA.id_movimiento = M.id_movimiento
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE {$criterio}
				GROUP BY C.id_credito, M.fecha_mov, C.expediente, C.status
				ORDER BY M.fecha_mov, monto";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
		
		// AND M.fecha_mov < DATEADD(day, 14, GETDATE())
	}
	
	function getCargosByCriterio($data){
		$values = array();
		
		//Criterio por defecto es un cargo
		$criterio[] =  'M.tipo_mov = 0';
		if(isset($data['fecha'])){
			$criterio[] = 'M.fecha_mov < :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['idCred'])){
			$criterio[] = 'C.id_credito = :id';
			$values[':id'] = $data['idCred'];
		}
		if(isset($data['tipoCargo'])){
			$criterio[] = 'CA.tipo_cargo = :t';
			$values[':t'] = $data['tipoCargo'];
		}
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva, 
				SUM(M.capital) as capital, M.fecha_mov, CA.tipo_cargo, CA.status
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN cargos AS CA ON CA.id_movimiento = M.id_movimiento
				WHERE  {$criterio}
				GROUP BY M.fecha_mov, CA.tipo_cargo, CA.status
				ORDER BY M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAbonosByCriterio($data){
		$values = array();
		
		//Criterio por defecto es un abono
		$criterio[] =  'M.tipo_mov = 1';
		if(isset($data['fecha'])){
			$criterio[] = 'M.fecha_mov < :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['idCred'])){
			$criterio[] = 'C.id_credito = :id';
			$values[':id'] = $data['idCred'];
		}
		if(isset($data['tipoAbono'])){
			$criterio[] = 'AB.tipo_abono = :t';
			$values[':t'] = $data['tipoAbono'];
		}
		if(isset($data['idAcred'])){
			$criterio[] = 'M.id_acreditado = :a';
			$values[':a'] = $data['idAcred'];
		}
		if(isset($data['fechas'])){
			$criterio[] = '(M.fecha_mov BETWEEN :f1 AND :f2)';
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['cancelado'])){
			if(!$data['cancelado']){
				$criterio[] = "(C.expediente <> 'Cancelado')";
			}
		}
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT C.expediente, C.status, SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva,
				SUM(M.capital) as capital, M.fecha_mov, AB.tipo_abono, AB.id_deposito,
				COALESCE(MIN(tg.nombre), MIN(tp.nombcomp)) AS nombreGrupo, GROUP_CONCAT(M.id_movimiento) movimientos 
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN abonos AS AB ON AB.id_movimiento = M.id_movimiento
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE  {$criterio}
				GROUP BY M.fecha_mov, AB.tipo_abono, AB.id_deposito, C.expediente, C.status
				ORDER BY M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
			
		//COALESCE(MIN(tg.nombre), MIN(tp.NombComp)) AS nombreGrupo
		//LEFT OUTER JOIN (SELECT G.nombre FROM grpos) tg ON tg.id_credito = C.id_credito
		//		LEFT OUTER JOIN (SELECT CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) AS NombComp FROM personas P) tp
		//						ON tp.id_persona=A.id_persona
	}
	function getMovimientosByDeposito($deposito){
		$values = array(':id'=>$deposito);
		$sql = "SELECT M.*, AB.tipo_abono, C.id_credito
				FROM movimientos M 
				JOIN abonos AB ON AB.id_movimiento=M.id_movimiento
				JOIN depositos_bancarios D ON D.id_deposito=AB.id_deposito
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE D.id_deposito=:id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getMovimientosByCredito($idCred){
		$values = array(':id'=>$idCred);
		$sql = "SELECT SUM(M.monto) monto, SUM(M.interes) interes, SUM(M.iva) iva, SUM(M.capital) capital, M.tipo_mov, M.fecha_mov
				FROM movimientos M
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE C.id_credito = :id AND
				(EXISTS (SELECT 1 FROM cargos CA WHERE CA.id_movimiento = M.id_movimiento AND CA.tipo_cargo = 0) OR
				EXISTS (SELECT 1 FROM abonos AB WHERE AB.id_movimiento = M.id_movimiento AND AB.tipo_abono = 0))
				GROUP BY M.tipo_mov, M.fecha_mov
				ORDER BY M.fecha_mov, M.tipo_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAbonosByCredito($idCred){
		$values = array(':id'=>$idCred);
		$sql = "SELECT AB.id_deposito, D.monto, SUM(M.interes) interes, SUM(M.iva) iva, SUM(M.capital) capital, 
				AB.tipo_abono, D.fecha_deposito, D.id_banco
				FROM depositos_bancarios D
				JOIN abonos AB ON AB.id_deposito=D.id_deposito
				JOIN movimientos M ON M.id_movimiento=AB.id_movimiento
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE C.id_credito=:id
				GROUP BY AB.id_deposito, AB.tipo_abono, D.fecha_deposito, D.monto
				ORDER BY D.fecha_deposito, AB.id_deposito, AB.tipo_abono";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getMoratoriosByCriterio($data,$group=""){
		$values = array();
		//Criterio por defecto es mora
		$criterio[] =  'M.tipo_mov = 0';
		$criterio[] = 'CA.tipo_cargo = 1';
		$groupby = "M.fecha_mov";
		if(isset($data['fecha'])){
			$criterio[] = 'M.fecha_mov < :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['idCred'])){
			$criterio[] = 'C.id_credito = :id';
			$values[':id'] = $data['idCred'];
		}
		if(isset($data['idAcred'])){
			$criterio[] = 'M.id_acreditado = :a';
			$values[':a'] = $data['idAcred'];
		}
		if($group != "")
			$groupby = $group;
		$criterio = implode(' AND ',$criterio);
	
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva,
				SUM(M.capital) as capital, M.fecha_mov
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN cargos AS CA ON CA.id_movimiento = M.id_movimiento
				WHERE  {$criterio}
				GROUP BY {$groupby}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getMoratoriosByCredito($idCred){
		$values = array(':id'=>$idCred);
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva, SUM(M.capital) as capital, 
				M.tipo_mov, M.fecha_mov, CB.banco
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				LEFT JOIN cargos CA ON CA.id_movimiento=M.id_movimiento
				LEFT JOIN abonos AB ON AB.id_movimiento=M.id_movimiento
				LEFT JOIN depositos_bancarios D ON D.id_deposito=AB.id_deposito
				LEFT JOIN cuentas_bancarias CB ON CB.id_banco=D.id_banco
				WHERE  C.id_credito = :id AND (CA.tipo_cargo = '1' OR AB.tipo_abono = '1')
				GROUP BY M.tipo_mov, M.fecha_mov
				ORDER BY M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
		/*SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva, SUM(M.capital) as capital, 
				M.tipo_mov, M.fecha_mov
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				WHERE  C.id_credito = :id AND 
				(EXISTS (SELECT 1 FROM cargos CA WHERE CA.id_movimiento = M.id_movimiento AND CA.tipo_cargo = '1') 
				OR EXISTS (SELECT 1 FROM abonos AB WHERE AB.id_movimiento = M.id_movimiento AND AB.tipo_abono = '1'))
				GROUP BY M.tipo_mov, M.fecha_mov
				ORDER BY M.fecha_mov
		 * */
	}
	
	function getFlujoEfectivo($criterio){
		
		if(isset($criterio['cuenta'])){
			$criterios[] = "P.id_cuenta = :c";
			$values[':c'] = $criterio['cuenta'];
			 
		}
		if(isset($criterio['mes'])){
			$criterios[] = "MONTH(T.fecha) = :m";
			$values[':m'] = $criterio['mes'];
		
		}
		if(isset($criterio['fecha1']) && isset($criterio['fecha2'])){
			$criterios[] = "T.fecha >= :f1";
			$criterios[] = "T.fecha < :f2";
			$values[':f1'] = $criterio['fecha1'];
			$values[':f2'] = $criterio['fecha2'];
		
		}
		$criterios = implode(' AND ',$criterios);
		
		$sql = "SELECT T.fecha, T.descripcion, C.nombre_cuenta, P.cantidad
				FROM transacciones_efectivo T
				JOIN partidas_efectivo P ON P.id_transaccion=T.id_transaccion
				JOIN cuentas_efectivo C ON C.id_cuenta=P.id_cuenta
				WHERE {$criterios}
				ORDER BY fecha";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getSaldoFlujoEfectivo($criterio){
		if(isset($criterio['cuenta'])){
			$criterios[] = "P.id_cuenta = :c";
			$values[':c'] = $criterio['cuenta'];
		
		}
		if(isset($criterio['fechaSaldo'])){
			$criterios[] = "T.fecha < :f";
			$values[':f'] = $criterio['fechaSaldo'];
		
		}
		$criterios = implode(' AND ',$criterios);
		
		$sql = "SELECT SUM(P.cantidad) saldo
				FROM transacciones_efectivo T
				JOIN partidas_efectivo P ON P.id_transaccion=T.id_transaccion
				WHERE {$criterios}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getPagoMoratoriosByCriterio($data){
		$values = array();
		//Criterio por defecto es mora
		$criterio[] =  'M.tipo_mov = 1';
		$criterio[] = 'AB.tipo_abono = 1';
		if(isset($data['fecha'])){
			$criterio[] = 'M.fecha_mov < :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['idCred'])){
			$criterio[] = 'C.id_credito = :id';
			$values[':id'] = $data['idCred'];
		}
		if(isset($data['idAcred'])){
			$criterio[] = 'M.id_acreditado = :a';
			$values[':a'] = $data['idAcred'];
		}
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva,
				SUM(M.capital) as capital, M.fecha_mov
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN abonos AS AB ON AB.id_movimiento = M.id_movimiento
				WHERE  {$criterio}
				GROUP BY AB.id_deposito, M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
		return $this->arrayResult;
	}
	
	function getSumPagos($idCred){
		$values = array(':id'=>$idCred);
		
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva,
				SUM(M.capital) as capital
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				JOIN abonos AS AB ON AB.id_movimiento = M.id_movimiento
				WHERE C.id_credito=:id AND AB.tipo_abono=0
				GROUP BY C.id_credito";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getInteresDevengado($data){
		$values = array();
		$criterio =  array();
		//Default
		$criterio[] = 'M.fecha_mov < CURDATE()';
		$orderby = "YEAR(M.fecha_mov), MONTH(M.fecha_mov), M.tipo_mov";
		
		if(isset($data['tipo'])){
			$criterio[] = 'C.tipo = :t';
			$values[':t'] = $data['tipo'];
		}
		if(isset($data['status'])){
			$criterio[] = 'C.status = :s';
			$values[':s'] = $data['status'];
		}
		if(isset($data['expediente'])){
			$criterio[] = 'C.expediente = :e';
			$values[':e'] = $data['expediente'];
			$groupby = 'M.tipo_mov, A.id_acreditado';
		}
		else
			$groupby = 'YEAR(M.fecha_mov), MONTH(M.fecha_mov), M.tipo_mov';
		if(isset($data['persona']) && $data['persona']){
			$criterio[] = "MONTH(M.fecha_mov) IN({$data['mes']})";
			$criterio[] = "YEAR(M.fecha_mov) IN({$data['a�o']})";
			if($data['saldos'])
				$groupby = 'YEAR(M.fecha_mov), MONTH(M.fecha_mov), A.id_acreditado';
			else
				$groupby = 'YEAR(M.fecha_mov), MONTH(M.fecha_mov), M.tipo_mov, A.id_acreditado';
			$orderby .= ",LEFT(C.expediente,LOCATE('-', C.expediente)),CAST(SUBSTRING(C.expediente FROM LOCATE('-', C.expediente)+1) AS SIGNED)";
		}
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT YEAR(M.fecha_mov) a�o, MONTH(M.fecha_mov) mes, SUM(monto) monto, SUM(interes) interes,
				SUM(iva) iva, SUM(capital) capital, M.tipo_mov, C.expediente,
				CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) nombres,
				G.nombre grupo
				FROM  movimientos M
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				JOIN personas P ON P.id_persona=A.id_persona
				LEFT JOIN representantes R ON R.id_credito=C.id_credito
				LEFT JOIN agrupaciones G ON G.id_grupo=R.id_grupo
				WHERE  {$criterio} AND 
				(EXISTS (SELECT 1 FROM cargos CA WHERE CA.id_movimiento = M.id_movimiento AND CA.tipo_cargo = 0) 
				OR EXISTS (SELECT 1 FROM abonos AB WHERE AB.id_movimiento = M.id_movimiento AND AB.tipo_abono = 0))
				GROUP BY {$groupby}
				ORDER BY {$orderby}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
			
			//GROUP BY A.id_acreditado, M.tipo_mov
	}
	
	function getPagosNoDevengados(){
		$values = array();
		$sql = "SELECT SUM(M.monto) monto, SUM(M.interes) interes, SUM(M.iva) iva, SUM(M.capital) capital
				FROM movimientos M
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				JOIN cargos CA ON CA.id_movimiento=M.id_movimiento
				WHERE CA.tipo_cargo=0 AND CA.status=1 AND M.fecha_mov > NOW()";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getBalanceByCriterio($data){
		$values = array();
		
		if(isset($data['fecha'])){
			$criterio1[] = 'M.fecha_mov <= :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['idCred'])){
			$criterio1[] = 'C.id_credito = :id';
			$values[':id'] = $data['idCred'];
		}
		$criterio1 = implode(' AND ',$criterio1);
		if(isset($data['tipoCargo'])){
			$criterio2[] = 'EXISTS (SELECT 1 FROM cargos CA WHERE CA.id_movimiento = M.id_movimiento AND CA.tipo_cargo = :tc)';
			$values[':tc'] = $data['tipoCargo'];
		}
		if(isset($data['tipoAbono'])){
			$criterio2[] = 'EXISTS (SELECT 1 FROM abonos AB WHERE AB.id_movimiento = M.id_movimiento AND AB.tipo_abono = :ta)';
			$values[':ta'] = $data['tipoAbono'];
		}
		$criterio2 = implode(' OR ',$criterio2);
		$criterio = "{$criterio1} AND ({$criterio2})";
		
		
		$sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva, SUM(M.capital) as capital
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				WHERE {$criterio}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
		/*
		 $sql = "SELECT SUM(M.monto) as monto, SUM(M.interes) as interes, SUM(M.iva) as iva, SUM(M.capital) as capital
				FROM movimientos AS M
				JOIN acreditados AS A ON A.id_acreditado = M.id_acreditado
				JOIN creditos AS C ON C.id_credito = A.id_credito
				WHERE  {$criterio}";
		 */
	}
	
	function getAmortizacionByCredito($id){
		$values = array(':id'=>$id);
		$sql = "SELECT SUM(M.monto) AS monto, SUM(interes) AS interes, SUM(iva) AS iva, SUM(capital) AS capital, M.fecha_mov,
				CA.status, GROUP_CONCAT(M.id_movimiento) movimientos
				FROM movimientos M
				JOIN acreditados A ON A.id_acreditado = M.id_acreditado
				JOIN creditos C ON C.id_credito = A.id_credito
				JOIN cargos CA ON CA.id_movimiento = M.id_movimiento
				WHERE C.id_credito = :id AND CA.tipo_cargo=0
				GROUP BY M.fecha_mov, CA.status
				ORDER BY M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAmortizacionByAcreditado($idAcred){
		$values = array(':id'=>$idAcred);
		$sql = "SELECT M.id_movimiento, M.monto, interes, iva, capital, M.fecha_mov,CA.status
				FROM movimientos M
				JOIN cargos CA ON CA.id_movimiento = M.id_movimiento
				WHERE M.id_acreditado = :id AND CA.tipo_cargo=0
				ORDER BY M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getReferenciasBancSubidas(){
		$values = array();
		$sql = "SELECT RB.*,  UNIX_TIMESTAMP(RB.fecha_subida) unixtime, CONCAT(A.nombres,' ',A.apellido_paterno,' ',A.apellido_materno) nombre, CB.banco
				FROM referencias_bancarias RB
				JOIN administradores A ON A.id=RB.id_usuario
				JOIN cuentas_bancarias CB ON CB.id_banco=RB.id_cuenta
				ORDER BY RB.fecha_subida";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function deleteDepositoById($id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),);
		$sql = "DELETE FROM depositos_bancarios
				WHERE id_deposito = :id";
		return $this->setDataByQuery($sql, $values)?true:false;
	}
	
	function deletePagosByCredito($idCred){
		$values = array(':id'=>array('data'=>$idCred,'type'=>'i'),
						);
		$sql = "DELETE M FROM movimientos M
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE C.id_credito=:id AND M.tipo_mov=1";
		return $this->setDataByQuery($sql, $values)?true:false;
	}
	
	function deleteMovimientoById($idMov){
		$values = array(':id'=>array('data'=>$idMov,'type'=>'i'),
						);
		$sql = "DELETE FROM movimientos WHERE id_movimiento = :id";
		return $this->setDataByQuery($sql, $values)?true:false;
	}
	
	function deleteMoratoriosByCredito($idCred){
		$values = array(':id'=>array('data'=>$idCred,'type'=>'i'),
						);
		$sql = "DELETE M FROM movimientos M
				JOIN cargos CA ON CA.id_movimiento=M.id_movimiento
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE C.id_credito=:id AND CA.tipo_cargo=1";
		return $this->setDataByQuery($sql, $values)?true:false;
	}
	
	function deleteAmortizacionByCredito($idCred){
		$values = array(':id'=>array('data'=>$idCred,'type'=>'i'),
		);
		$sql = "DELETE M FROM movimientos M
				JOIN cargos CA ON CA.id_movimiento=M.id_movimiento
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				WHERE C.id_credito=:id AND CA.tipo_cargo=0";
		return $this->setDataByQuery($sql, $values)?true:false;
	}
	
	function existsReferenciaBanc($fIni, $fFin){
		$values = array(':fi'=>$fIni,':ff'=>$fFin);
		$sql = "SELECT * 
				FROM referencias_bancarias
				WHERE (:fi BETWEEN fecha_inicial AND fecha_final) OR
				(:ff BETWEEN fecha_inicial AND fecha_final)";
		if($this->getDataByQuery($sql, $values))
			return !empty($this->arrayResult);
	}
}
?>
