<?php
require_once('DBClass.php');

class LoginDB extends DB{
	
	var $dbLink;
	var $check_browser = true;
	var $check_ip_blocks = 0;
	var $secure_word = 'CHUPELUPE';
	var $regenerate_id = true;
	
	
	/**
	 * Check if the user is logged in
	 * 
	 * @return true or false
	 */
	function isLoggedIn(){
		if($_SESSION['LoggedIn'])
			return true;
		else
			return false;
	}
	
	/**
	 * Check username and password against DB
	 *
	 * @return true/false
	 */
	function doLogin($username, $password){
		
		$this->username = $username;
		$this->password = $password;
		
		// check db for user and pass here.
		$values = array(':u'=>$this->clean($this->username),':p'=>md5($this->clean($this->password)));
		$sql = "SELECT * FROM administradores WHERE usuario = :u AND password = :p";
		$result = $this->getDataByQuery($sql, $values);
		
		// If no user/password combo exists return false
		if(is_array($result) && !empty($result))
		{
			//set session vars up
			$_SESSION['LoggedIn'] = true;
			$_SESSION['userName'] = $this->username;
			$_SESSION['userID'] = $result[0]['id'];
			$_SESSION['priv'] = $result[0]['privilegios'];
			//Actualizo el ultimo acceso al sistema
			$sql = "UPDATE administradores SET ultimo_acceso=NOW() WHERE id={$result[0]['id']}";
			$this->setDataByQuery($sql, array());
			//creamos la huella
			$this->Open();
		}
		else
			return false;
		return true;
	}
	
	/**
	 * Destroy session data/Logout.
	 */
	function logout(){
		$values = array(':id'=>array('data'=>$_SESSION['userID'],'type'=>'i'));
		$sql = "UPDATE administradores SET ultima_actualizacion=0
				WHERE id=:id";
		$this->setDataByQuery($sql, $values);
		unset($_SESSION['LoggedIn']);
		unset($_SESSION['userName']);
		session_destroy();
	}
	
	/**
	 * Cleans a string for input into a MySQL Database.
	 * Gets rid of unwanted characters/SQL injection etc.
	 * 
	 * @return string
	 */
	function clean($str){
		// Only remove slashes if it's already been slashed by PHP
		if(get_magic_quotes_gpc())
		{
			$str = stripslashes($str);
		}
		
		return $str;
	}
	
	
	// Call this when init session.
	function Open(){
		$_SESSION['ss_fprint'] = $this->_Fingerprint();
		$this->_RegenerateId();
	}

	// Call this to check session.
	function Check(){
		$this->_RegenerateId();
		return (isset($_SESSION['ss_fprint']) && $_SESSION['ss_fprint'] == $this->_Fingerprint());
	}

	// Internal function. Returns MD5 from fingerprint.
	function _Fingerprint(){
		$fingerprint = $this->secure_word;
		if ($this->check_browser) {
			$fingerprint .= $_SERVER['HTTP_USER_AGENT'];
		}
		if ($this->check_ip_blocks) {
			$num_blocks = abs(intval($this->check_ip_blocks));
			if ($num_blocks > 4) {
				$num_blocks = 4;
			}
			$blocks = explode('.', $_SERVER['REMOTE_ADDR']);
			for ($i = 0; $i < $num_blocks; $i++) {
				$fingerprint .= $blocks[$i] . '.';
			}
		}
		return md5($fingerprint);
	}

	// Internal function. Regenerates session ID if possible.
	function _RegenerateId(){
		if ($this->regenerate_id && function_exists('session_regenerate_id')) {
			if (version_compare(phpversion(), '5.1.0', '>=')) {
				session_regenerate_id(true);
			} else {
				session_regenerate_id();
			}
		}
	}
	
	function lastSeen($userID){
		$values = array(':id'=>array('data'=>$userID,'type'=>'i'),
						':u'=>array('data'=>$_SERVER['HTTP_USER_AGENT'],'type'=>'s'));
		$sql = "UPDATE administradores SET ultima_actualizacion=NOW(),
				last_userAgent=:u 
				WHERE id=:id";
		$this->setDataByQuery($sql, $values);
	}
}
?>