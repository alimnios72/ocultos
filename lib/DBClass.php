<?php

class DB{

	//-----------------------------------------//
	var $dbLink;		//link a la BD
	var $lastId;		//ultimo id
	var $arrayResult;	//arreglo con resultados...
	var $errorInfo;
	/*
	var $errorInfo;		//string con el mensaje d error
	var $errorNumber;	//numero del error en el mysql
	var $mysqlResult;	//arreglo d resultados
	var $flagDB;	*/	//bandera de conexion a la BD exitosa	
	//------------------------------------------------------------//
    function __construct($dbLink=false, $dsn="", $user="", $password=""){
        try{
            if(is_object($dbLink))
                $this->dbLink = $dbLink;
            else
                $this->dbLink = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1'));
            
            $timezone = date('P');    
            $this->dbLink->exec("SET SESSION time_zone = '{$timezone}'");
        }
        catch(PDOException $e){
	    echo "Error de conexi�n con la base de datos: ".$e->getMessage() ."\n";
            exit;
        }
    }
    
    function getDataByQuery($query, $values){
        $sth = $this->dbLink->prepare($query);
        $result = $sth->execute($values);
        if(!$result)
            return array();
		$this->arrayResult = $sth->fetchAll(PDO::FETCH_ASSOC);
		//$this->dbLink = null;
		return $this->arrayResult;
    }
    
    function setDataByQuery($query, $values){
		$sth = $this->dbLink->prepare($query);
		$this->bindParams($sth, $values);
		if(!$sth->execute()){
			$this->errorInfo = $sth->errorInfo();
			return false;
		}
		//$this->lastId = $this->lastInsertId();
		$this->lastId = $this->dbLink->lastInsertId();
		//$this->dbLink = null;
		return true;
    }
    
    private function bindParams(&$sth, $values){
	$type = array('i'=>PDO::PARAM_INT, 's'=>PDO::PARAM_STR);
	foreach($values as $key=>$value)
	    $sth->bindParam($key, $value['data'], $type[$value['type']]);
    }
    
    private function lastInsertId($seqname = null){
	$sth = $this->dbLink->prepare('SELECT SCOPE_IDENTITY() AS id');
	$sth->execute();
	$result = $sth->fetch();
	return $result['id'];
    }
}
?>