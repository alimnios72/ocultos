<?php
require_once('DBClass.php');
class Ubicacion extends DB{
	
	function addDelegacion($data){
		$values = array(':n'=>array('data'=>$data['delegacion'],'type'=>'s'),
						':id'=>array('data'=>$data['estado'],'type'=>'i'),
		);
		$sql = "INSERT INTO delegaciones_municipios (nombre_del, id_estado)
				VALUES (:n, :id)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addDiaInhabil($data){
		$values = array(':f'=>array('data'=>$data['diainhabil'],'type'=>'s'),
					':d'=>array('data'=>$data['descripcion'],'type'=>'s'),
			);
		$sql = "INSERT INTO dias_inhabiles (fecha, descripcion)
				VALUES (:f, :d)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addZona($data){
		$values = array(':n'=>array('data'=>$data['zona'],'type'=>'s'),
						':id'=>array('data'=>$data['delegacion'],'type'=>'i'),
		);
		$sql = "INSERT INTO zonas (nombre_zona, id_delegacion_zona)
				VALUES (:n, :id)";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateDelegacion($data){
		$values = array(':n'=>array('data'=>mb_convert_encoding($data['nombre_del'],"ISO-8859-9", "UTF-8"),'type'=>'s'),
						':id'=>array('data'=>$data['id_del'],'type'=>'i'),
						);
		$sql = "UPDATE delegaciones_municipios SET nombre_del=:n
				WHERE id_delegacion=:id";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateZona($data){
		$values = array(':n'=>array('data'=>mb_convert_encoding($data['nombre_zona'],"ISO-8859-9", "UTF-8"),'type'=>'s'),
						':id'=>array('data'=>$data['id_zona'],'type'=>'i'),
						);
		$sql = "UPDATE zonas SET nombre_zona=:n
				WHERE id_zona=:id";
		$this->setDataByQuery($sql, $values);
	}
	
	function getZonas(){
		$values = array();
		$sql = "SELECT Z.id_zona, Z.nombre_zona AS zona 
				FROM zonas AS Z
				ORDER BY nombre_zona";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getEstados(){
		$values = array();
		$sql = "SELECT * FROM estados_republica";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}

    function getDelegacionById($id){
        $values = array(':id'=> $id);
        $sql = "SELECT D.*
				FROM delegaciones_municipios D
				WHERE D.id_delegacion = :id";
        if($this->getDataByQuery($sql, $values))
            return $this->arrayResult[0];
    }
	
	function getEstadoByDelegacion($idDel){
		$values = array(':id'=>$idDel);
		$sql = "SELECT E.*
				FROM estados_republica E
				JOIN delegaciones_municipios D ON D.id_estado=E.id_estado
				WHERE D.id_delegacion = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getDelegacionesByEstado($idEdo){
		$values = array(':id'=>$idEdo);
		$sql = "SELECT D.id_delegacion, D.nombre_del
				FROM delegaciones_municipios AS D
				WHERE D.id_estado = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getZonasByDelegacion($idDel){
		$values = array(':id'=>$idDel);
		$sql = "SELECT id_zona, nombre_zona, id_delegacion_zona
				FROM zonas 
				WHERE id_delegacion_zona = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getSucursales(){
		$values = array();
		$sql = "SELECT * FROM sucursales";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDiasInhabiles(){
		$values = array();
		$sql = "SELECT * FROM dias_inhabiles ORDER BY fecha";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	/************************************************/
	function getAllDelegacionTemp(){
		$values = array();
		$sql = "SELECT * FROM estados_republica E
				JOIN delegaciones_municipios D ON D.id_estado=E.id_estado
				WHERE D.nombre_del='Temporal'";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function insertaPPE($data){
		$values = array(':n'=>array('data'=>trim(strtoupper($data['nombres'])),'type'=>'s'),
						':p'=>array('data'=>trim(strtoupper($data['paterno'])),'type'=>'s'),
						':m'=>array('data'=>trim(strtoupper($data['materno'])),'type'=>'s'),
						':f'=>array('data'=>$data['nacimiento'],'type'=>'s'),
						':s'=>array('data'=>$data['sexo'],'type'=>'s'),
						':r'=>array('data'=>$data['rfc'],'type'=>'s'),
						':d'=>array('data'=>$data['delegacion'],'type'=>'i'),
		);
		$sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, 
									  fecha_nacimiento, sexo, rfc, id_delegacion)
				VALUES (:n, :p, :m, :f, :s, :r, :d)";
		if($this->setDataByQuery($sql, $values)){
			$idPersona = $this->lastId;
			$this->insertaListaNegra($data,$idPersona);
		}
	}
	
	function insertaListaNegra($data,$id){
		$values = array(':c'=>array('data'=>$data['categoria'],'type'=>'i'),
						':e'=>array('data'=>$data['entidad'],'type'=>'s'),
						':p'=>array('data'=>trim(strtoupper($data['partido'])),'type'=>'s'),
						':t'=>array('data'=>$data['tipo'],'type'=>'s'),
						':l'=>array('data'=>trim(strtoupper($data['clase'])),'type'=>'s'),
						':i'=>array('data'=>trim(strtoupper($data['calidad'])),'type'=>'s'),
						':d'=>array('data'=>$data['distrito'],'type'=>'s'),
						':id'=>array('data'=>$id,'type'=>'i'),
						);
		$sql = "INSERT INTO lista_negra (categoria, entidad, partido,
									  tipo, clase, calidad, distrito,id_persona)
				VALUES (:c, :e, :p, :t, :l, :i, :d, :id)";
		$this->setDataByQuery($sql, $values);
	}
	
	function getSingleGrupos(){
		$values = array();
		$sql = "SELECT CASE 
         		WHEN RIGHT(nombre, 1) BETWEEN '0' AND '9' THEN 
         		LEFT(nombre, Length(nombre) - 2) 
         		ELSE nombre 
       			END AS nombrechecked, Group_concat(id_representante) representantes, 
       			Group_concat(id_credito) creditos, Group_concat(id_grupo) grupos
				FROM   grupos 
				GROUP  BY 1 ";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function insertAgrupaciones($data){
		$values = array(
				':n'=>array('data'=>trim(strtoupper($data['nombrechecked'])),'type'=>'s'),
		);
		$sql = "INSERT INTO agrupaciones (nombre) VALUES (:n)";
		$this->setDataByQuery($sql, $values);
		return  $this->lastId;
	}
	
	function insertRepresentantes($data){
		$values = array(
				':p'=>array('data'=>$data['persona'],'type'=>'i'),
				':g'=>array('data'=>$data['idGrupo'],'type'=>'i'),
				':c'=>array('data'=>$data['credito'],'type'=>'i'),
		);
		$sql = "INSERT INTO representantes (id_persona, id_grupo, id_credito) 
				VALUES (:p,:g,:c)";
		$this->setDataByQuery($sql, $values);
	}
}
?>
