<?php
	function validaCredito($data, &$errors, $creditoDB){
		$acreditado = $data['acreditado'];
		$opcCred = $data['opcCred'];
		$solidario = $data['solidario'];
		if(!validaOpcCredito($opcCred, $errors, $creditoDB))
			return false;
		if($opcCred['tipo'] == 0 && !validaSolidario($solidario, $errors, $creditoDB, $acreditado[0]['info_personal']))
			return false;
		if($opcCred['tipo'] == 1 && !validaRepresentante($acreditado, $errors))
			return false;
		if(!validaAcreditado($acreditado, $errors, $creditoDB, $opcCred['tipo']))
			return false;
		return true;
	}
	
	function validaOpcCredito($opcCred, &$errors, $creditoDB){
		$flag = true;
		if($opcCred['grupo'] == "" && $opcCred['tipo'] == '1'){
			$errors[] = "Debes especificar un nombre para el grupo";
			$flag = false;
		}
		$grupos = $creditoDB->existsGrupo($opcCred);
		if(!empty($grupos)){
			$errors[] = "{$opcCred['grupo']} ya existe, por favor elige otro nombre para el grupo.";
			$flag = false;
		}
		return $flag;
	}
	
	function validaAcreditado($acreds, &$errors, $creditoDB, $tipo){
		$flag = true;
		$tels = 0;
		$hombres = 0;
		$last_key = end(array_keys($acreds));
		foreach ($acreds as $k=>$per){
			$infoP = $per['info_personal'];
			$infoE = $per['info_econom'];
			$nCompleto = $infoP['nombres']." ".$infoP['apellidoP']." ".$infoP['apellidoM'];
			$nCompleto = trim(strtoupper($nCompleto));
			if($infoP['nombres'] == ''){
				$errors[] = "Debe especificar al menos un nombre. ({$nCompleto})";
				$flag = false;
			}
			elseif($infoP['apellidoP'] == '' && $infoP['apellidoM'] == ''){
				$errors[] = "Debe especificar al menos un apellido. ({$nCompleto})";
				$flag = false;
			}
			if(!isset($infoP['sexo']) || $infoP['sexo'] == ''){
				$errors[] = "Define el sexo de la persona. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['nacimiento'] == "" || !validaFecha($infoP['nacimiento'])){
				$errors[] = "La fecha de nacimiento no puede estar en blanco o debe ser una fecha v�lida. ({$nCompleto})";
				$flag = false;
			}
			elseif(getEdad($infoP['nacimiento']) < 21 || getEdad($infoP['nacimiento']) > 65){
				$errors[] = "El rango de edades debe ser 21-65 a�os. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['enfermedad_desc'] == '' && ($infoP['enfermedad_solic'] == 1 || $infoP['enfermedad_fam'] == 1)){
				$errors[] = "Debe describir la enfermedad. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['ant_domicilio'] == '' || is_nan($infoP['ant_domicilio'])){
				$errors[] = "Debe especificar correctamente la antig�edad en el domicilio. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['calle'] == '' || $infoP['colonia'] == ''){
				$errors[] = "Debe especificar calle y/o colonia. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['tel_casa'] == '' && $infoP['celular'] == '' && $tipo == "0"){
				$errors[] = "Especifica al menos un tel�fono/celular. ({$nCompleto})";
				$flag = false;
			}
			if($infoP['tel_casa'] == "" && $infoP['celular'] == "" && $tipo == "1" && $per['representante'] == 1){
				$errors[] = "Especifica al menos un tel�fono/celular para el representante. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['nombre_negocio'] == ''){
				$errors[] = "Especificar nombre del negocio o actividad productiva. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['pag_cred'] == 1 && $infoE['pag_cred_monto'] == ''){
				$errors[] = "Especificar el monto del cr�dito que paga. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['pen_alim'] == 1 && $infoE['pen_alim_monto'] == ''){
				$errors[] = "Especificar el monto de la pensi�n alimentaria. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['gastos_fam'] == '' || $infoE['gastos_fam'] < 0){
				$errors[] = "Definir monto para gastos familiares o marcar 0 (cero) si no hay. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['ing_mensuales'] == '' || $infoE['ing_mensuales'] < 0){
				$errors[] = "Definir monto de los ingresos mensuales o marcar 0 (cero) si no hay. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['inv_semanal'] == '' || $infoE['inv_semanal'] < 0){
				$errors[] = "Definir monto de la inversi�n mensual en el negocio o marcar 0 (cero) si no hay. ({$nCompleto})";
				$flag = false;
			}
			if($infoE['monto_sol'] == '' || $infoE['monto_sol'] < 0){
				$errors[] = "Especificar el monto solicitado. ({$nCompleto})";
				$flag = false;
			}
			//Los egresos o gastos del acreditado no deben rebasar sus ingresos.
			$ingresosLibres = $infoE['ing_mensuales'] - ($infoE['gastos_fam']+$infoE['pag_cred_monto']+$infoE['pen_alim_monto']+$infoE['inv_semanal']);
			if($ingresosLibres < 1){
				$errors[] = "Sus egresos son mayores a sus ingresos. ({$nCompleto})";
				$flag = false;
			}
			//S�lo debe haber un hombre por grupo
			if($infoP['sexo'] == 'H')
				$hombres++;
			//Restringimos a que el 75% de los acreditados deben tener un tel�fono local
			if($infoP['tel_casa'] != "" && $infoP['tel_casa'] != " ")
				$tels++;
			if ($k == $last_key){
				if($tels/count($acreds) < 0.75){
					$errors[] = "Al menos el 75% de los acreditados debe tener tel�fono fijo/local.";
					$flag = false;
				}
				if($hombres > 1 && count($acreds) > 1){
					$errors[] = "M�ximo puede haber un hombre por grupo.";
					$flag = false;
				}
			}	
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$count = 0;
			foreach ($acreds as $a){
				$nCompleto2 = $a['info_personal']['nombres']." ".$a['info_personal']['apellidoP']." ".$a['info_personal']['apellidoM'];
				$nCompleto2 = trim(strtoupper($nCompleto2));
				if($nCompleto == $nCompleto2)
					$count++;
				if($nCompleto != $nCompleto2){
					if($a['info_personal']['delegacion'] == $infoP['delegacion']){
						if($a['info_personal']['colonia'] == $infoP['colonia']){
							if($a['info_personal']['calle'] == $infoP['calle'] && $infoP['calle'] != ''){
								if($a['info_personal']['num_ext'] == $infoP['num_ext']){
									if($a['info_personal']['num_int'] == $infoP['num_int']){
										if($a['info_personal']['dpto'] == $infoP['dpto']){
											if($a['info_personal']['mza'] == $infoP['mza']){
												if($a['info_personal']['lote'] == $infoP['lote']){
													$errors[] = "{$nCompleto} y {$nCompleto2} tienen el mismo domicilio";
													$flag = false;
												}
											}
										}
									}
								}
							}
						}
					}
					if($a['info_personal']['tel_casa'] == $infoP['tel_casa'] && $infoP['tel_casa'] != ''){
						$errors[] = "{$nCompleto} y {$nCompleto2} tienen el mismo tel�fono";
						$flag = false;
					}
					if($a['info_personal']['celular'] == $infoP['celular'] && $infoP['celular'] != ''){
						$errors[] = "{$nCompleto} y {$nCompleto2} tienen el mismo celular.";
						$flag = false;
					}
				}
			}
			if($count > 1){
				$errors[] = "{$nCompleto} aparece repetido(a).";
				$flag = false;
			}
		}
		return $flag;
	}
	
	function validaSolidario($sol, &$errors, $creditoDB, $acred){
		$flag = true;
		$nCompleto = $sol['nombres']." ".$sol['apellidoP']." ".$sol['apellidoM'];
		$nCompleto = trim(strtoupper($nCompleto));
		$nCompleto2 = $acred['nombres']." ".$acred['apellidoP']." ".$acred['apellidoM'];
		$nCompleto2 = trim(strtoupper($nCompleto2));
		if($nCompleto == $nCompleto2){
			$errors[] = "Obligado y acreditado son la misma persona.";
			$flag = false;
		}
		if($sol['nombres'] == ''){
			$errors[] = "Debe especificar al menos un nombre. ({$nCompleto})";
			$flag = false;
		}
		elseif($sol['apellidoP'] == '' && $sol['apellidoM'] == ''){
			$errors[] = "Debe especificar al menos un apellido. ({$nCompleto})";
			$flag = false;
		}
		if(!isset($sol['sexo']) || $sol['sexo'] == ''){
			$errors[] = "Define el sexo de la persona. ({$nCompleto})";
			$flag = false;
		}
		if($sol['nacimiento'] == "" || !validaFecha($sol['nacimiento'])){
			$errors[] = "La fecha de nacimiento no puede estar en blanco o debe ser una fecha v�lida. ({$nCompleto})";
			$flag = false;
		}
		elseif(getEdad($sol['nacimiento']) < 21 || getEdad($sol['nacimiento']) > 65){
			$errors[] = "El rango de edades debe ser 21-65 a�os. ({$nCompleto})";
			$flag = false;
		}
		if($sol['calle'] == '' || $sol['colonia'] == ''){
			$errors[] = "Debe especificar calle y/o colonia. ({$nCompleto})";
			$flag = false;
		}
		if($sol['tel_casa'] == '' && $sol['celular'] == ''){
			$errors[] = "Especifica al menos un tel�fono/celular. ({$nCompleto})";
			$flag = false;
		}
		if($sol['delegacion'] == $acred['delegacion']){
			if($sol['colonia'] == $acred['colonia'] && $sol['colonia'] != ''){
				if($sol['calle'] == $acred['calle'] && $sol['calle'] != ''){
					if($acred['num_ext'] == $sol['num_ext']){
						if($acred['num_int'] == $sol['num_int']){
							if($acred['dpto'] == $sol['dpto']){
								if($acred['mza'] == $sol['mza']){
									if($acred['lote'] == $sol['lote']){
										$errors[] = "Acreditado y solidario tienen el mismo domicilio";
										$flag = false;
									}
								}
							}
						}
					}
				}
			}
		}
		if($sol['tel_casa'] == $acred['tel_casa'] && $sol['tel_casa'] != ''){
			$errors[] = "Acreditado y solidario tienen el mismo tel�fono";
			$flag = false;
		}
		if($sol['celular'] == $acred['celular'] && $sol['celular'] != ''){
			$errors[] = "Acreditado y solidario tienen el mismo celular.";
			$flag = false;
		}
		return $flag;
	}
	
	function validaRepresentante($acreds, &$errors){
		foreach($acreds as $acred){
			if(isset($acred['representante']))
				return true;
		}
		$errors[] = "Especifica un representante para el grupo.";
		return false;
	}
		
	function validaFecha($fecha){
		$fecha = explode('-', $fecha);
		return checkdate($fecha[1], $fecha[2], $fecha[0]);
	}
?>