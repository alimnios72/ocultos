<?php
define('_dias_hab',360);
define('_mes',30);
define('_iva',1.16);

class Amortizacion{   
    private $periodo;
    private $tasa;
    private $monto;
    private $diaSemana;
    private $esSemana;
    private $diaMes;
    private $esMes;
    public $diasEnPeriodo;
    private $diasTotales;
    private $fecha;
    private $viejo;
    public $plazo;
    public $montoTotal;
    public $pagoFijo;
    public $CAT;
    
    function __construct($cCredito){
        $this->monto = $cCredito['montoTotal'];
        $this->tasa = $cCredito['tasa'];
        $this->periodo = $cCredito['periodo'];
        $this->plazo = $cCredito['plazo'];
        $this->fecha = $cCredito['fecha'];
        $this->diaSemana = $cCredito['diaSemana'];
        $this->diaMes = $cCredito['diaMes'];
        $this->esSemana = ($cCredito['esSemana'] == 'true' || $cCredito['esDiaSemana'] == 1);
        $this->esMes = ($cCredito['esMes'] == 'true' || $cCredito['esDiaMes'] == 1);
        $this->viejo = intval($cCredito['viejo']) == 1 ? true : false;
    }
    
    function getTabla(){
        $tabla = array();
        $this->pagoFijo = $this->getPagoFijo();
        $fechasPago = $this->getFechasPago();
        $this->CAT = $this->getCAT();
        $flujoCaja = array_fill(1, $this->plazo, $this->pagoFijo);
        array_unshift($flujoCaja, -1 * $this->monto);
        $tir = getTIR($flujoCaja);
        $deuda = $this->monto;
        /////////////////////////CREDITOS VIEJOS NO SE REDONDEAN/////////////////////////////
        if(!$this->viejo)
        	$this->montoTotal = $this->pagoFijo * $this->plazo;
        ////////////////////////////////////////////////////////////////////////////////////
        $deudaTotal = $this->montoTotal;
        
        for($i = 0; $i < $this->plazo; $i++){
            $deudaTotal -= $this->pagoFijo;
            $tabla[$i]["num"] = $i + 1;
            $tabla[$i]["prestamo"] = $deuda;
            $tabla[$i]["pagoFijo"] = $this->pagoFijo;
            $tabla[$i]["interes"] = ($deuda * $tir) / _iva;
            $tabla[$i]["iva"] = $tabla[$i]["interes"] * (_iva -1);
            $tabla[$i]["capital"] = $this->pagoFijo - ($deuda * $tir);
            $tabla[$i]["saldo"] = $deudaTotal;
            $tabla[$i]["fecha"] = $fechasPago[$i];
            $deuda -= $tabla[$i]["capital"];
        }
        return $this->ajustePagoFijo($tabla);
    }
    
    private function getCAT(){
        $a�o = 12;
        /*Calculamos el CAT (Costo Anual Total) de manera anualizada sin importar el 
        periodo. El c�lculo del CAT se hace con la tasa de inter�s sin IVA*/
        $total_S_iva = $this->monto * (1 + (($this->tasa / 100) / _iva));
        $mensualidad = $total_S_iva / $a�o;

        /*El flujo de caja contempla las 12 mensualidades y el negativo del monto solicitado*/
        $flujoCaja[0] = -$this->monto;
            for ($i = 1; $i < $a�o+1; $i++)
                $flujoCaja[$i] = $mensualidad;
        //LLamamos a la Clase de Finanzas para calcular la TIR
        $tir = getTIR($flujoCaja);
        $CAT = pow(1 + $tir, $a�o) - 1;

        return round($CAT*100,2);
    }
    
    private function getDiasTotales(){
        $this->diasTotales = _mes * $this->plazo;
        switch($this->periodo){
            case 's':
                $this->plazo *= 4;
                $this->diasEnPeriodo = 7;
                break;
            case 'q':
                $this->plazo *= 2;
                $this->diasEnPeriodo = 15;
                break;
            case 'm':
                $this->plazo *= 1;
                $this->diasEnPeriodo = 30;
                break;
           	case 'b':
                $this->plazo *= 1/2;
                $this->diasEnPeriodo = 60;
                break;
            case 't':
               	$this->plazo *= 1/3;
                $this->diasEnPeriodo = 90;
                break;
            case 'u':
                $this->plazo *= 1/5;
                $this->diasEnPeriodo = 150;
                break;
            case 'e':
            	$this->plazo *= 1/6;
            	$this->diasEnPeriodo = 180;
            	break;
            case 'a':
            	$this->plazo *= 1/12;
            	$this->diasEnPeriodo = 360;
            	break;
            default:
                break;
        }
    }
    
    private function getPagoFijo(){
        $this->getDiasTotales();
        $tasaDiaria = ($this->tasa / 100) * ($this->diasTotales / _dias_hab);
        $this->montoTotal = (1 + $tasaDiaria) * $this->monto;
        //return $this->montoTotal / $this->plazo;
        return $this->viejo ?  $this->montoTotal / $this->plazo : ceil($this->montoTotal / $this->plazo);
    }
    
    private function getFechasPago(){
        $fechasPago = array();
        if(($this->periodo == 'm' || $this->periodo == 't' || $this->periodo == 'e' || $this->periodo == 'a' || $this->periodo == 'b' || $this->periodo == 'u') && $this->esMes){
            $this->fecha = getStartingMonth($this->fecha, $this->diaMes);
            if($this->periodo == 'm')
            	$factor = 1;
            elseif($this->periodo == 'b')
            	$factor = 2;
            elseif($this->periodo == 't')
            	$factor = 3;
            elseif($this->periodo == 'u')
            	$factor = 5;
            elseif($this->periodo == 'e')
            	$factor = 6;
            elseif($this->periodo == 'a')
            	$factor = 12;
            for($i = 1; $i <= $this->plazo; $i++)
                $fechasPago[] = addMonths($this->fecha, $i*$factor);
        }
        else if(($this->periodo == 's' || $this->periodo == 'q') && $this->esSemana){
            $semanas = $this->periodo == 's' ? 1 : 2;
            $actual = date("w", strtotime($this->fecha));
            $deseado = $this->diaSemana;
            $diferencia = (7 - $actual + $deseado);
            $fechasPago[0] = ($semanas == 1) ? addDays($this->fecha, $diferencia) : addDays($this->fecha, $diferencia + 7);
            for($i = 1; $i < $this->plazo; $i++)
                $fechasPago[] = addDays($fechasPago[$i-1], 7 * $semanas);
        }
        else{
             for($i = 0; $i < $this->plazo; $i++)
                $fechasPago[] = addDays($this->fecha, $this->diasEnPeriodo*($i+1));
        }
        return $fechasPago;
    }
    
    private function ajustePagoFijo($tabla){
    	$prestamo = $tabla[0]['prestamo'];
    	$ult = count($tabla) - 1;
    	for($i=0;$i<count($tabla);$i++){
    		if($i != $ult){
    			$tabla[$i]['interes'] = round($tabla[$i]['interes'], 2, PHP_ROUND_HALF_UP);
    			$tabla[$i]['iva'] = round($tabla[$i]['iva'], 2, PHP_ROUND_HALF_UP);
    			$tabla[$i]['capital'] = $tabla[$i]['pagoFijo'] - $tabla[$i]['interes'] - $tabla[$i]['iva'];
    			$prestamo -= $tabla[$i]['capital'];
    		}
    		else{
    			$tabla[$i]['capital'] = $prestamo;
    			$tabla[$i]['iva'] = round($tabla[$i]['iva'], 2, PHP_ROUND_HALF_UP);
    			$tabla[$i]['interes'] = $tabla[$i]['pagoFijo'] - $tabla[$i]['capital'] - $tabla[$i]['iva'];
    		}
    		
    	}
    	return $tabla;
    }
}
?>
