<?php
class xmlTrans{
	private $xml;
	private $file;
	
	function __construct($filename) {
		$this->file = RUTA_XML.$filename;
		$this->xml = simplexml_load_file($this->file);
	}
	
	function addNode($parent, $name){
		$child = $parent->addChild($name);
		$this->xml->asXML($this->file);
		return $child;
	}
	
	function addNodewAttr($node, $attr, $value){
		if(!$this->attrExists($node, $attr, $value)){
			$n = $this->xml->addChild($node);
			$a = $n->addAttribute($attr, $value);
			$this->xml->asXML($this->file);
		}
		return $this->xml->xpath(sprintf("//{$node}[@{$attr}='%s']", $value));
	}
	
	function array2xml($parent, $array, $name){
		$child = $parent->addChild($name);
		foreach($array as $key=>$value){
			if(is_numeric($key))
				$key = "acreditado";
			if(is_array($value))
				$this->array2xml($child, $value, $key);
			else
				$child->addChild($key,$value);
		}
		$this->xml->asXML($this->file);
	}
	
	private function attrExists($node, $attr, $value){
		$nodes = $this->xml->xpath(sprintf("//{$node}[@{$attr}='%s']", $value));
		if(empty($nodes))
			return false;
		else
			return true;
	}
}
?>