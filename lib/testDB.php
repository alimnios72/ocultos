<?php
require_once('DBClass.php');
class Test extends DB{
	function getQuery($sql){
		$values = array();
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function setQuery($sql){
		$values = array();
		$this->setDataByQuery($sql, $values);
	}
}