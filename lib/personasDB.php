<?php
require_once('DBClass.php');
class Persona extends DB{
	
   /*************************************************************************/
   function solvethis(){
      $values = array();
      $sql = "INSERT INTO acreditados (id_credito,id_persona,cantidad)
              VALUES (1212,2151,4000)";
      $this->setDataByQuery($sql,$values);
   }
   /*************************************************************************/
	function addUsuario($data){
		$values = array(':n'=>array('data'=>trim($data['nombres']), 'type'=>'s'),
                        ':aP'=>array('data'=>trim($data['apellidoP']), 'type'=>'s'),
                        ':aM'=>array('data'=>trim($data['apellidoM']), 'type'=>'s'),
						':f'=>array('data'=>$data['nacimiento'], 'type'=>'s'),
						':u'=>array('data'=>$data['usuario'], 'type'=>'s'),
						':p'=>array('data'=>md5($data['password']), 'type'=>'s'),
						':pv'=>array('data'=>$data['privilegios'], 'type'=>'s'),
						);
		$sql = "INSERT INTO administradores (nombres,apellido_paterno,apellido_materno,fecha_nacimiento,
				usuario,password,privilegios) VALUES(:n,:aP,:aM,:f,:u,:p,:pv)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addPersonaListaNegra($data){
		$values = array(':c'=>array('data'=>$data['categoria'], 'type'=>'i'),
						':e'=>array('data'=>$data['entidad'], 'type'=>'s'),
						':p'=>array('data'=>$data['partido'], 'type'=>'s'),
						':t'=>array('data'=>$data['tipo'], 'type'=>'s'),
						':l'=>array('data'=>$data['clase'], 'type'=>'s'),
						':a'=>array('data'=>$data['calidad'], 'type'=>'s'),
						':d'=>array('data'=>$data['distrito'], 'type'=>'s'),
						':o'=>array('data'=>$data['observaciones'], 'type'=>'s'),
						':i'=>array('data'=>$data['id_persona'], 'type'=>'i'),
						':u'=>array('data'=>$_SESSION['userID'], 'type'=>'i'),
		);
		$sql = "INSERT INTO lista_negra (categoria,entidad,partido,tipo,clase,calidad,
				distrito,observaciones,id_persona,id_usuario,fecha_agregado)
				VALUES(:c,:e,:p,:t,:l,:a,:d,:o,:i,:u,NOW())";
		$this->setDataByQuery($sql, $values);
	}
	
	function addPersona($data, &$ids, &$repre){
		$infoP = $data['info_personal'];
		$infoE = $data['info_econom'];
		$ref = $data['referencia'];
		$values = array(':n'=>array('data'=>trim(strtoupper($infoP['nombres'])), 'type'=>'s'),
                        ':aP'=>array('data'=>trim(strtoupper($infoP['apellidoP'])), 'type'=>'s'),
                        ':aM'=>array('data'=>trim(strtoupper($infoP['apellidoM'])), 'type'=>'s'),
                        ':f'=>array('data'=>$infoP['nacimiento'], 'type'=>'s'),
						':s'=>array('data'=>$infoP['sexo'], 'type'=>'s'),
						':rfc'=>array('data'=>trim(strtoupper($infoP['rfc'])), 'type'=>'s'),
						':curp'=>array('data'=>trim(strtoupper($infoP['curp'])), 'type'=>'s'),
                        ':eC'=>array('data'=>$infoP['edocivil'], 'type'=>'s'),
						':tc'=>array('data'=>$infoP['t_conyuge']==NULL?0:$infoP['t_conyuge'], 'type'=>'i'),
						':de'=>array('data'=>$infoP['dep_econom'], 'type'=>'i'),
                        ':ca'=>array('data'=>trim(strtoupper($infoP['calle'])), 'type'=>'s'),
                        ':co'=>array('data'=>trim(strtoupper($infoP['colonia'])), 'type'=>'s'),
						':ne'=>array('data'=>$infoP['num_ext'], 'type'=>'s'),
						':ni'=>array('data'=>$infoP['num_int'], 'type'=>'s'),
						':dp'=>array('data'=>$infoP['dpto'], 'type'=>'s'),
						':mz'=>array('data'=>$infoP['mza'], 'type'=>'s'),
						':lt'=>array('data'=>$infoP['lote'], 'type'=>'s'),
                        ':cp'=>array('data'=>$infoP['cp'], 'type'=>'s'),
						':r'=>array('data'=>$infoP['residencia'], 'type'=>'s'),
                        ':t'=>array('data'=>$infoP['tel_casa'], 'type'=>'s'),
                        ':cel'=>array('data'=>$infoP['celular'], 'type'=>'s'),
                        ':em'=>array('data'=>trim(strtolower($infoP['email'])), 'type'=>'s'),
                        ':del'=>array('data'=>$infoP['delegacion'], 'type'=>'i'),
                        );
        $sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, fecha_nacimiento, sexo, rfc, curp, estado_civil, t_conyuge, 
        		dep_econom, calle, colonia, num_ext, num_int, dpto, mza, lote, codigo_postal, residencia, telefonos, celular, email, id_delegacion)
                VALUES (:n,:aP,:aM,:f,:s,:rfc,:curp,:eC,:tc,:de,:ca,:co,:ne,:ni,:dp,:mz,:lt,:cp,:r,:t,:cel,:em,:del)";
        if($this->setDataByQuery($sql, $values)){
        	$idPersona = $this->lastId;
        	if(isset($data['representante']))
        		$repre = $idPersona;
        	$ids[] = array($idPersona, $infoE['montoPreaprobado']) ;
        	$this->addInfoSolicitante($infoP,$idPersona);
        	$this->addInfoEconomica($infoE, $idPersona);
        	if($infoP['renovacion'] != 1 && $ref['nombres'] != '' && ($ref['apellidoP'] != '' || $ref['apellidoM'] != ''))
        		$this->addReferencia($ref, $idPersona);
        }
        else
        	throw new Exception('Error al agregar a la persona');
	}
	
	function addSolidario($sol, &$id){
		$values = array(':n'=>array('data'=>strtoupper($sol['nombres']), 'type'=>'s'),
				':aP'=>array('data'=>strtoupper($sol['apellidoP']), 'type'=>'s'),
				':aM'=>array('data'=>strtoupper($sol['apellidoM']), 'type'=>'s'),
				':f'=>array('data'=>$sol['nacimiento'], 'type'=>'s'),
				':s'=>array('data'=>$sol['sexo'], 'type'=>'s'),
				':rfc'=>array('data'=>$sol['rfc'], 'type'=>'s'),
				':curp'=>array('data'=>$sol['curp'], 'type'=>'s'),
				':eC'=>array('data'=>$sol['edocivil'], 'type'=>'s'),
				':tc'=>array('data'=>$sol['t_conyuge'], 'type'=>'i'),
				':de'=>array('data'=>$sol['dep_econom'], 'type'=>'i'),
				':ca'=>array('data'=>strtoupper($sol['calle']), 'type'=>'s'),
				':co'=>array('data'=>strtoupper($sol['colonia']), 'type'=>'s'),
				':ne'=>array('data'=>$infoP['num_ext'], 'type'=>'s'),
				':ni'=>array('data'=>$infoP['num_int'], 'type'=>'s'),
				':dp'=>array('data'=>$infoP['dpto'], 'type'=>'s'),
				':mz'=>array('data'=>$infoP['mza'], 'type'=>'s'),
				':lt'=>array('data'=>$infoP['lote'], 'type'=>'s'),
				':cp'=>array('data'=>$sol['cp'], 'type'=>'s'),
				':r'=>array('data'=>$sol['residencia'], 'type'=>'s'),
				':t'=>array('data'=>$sol['tel_casa'], 'type'=>'s'),
				':cel'=>array('data'=>$sol['celular'], 'type'=>'s'),
				':em'=>array('data'=>$sol['email'], 'type'=>'s'),
				':del'=>array('data'=>$sol['delegacion'], 'type'=>'i'),
		);
		$sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, fecha_nacimiento, sexo, rfc, curp, estado_civil, t_conyuge,
		dep_econom, calle, colonia, num_ext, num_int, dpto, mza, lote, codigo_postal, residencia, telefonos, celular, email, id_delegacion)
		VALUES (:n,:aP,:aM,:f,:s,:rfc,:curp,:eC,:tc,:de,:ca,:co,:ne,:ni,:dp,:mz,:lt,:cp,:r,:t,:cel,:em,:del)";
		if($this->setDataByQuery($sql, $values))
			$id = $this->lastId;
	}
	
	private function addInfoSolicitante($info, $id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),
				':n'=>array('data'=>$info['nivel_estudios'], 'type'=>'s'),
				':eS'=>array('data'=>$info['enfermedad_solic'], 'type'=>'i'),
				':eF'=>array('data'=>$info['enfermedad_fam'], 'type'=>'i'),
				':eD'=>array('data'=>$info['enfermedad_desc'], 'type'=>'s'),
				':a'=>array('data'=>$info['ant_domicilio'], 'type'=>'i'),
		);
		$sql = "INSERT INTO inf_solicitante (id_persona, nivel_estudios, enfermedad_solic, enfermedad_fam, enfermedad_desc, ant_domicilio)
				VALUES (:id, :n, :eS, :eF, :eD, :a)";
		if(!$this->setDataByQuery($sql, $values))
			throw new Exception('Error al agregar informaci�n del solicitante');
	}
	
	private function addInfoEconomica($info, $id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),
				':d'=>array('data'=>isset($info['delegacion'])?$info['delegacion']:1, 'type'=>'i'),
				':nN'=>array('data'=>trim(strtoupper($info['nombre_negocio'])), 'type'=>'s'),
				':ca'=>array('data'=>trim(strtoupper($info['calle'])), 'type'=>'s'),
				':co'=>array('data'=>trim(strtoupper($info['colonia'])), 'type'=>'s'),
				':cp'=>array('data'=>$info['cp'], 'type'=>'s'),
				':t'=>array('data'=>$info['tel_neg'], 'type'=>'s'),
				':aN'=>array('data'=>$info['ant_negocio'], 'type'=>'i'),
				':pC'=>array('data'=>$info['pag_cred_monto'] == '' ? 0 : floatval($info['pag_cred_monto']), 'type'=>'s'),
				':gF'=>array('data'=>$info['gastos_fam']  == '' ? 0 : floatval($info['gastos_fam']), 'type'=>'s'),
				':pA'=>array('data'=>$info['pen_alim_monto'] == '' ? 0 : floatval($info['pen_alim_monto']), 'type'=>'s'),
				':iM'=>array('data'=>$info['ing_mensuales'] == '' ? 0 : floatval($info['ing_mensuales']), 'type'=>'s'),
				':iS'=>array('data'=>$info['inv_semanal'] == '' ? 0 : floatval($info['inv_semanal']), 'type'=>'s'),
				':mS'=>array('data'=>$info['monto_sol'] == '' ? 0 : floatval($info['monto_sol']), 'type'=>'s'),
		);
		$sql = "INSERT INTO inf_economica (id_persona, id_delegacion_negocio, nombre_negocio, calle_negocio, colonia_negocio, cp_negocio, 
							telefonos_negocio, ant_negocio, pag_credito, gastos_fam, pension_alim, ing_mensuales, 
							inv_semanal, monto_sol)
				VALUES (:id, :d, :nN, :ca, :co, :cp, :t, :aN, :pC, :gF, :pA, :iM, :iS, :mS)";
		if(!$this->setDataByQuery($sql, $values))
			throw new Exception('Error al agregar informaci�n econ�mica');
	}
	
	private function addReferencia($info, $id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),
				':n'=>array('data'=>trim(strtoupper($info['nombres'])), 'type'=>'s'),
				':aP'=>array('data'=>trim(strtoupper($info['apellidoP'])), 'type'=>'s'),
				':aM'=>array('data'=>trim(strtoupper($info['apellidoM'])), 'type'=>'s'),
				':p'=>array('data'=>trim(strtoupper($info['parentesco'])), 'type'=>'s'),
				':ca'=>array('data'=>trim(strtoupper($info['calle'])), 'type'=>'s'),
				':co'=>array('data'=>trim(strtoupper($info['colonia'])), 'type'=>'s'),
				':d'=>array('data'=>$info['delegacion'], 'type'=>'i'),
				':cp'=>array('data'=>$info['cp'], 'type'=>'i'),
				':t'=>array('data'=>$info['tel_casa'], 'type'=>'s'),
				':ce'=>array('data'=>$info['celular'], 'type'=>'s'),
		);
		$sql = "INSERT INTO referencias_personales (id_persona, nombres_ref, apellido_paterno_ref, 
				apellido_materno_ref, parentesco, calle_ref, colonia_ref, codigo_postal_ref,
				id_delegacion_ref, telefonos_ref, celular_ref)
				VALUES (:id, :n, :aP, :aM, :p, :ca, :co, :cp, :d, :t, :ce)";
		if(!$this->setDataByQuery($sql, $values))
			throw new Exception('Error al agregar referencia personal');
	}
	
	function updatePersona($data, &$ids, &$repre){
		$infoP = $data['info_personal'];
		$infoE = $data['info_econom'];
		$ref = $data['referencia'];
		$idPersona = $infoP['id_persona'];
		$values = array(':id'=>array('data'=>$idPersona, 'type'=>'i'),
				':n'=>array('data'=>strtoupper($infoP['nombres']), 'type'=>'s'),
				':aP'=>array('data'=>strtoupper($infoP['apellidoP']), 'type'=>'s'),
				':aM'=>array('data'=>strtoupper($infoP['apellidoM']), 'type'=>'s'),
				':f'=>array('data'=>$infoP['nacimiento'], 'type'=>'s'),
				':s'=>array('data'=>$infoP['sexo'], 'type'=>'s'),
				':rfc'=>array('data'=>trim(strtoupper($infoP['rfc'])), 'type'=>'s'),
				':curp'=>array('data'=>trim(strtoupper($infoP['curp'])), 'type'=>'s'),
				':eC'=>array('data'=>$infoP['edocivil'], 'type'=>'s'),
				':tc'=>array('data'=>$infoP['t_conyuge'], 'type'=>'i'),
				':de'=>array('data'=>$infoP['dep_econom'], 'type'=>'i'),
				':ca'=>array('data'=>trim(strtoupper($infoP['calle'])), 'type'=>'s'),
				':co'=>array('data'=>trim(strtoupper($infoP['colonia'])), 'type'=>'s'),
				':ne'=>array('data'=>$infoP['num_ext'], 'type'=>'s'),
				':ni'=>array('data'=>$infoP['num_int'], 'type'=>'s'),
				':dp'=>array('data'=>$infoP['dpto'], 'type'=>'s'),
				':mz'=>array('data'=>$infoP['mza'], 'type'=>'s'),
				':lt'=>array('data'=>$infoP['lote'], 'type'=>'s'),
				':cp'=>array('data'=>$infoP['cp'], 'type'=>'s'),
				':r'=>array('data'=>$infoP['residencia'], 'type'=>'s'),
				':t'=>array('data'=>$infoP['tel_casa'], 'type'=>'s'),
				':cel'=>array('data'=>$infoP['celular'], 'type'=>'s'),
				':em'=>array('data'=>trim(strtolower($infoP['email'])), 'type'=>'s'),
				':del'=>array('data'=>$infoP['delegacion'], 'type'=>'i'),
		);
		//var_dump($values);die();
		$sql = "UPDATE personas SET nombres=:n, apellido_paterno=:aP, apellido_materno=:aM, fecha_nacimiento=:f, sexo=:s, rfc=:rfc,
				curp=:curp, estado_civil=:eC, t_conyuge=:tc, dep_econom=:de, calle=:ca, colonia=:co, num_ext=:ne, num_int=:ni,
				dpto=:dp, mza=:mz, lote=:lt, codigo_postal=:cp, residencia=:r, telefonos=:t, celular=:cel, email=:em, id_delegacion=:del
				WHERE id_persona = :id";
		if($this->setDataByQuery($sql, $values)){
			if(isset($data['representante']))
				$repre = $idPersona;
			$ids[] = array($idPersona, $infoE['montoPreaprobado']) ;
			$infSolic = $this->existsInfoSolicitante($idPersona);
			$infEcon = $this->existsInfoEconomica($idPersona);
			!empty($infSolic) ? $this->updateInfoSolicitante($infoP,$idPersona) : $this->addInfoSolicitante($infoP,$idPersona);
			!empty($infEcon) ? $this->updateInfoEconomica($infoE, $idPersona) : $this->addInfoEconomica($infoE,$idPersona);
		}
	}
	
	function updateSolidario($sol, &$id){
		$values = array(':id'=>array('data'=>$sol['id_persona'], 'type'=>'s'),
				':n'=>array('data'=>strtoupper($sol['nombres']), 'type'=>'s'),
				':aP'=>array('data'=>strtoupper($sol['apellidoP']), 'type'=>'s'),
				':aM'=>array('data'=>strtoupper($sol['apellidoM']), 'type'=>'s'),
				':f'=>array('data'=>$sol['nacimiento'], 'type'=>'s'),
				':s'=>array('data'=>$sol['sexo'], 'type'=>'s'),
				':rfc'=>array('data'=>$sol['rfc'], 'type'=>'s'),
				':curp'=>array('data'=>$sol['curp'], 'type'=>'s'),
				':eC'=>array('data'=>$sol['edocivil'], 'type'=>'s'),
				':tc'=>array('data'=>$sol['t_conyuge'], 'type'=>'i'),
				':de'=>array('data'=>$sol['dep_econom'], 'type'=>'i'),
				':ca'=>array('data'=>strtoupper($sol['calle']), 'type'=>'s'),
				':co'=>array('data'=>strtoupper($sol['colonia']), 'type'=>'s'),
				':ne'=>array('data'=>$infoP['num_ext'], 'type'=>'s'),
				':ni'=>array('data'=>$infoP['num_int'], 'type'=>'s'),
				':dp'=>array('data'=>$infoP['dpto'], 'type'=>'s'),
				':mz'=>array('data'=>$infoP['mza'], 'type'=>'s'),
				':lt'=>array('data'=>$infoP['lote'], 'type'=>'s'),
				':cp'=>array('data'=>$sol['cp'], 'type'=>'s'),
				':r'=>array('data'=>$sol['residencia'], 'type'=>'s'),
				':t'=>array('data'=>$sol['tel_casa'], 'type'=>'s'),
				':cel'=>array('data'=>$sol['celular'], 'type'=>'s'),
				':em'=>array('data'=>$sol['email'], 'type'=>'s'),
				':del'=>array('data'=>$sol['delegacion'], 'type'=>'i'),
		);
		$sql = "UPDATE personas SET nombres=:n, apellido_paterno=:aP, apellido_materno=:aM, fecha_nacimiento=:f, sexo=:s, rfc=:rfc, curp=:curp, 
				estado_civil=:eC, t_conyuge=:tc, dep_econom=:de, calle=:ca, colonia=:co, num_ext=:ne, num_int=:ni,
				dpto=:dp, mza=:mz, lote=:lt, codigo_postal=:cp, residencia=:r, telefonos=:t, celular=:cel, email=:em, id_delegacion=:del
				WHERE id_persona = :id";
		if($this->setDataByQuery($sql, $values))
			$id = $sol['id_persona'];
	}
	
	private function updateInfoSolicitante($info, $id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),
				':n'=>array('data'=>$info['nivel_estudios'], 'type'=>'s'),
				':eS'=>array('data'=>$info['enfermedad_solic'], 'type'=>'i'),
				':eF'=>array('data'=>$info['enfermedad_fam'], 'type'=>'i'),
				':eD'=>array('data'=>$info['enfermedad_desc'], 'type'=>'s'),
				':a'=>array('data'=>$info['ant_domicilio'], 'type'=>'i'),
		);
		$sql = "UPDATE inf_solicitante SET nivel_estudios=:n, enfermedad_solic=:eS, enfermedad_fam=:eF, 
				enfermedad_desc=:eD, ant_domicilio=:a
				WHERE id_persona = :id";
		$this->setDataByQuery($sql, $values);
	}
	
	private function updateInfoEconomica($info, $id){
		$values = array(':id'=>array('data'=>$id, 'type'=>'i'),
				':d'=>array('data'=>$info['delegacion'], 'type'=>'i'),
				':nN'=>array('data'=>trim(strtoupper($info['nombre_negocio'])), 'type'=>'s'),
				':ca'=>array('data'=>trim(strtoupper($info['calle'])), 'type'=>'s'),
				':co'=>array('data'=>trim(strtoupper($info['colonia'])), 'type'=>'s'),
				':cp'=>array('data'=>$info['cp'], 'type'=>'s'),
				':t'=>array('data'=>$info['tel_neg'], 'type'=>'s'),
				':aN'=>array('data'=>$info['ant_negocio'], 'type'=>'i'),
				':pC'=>array('data'=>$info['pag_cred_monto'] == '' ? 0 : floatval($info['pag_cred_monto']), 'type'=>'s'),
				':gF'=>array('data'=>$info['gastos_fam']  == '' ? 0 : floatval($info['gastos_fam']), 'type'=>'s'),
				':pA'=>array('data'=>$info['pen_alim_monto'] == '' ? 0 : floatval($info['pen_alim_monto']), 'type'=>'s'),
				':iM'=>array('data'=>$info['ing_mensuales'] == '' ? 0 : floatval($info['ing_mensuales']), 'type'=>'s'),
				':iS'=>array('data'=>$info['inv_semanal'] == '' ? 0 : floatval($info['inv_semanal']), 'type'=>'s'),
				':mS'=>array('data'=>$info['monto_sol'] == '' ? 0 : floatval($info['monto_sol']), 'type'=>'s'),
		);
		$sql = "UPDATE inf_economica SET id_delegacion_negocio=:d, nombre_negocio=:nN, calle_negocio=:ca, colonia_negocio=:co, cp_negocio=:cp,
							telefonos_negocio=:t, ant_negocio=:aN, pag_credito=:pC, gastos_fam=:gF, pension_alim=:pA, ing_mensuales=:iM,
							inv_semanal=:iS, monto_sol=:mS
							WHERE id_persona=:id";
		$this->setDataByQuery($sql, $values);
	}
	
	function existsPersona($info){
        $values = array(':n'=>$info['nombres'],
        				':aP'=>$info['apellidoP'],
        				':aM'=>$info['apellidoM'],
        				':f'=>$info['nacimiento']);
        $sql = "SELECT P.*, P.id_persona AS idP, ISO.*, IE.*, D.id_estado, D2.id_estado AS id_estado_negocio
        		FROM personas AS P
        		LEFT JOIN inf_solicitante AS ISO ON ISO.id_persona = P.id_persona
        		LEFT JOIN inf_economica AS IE ON IE.id_persona = P.id_persona
        		LEFT JOIN referencias_personales AS R ON R.id_persona = R.id_persona
        		JOIN delegaciones_municipios AS D ON D.id_delegacion = P.id_delegacion
        		LEFT JOIN delegaciones_municipios AS D2 ON D2.id_delegacion = IE.id_delegacion_negocio
        		WHERE P.nombres = :n AND P.apellido_paterno = :aP AND P.apellido_materno = :aM AND P.fecha_nacimiento = :f
        		AND NOT EXISTS (SELECT * FROM lista_negra LN WHERE P.id_persona=LN.id_persona AND LN.categoria <> 0)";
        if($this->getDataByQuery($sql, $values))
           return $this->arrayResult[0];
    }
    
    function existsPersonaListaNegra($info){
    	$values = array(
    			':aP'=>$info['apellidoP'],
    			':aM'=>$info['apellidoM'],);
    	$sql = "SELECT * FROM personas P
				JOIN lista_negra LN ON LN.id_persona=P.id_persona
				WHERE P.apellido_paterno=:aP AND P.apellido_materno=:aM";
    	if($this->getDataByQuery($sql, $values))
    		return $this->arrayResult;
    }
    
    private function existsInfoSolicitante($idP){
    	$values = array(':id'=>$idP);
    	$sql = "SELECT *
		    	FROM inf_solicitante 
		    	WHERE id_persona = :id";
    	if($this->getDataByQuery($sql, $values))
    		return $this->arrayResult[0];
    }
    
    private function existsInfoEconomica($idP){
    	$values = array(':id'=>$idP);
    	$sql = "SELECT *
		    	FROM inf_economica
		    	WHERE id_persona = :id";
    	if($this->getDataByQuery($sql, $values))
    		return $this->arrayResult[0];
    }
    
    function getDatosPersona($idPersona){
    	$values = array(':id'=>$idPersona);
    	$sql = "SELECT P.*, P.id_persona AS idP, ISO.*, IE.*, D.*, D.id_estado, D2.id_estado AS id_estado_negocio, 
    			D2.nombre_del AS delegacion_negocio, E.id_estado, E.nombre_edo, E2.nombre_edo AS nombre_edo_negocio
    			FROM personas AS P
    			LEFT JOIN inf_solicitante AS ISO ON ISO.id_persona = P.id_persona
        		LEFT JOIN inf_economica AS IE ON IE.id_persona = P.id_persona
        		LEFT JOIN referencias_personales AS R ON R.id_persona = R.id_persona
        		JOIN delegaciones_municipios AS D ON D.id_delegacion = P.id_delegacion
        		JOIN estados_republica AS E ON E.id_estado = D.id_estado
        		LEFT JOIN delegaciones_municipios AS D2 ON D2.id_delegacion = IE.id_delegacion_negocio
        		LEFT JOIN estados_republica AS E2 ON E2.id_estado = D2.id_estado
    			WHERE P.id_persona = :id";
    	if($this->getDataByQuery($sql, $values))
    		return $this->arrayResult[0];
    }
    
    function getListaNegraByCriterios($data){
    	$values = array();
    	$criterio =  array();
    	
    	if(isset($data['categoria'])){
    		$criterio[] = 'L.categoria = :c';
    		$values[':c'] = $data['categoria'];
    	}
    	if(isset($data['idPersona'])){
    		$criterio[] = 'P.id_persona = :p';
    		$values[':p'] = $data['idPersona'];
    	}
    	
    	$criterio = empty($data) ? "1=1" : implode(' AND ',$criterio);
    	
    	$sql = "SELECT CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) acreditado,
    			P.fecha_nacimiento, CONCAT(A.nombres,' ',A.apellido_paterno,' ',A.apellido_materno) usuario,
    			L.*
    			FROM lista_negra L
				JOIN personas P USING(id_persona)
				JOIN administradores A ON L.id_usuario=A.id
				WHERE {$criterio}
				ORDER BY L.fecha_agregado DESC";
    	if($this->getDataByQuery($sql, $values))
    		return $this->arrayResult;
    	
    }
    
    function getPersonaByNombres($data){
    	$values = array(':x'=>'%'.$data['term'].'%');
    	$sql = "SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS value
    			FROM personas WHERE (CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) LIKE :x
    			OR nombres LIKE :x OR apellido_paterno LIKE :x OR apellido_materno LIKE :x) AND
    			NOT EXISTS (SELECT * FROM lista_negra LN WHERE LN.id_persona = personas.id_persona AND LN.categoria <> 0)
    			LIMIT 10";
    	$result = $this->getDataByQuery($sql, $values);
    	return $result;
    }
}
?>
