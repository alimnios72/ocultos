<?php
require_once('DBClass.php');
class Credito extends DB{
	private $periodo = array('s'=>'Semanal','q'=>'Quincenal','m'=>'Mensual','b'=>'Bimestral','t'=>'Trimestral','u'=>'Quinquenal','e'=>'Semestral','a'=>'Anual');
	
	///////////////////////////////////////////////////////////////////////////
	function getacreds($id){
		$values = array();
		
		$sql="SELECT * FROM acreditados WHERE id_credito=$id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function updatemov($acred,$monto,$idDep){
		$values = array(':m'=>array('data'=>$monto,'type'=>'i'),
						':a'=>array('data'=>$acred,'type'=>'i'),
						':i'=>array('data'=>$idDep,'type'=>'i'),
						);
		$sql = "UPDATE movimientos M
				JOIN abonos AB ON AB.id_movimiento=M.id_movimiento 
				SET M.capital=:m
				WHERE M.id_acreditado=:a AND AB.id_deposito=:i AND M.tipo_mov=1";
		$this->setDataByQuery($sql, $values);
		
		unset($values[':m']);
		$sql = "UPDATE movimientos M
				JOIN abonos AB ON AB.id_movimiento=M.id_movimiento 
				SET M.monto=M.interes+M.iva+M.capital
				WHERE M.id_acreditado=:a AND AB.id_deposito=:i AND M.tipo_mov=1";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function addtomora($acred,$monto,$interes,$iva,$idDep,$fecha){
		$values = array();
		$sql = "INSERT INTO movimientos (id_acreditado,tipo_mov,monto,interes,iva,capital,fecha_mov)
				VALUES ({$acred},1,{$monto},{$interes},{$iva},0,'{$fecha}')";
		if($this->setDataByQuery($sql, $values)){
			$idmov = $this->lastId;
			$this->addabono($idmov,$idDep);
		}
		return false;
	}
	function addabono($id,$idDep){
		$values = array(':i'=>array('data'=>$id,'type'=>'i'),
						':d'=>array('data'=>$idDep,'type'=>'i'),
		);
		$sql = "INSERT INTO abonos (id_movimiento,id_deposito,tipo_abono)
				VALUES (:i,:d,1)";
		$this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function checkAllparcialidades($id){
		$values=array();
		$sql="SELECT M.*,CA.* FROM creditos C
			JOIN acreditados A USING(id_credito)
			JOIN movimientos M USING(id_acreditado)
			JOIN cargos CA USING(id_movimiento)
			WHERE C.id_credito={$id} AND CA.tipo_cargo=0 AND CA.status=0";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function calculaSobrante($id){
		$values=array();
		$sql="SELECT capital 
			  FROM saldos_creditos
			  WHERE id_credito={$id}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0]['capital'];
	}
	
	function ultimoPago($id){
		$values=array();
		$sql = "SELECT SUM(M.monto) monto,SUM(M.interes) interes, SUM(M.iva) iva,
				SUM(M.capital) capital, M.fecha_mov,
				AB.id_deposito, AB.tipo_abono
				FROM creditos C
				JOIN acreditados A USING(id_credito)
				JOIN movimientos M USING(id_acreditado)
				JOIN abonos AB USING(id_movimiento)
				WHERE C.id_credito={$id} AND AB.tipo_abono=0
				GROUP BY M.fecha_mov, AB.id_deposito
				ORDER BY M.fecha_mov DESC
				LIMIT 1";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	///////////////////////////////////////////////////////////////////////////
	
	function addGestionCobranza($data){
		$values = array(':idA'=>array('data'=>$data['acreditado'],'type'=>'i'),
						':idU'=>array('data'=>$data['userID'],'type'=>'i'),
						':tg'=>array('data'=>$data['tipo_gestion'],'type'=>'i'),
						':f'=>array('data'=>$data['fecha_gestion'],'type'=>'s'),
						':d'=>array('data'=>trim($data['descripcion']),'type'=>'s'),
						);
		$sql = "INSERT INTO gestiones_cobranza (id_acreditado, id_usuario, tipo_gestion, 
				fecha_gestion, descripcion)
				VALUES (:idA, :idU, :tg, :f, :d)";
		$this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function addCredito($data, $personaDB, $renov=false, $ids = array(), $repre="", $idSol=""){
		$acreditados = $data['acreditado'];
		$opcCred = $data['opcCred'];
		$solidario = $data['solidario'];
		
		//Informaci�n del cr�dito
		$values = array(':p'=>array('data'=>$opcCred['producto'],'type'=>'i'),
				':t'=>array('data'=>$opcCred['tipo'],'type'=>'i'),
				':ts'=>array('data'=>$opcCred['tasa'],'type'=>'s'),
				':tm'=>array('data'=>$opcCred['tasa_mora'],'type'=>'s'),
				':pl'=>array('data'=>$opcCred['plazo'],'type'=>'i'),
				':pe'=>array('data'=>$this->periodo[$opcCred['periodo']], 'type'=>'s'),
				':z'=>array('data'=>$opcCred['zona'],'type'=>'i'),
				':v'=>array('data'=>$opcCred['viejo'],'type'=>'i'),
		);
		
		//Inicio la transacci�n de inserciones en la BD
		try{
			$this->dbLink->beginTransaction();
			//Informaci�n de las personas en el cr�dito o solidarios, inserci�n y actualizaci�n de los mismos
			if(!$renov){
				foreach($acreditados as $acreditado){
					if (isset($acreditado['info_personal']['id_persona']))
						$personaDB->updatePersona($acreditado, $ids, $repre);
					else
						$personaDB->addPersona($acreditado, $ids, $repre);
				}
			}
			if($opcCred['tipo'] == 0){
				if(isset($solidario['id_persona']) && !$renov)
					$personaDB->updateSolidario($solidario, $idSol);
				else
					$personaDB->addSolidario($solidario, $idSol);
			}
			$sql = "INSERT INTO creditos (producto, tipo, status, tasa_interes, tasa_mora, plazo, periodo, expediente,
				fecha_captura, cta_deposito, id_zona, viejo)
				VALUES (:p, :t, 0, :ts, :tm, :pl, :pe, 'Indefinido', NOW(), 1, :z, :v)";
			if($this->setDataByQuery($sql, $values)){
				$idCredito = $this->lastId;
				$this->addVendedores($idCredito, $opcCred['vendedor']);
				$this->addAcreditados($ids, $idCredito);
				//$this->addReferenciaBancaria($idCredito);
				if($opcCred['tipo'] == 1)
					if($renov)
						$this->updateGrupo($idCredito,$repre,$opcCred['grupo']);
					else
						$this->addGrupo($opcCred, $repre, $idCredito);
				else
					$this->addSolidario($idCredito, $idSol);
			}
			$this->dbLink->commit();
		}
		catch(Exception $e){
			$this->dbLink->rollBack();
			throw new Exception($e->getMessage());
		}
	}
	
	private function addReferenciaBancaria($idCred){
		$credito = $this->getCreditoById($idCred);
		$acreditados = $this->getAcreditadosByCredito($idCred);
		$ref = new RefBancaria($credito, $acreditados);
		$values = array(':idC'=>array('data'=>$idCred,'type'=>'i'),
						':ref'=>array('data'=>$ref->referencia,'type'=>'s'),);
		$sql = "UPDATE creditos SET ref_banc=:ref WHERE id_credito=:idC";
		$this->setDataByQuery($sql, $values);
	}
	
	private function addVendedores($idCred, $idVend){
		$values = array(':idC'=>array('data'=>$idCred,'type'=>'i'),
						':idV'=>array('data'=>$idVend,'type'=>'i'),
		);
		$sql = "INSERT INTO vendedores (id_credito, id_usuario)
				VALUES (:idC, :idV)";
		$this->setDataByQuery($sql, $values);
	}
	
	private function addAcreditados($acreds, $idC){
		foreach ($acreds as $acred){
			$values = array(':idC'=>array('data'=>$idC,'type'=>'i'),
							':idP'=>array('data'=>$acred[0],'type'=>'i'),
							':m'=>array('data'=>$acred[1],'type'=>'s'),
			);
			$sql = "INSERT INTO acreditados (id_credito, id_persona, cantidad)
					VALUES (:idC, :idP, :m)";
			$this->setDataByQuery($sql, $values);
		}
	}
	
	private function addGrupo($data, $idR, $idC){
		$values = array(':n'=>array('data'=>trim(strtoupper($data['grupo'])),'type'=>'s'),
					);
		$sql = "INSERT INTO agrupaciones (nombre)
				VALUES (:n)";
		if($this->setDataByQuery($sql, $values)){
			$idG = $this->lastId;
			$this->addRepresentante($idG,$idR,$idC);
		}
		else{
			throw new Exception('Nombre de grupo ya existente.');
		}
	}
	
	private function addRepresentante($idG,$idR,$idC){
		$values = array(':g'=>array('data'=>$idG,'type'=>'i'),
						':r'=>array('data'=>$idR,'type'=>'i'),
						':c'=>array('data'=>$idC,'type'=>'i'),
					);
		$sql = "INSERT INTO representantes (id_persona,id_grupo,id_credito)
				VALUES (:r,:g,:c)";
		$this->setDataByQuery($sql, $values);
	}
	
	private function addSolidario($idC, $idS){
		$values = array(':idC'=>array('data'=>$idC,'type'=>'i'),
						':idS'=>array('data'=>$idS,'type'=>'i'),
					);
		$sql = "INSERT INTO obligados_solidarios (id_credito, id_persona)
				VALUES (:idC, :idS)";
		$this->setDataByQuery($sql, $values);
	}
	
	function addCitaCredito($data){
		$values = array(':id'=>array('data'=>$data['idC'],'type'=>'i'),
						':ini'=>array('data'=>$data['fecha']." ".$data['hIni'],'type'=>'s'),
						':fin'=>array('data'=>$data['fecha']." ".$data['hFin'],'type'=>'s'),
						':suc'=>array('data'=>$data['sucursal'],'type'=>'i'),
		);

		$sql = "INSERT INTO citas_credito (id_credito, inicio, fin, id_sucursal)
				VALUES (:id, :ini, :fin, :suc)";
		$this->setDataByQuery($sql, $values);
	}
	
	function updateCreditoById($data){
		$values = array(':id'=>array('data'=>$data['id_credito'],'type'=>'i'),
						':pd'=>array('data'=>$data['producto'],'type'=>'i'),
						':st'=>array('data'=>$data['status'],'type'=>'i'),
						':ts'=>array('data'=>$data['tasaInteres'],'type'=>'s'),
						':pl'=>array('data'=>$data['plazo'],'type'=>'i'),
						':pr'=>array('data'=>$data['periodo'],'type'=>'s'),
						':ex'=>array('data'=>$data['expediente'],'type'=>'s'),
						':zo'=>array('data'=>$data['zona'],'type'=>'i'),
						':ba'=>array('data'=>$data['banco'],'type'=>'i'));
		
		$sql = "UPDATE creditos SET producto = :pd, status = :st, tasa_interes = :ts,
				plazo = :pl, periodo = :pr, expediente = :ex, id_zona = :zo, cta_deposito = :ba
				WHERE id_credito = :id";
		if($this->setDataByQuery($sql, $values)){
			$exists = $this->existsVendedorCredito($data['id_credito']);
			if(!empty($exists))
				$this->updateVendedorByCredito($data['id_credito'], $data['vendedor']);
			else
				$this->addVendedores($data['id_credito'], $data['vendedor']);
			
		}
		return true;
			/*if($this->updateVendedorByCredito($data['id_credito'], $data['vendedor']))
				return  true;*/
	}
	
	function updateVendedorByCredito($idCred, $idVend){
		$values = array(':idC'=>array('data'=>$idCred,'type'=>'i'),
						':idV'=>array('data'=>$idVend,'type'=>'i'),
						);
		
		$sql = "UPDATE vendedores SET id_usuario = :idV
				WHERE id_credito = :idC";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function updateCreditoPreaprobado($cred, $status){
		$values = array(':c'=>array('data'=>$cred,'type'=>'i'),
						':s'=>array('data'=>$status,'type'=>'i'));
		$sql = "UPDATE creditos SET fecha_aprobacion = NOW(), status = :s
				WHERE id_credito = :c";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function updateGrupo($idC,$idR,$grupo){
		$values = array(':c'=>array('data'=>$idC,'type'=>'i'),
						':r'=>array('data'=>$idR,'type'=>'i'),
						':g'=>array('data'=>$grupo,'type'=>'s'),
					);
		$sql = "INSERT INTO representantes (id_persona,id_credito,id_grupo)
				VALUES (:r,:c,(SELECT id_grupo FROM agrupaciones WHERE nombre=:g LIMIT 1))";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function updateStatusCredito($id, $status){
		$values = array(':id'=>array('data'=>$id,'type'=>'i'),
						':s'=>array('data'=>$status,'type'=>'i'),
						);
		
		$sql = "UPDATE creditos SET status = :s
				WHERE id_credito = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function updateCreditoAprobado($data){
		$values = array(':id'=>array('data'=>$data['id'],'type'=>'i'),
						':e'=>array('data'=>strtoupper($data['expediente']),'type'=>'s'),
						':f'=>array('data'=>$data['fecha'],'type'=>'s'),
						':eS'=>array('data'=>$data['esSemana']=="true"?1:0,'type'=>'i'),
						':eM'=>array('data'=>$data['esMes']=="true"?1:0,'type'=>'i'),
						':dS'=>array('data'=>$data['diaSemana'],'type'=>'i'),
						':dM'=>array('data'=>$data['diaMes'],'type'=>'i'),
						':b'=>array('data'=>$data['cta_deposito'],'type'=>'i'),
						);
		$sql = "UPDATE creditos SET expediente = :e, fecha_entrega = :f, esDiaSemana = :eS, esDiaMes = :eM, 
				diaSemana = :dS, diaMes = :dM, cta_deposito = :b
				WHERE id_credito = :id";
		if($this->setDataByQuery($sql, $values))
			$this->addReferenciaBancaria($values[':id']['data']);
	}
		
	function updateCreditoActivo($data){
		$values = array(':id'=>array('data'=>$data['idC'],'type'=>'i'),
						);
		$sql = "UPDATE creditos SET status = 2
				WHERE id_credito = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function updateAcreditado($data){
		$values = array(':id'=>array('data'=>$data['idAcred'],'type'=>'i'),
						':m'=>array('data'=>$data['monto'],'type'=>'s'),
						);
		$sql = "UPDATE acreditados SET cantidad = :m
				WHERE id_acreditado = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function deleteAcreditado($data){
		$values = array(':id'=>array('data'=>$data['idAcred'],'type'=>'i'),
						);
		$sql = "DELETE FROM acreditados
				WHERE id_acreditado = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function deleteCredito($data){
		$values = array(':id'=>array('data'=>$data['idCredito'],'type'=>'i'),
				);
		$sql = "DELETE FROM creditos
				WHERE id_credito = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function deleteGestionCobranza($data){
		$values = array(':id'=>array('data'=>$data['id'],'type'=>'i'),
		);
		$sql = "DELETE FROM gestiones_cobranza
				WHERE id_gestion = :id";
		return $this->setDataByQuery($sql, $values) ? true : false;
	}
	
	function getCreditosByStatus($status){
		$values = array(':s'=>$status);
		$sql = "SELECT DISTINCT(C.id_credito), C.*, Z.*, CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) AS nombre, 
				COALESCE(tg.nombre, tp.NombComp) AS nombreGrupo 
				FROM creditos AS C
				LEFT JOIN vendedores AS V ON V.id_credito = C.id_credito
				LEFT JOIN administradores AS U ON U.id = V.id_usuario
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN acreditados A ON A.id_credito = C.id_credito
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE C.status = :s ORDER BY C.fecha_captura DESC";

		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditoIndividual($id){
		$values = array(':id'=>$id);
		$sql = "SELECT C.*, A.cantidad, CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) AS acreditado, P.*,
				B.banco, B.no_cuenta, B.clabe, A.id_acreditado, PE.id_persona AS id_solidario, CONCAT(PE.nombres,' ',PE.apellido_paterno,' ',PE.apellido_materno) AS obligado,
				PE.fecha_nacimiento AS nacS, PE.calle  AS calleS, PE.colonia  AS coloniaS, PE.telefonos AS telefonosS, PE.celular AS celularS,
				CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) AS vendedor, Z.nombre_zona
				FROM creditos AS C
				JOIN acreditados AS A ON A.id_credito = C.id_credito
				JOIN vendedores AS V ON V.id_credito = C.id_credito
				JOIN administradores AS U ON U.id = V.id_usuario
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN personas AS P ON P.id_persona = A.id_persona
				JOIN obligados_solidarios AS O ON O.id_credito = C.id_credito
				JOIN cuentas_bancarias B ON B.id_banco = C.cta_deposito
				JOIN personas AS PE ON PE.id_persona = O.id_persona
				WHERE C.id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getCreditoGrupal($id){
		$values = array(':id'=>$id);
		$sql = "SELECT C.*, SUM(A.cantidad) AS Total,
				COUNT(A.id_acreditado) AS num_int, B.banco, B.no_cuenta, B.clabe, G.nombre, 
				R.id_persona AS id_representante, CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) AS vendedor, 
				Z.nombre_zona
				FROM creditos AS C
				JOIN acreditados AS A ON A.id_credito = C.id_credito
				JOIN vendedores AS V ON V.id_credito = C.id_credito
				JOIN administradores AS U ON U.id = V.id_usuario
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN representantes R ON R.id_credito=C.id_credito
				JOIN agrupaciones G ON G.id_grupo=R.id_grupo
				JOIN cuentas_bancarias B ON B.id_banco = C.cta_deposito
				WHERE C.id_credito = :id
				GROUP BY C.id_credito, C.fecha_aprobacion, C.tasa_interes, C.tasa_mora, C.plazo, C.diaSemana, C.diaMes, C.periodo,
				C.tipo, C.producto, C.esDiaSemana, C.esDiaMes, C.expediente, C.fecha_captura, C.fecha_entrega, C.fecha_aprobacion, 
				C.status, C.viejo, C.cta_deposito, C.id_zona, C.ref_banc, B.banco, B.no_cuenta, B.clabe, G.nombre, R.id_representante, 
				U.nombres, U.apellido_paterno, U.apellido_materno, Z.nombre_zona";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getAcreditadosByCredito($id){
		$values = array(':id'=>$id);
		$sql = "SELECT A.id_persona, A.id_acreditado, A.cantidad, CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) AS acreditado, P.*
				FROM creditos AS C
				JOIN acreditados AS A ON A.id_credito = C.id_credito
				JOIN personas AS P ON P.id_persona = A.id_persona
				WHERE C.id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditosByAcreditado($idP, $status){
		$status = implode(',',$status);
		$values = array(':id'=>$idP);
		$sql = "SELECT C.*, A.cantidad
				FROM creditos AS C
				JOIN acreditados AS A ON A.id_credito = C.id_credito
				JOIN personas AS P ON P.id_persona = A.id_persona
				WHERE P.id_persona = :id AND C.status IN ({$status})
				ORDER BY A.cantidad DESC";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditosByObligado($idP, $status){
		$status = implode(',',$status);
		$values = array(':id'=>$idP);
		$sql = "SELECT C.*
				FROM creditos AS C
				JOIN acreditados AS A ON A.id_credito = C.id_credito
				JOIN obligados_solidarios AS O ON O.id_credito = C.id_credito
				WHERE O.id_persona = :id AND C.status IN ({$status})";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditosByPersona($idPersona){
		$values = array(':id'=>$idPersona);
		$sql = "SELECT C.*, A.id_acreditado, A.cantidad
				FROM personas AS P
				JOIN acreditados AS A ON A.id_persona = P.id_persona
				JOIN creditos AS C ON C.id_credito = A.id_credito
				WHERE P.id_persona = :id
				ORDER BY C.fecha_entrega"; 
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCitasCredito($ini, $fin, $suc){
		$values = array(':ini'=>$ini,':fin'=>$fin, ':suc'=>$suc);
		$sql = "SELECT C.id_credito, C.tipo, CI.inicio, CI.fin,
				COALESCE(tg.nombre, tp.NombComp) AS nombre
				FROM citas_credito CI
				JOIN creditos C ON C.id_credito = CI.id_credito
				JOIN acreditados A ON A.id_credito = C.id_credito
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE (CI.inicio BETWEEN :ini AND :fin) AND (CI.fin BETWEEN :ini AND :fin) AND CI.id_sucursal = :suc
				ORDER BY time(CI.inicio)";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditoById($id){
		$values = array(':id'=>$id);
		$sql = "SELECT C.*, Z.nombre_zona, CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) AS nombre, SUM(A.cantidad) as montoTotal, 
				COALESCE(tg.nombre, tp.NombComp) AS nombreGrupo, B.banco, B.no_cuenta, B.clabe, V.id_usuario
				FROM creditos AS C
				LEFT JOIN vendedores AS V ON V.id_credito = C.id_credito
				LEFT JOIN administradores AS U ON U.id = V.id_usuario
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN acreditados A ON A.id_credito = C.id_credito
				JOIN cuentas_bancarias AS B ON B.id_banco = C.cta_deposito
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito 
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE C.id_credito = :id
				GROUP BY C.id_credito, C.producto, C.tipo, C.status, C.tasa_interes, C.tasa_mora, C.plazo, C.periodo, C.esDiaSemana, C.esDiaMes,
				C.diaSemana, C.diaMes, C.expediente, C.fecha_captura, C.fecha_aprobacion, C.fecha_entrega, C.cta_deposito, C.id_zona, C.ref_banc,
				C.viejo, Z.nombre_zona, U.nombres, U.apellido_paterno, U.apellido_materno, B.banco, B.no_cuenta,
				B.clabe, V.id_usuario";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getGestionesByCriterio($data, $order=array()){
		$values = array();
		$criterio =  array();
		$orderby = "";
		
		if(isset($data['expediente'])){
			$criterio[] = 'C.expediente = :e';
			$values[':e'] = $data['expediente'];
		}
		if(isset($data['fecha'])){
			$criterio[] = 'GC.fecha_gestion = :f';
			$values[':f'] = $data['fecha'];
		}
		if(isset($data['usuario'])){
			$criterio[] = 'U.id = :u';
			$values[':u'] = $data['usuario'];
		}
		if(isset($order['order'])){
			$orderby = "{$order['order']},";
		}
		$criterio = empty($data) ? "1=1" : implode(' AND ',$criterio);
				
		$sql = "SELECT GC.id_gestion, CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) gestionado, 
				CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) usuario, GC.*, C.expediente
				FROM gestiones_cobranza GC
				JOIN acreditados A ON A.id_acreditado=GC.id_acreditado
				JOIN personas P ON P.id_persona=A.id_persona
				JOIN administradores U ON U.id=GC.id_usuario
				JOIN creditos C ON C.id_credito = A.id_credito
				WHERE {$criterio}
				ORDER BY {$orderby} GC.fecha_gestion";
		//var_dump($sql);die();
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getCreditoByCriterio($data, $orderBy=""){
		$values = array();
		$criterio =  array();
		
		if(isset($data['expediente'])){
			$criterio[] = 'C.expediente = :e';
			$values[':e'] = $data['expediente'];
		}
		if(isset($data['persona'])){
			$criterio[] = ' PS.id_persona = :p';
			$values[':p'] = $data['persona'];
		}
		if(isset($data['grupo'])){
			$criterio[] = 'GP.nombre LIKE :g';
			$values[':g'] = '%'.$data['grupo'].'%';
		}
		if(isset($data['fechas'])){
			$criterio[] = '(C.fecha_entrega BETWEEN :f1 AND :f2)';
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['cancelado'])){
			if(!$data['cancelado']){
				$criterio[] = '(C.expediente <> "Cancelado")';
			}
		}
		if(isset($orderBy['status']))
			$orderBy = 'C.status';
		elseif(isset($orderBy['fecha']))
			$orderBy = 'C.fecha_entrega';
		elseif(isset($orderBy['tasa']))
			$orderBy = 'C.tasa_interes';
		elseif(isset($orderBy['nombre']))
			$orderBy = "COALESCE(tg.nombre, tp.NombComp)";
		elseif(isset($orderBy['expediente'])){
			$criterio[] = "LOCATE('-',expediente) > 0";
			$orderBy = "LEFT(expediente,LOCATE('-', expediente)),CAST(SUBSTRING(expediente FROM LOCATE('-', expediente)+1) AS SIGNED)";
		}
		else
			$orderBy = 'C.fecha_entrega';
		
		$criterio = implode(' AND ',$criterio);
		//TOP 15
		$sql = "SELECT C.*, Z.nombre_zona, CONCAT(U.nombres,' ',U.apellido_paterno,' ',U.apellido_materno) AS nombre, SUM(A.cantidad) as montoTotal,
				COALESCE(tg.nombre, tp.NombComp) AS nombreGrupo, B.banco, B.no_cuenta, B.clabe
				FROM creditos AS C
				LEFT JOIN vendedores AS V ON V.id_credito = C.id_credito
				LEFT JOIN administradores AS U ON U.id = V.id_usuario
				JOIN zonas AS Z ON Z.id_zona = C.id_zona
				JOIN acreditados A ON A.id_credito = C.id_credito
				JOIN cuentas_bancarias AS B ON B.id_banco = C.cta_deposito
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito  
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
								FROM   personas) tp ON A.id_persona = tp.id_persona
				LEFT JOIN representantes R ON R.id_credito=C.id_credito
				LEFT JOIN agrupaciones GP ON R.id_grupo=GP.id_grupo
				LEFT JOIN personas PS ON PS.id_persona = A.id_persona
				WHERE {$criterio}
				GROUP BY C.id_credito, C.producto, C.tipo, C.status, C.tasa_interes, C. tasa_mora, C.plazo, C.periodo, C.esDiaSemana, C.esDiaMes,
				C.diaSemana, C.diaMes, C.expediente, C.fecha_captura, C.fecha_aprobacion, C.fecha_entrega, C.cta_deposito, C.id_zona, C.ref_banc,
				C.viejo, Z.nombre_zona, U.nombres, U.apellido_paterno, U.apellido_materno, B.banco, B.no_cuenta,
				B.clabe
				ORDER BY {$orderBy}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	///////////////////////////////////////////////////////////////
	
	function getRepresentanteByCredito($id){
		$values = array(':id'=>$id);
		
		$sql = "SELECT A.*, R.id_persona AS id_representante FROM agrupaciones A
				JOIN representantes R USING(id_grupo) 
				WHERE id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getObligadoSolidarioByCredito($id){
		$values = array(':id'=>$id);
		
		$sql = "SELECT CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) AS nombre, P.* 
				FROM obligados_solidarios O
				JOIN personas P ON P.id_persona=O.id_persona
				WHERE id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	
	function getPagosVencidosByCriterio($criterios){
		$order = "";
		$having = "";
		if(isset($criterios['filtro'])){
			if(isset($criterios['filtro']['parcialidad']))
				$having = "HAVING COUNT(PV.id_credito) {$criterios['filtro']['parcialidad']}";
			elseif(isset($criterios['filtro']['dias']))
				$having = "HAVING SUM(DATEDIFF(PV.fecha_mov, NOW())) {$criterios['filtro']['dias']}";
			elseif(isset($criterios['filtro']['monto']))
				$having = "HAVING SUM(monto) {$criterios['filtro']['monto']}";
		}
		if(isset($criterios['order'])){
			if(isset($criterios['order']['pagos']))
				$order = "ORDER BY pagos {$criterios['order']['pagos']}";
			else
				$order = "ORDER BY dias {$criterios['order']['dias']}";
		}
		$values = array();
		//HAVING COUNT(id_credito) = 4
		$sql = "SELECT PV.id_credito, PV.expediente, PV.nombreGrupo, PV.nombre_zona, SC.cantidad,
				COUNT(PV.id_credito) pagos, DATEDIFF(NOW(), MIN(PV.fecha_mov)) dias,
				SUM(DATEDIFF(NOW(),PV.fecha_mov)) diasacum, 
				SUM(DISTINCT(SC.monto)) monto, SUM(DISTINCT(SC.interes)) interes, 
				SUM(DISTINCT(SC.iva)) iva, SUM(DISTINCT(SC.capital)) capital 
				FROM pagosvencidos PV
				JOIN saldos_creditos SC ON SC.id_credito=PV.id_credito
				GROUP BY PV.id_credito
				{$having}
				{$order}";
		//var_dump($sql);
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getGraficaReporte($sql){
		$values = array();
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	function getPrestadoVSRecuperado(){
		$values = array();
		$sql = "SELECT * FROM prestadorecuperado ORDER BY a�o,mes";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getDesgloseDepositos($data){
		$values = array();
		$criterio =  array();
		
		if(isset($data['fechas']) && !empty($data['fechas'])){
			$criterio[] = "(M.fecha_mov BETWEEN :f1 AND :f2)";
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['bancos']) && $data['bancos']!=''){
			$criterio[] = "B.id_banco IN ({$data['bancos']})";
		}
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT M.fecha_mov, D.monto, -SUM(M.interes) interes, -SUM(M.iva) iva,
				-SUM(M.capital) capital, B.banco, COALESCE(tg.nombre, tp.NombComp) nombreGrupo, 
				D.clave_deposito, C.expediente 
				FROM movimientos M
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN creditos C ON C.id_credito=A.id_credito
				JOIN abonos AB ON AB.id_movimiento=M.id_movimiento
				JOIN depositos_bancarios D ON D.id_deposito=AB.id_deposito
				JOIN cuentas_bancarias B ON B.id_banco=D.id_banco
				LEFT OUTER JOIN (SELECT R.id_credito, AG.nombre FROM representantes R JOIN agrupaciones AG USING (id_grupo)) tg ON tg.id_credito = C.id_credito
				LEFT OUTER JOIN (SELECT id_persona, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS NombComp 
				FROM   personas) tp ON A.id_persona = tp.id_persona
				WHERE {$criterio}
				GROUP BY D.id_deposito
				ORDER BY B.id_banco, M.fecha_mov";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getRecuperadoPorPersona($data){
		$values = array();
		$criterio =  array();
		$having = '';
		$groupby = 'M.id_acreditado, D.id_deposito';
	
		if(isset($data['fechas']) && !empty($data['fechas'])){
			$criterio[] = "(M.fecha_mov BETWEEN :f1 AND :f2)";
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		if(isset($data['bancos']) && $data['bancos']!=''){
			$criterio[] = "B.id_banco IN ({$data['bancos']})";
		}
		if(isset($data['deposito']) && $data['deposito']!=''){
			$having = "HAVING -SUM(M.monto) > {$data['deposito']}";
		}
		if(isset($data['groupby']) && $data['groupby'] != ''){
			$groupby = $data['groupby'];
		}
		$criterio = empty($criterio) ? '1=1':implode(' AND ',$criterio);
	
		$sql = "SELECT M.fecha_mov, -SUM(M.monto) monto, -SUM(M.interes) interes, -SUM(M.iva) iva,
				-SUM(M.capital) capital, B.banco, 
				COALESCE(tg.nombre, COALESCE(tg.nombre, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno))) nombreGrupo,
				CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) nombre, C.expediente, C.id_credito
				FROM movimientos M 
				JOIN acreditados A ON A.id_acreditado=M.id_acreditado
				JOIN personas P ON P.id_persona=A.id_persona
				JOIN creditos C ON C.id_credito=A.id_credito
				JOIN abonos AB ON AB.id_movimiento=M.id_movimiento
				JOIN depositos_bancarios D ON D.id_deposito=AB.id_deposito
				JOIN cuentas_bancarias B ON B.id_banco=D.id_banco
				LEFT OUTER JOIN grupos_view tg ON tg.id_credito=C.id_credito
				WHERE {$criterio}
				GROUP BY {$groupby}
				{$having}
				ORDER BY B.id_banco, M.fecha_mov, C.id_credito";
				
		if($this->getDataByQuery($sql, $values))
		return $this->arrayResult;
	}
	
	function getPrestadoAcreditados($data,$orderBy=""){
		$values = array();
		$criterio =  array();
		
		if(isset($data['fechas']) && !empty($data['fechas'])){
			$criterio[] = "(C.fecha_entrega BETWEEN :f1 AND :f2)";
			$values[':f1'] = $data['fechas'][0];
			$values[':f2'] = $data['fechas'][1];
		}
		
		if(isset($orderBy['nombre']))
			$orderBy = "CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno)	";
		elseif(isset($orderBy['expediente'])){
			$criterio[] = "LOCATE('-',expediente) > 0";
			$orderBy = "LEFT(expediente,LOCATE('-', expediente)),CAST(SUBSTRING(expediente FROM LOCATE('-', expediente)+1) AS SIGNED)";
		}
		else
			$orderBy = 'C.fecha_entrega';
		$criterio = implode(' AND ',$criterio);
		
		$sql = "SELECT C.expediente, CONCAT(P.nombres,' ',P.apellido_paterno,' ',P.apellido_materno) nombre,
				A.cantidad, C.fecha_entrega, Z.nombre_zona
				FROM acreditados A
				JOIN creditos C ON C.id_credito=A.id_credito
				JOIN personas P ON A.id_persona=P.id_persona
				LEFT JOIN zonas Z ON Z.id_zona=C.id_zona
				WHERE {$criterio}
				ORDER BY {$orderBy}";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
		
	}
	
	///////////////////////////////////////////////////////////////////////
	
	function existsGrupo($data){
		$values = array(':n'=>$data['grupo']);
		$sql = "SELECT * FROM agrupaciones WHERE nombre = :n";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function existsVendedorCredito($idCred){
		$values = array(':id'=>$idCred);
		$sql = "SELECT * FROM vendedores WHERE id_credito = :id";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
}
?>
