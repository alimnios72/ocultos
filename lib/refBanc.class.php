<?php
class RefBancaria{
	
	public $referencia;
	private $expediente;
	private $credito;
	private $monto;
	private $periodo = array('s'=>'Semanal','q'=>'Quincenal','m'=>'Mensual','b'=>'Bimestral','t'=>'Trimestral','u'=>'Quinquenal','e'=>'Semestral','a'=>'Anual');
		
	function __construct($credito, $acreditados) {
		$pFijo = $this->getPagoFijo($credito, $acreditados);
		$this->expediente = $credito['expediente'];
		$this->credito = $credito['id_credito'];
		$this->monto = $pFijo;
		$this->addMask();
		$this->referencia = "{$this->expediente}{$this->credito}{$this->monto}";
		$this->referencia = $this->referencia.modulo97::calcular($this->referencia, $pFijo);
	}
	
	private function addMask(){
		$mask_exp = '00000';
		$mask_cred = '00000';
		$mask_monto = '00000';
		
		$this->expediente = preg_replace("/[^a-zA-Z0-9]/", '', $this->expediente);
		if(strlen($this->expediente) > strlen($mask_exp))
			$this->expediente = $mask_exp;
		else 
			$this->expediente = str_pad($this->expediente, strlen($mask_exp), "0", STR_PAD_LEFT);
		if(strlen($this->credito) > strlen($mask_cred))
			$this->credito = $mask_cred;
		else
			$this->credito = str_pad($this->credito, strlen($mask_cred), "0", STR_PAD_LEFT);
		
		if(strlen($this->monto) > strlen($mask_monto))
			$this->monto = $mask_monto;
		else
			$this->monto = str_pad($this->monto, strlen($mask_monto), "0", STR_PAD_LEFT);
	}
	
	private function getPagoFijo($credito, $acreditados){
		$config['tasa'] = $credito['tasa_interes'];
		$config['fecha'] = $credito['fecha_entrega'];
		$config['periodo'] = array_search($credito['periodo'], $this->periodo);
		$config['plazo'] = $credito['plazo'];
		$pagoFijo = 0;
		if(!empty($acreditados)){
			foreach($acreditados as $acreditado){
				$config['montoTotal'] = $acreditado['cantidad'];
				$am = new Amortizacion($config);
				$am->getTabla();
				$pagoFijo += $am->pagoFijo;
			}
		}
		return $pagoFijo;
	}
}

//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo
class modulo97 {
	static public $tabla = array(
			'A'=>2, 'B'=>2, 'C'=>2,
			'D'=>3, 'E'=>3, 'F'=>3,
			'G'=>4, 'H'=>4, 'I'=>4,
			'J'=>5, 'K'=>5, 'L'=>5,
			'M'=>6, 'N'=>6, 'O'=>6,
			'P'=>7, 'Q'=>7, 'R'=>7,
			'S'=>8, 'T'=>8, 'U'=>8,
			'V'=>9, 'W'=>9, 'X'=>9,
			'Y'=>0, 'Z'=>0
	);

	static function calcular($ref, $pFijo) {
		//Sustituyo caracteres por valor num�rico
		$ref = strtr(strtoupper($ref), self::$tabla);
		//Calculo el digito verificador del monto
		$dM = self::digitoMonto($pFijo);
		$ref .= $dM;
		//Calculo el digito verificador por cierto peso
		$ref = strrev($ref);
		$peso = array(11,13,17,19,23);
		$j = 0; $sum = 0;
		for ($i=0; $i<strlen($ref); $i++){
			$sum += $ref[$i]*$peso[$j];
			if($j+1 == count($peso))
				$j = 0;
			else
				$j++;
		}
		$mod97 = sprintf("%02s", ($sum%97) + 1);
		return "{$dM}{$mod97}";
	}
	
	static function digitoMonto($monto){
		//Agrego decimales
		$monto .= '00';
		//Volteo la cadena de derecha a izquierda
		$monto = strrev($monto);
		//Establezco factor de peso
		$peso = array(7,3,1);
		$j = 0; $sum = 0;
		for ($i=0; $i<strlen($monto); $i++){
			$sum += $monto[$i]*$peso[$j];
			if($j+1 == count($peso))
				$j = 0;
			else
				$j++;
		}
		return $sum%10;
	}
}

//$this->referencia = $this->referencia.Verhoeff::calc(preg_replace("/[a-zA-Z]/e", "ord('\\0')", $this->referencia));
//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo//--oo
