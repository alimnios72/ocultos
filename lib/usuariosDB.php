<?php
require_once('DBClass.php');
class Usuario extends DB{
	function getVendedores(){
		$values = array(':t'=>'ventas');
		$sql = "SELECT id, CONCAT(nombres,' ',apellido_paterno,' ',apellido_materno) AS nombre FROM administradores	
				WHERE privilegios=:t";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAdministradoresByNombre($data){
		$values = array(':x'=>'%'.$data['term'].'%');
    	$sql = "SELECT TOP 10 id, CONCAT(nombres ,' ', apellido_paterno ,' ', apellido_materno) AS value
    			FROM administradores WHERE CONCAT(nombres ,' ', apellido_paterno ,' ', apellido_materno) LIKE :x
    			OR nombres LIKE :x OR apellido_paterno LIKE :x OR apellido_materno LIKE :x";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function getAdministradorByID($userID){
		$values = array(':id'=>$userID);
		$sql = "SELECT * FROM administradores WHERE id=:id LIMIT 1";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult[0];
	}
	
	function getSuperUsers(){
		$values = array(':id'=>$userID);
		$sql = "SELECT * FROM administradores WHERE privilegios='admin'";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
	
	function updateAdministrador($data){
		$values = array(':id'=>array('data'=>$data['id'],'type'=>'i'),
						':n'=>array('data'=>$data['nombres'],'type'=>'s'),
						':ap'=>array('data'=>$data['apellido_paterno'],'type'=>'s'),
						':am'=>array('data'=>$data['apellido_materno'],'type'=>'s'),
						':f'=>array('data'=>$data['fecha_nacimiento'],'type'=>'s'),
		);
		$sql = "UPDATE administradores SET nombres=:n, apellido_paterno=:ap,
				apellido_materno=:am, fecha_nacimiento=:f
				WHERE id=:id";
		if($this->setDataByQuery($sql, $values))
			return true;
		return false;
	}
	
	function updateAdministradorPass($id,$password){
		$values = array(':id'=>array('data'=>$id,'type'=>'i'),
						':p'=>array('data'=>$password,'type'=>'s')
		);
		$sql = "UPDATE administradores SET password=MD5(:p)
				WHERE id=:id";
		if($this->setDataByQuery($sql, $values))
			return true;
		return false;
	}
}
?>