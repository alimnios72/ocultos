<?php
include_once('class.ezpdf.php');

class CustomPDF extends Cezpdf{
    
    function addSignatures($firmas, $xStart, $yStart, $xEnd, $yEnd, &$pages){
        $xGap = 300;
        $yGap = 90;
        $yText = 15;
        $width = 200;
        $fSize = 10;
        $dx = $xEnd - $xStart;
        $dy = $yText + $fSize;
        
        $x = $xStart;
        $y = $yStart;
      
        foreach($firmas as $key => $firma){
            $this->signatureObject($x, $y, $yText, $width, $fSize, $key, $firma);
            $x += $xGap;
            if($x > $dx){
                $y -= $yGap;
                $x = $xStart;
                if($y - $dy < $yEnd){
                    $this->ezNewPage();
                    $pages++;
                    $x = 50;
                    $y = 670;
                    $xStart = $x;
                    $xEnd = 620;
                    $dx = $xEnd - $xStart;
                }
            }
        }
    }
    private function signatureObject($x, $y, $yText, $w, $font, $key, $text){
        $this->setColor(0, 0, 0);
        $this->setStrokeColor(0, 0 ,0);
        $this->setLineStyle(1);
        $tmp = $this->openObject();
        $this->line($x,$y,$x+$w,$y);
        $truncated = $this->addTextWrap($x,$y-$yText,$w,$font, $text, "center");
        if(!empty($truncated))
            $this->addTextWrap($x,$y-($yText*2),$w,$font, $truncated, "center");
        if($key==="cbi")
            $this->addTextWrap($x,$y-$yText-$font,$w,$font, "CREA BIENESTAR SA DE CV SOFOM ENR", "center");
        $this->closeObject();
        $this->addObject($tmp, "add");
    }
}
?>