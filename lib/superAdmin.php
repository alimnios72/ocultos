<?php
require_once('DBClass.php');
class SuperAdmin extends DB{
	
	function getActiveAdmins(){
		$values = array();
		$sql = "SELECT *,
				IF(ultima_actualizacion > (NOW()-INTERVAL 5 MINUTE),'Activo',
				IF(ultima_actualizacion > (NOW()-INTERVAL 30 MINUTE),'Ausente','Inactivo')) as Status
				FROM administradores
				ORDER BY ultima_actualizacion DESC";
		if($this->getDataByQuery($sql, $values))
			return $this->arrayResult;
	}
}
