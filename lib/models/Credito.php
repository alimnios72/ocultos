<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 1/28/16
 * Time: 12:10 AM
 */

namespace CBI\DB\Models;

class Credito
{
    protected $id;

    protected $producto;

    protected $tipo;

    protected $status;

    protected $tasaInteres;

    protected $tasaMora;

    protected $plazo;

    protected $periodo;

    protected $esDiaSemana;

    protected $esDiaMes;

    protected $diaSemana;

    protected $diaMes;

    protected $expediente;

    protected $fechaCaptura;
}