<?php
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
$configDB['dsn_old']='dblib:host=172.16.1.1:2301\SQLExpress;dbname=SACREDI_OLD';
$configDB['dsn_new']='dblib:host=172.16.1.1:2301\SQLExpress;dbname=SACREDI';
$configDB['user']='sa';
$configDB['password']='Cbi1007282ga';
require_once('DBClass.php');
$old = new DB(false, $configDB['dsn_old'], $configDB['user'], $configDB['password']);
$new = new DB(false, $configDB['dsn_new'], $configDB['user'], $configDB['password']);
////////////////////////////////////////////////////////////////////////////////////
//Primero obtengo los campos que voy a tomar de la BD vieja
////////////////////////////////////////////////////////////////////////////////////
$values = array();
$sql = "SELECT C.*, BC.id_banco
		FROM creditos C
		JOIN bancos_credito BC ON BC.id_credito =  C.id_credito
		WHERE C.tipo = 1";
if($old->getDataByQuery($sql, $values))
	$creditos = $old->arrayResult;
foreach ($creditos as &$credito){
	$values = array();
	$sql = "SELECT G.*
			FROM grupos G
			JOIN agrupaciones AG ON AG.id_grupo = G.id_grupo
			JOIN acreditados A ON A.id_acreditado = AG.id_acreditado
			JOIN creditos C ON C.id_credito = A.id_credito
			WHERE C.id_credito = {$credito['id_credito']}
			GROUP BY G.id_grupo, G.nombre, G.id_representante";
	if($old->getDataByQuery($sql, $values))
	$credito['grupo'] = $old->arrayResult[0];
	////////////////////////////////////////////////////////
	$sql = "SELECT P.*, A.id_acreditado, A.cantidad
			FROM personas P
			JOIN acreditados A ON A.id_persona = P.id_persona
			JOIN creditos C ON C.id_credito = A.id_credito
			WHERE C.id_credito = {$credito['id_credito']}";
	if($old->getDataByQuery($sql, $values))
		$credito['acreditados'] = $old->arrayResult;
	////////////////////////////////////////////////////////
	foreach ($credito['acreditados'] as $key=>$acreditado){
		if($credito['grupo']['id_representante'] == $acreditado['id_persona'])
			$credito['acreditados'][$key]['repre'] = true;
		$sql = "SELECT M.*
				FROM movimientos M
				JOIN acreditados A ON A.id_acreditado = M.id_acreditado
				WHERE A.id_acreditado = {$acreditado['id_acreditado']}";
		if($old->getDataByQuery($sql, $values))
			$credito['acreditados'][$key]['tabla'] = $old->arrayResult;
	}
}
////////////////////////////////////////////////////////////////////////////////////
//Ahora inserto los resultados obtenidos en la nueva BD
///////////////////////////////////////////////////////////////////////////////////
$tmp = $creditos;
unset($creditos);
foreach ($tmp as $val){
	$values = array(':p'=>array('data'=>$val['producto'],'type'=>'i'),
			':t'=>array('data'=>$val['tipo'],'type'=>'i'),
			':s'=>array('data'=>$val['status'],'type'=>'i'),
			':ta'=>array('data'=>$val['tasa_interes'],'type'=>'s'),
			':pl'=>array('data'=>$val['plazo'],'type'=>'i'),
			':pe'=>array('data'=>$val['periodo'],'type'=>'s'),
			':eD'=>array('data'=>$val['esDiaSemana'],'type'=>'i'),
			':eM'=>array('data'=>$val['esDiaMes'],'type'=>'i'),
			':dS'=>array('data'=>$val['diaSemana'],'type'=>'i'),
			':dM'=>array('data'=>$val['diaMes'],'type'=>'i'),
			':e'=>array('data'=>$val['expediente'],'type'=>'s'),
			':fC'=>array('data'=>$val['fecha_captura'],'type'=>'s'),
			':fA'=>array('data'=>$val['fecha_aprobacion'],'type'=>'s'),
			':fE'=>array('data'=>$val['fecha_entrega'],'type'=>'s'),
			':d'=>array('data'=>$val['id_banco'],'type'=>'i'),
	);
	$sql = "INSERT INTO creditos (producto, tipo, status, tasa_interes, plazo, periodo, esDiaSemana, esDiaMes,
			diaSemana, diaMes, expediente, fecha_captura, fecha_aprobacion, fecha_entrega, cta_deposito, id_zona)
			VALUES (:p, :t, :s, :ta, :pl, :pe, :eD, :eM, :dS, :dM, :e, :fC, :fA, :fE, :d, 5)";
	if($new->setDataByQuery($sql, $values))
		$idC = $new->lastId;
	////////////////////////////////////////////////////////////////////////////////////////////////////
	foreach ($val['acreditados'] as $acreditado){
		$per = $acreditado;
		$values = array(':n'=>array('data'=>trim(strtoupper($per['nombres'])),'type'=>'s'),
				':aP'=>array('data'=>trim(strtoupper($per['apellido_paterno'])),'type'=>'s'),
				':aM'=>array('data'=>trim(strtoupper($per['apellido_materno'])),'type'=>'s'),
				':f'=>array('data'=>$per['fecha_nacimiento'],'type'=>'s'),
				':s'=>array('data'=>$per['sexo'],'type'=>'s'),
				':rfc'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
				':curp'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
				':eC'=>array('data'=>$per['estado_civil'],'type'=>'s'),
				':c'=>array('data'=>trim(strtoupper($per['calle'])),'type'=>'s'),
				':co'=>array('data'=>trim(strtoupper($per['colonia'])),'type'=>'s'),
				':cp'=>array('data'=>trim($per['codigo_postal']),'type'=>'s'),
				':d'=>array('data'=>$per['id_delegacion'],'type'=>'i'),
				':t'=>array('data'=>trim($per['telefonos']),'type'=>'s'),
				':ce'=>array('data'=>trim($per['celular']),'type'=>'s'),
				':e'=>array('data'=>trim($per['email']),'type'=>'s'),
		);
		$sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, sexo, fecha_nacimiento, rfc, curp, estado_civil, t_conyuge,
				dep_econom, calle, colonia, codigo_postal, id_delegacion, telefonos, celular, email)
				VALUES (:n, :aP, :aM, :s, :f, :rfc, :curp, :eC, 0, 0, :c, :co, :cp, :d, :t, :ce, :e)";
		if($new->setDataByQuery($sql, $values))
			$idP = $new->lastId;
		if($acreditado['repre'])
			$idR = $idP;
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$values = array(':c'=>array('data'=>$idC,'type'=>'i'),
				':p'=>array('data'=>$idP,'type'=>'i'),
				':m'=>array('data'=>$per['cantidad'],'type'=>'s'),
		);
		$sql = "INSERT INTO acreditados (id_credito, id_persona, cantidad)
				VALUES (:c, :p, :m)";
		if($new->setDataByQuery($sql, $values))
			$idA = $new->lastId;
		////////////////////////////////////////////////////////////////////////////////////////////////////
		foreach ($per['tabla'] as $pago){
			$values = array(':a'=>array('data'=>$idA,'type'=>'i'),
					':t'=>array('data'=>$pago['tipo_mov'],'type'=>'i'),
					':m'=>array('data'=>$pago['monto'],'type'=>'s'),
					':i'=>array('data'=>$pago['interes'],'type'=>'s'),
					':iv'=>array('data'=>$pago['iva'],'type'=>'s'),
					':c'=>array('data'=>$pago['capital'],'type'=>'s'),
					':f'=>array('data'=>$pago['fecha_mov'],'type'=>'s'),
			);
			$sql = "INSERT INTO movimientos (id_acreditado, tipo_mov, monto, interes, iva, capital, fecha_mov)
					VALUES (:a, :t, :m, :i, :iv, :c, :f)";
			if($new->setDataByQuery($sql, $values))
				$idM = $new->lastId;
			//////////////////////////////////////////////////////////////////
			$values = array(':id'=>array('data'=>$idM,'type'=>'i'),
			);
			$sql = "INSERT INTO cargos (id_movimiento, tipo_cargo, status)
					VALUES (:id, 0, 0)";
			$new->setDataByQuery($sql, $values);
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$values = array(':n'=>array('data'=>trim(strtoupper($credito['grupo']['nombre'])),'type'=>'s'),
					':r'=>array('data'=>$idR,'type'=>'i'),
					':c'=>array('data'=>$idC,'type'=>'i'),
			
	);
	$sql = "INSERT INTO grupos (nombre, id_representante, id_credito)
			VALUES (:n, :r, :c)";
	$new->setDataByQuery($sql, $values);
}


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//Primero obtengo los campos que voy a tomar de la BD vieja
////////////////////////////////////////////////////////////////////////////////////
$values = array();
$sql = "SELECT C.*, BC.id_banco
		FROM creditos C
		JOIN bancos_credito BC ON BC.id_credito =  C.id_credito
		WHERE C.tipo = 0";
if($old->getDataByQuery($sql, $values))
	$creditos = $old->arrayResult;
foreach ($creditos as &$credito){
	$values = array();
	$sql = "SELECT P.*, A.id_acreditado, A.cantidad
			FROM personas P
			JOIN acreditados A ON A.id_persona = P.id_persona
			JOIN creditos C ON C.id_credito = A.id_credito
			WHERE C.id_credito = {$credito['id_credito']}";
	if($old->getDataByQuery($sql, $values))
		$credito['acreditado'] = $old->arrayResult[0];
	////////////////////////////////////////////////////////
	$sql = "SELECT M.*
			FROM movimientos M
			JOIN acreditados A ON A.id_acreditado = M.id_acreditado
			WHERE A.id_acreditado = {$credito['acreditado']['id_acreditado']}";
	if($old->getDataByQuery($sql, $values))
		$credito['acreditado']['tabla'] = $old->arrayResult;
	////////////////////////////////////////////////////////
	$sql = "SELECT P.*
			FROM personas P
			JOIN obligados_solidarios O ON O.id_persona = P.id_persona
			JOIN acreditados A ON A.id_acreditado = O.id_acreditado
			JOIN creditos C ON C.id_credito = A.id_credito
			WHERE C.id_credito = {$credito['id_credito']}";
	if($old->getDataByQuery($sql, $values))
		$credito['solidario'] = $old->arrayResult[0];
}
////////////////////////////////////////////////////////////////////////////////////
//Ahora inserto los resultados obtenidos en la nueva BD
///////////////////////////////////////////////////////////////////////////////////
$tmp = $creditos;
unset($creditos);
foreach ($tmp as $val){
	$values = array(':p'=>array('data'=>$val['producto'],'type'=>'i'),
					':t'=>array('data'=>$val['tipo'],'type'=>'i'),
					':s'=>array('data'=>$val['status'],'type'=>'i'),
					':ta'=>array('data'=>$val['tasa_interes'],'type'=>'s'),
					':pl'=>array('data'=>$val['plazo'],'type'=>'i'),
					':pe'=>array('data'=>$val['periodo'],'type'=>'s'),
					':eD'=>array('data'=>$val['esDiaSemana'],'type'=>'i'),
					':eM'=>array('data'=>$val['esDiaMes'],'type'=>'i'),
					':dS'=>array('data'=>$val['diaSemana'],'type'=>'i'),
					':dM'=>array('data'=>$val['diaMes'],'type'=>'i'),
					':e'=>array('data'=>$val['expediente'],'type'=>'s'),
					':fC'=>array('data'=>$val['fecha_captura'],'type'=>'s'),
					':fA'=>array('data'=>$val['fecha_aprobacion'],'type'=>'s'),
					':fE'=>array('data'=>$val['fecha_entrega'],'type'=>'s'),
					':d'=>array('data'=>$val['id_banco'],'type'=>'i'),
	);
	$sql = "INSERT INTO creditos (producto, tipo, status, tasa_interes, plazo, periodo, esDiaSemana, esDiaMes, 
			diaSemana, diaMes, expediente, fecha_captura, fecha_aprobacion, fecha_entrega, cta_deposito, id_zona)
			VALUES (:p, :t, :s, :ta, :pl, :pe, :eD, :eM, :dS, :dM, :e, :fC, :fA, :fE, :d, 5)";
	if($new->setDataByQuery($sql, $values))
		$idC = $new->lastId;
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$per = $val['acreditado'];
	$values = array(':n'=>array('data'=>trim(strtoupper($per['nombres'])),'type'=>'s'),
					':aP'=>array('data'=>trim(strtoupper($per['apellido_paterno'])),'type'=>'s'),
			  		':aM'=>array('data'=>trim(strtoupper($per['apellido_materno'])),'type'=>'s'),
					':f'=>array('data'=>$per['fecha_nacimiento'],'type'=>'s'),
					':s'=>array('data'=>$per['sexo'],'type'=>'s'),
					':rfc'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
					':curp'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
					':eC'=>array('data'=>$per['estado_civil'],'type'=>'s'),
					':c'=>array('data'=>trim(strtoupper($per['calle'])),'type'=>'s'),
					':co'=>array('data'=>trim(strtoupper($per['colonia'])),'type'=>'s'),
					':cp'=>array('data'=>trim($per['codigo_postal']),'type'=>'s'),
					':d'=>array('data'=>$per['id_delegacion'],'type'=>'i'),
					':t'=>array('data'=>trim($per['telefonos']),'type'=>'s'),
					':ce'=>array('data'=>trim($per['celular']),'type'=>'s'),
					':e'=>array('data'=>trim($per['email']),'type'=>'s'),
			);
	$sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, sexo, fecha_nacimiento, rfc, curp, estado_civil, t_conyuge,
			dep_econom, calle, colonia, codigo_postal, id_delegacion, telefonos, celular, email)
			VALUES (:n, :aP, :aM, :s, :f, :rfc, :curp, :eC, 0, 0, :c, :co, :cp, :d, :t, :ce, :e)";
	if($new->setDataByQuery($sql, $values))
		$idP = $new->lastId;
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$values = array(':c'=>array('data'=>$idC,'type'=>'i'),
					':p'=>array('data'=>$idP,'type'=>'i'),
					':m'=>array('data'=>$per['cantidad'],'type'=>'s'),
			);
	$sql = "INSERT INTO acreditados (id_credito, id_persona, cantidad)
			VALUES (:c, :p, :m)";
	if($new->setDataByQuery($sql, $values))
		$idA = $new->lastId;
	////////////////////////////////////////////////////////////////////////////////////////////////////
	foreach ($per['tabla'] as $pago){
		$values = array(':a'=>array('data'=>$idA,'type'=>'i'),
						':t'=>array('data'=>$pago['tipo_mov'],'type'=>'i'),
						':m'=>array('data'=>$pago['monto'],'type'=>'s'),
						':i'=>array('data'=>$pago['interes'],'type'=>'s'),
						':iv'=>array('data'=>$pago['iva'],'type'=>'s'),
						':c'=>array('data'=>$pago['capital'],'type'=>'s'),
						':f'=>array('data'=>$pago['fecha_mov'],'type'=>'s'),
					);
		$sql = "INSERT INTO movimientos (id_acreditado, tipo_mov, monto, interes, iva, capital, fecha_mov)
				VALUES (:a, :t, :m, :i, :iv, :c, :f)";
		if($new->setDataByQuery($sql, $values))
			$idM = $new->lastId;
		//////////////////////////////////////////////////////////////////
		$values = array(':id'=>array('data'=>$idM,'type'=>'i'),
		);
		$sql = "INSERT INTO cargos (id_movimiento, tipo_cargo, status)
				VALUES (:id, 0, 0)";
		$new->setDataByQuery($sql, $values);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	unset($per);
	unset($idP);
	$per = $val['solidario'];
	$values = array(':n'=>array('data'=>trim(strtoupper($per['nombres'])),'type'=>'s'),
			':aP'=>array('data'=>trim(strtoupper($per['apellido_paterno'])),'type'=>'s'),
			':aM'=>array('data'=>trim(strtoupper($per['apellido_materno'])),'type'=>'s'),
			':f'=>array('data'=>$per['fecha_nacimiento'],'type'=>'s'),
			':s'=>array('data'=>$per['sexo'],'type'=>'s'),
			':rfc'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
			':curp'=>array('data'=>$per['rfc']==NULL ? '': $per['rfc'],'type'=>'s'),
			':eC'=>array('data'=>$per['estado_civil'],'type'=>'s'),
			':c'=>array('data'=>trim(strtoupper($per['calle'])),'type'=>'s'),
			':co'=>array('data'=>trim(strtoupper($per['colonia'])),'type'=>'s'),
			':cp'=>array('data'=>trim($per['codigo_postal']),'type'=>'s'),
			':d'=>array('data'=>$per['id_delegacion'],'type'=>'i'),
			':t'=>array('data'=>trim($per['telefonos']),'type'=>'s'),
			':ce'=>array('data'=>trim($per['celular']),'type'=>'s'),
			':e'=>array('data'=>trim($per['email']),'type'=>'s'),
	);
	$sql = "INSERT INTO personas (nombres, apellido_paterno, apellido_materno, sexo, fecha_nacimiento, rfc, curp, estado_civil, t_conyuge,
			dep_econom, calle, colonia, codigo_postal, id_delegacion, telefonos, celular, email)
			VALUES (:n, :aP, :aM, :s, :f, :rfc, :curp, :eC, 0, 0, :c, :co, :cp, :d, :t, :ce, :e)";
	if($new->setDataByQuery($sql, $values))
		$idP = $new->lastId;
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	$values = array(':c'=>array('data'=>$idC,'type'=>'i'),
					':p'=>array('data'=>$idP,'type'=>'i'),
	);
	$sql = "INSERT INTO obligados_solidarios (id_credito, id_persona)
			VALUES (:c, :p)";
	$new->setDataByQuery($sql, $values);
}
echo "Lo logr�!!!!!!";
//var_dump($tmp);

?>