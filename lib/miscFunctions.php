<?php
//Incluye css necesarios para la p�gina
function includeCSS($ruta,$arreglo){
    $css="";
    if(is_array($arreglo) && !empty($arreglo)){
        foreach($arreglo as $val)
        {
            if($val == "main-ie6.css")
                $css.='<!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="'.$ruta.$val.'" /><![endif]-->'.PHP_EOL;
            elseif(strpos ($val,"col") > 0)
                $css.='<link rel="stylesheet" media="screen,projection" type="text/css" href="'.$ruta.$val.'" title="'.current(explode(".",$val)).'"/>'.PHP_EOL;
            else
                $css.='<link rel="stylesheet" media="screen,projection" type="text/css" href="'.$ruta.$val.'" />'.PHP_EOL;
        }
    }
    return $css;
    
}
//Incluye js necesarios para la p�gina
function includeJS($ruta,$arreglo){
    $js="";
    if(is_array($arreglo) && !empty($arreglo)){
        foreach($arreglo as $val)
            $js.='<script type="text/javascript" src="'.$ruta.$val.'"> </script>'.PHP_EOL;
    }
    return $js;
}
//Agrega d�as a la fecha inicial 
function addDays($date_str, $days){
    $date = new DateTime($date_str);
    $date->modify("+{$days} days");
    return $date->format('Y-m-d');
}
//Substrae d�as a la fecha inicial
function subDays($date_str, $days){
	$date = new DateTime($date_str);
	$date->modify("-{$days} days");
	return $date->format('Y-m-d');
}
//Substrae meses a la fecha inicial considerando que hay meses con menor n�mero de d�as
function subMonths($date_str, $months){
	$date = new DateTime($date_str);
	$start_day = $date->format('j');

	$date->modify("-{$months} month");
	$end_day = $date->format('j');

	if ($start_day != $end_day)
		$date->modify('last day of last month');
	return $date->format('Y-m-d');
}
//Agrega meses a la fecha inicial considerando que hay meses con menor n�mero de d�as
function addMonths($date_str, $months){
    $date = new DateTime($date_str);
    $start_day = $date->format('j');
    
    $date->modify("+{$months} month");
    $end_day = $date->format('j');
    
    if ($start_day != $end_day)
        $date->modify('last day of last month');
    return $date->format('Y-m-d');
}
//Regresa el mes inicial a partir del cual se va a contar
function getStartingMonth($date_str, $dayofMonth){
    $date = new DateTime($date_str);
    $start_day = $date->format('j');
    $diff = $start_day - $dayofMonth;
    
    if($diff <= 0){
        $format = $date->format('Y')."-".$date->format('m')."-".$dayofMonth;
        $date = DateTime::createFromFormat('Y-m-j', $format);
    }
    else{
        $format = $date->format('Y')."-".$date->format('m')."-".$dayofMonth;
        $date = DateTime::createFromFormat('Y-m-j', $format);
        $date->modify("+1 month");
    }
    return $date->format('Y-m-d');
}
//Calcula la Tasa Interna de Retorno
function getTIR($values){
    $math = new Math_Finance();
    return $math->internalRateOfReturn($values);
}
//Devuelve el n�mero buscado en letra
function numberToText($number, $currency=false){
    $peso = "PESO";
    $pesos = "PESOS";
    $min = 0;
    $max = 999999;
    
    if($number >= $min && $number <= $max){
        if($currency){
            $last = $peso;
            if($number >= 2)
                $last = $pesos;
            $result = recurseNumber($number);
            $cents = round($number - intval($number),2)*100;
            if($cents < 10)
                $result = $result." ".$last." 0".$cents."/100 M.N.";
            else
                $result = $result." ".$last." ".$cents."/100 M.N.";
        }   
        else
            $result = recurseNumber($number);
    }
    return $result;
}
//Funci�n que busca un valor en un arreglo asociativo
function searchAssocArray($array, $value, $searchKey, $returnKey){
	foreach ($array as $v)
	{
		if($v[$searchKey] == $value)
			return $v[$returnKey];
	}
}
//Funci�n que regresa la edad de la persona a partir de una fecha de nacimiento
function getEdad($fecha){
	list($Year, $Month, $Day) = explode("-",$fecha);
	$YearDiff = date("Y") - $Year;
	if(date("m") < $Month || (date("m") == $Month && date("d") < $Day))
		$YearDiff--;
	return $YearDiff;
}
//Funci�n auxiliar para numberToText, analiza la cantidad recursivamente y regresa la cadena correspondiente
function recurseNumber($number){
    $digits = array("CERO", "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "DIEZ",
                    "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECIS�IS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
                    "VEINTE", "VEINTI�N", "VEINTID�S", "VEINTITR�S", "VEINTICUATRO", "VEINTICINCO", "VEINTIS�IS",
                    "VEINTISIETE", "VEINTIOCHO", "VEINTINUEVE");
    $tens = array("CERO", "DIEZ", "VEINTE", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA", "CIEN");
    $hundreds = array("CERO", "CIENTO", "DOSCIENTOS", "TRESCIENTOS", "CUATROCIENTOS", "QUINIENTOS", "SEISCIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS");
    
    if($number > 0 && $number < 30)
        $result = $digits[$number];
    elseif ($number >= 30 && $number <= 100){
        $comp = $number % 10!= 0?" Y ".recurseNumber($number % 10): "";
        $result = $tens[$number/10].$comp;
    }
    elseif ($number > 100 && $number < 1000){
        $comp = $number % 100!= 0?" ".recurseNumber($number % 100): "";
        $result = $hundreds[$number/100].$comp;
    }
     elseif ($number >= 1000 && $number < 1000000){
        $comp = $number % 1000!= 0?" ".recurseNumber($number % 1000): "";
        $result = recurseNumber($number/1000)." MIL ".$comp;
    }
    return $result;
}
//Funci�n para guardar el archivo pdf con el nombre
 function saveFile($pdfCode, $name){
    $path = ROOT.'tmp/';
    $name .= '.pdf';
    $handler = @fopen($path.$name,'wb');
    fwrite($handler,$pdfCode);
    fclose($handler);
    return false;
}
//Funci�n para abrir un documento PDF en la ventana
 function openFile($pathName){
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename='.basename($pathName));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($pathName));
    ob_clean();
    flush();
    readfile($pathName);
    exit;
}
//Muestra en un combo los elementos de un arreglo
function getComboBox($array, $name, $id, $config=array('value'=>'Id','text'=>'nombre'), $selected="", $extra=""){
    echo "<select name='{$name}' id='{$id}' {$extra}>";
    foreach($array as $val){
    	if($selected != "" && $selected == $val[$config['value']])
    		echo "<option value='{$val[$config['value']]}' selected='selected'>{$val[$config['text']]}</option>";
    	else
        	echo "<option value='{$val[$config['value']]}'>{$val[$config['text']]}</option>";
    }
    echo "</select>";
}
//Esta funci�n estima un monto preaprobado para un aceditado bas�ndose en la informaci�n econ�mica de su solicitud
function estimaMonto($cred, $acred, $creditoDB){
	$acreditado = $acred['info_personal'];
	$infoEconom = $acred['info_econom'];
	$opcCred = $cred;
	$montoMin = false;
	//Si es hombre
	if($acreditado['sexo'] == 'H')
		$montoMin = true;
	//Si renta
	elseif($acreditado['residencia'] == 'Rentada')
		$montoMin = true;
	//Si no tiene tel�fono fijo/local
	elseif($acreditado['tel_casa'] == '')
		$montoMin = true;
	//Si es madre soltera
	elseif($acreditado['t_conyuge'] == '0' && $acreditado['dep_econom'] > 0)
		$montoMin = true;
	$montoPreaprobado = $montoMin ? 3000: 4000;
	$montoPreaprobado = $infoEconom['monto_sol'] <= $montoPreaprobado ? $infoEconom['monto_sol'] : $montoPreaprobado; 
	
	return $montoPreaprobado;
}
/*function estimaMonto($cred, $acred, $creditoDB){
    $acreditado = $acred['info_personal'];
    $infoEconom = $acred['info_econom'];
    $opcCred = $cred;
    if($infoEconom['ing_div'] == "0")
        $infoEconom['ing_div_monto'] = 0;
    if($infoEconom['pag_cred'] == "0")
        $infoEconom['pag_cred_monto'] = 0;
    if($infoEconom['pen_alim'] == "0")
        $infoEconom['pen_alim_monto'] = 0;
    
    $ingresoGeneral = $infoEconom['ing_mensuales'] + $infoEconom["ing_div_monto"];
    $gastoGeneral = $infoEconom['pag_cred_monto'] + $infoEconom['gastos_fam'] + $infoEconom['pen_alim_monto'] + ($infoEconom['inv_semanal']*4);
    $ingresoNeto = $ingresoGeneral - $gastoGeneral;
    $ingresoNeto = $acreditado['sexo'] == 'H' ? $ingresoNeto * 0.9 : $ingresoNeto;
    $factor = 1;
    $factor = $acreditado['residencia'] == 'Propia' ? $factor + 0.05 : $factor;
    $factor = $acreditado['t_conyuge'] == '1' ? $factor + 0.05 : $factor;
    for($i=0;$i<$acreditado['dep_econom'];$i++)
        $factor = $acreditado['t_conyuge'] == '1' ? $factor - 0.02 : $factor - 0.03;
    $ingresoMenEst = ($ingresoNeto * $factor) * 0.5;
    $montoPreaprobado = ($ingresoMenEst * $opcCred['plazo'])/((($opcCred['tasa']/(100*12))*$opcCred['plazo'])+1);
    $montoRed = round($montoPreaprobado, -3);
    $montoPreaprobado = $montoPreaprobado < $montoRed ? $montoRed - 1000 : $montoRed;
    //Si el monto solicitado es menor al preaprobado entonces aceptamos el monto solicitado
    $montoPreaprobado = $infoEconom['monto_sol'] <= $montoPreaprobado ? $infoEconom['monto_sol'] : $montoPreaprobado;
    //Si el monto preaprobado es mayor a $7,000, lo truncamos a esa cantidad
    $montoPreaprobado = $montoPreaprobado > 7000 ? 7000 : $montoPreaprobado;

    //////////////////////////////////////////////
   	$montoPreaprobado = $infoEconom['monto_sol'];
   	//////////////////////////////////////////////
   	$acred = $creditoDB->getCreditosByAcreditado($acreditado['id_persona'], array(4));
   	if(!empty($acred)){
   		$renovacion = getMontoRenovacion($acred[0]['cantidad']);
   		$montoPreaprobado = $montoPreaprobado < $renovacion ? $montoPreaprobado : $renovacion;
   	}
   
    return $montoPreaprobado;
}*/
//Calcula el monto para la renovaci�n de un cr�dito dependiento del cr�dito anterior
function getMontoRenovacion($ultCred){
	$result = 0;
	if($ultCred < 6000)
		$result = $ultCred * 1.40;
	elseif($ultCred >= 6000 && $ultCred < 8999)
		$result = $ultCred * 1.30;
	elseif($ultCred >= 9000 && $ultCred < 11999)
		$result = $ultCred * 1.20;
	else 
		$result = $ultCred * 1.15;
	return $result;
}

//Esta funci�n ordena las citas de cr�dito por hora, independientemente de la fecha, adicionalmente agrega un campo de duraci�n
function getTimeTable($citas){
	$tmp = "";
	$result = array();
	if(!empty($citas)){
		foreach($citas as $cita){
			$hora = date('H:i', strtotime($cita['inicio']));
			$diff = round(abs(strtotime($cita['fin']) - strtotime($cita['inicio']))/60,2);
			$duration = $diff / 30;
			$result[$hora][] = array('id_credito'=>$cita['id_credito'],
									'tipo'=>$cita['tipo'],
									'nombre'=>$cita['nombre'],
									'fecha'=>date('Y-m-d', strtotime($cita['inicio'])),
									'duracion'=>$duration);
		}
	}
	return  $result;
}

//Calcula los d�as de mora entre dos fechas considerando que los fines de semana y d�as festivos no cuentan en ciertas situaciones
function DiasMora($ini, $fin, $diasInhabiles){
	$dias = 0;
	$tmp = $ini;
	while($tmp != $fin){
		$dayOfWeek = date('N', strtotime($tmp));
		if($dayOfWeek != 6 && $dayOfWeek != 7 && !isDiaInhabil($tmp, $diasInhabiles))
			$dias ++;
		$tmp = addDays($tmp, 1);
	}
	if($dias > 0){
		$ini = new DateTime($ini);
		$fin = new DateTime($fin);
		$interval = $ini->diff($fin);
		$dias = $interval->days;
	}
	return $dias;
}

function isDiaInhabil($dia, $listaDias){
	if(!empty($listaDias)){
		foreach ($listaDias as $d){
			if($d['fecha'] == $dia)
				return  true;
		}
	}
	return false;
}

//Calcula los d�as totales de mora de un cr�dito buscando las amortizaciones vencidas
function MoraTotal($cargos, $fecha, $diasInhabiles){
	//$fecha = date('Y-m-d');
	$moraTotal = 0;
	if(!empty($cargos)){
		foreach($cargos as $cargo){
			if($cargo['status'] == 0 && strtotime($cargo['fecha_mov']) < strtotime($fecha))
				$moraTotal += DiasMora($cargo['fecha_mov'], $fecha, $diasInhabiles);
		}
	}
	return $moraTotal;
}
//Regresa un valor de un par id, nombre en un arreglo bidimensional
function getArrayValue($array, $findKey, $findValue, $returnKey){
	$result = "";
	if(!empty($array)){
		foreach ($array as $val){
			if($val[$findKey] == $findValue)
				$result = $val[$returnKey];
		}
	}
	return $result;
}
//Convierte un arreglo regresado por XML en un arreglo bidimensional 
/*function xmltoarray($xml){
	$tasas = array();
	foreach ($xml->tasa as $tasa)
		if(intval($tasa->activo))
			$tasas[] =array("id"=>$tasa->monto, "tasa"=>$tasa->monto."%");
			//$tasas[] =array("id"=>intval($tasa->monto), "tasa"=>intval($tasa->monto)."%");
	return $tasas;
}*/
function xmltoarray($xml, $ind, $val, $end){
	$result = array();
	foreach ($xml->$ind as $value)
		if(intval($value->activo))
			$result[] = array("id"=>intval($value->$val), $ind=>$value->$val.$end);
	return $result;
}
//Obtiene el nombre de la renovaci�n del grupo buscando el consecutivo
function getNextName($name){
	$lastChars = substr($name, -2);
	//Si ya tiene m�s de dos renovaciones
	if($lastChars[0] == ' ' && is_numeric($lastChars[1]))
		$name = substr($name, 0, -1).($lastChars[1] + 1);
	//Si es la primera renovaci�n
	else
		$name = "{$name} 2";
	return $name;
}

function deleteTablaAmortizacion($transaccionDB, $idCredito){
	//Se borra toda la informaci�n del credito (amortizacion, pagos).
	//Obtengo los dep�sitos afectados
	$depositos = $transaccionDB->getDepositoByCredito($idCredito);
	//Borro todos los pagos del cr�dito
	$transaccionDB->deletePagosByCredito($idCredito);
	//Borro todos los cargos de inter�s moratorio hechos
	$transaccionDB->deleteMoratoriosByCredito($idCredito);
	//Borro todas las amortizaciones correspondientes al cr�dito
	$transaccionDB->deleteAmortizacionByCredito($idCredito);
	//Cambio el status de los dep�sito a NO CONCILIADO
	$transaccionDB->updateStatusDepositos(getIdsDepositos($depositos), 0);
}

function deletePagosCredito($transaccionDB, $idCredito){
	//Obtengo los dep�sitos afectados
	$depositos = $transaccionDB->getDepositoByCredito($idCredito);
	//Borro todos los pagos del cr�dito
	$transaccionDB->deletePagosByCredito($idCredito);
	//Borro todos los cargos de inter�s moratorio hechos
	$transaccionDB->deleteMoratoriosByCredito($idCredito);
	//Cambio el status de las amortizaci�n a NO PAGADO
	$transaccionDB->updateStatusAmortizacionesByCredito($idCredito, 0);
	//Cambio el status de los dep�sito a NO CONCILIADO
	$transaccionDB->updateStatusDepositos(getIdsDepositos($depositos), 0);
	return $depositos;
}

function createTablaAmortizacion($creditoDB, $transaccionDB, $idCredito, $data){
	$periodo = array('s'=>'Semanal','q'=>'Quincenal','m'=>'Mensual','b'=>'Bimestral','t'=>'Trimestral','u'=>'Quinquenal','e'=>'Semestral','a'=>'Anual');
	$credito = $creditoDB->getCreditoById($idCredito);
	$cCredito = $credito;
	$acreditados = $creditoDB->getAcreditadosByCredito($idCredito);
	$cCredito['tasa'] =  $data['tasaInteres'];
	$cCredito['plazo'] = $data['plazo'];
	$cCredito['periodo'] = array_search($data['periodo'], $periodo);
	$cCredito['fecha'] = $cCredito['fecha_entrega'];
	$cCredito['viejo'] = $credito['viejo'];
	foreach ($acreditados as $acreditado){
		$cCredito['montoTotal'] =  $acreditado['cantidad'];
		$amortizacion = new Amortizacion($cCredito);
		$tabla = $amortizacion->getTabla();
		foreach($tabla as $mov)
			$transaccionDB->addMovimiento($acreditado['id_acreditado'], 0, $mov, 0);
		unset($amortizacion);
	}
}

function getIdsDepositos($depositos){
	$ids = array();
	if(!empty($depositos)){
		foreach ($depositos as $deposito)
			$ids[] = $deposito['id_deposito'];
	}
	return $ids;
}

function number_from_format($string, $decimals = 0, $dec_point = '.', $thousands_sep = ',') {
	$buffer = $string = (string) $string;
	$buffer = strtr($buffer, array($thousands_sep => '', $dec_point => '.'));
	$float = (float) $buffer;
	$test = number_format($float, $decimals, $dec_point, $thousands_sep);
	if ($test !== $string) {
		throw new InvalidArgumentException(sprintf('Unable to parse float from "%s" as number "%s".', $string, $test));
	}
	return $float;
}

//Devuelve los datos del credito a partir de la referencia bancaria
function getDataFromReferencia($referencia, $monto){
	$credito = array();
	if(strlen($referencia) == 18){
		$credito['expediente'] = ltrim(substr($referencia, 0, 5), "0");
		$credito['idCredito'] = ltrim(substr($referencia, 5, 5), "0");
		$credito['monto'] = $monto;
		return $credito;
	}
	return false;
}

function aplicaPago($data, $creditoDB, $transaccionDB, $ubicacionDB, $aplicaMora=false, $reacomodo = false, $deletePago = false){
	$deposito = $transaccionDB->getDepositosById($data['idDeposito']);
	//Verifico que el dep�sito sea consecutivo y no sea anterior a alguno introducido anteriormente
	$depositosPost = $transaccionDB->getDepositosGreaterThan($data['idCredito'], $deposito['fecha_deposito']);
	if(empty($depositosPost) && !$deletePago){
		if(balancePago($data, $creditoDB, $transaccionDB, $ubicacionDB, $aplicaMora))
			return true;
	}
	else{
		$depositos = deletePagosCredito($transaccionDB, $data['idCredito']);
		if(!$reacomodo)
			$depositos[] = $deposito;
		usort($depositos, 'comparaFechas');
		foreach($depositos as $val){
			$data['idDeposito'] = $val['id_deposito'];
			//Si el pago era mora se aplica de manera diferente
			if($val['tipo_abono'] == '0' || !isset($val['tipo_abono'])){
				balancePago($data, $creditoDB, $transaccionDB, $ubicacionDB, $aplicaMora);
			}
			else{
				$credito = $creditoDB->getCreditoById($data['idCredito']);
				$acreditados = $creditoDB->getAcreditadosByCredito($data['idCredito']);
				foreach($acreditados as $acred){
					$pagoMora['id_deposito'] = $val['id_deposito'];
					$pagoMora['fecha'] = $val['fecha_deposito'];
					$pagoMora['pagoFijo'] = $val['monto']* ($acred['cantidad']/$credito['montoTotal']);
					$pagoMora['interes'] = $pagoMora['pagoFijo']/1.16;
					$pagoMora['iva'] = $pagoMora['interes']*0.16;
					$pagoMora['capital'] = 0;
					$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoMora, 1);
				}
			} 
				
		}
		return true;
	}
}

function comparaFechas($a, $b){
	$t1 = strtotime($a['fecha_deposito']);
	$t2 = strtotime($b['fecha_deposito']);
	return $t1 - $t2;
}

function balancePago($data, $creditoDB, $transaccionDB, $ubicacionDB, $aplicaMora){
	$precision = 10000;
	$_iva = 1.16;
	
	$criterios['idCred'] = $data['idCredito'];
	$criterios['tipoCargo'] = 0; //amortizacion
	$criterios['tipoAbono'] = 0; //pago al credito
	$deposito = $transaccionDB->getDepositosById($data['idDeposito']);
	$credito = $creditoDB->getCreditoById($data['idCredito']);
	$acreditados = $creditoDB->getAcreditadosByCredito($data['idCredito']);
	//S�lo traigo los cargos que corresponden a pagos al cr�dito
	$cargos = $transaccionDB->getCargosByCriterio($criterios);
	//S�lo traigo los abonos que corresponden a pagos al cr�dito
	$abonos = $transaccionDB->getAbonosByCriterio($criterios);
	//El monto total de los pagos al cr�dito
	$sumPagos = $transaccionDB->getSumPagos($data['idCredito']);
	$criterios['fecha'] = $deposito['fecha_deposito'];
	$balance = $transaccionDB->getBalanceByCriterio($criterios);
	$pFijo = $cargos[0]['monto'];
	//Para calcular tan solo la mora que han generado al d�a de hoy
	$diasInhabiles = $ubicacionDB->getDiasInhabiles();
	$diasMora = MoraTotal($cargos, $criterios['fecha'], $diasInhabiles);
	$mora = $diasMora * $pFijo * ($credito['tasa_mora']/100) * $_iva;
	$deudaTotal = $mora + $balance['interes'] + $balance['iva'] + $balance['capital'];
	//Para tener un balance de la mora pasada que debe y ha pagado
	$criterios['tipoCargo'] = 1; //mora
	$criterios['tipoAbono'] = 1; //pago mora
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
	$mora += $balanceMora['monto'];
	$creditoCompleto = false;
	unset($criterios);
	
	$restante = 0;
	$pago = array();
	$restante = $deposito['monto'];
	
	if($restante > 0){
		$i = 0;
		$cubreAmort = false;
		while($restante > 0){
			$interes = 0;
			$iva = 0;
			$capital = 0;
			//Primero verifico que la amortizaci�n no haya sido pagada
			if(isset($cargos[$i]) && $cargos[$i]['status'] == 0){
				if(strtotime($cargos[$i]['fecha_mov']) <= strtotime($deposito['fecha_deposito'])){
					//Cuando tiene alguna amortizaci�n pagada parcialmente, tiene saldo a su favor de ese pago
					if($sumPagos['monto'] < 0 && $sumPagos['monto'] < -4/10){
						//Si con el pago no alcanza a cubrir el inter�s
						if(($restante - $sumPagos['interes']) < $cargos[$i]['interes']){
							$interes = $restante / $_iva;
							$iva = $interes * ($_iva-1);
							$capital = 0;
							$cubreAmort = false;
						}
						else{
							$interes = $cargos[$i]['interes'] + $sumPagos['interes'];
							$interes = ($interes < 1/$precision) ? 0 : $interes;
							$iva = $cargos[$i]['iva'] + $sumPagos['iva'];
							$iva = ($iva < 1/$precision) ? 0 : $iva;
							$dif = $restante - $interes - $iva - $sumPagos['capital'];
							$mayor = $dif > $cargos[$i]['capital'] ? $dif : $cargos[$i]['capital'];
							$menor = $dif < $cargos[$i]['capital'] ? $dif : $cargos[$i]['capital'];
							if($dif < $cargos[$i]['capital'] && ($mayor-$menor) > 1/10){
								$capital = $restante - $interes - $iva;
								$cubreAmort = false;
							}
							else{
								$capital = $cargos[$i]['capital'] + $sumPagos['capital'];
								$cubreAmort = true;
							}
						}
						$sumPagos['monto'] = 0;
					}
					//Para las amortizaciones que pueden ser pagadas completas
					elseif($cargos[$i]['monto'] <= $restante || abs($cargos[$i]['monto']-$restante) < 4/10){
						$interes = $cargos[$i]['interes'];
						$iva = $cargos[$i]['iva'];
						$capital = $cargos[$i]['capital'];
						$cubreAmort = true;
					}
					//En caso de que no alcance a cubrir la amortizaci�n se ajusta el pago cubriendo primero intereses
					elseif($cargos[$i]['monto'] > $restante){
						//Si ni siquiera alcanza a cubrir el interes e iva de la amortizaci�n hago un ajuste
						if($restante < ($cargos[$i]['interes'] + $cargos[$i]['iva'])){
							$interes = $restante / $_iva;
							$iva = $interes * ($_iva-1);
							$capital = 0;
						}
						//En caso de que alcance a cubrir interes e iva pero no el capital hago otro ajuste
						else{
							$interes = $cargos[$i]['interes'];
							$iva = $cargos[$i]['iva'];
							$capital = $restante - $interes - $iva;
						}
						$cubreAmort = false;
					}
				}
				///////////////////////////////////////////////////////////////////////////////////////////////
				//En caso de que cubra sus obligaciones de pago, lo que sobre adelantar� amortizaciones o mora
				///////////////////////////////////////////////////////////////////////////////////////////////
				elseif(strtotime($cargos[$i]['fecha_mov']) > strtotime($deposito['fecha_deposito'])){
					//Si ya cubri� sus pagos vencidos y sobra dinero, paga mora en caso de deber
					if($aplicaMora){
						if($mora > 0){
							if($restante > $mora){
								$pagoMora['interes'] = $mora / $_iva;
								$pagoMora['iva'] = $pagoMora['interes'] * ($_iva - 1);
							}
							else{
								$pagoMora['interes'] = $restante / $_iva;
								$pagoMora['iva'] = $pagoMora['interes'] * ($_iva - 1);
							}
							$restante -= $pagoMora['interes'] + $pagoMora['iva'];
							$restante = ($restante < 1/$precision) ? 0 : $restante;
							$mora = 0;
						}
					}
					//Si adelant� una parcialidad previamente
					if($sumPagos['monto'] < 0 && $sumPagos['monto'] < -4/10){
						//No cubre por completo la parte de capital de la parcialidad
						if(($restante - $sumPagos['capital']) < $cargos[$i]['capital']){
							$capital = $restante;
							$interes = 0;
							$iva = 0;
							$cubreAmort = false;
						}
						//En caso contrario alcanza a pagar todo el capital de la parcialidad y adelante inter�s
						else{
							$capital = $cargos[$i]['capital'] + $sumPagos['capital'];
							$capital = ($capital < 1/$precision) ? 0 : $capital;
							if($restante - $capital - $sumPagos['interes'] - $sumPagos['iva'] < $cargos[$i]['interes'] + $cargos[$i]['iva']){
								$interes = ($restante - $capital) / $_iva;
								$iva = $interes * ($_iva-1);
								$cubreAmort = false;
							}
							else{
								$interes = $cargos[$i]['interes'] + $sumPagos['interes'];
								$iva = $cargos[$i]['iva'] + $sumPagos['iva'];
								$cubreAmort = true;
							}
	
						}
						$sumPagos['monto'] = 0;
					}
					//Cubre por completo la parcialidad adelantada
					elseif($cargos[$i]['monto'] <= $restante || abs($cargos[$i]['monto']-$restante) < 4/10){
						$interes = $cargos[$i]['interes'];
						$iva = $cargos[$i]['iva'];
						$capital = $cargos[$i]['capital'];
						$cubreAmort = true;
					}
					elseif($cargos[$i]['monto'] > $restante){
						if($restante > $cargos[$i]['capital']){
							$capital = $cargos[$i]['capital'];
							$interes = ($restante - $capital) / $_iva;
							$iva = $interes * ($_iva-1);
						}
						else{
							$interes = 0;
							$iva = 0;
							$capital = $restante;
						}
						$cubreAmort = false;
					}
				}
				//Acumulo todos lo que voy a juntar en el pago
				$pago['interes'] += $interes;
				$pago['iva'] += $iva;
				$pago['capital'] += $capital;
				if($cubreAmort)
					$pago['cargos_cubiertos'][] = $i;
				//Descuento al sobrante lo que aplique del pago
				$restante -= $interes + $iva + $capital;
				$restante = ($restante < 1/$precision) ? 0 : $restante;
			}
			elseif(isset($cargos[$i]) && $cargos[$i]['status'] == 1){
				$sumPagos['monto'] += $cargos[$i]['monto'];
				$sumPagos['interes'] += $cargos[$i]['interes'];
				$sumPagos['iva'] += $cargos[$i]['iva'];
				$sumPagos['capital'] += $cargos[$i]['capital'];
			}
			//Si ya no hay m�s que pagar y sobra dinero, todo se lo pongo como capital
			else{
				$creditoCompleto = true;
				$pago['capital'] += $restante;
				$restante = 0;
			}
			$i++;
		}
	}
	//Aplicamos la parte proporcional del pago a cada integrante del cr�dito
	$pagoAcred['fecha'] = $deposito['fecha_deposito'];
	$pagoAcred['id_deposito'] = $deposito['id_deposito'];
	foreach($acreditados as $acred){
		$pagoAcred['interes'] = $pago['interes'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['iva'] = $pago['iva'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['capital'] = $pago['capital'] * ($acred['cantidad']/$credito['montoTotal']);
		$pagoAcred['pagoFijo'] = $pagoAcred['interes']+$pagoAcred['iva']+$pagoAcred['capital'];
		$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoAcred, 0);
	}
	//Si cubri� amortizaciones con su pago entonces cambiamos su status a pagado y adem�s
	//metemos la mora al sistema de las amortizaciones que ya pag� (si existe).
	if(!empty($pago['cargos_cubiertos'])){
		foreach ($pago['cargos_cubiertos'] as $num){
			$transaccionDB->updateCargo($cargos[$num]['fecha_mov'], $credito['id_credito'], 1);
			$cargosMora[] = $cargos[$num];
		}
	}
	//La mora efectiva es la que se va a guardar en el sistema y esta representa la mora de los
	//pagos que ya fueron cubiertos, no se cobra mora de pagos todav�a incompletos.
	if(!empty($cargosMora)){
		$diasMoraEfectiva = MoraTotal($cargosMora, $deposito['fecha_deposito'], $diasInhabiles);
		$moraEfectiva = $diasMoraEfectiva * $pFijo * ($credito['tasa_mora']/100) * $_iva;
	}
	if($moraEfectiva > 0){
		$moraAcred['fecha'] = $deposito['fecha_deposito'];
		foreach($acreditados as $acred){
			$moraAcred['interes'] = ($moraEfectiva/$_iva)* ($acred['cantidad']/$credito['montoTotal']);
			$moraAcred['iva'] = $moraAcred['interes'] * ($_iva-1);
			$moraAcred['capital'] = 0;
			$moraAcred['pagoFijo'] = $moraAcred['interes']+$moraAcred['iva'];
			$transaccionDB->addMovimiento($acred['id_acreditado'], 0, $moraAcred, 1);
		}
	}
	//Si adelanta y debe mora entonces el sobrante se va a �sta
	if($pagoMora['interes'] > 0){
		$pagoMoraAcred['capital'] = 0;
		$pagoMoraAcred['fecha'] = $deposito['fecha_deposito'];
		$pagoMoraAcred['id_deposito'] = $deposito['id_deposito'];
		foreach($acreditados as $acred){
			$pagoMoraAcred['interes'] = $pagoMora['interes'] * ($acred['cantidad']/$credito['montoTotal']);
			$pagoMoraAcred['iva'] = $pagoMora['iva'] * ($acred['cantidad']/$credito['montoTotal']);
			$pagoMoraAcred['pagoFijo'] = $pagoMoraAcred['interes']+$pagoMoraAcred['iva'];
			$transaccionDB->addMovimiento($acred['id_acreditado'], 1, $pagoMoraAcred, 1);
		}
	}
	///////////////////////////////////////////////////////////////////////////
	//Si termina de pagar el credito y no debe mora, el cr�dito queda concluido
	$criterios['idCred'] = $data['idCredito'];
	$criterios['tipoCargo'] = 0; //amortizacion
	$criterios['tipoAbono'] = 0; //pago al credito
	$cargos = $transaccionDB->getCargosByCriterio($criterios);
	$concluido = true;
	foreach($cargos as $val)
		if(intval($val['status']) == 0)
		$concluido =  false;
	$criterios['tipoCargo'] = 1; //mora
	$criterios['tipoAbono'] = 1; //pago mora
	$balanceMora = $transaccionDB->getBalanceByCriterio($criterios);
	//Si esta concluido y no debe mora, cambio status a concluido
	if($concluido && abs($balanceMora['monto']) < 4/10)
		$creditoDB->updateStatusCredito($criterios['idCred'], 4);
	//////////////////////////////////////////////////////////////////////////
	//Actualizamos la informaci�n del dep�sito y lo marcamos como 'conciliado'
	$deposito['aplicado'] = 1;
	$transaccionDB->updateDeposito($deposito);
	
	return true;
}

function saveDeposito($data, $transaccionDB)
{
    $id_deposito = 0;

    if(intval($data['tipo_cuenta']) === 4 || intval($data['tipo_cuenta']) === 5)
    {
        $id_deposito = $transaccionDB->addDeposito($data);
        if(!$id_deposito)
        {
            return false;
        }
    }
    else
    {
        $id_deposito = $transaccionDB->addDeposito($data);
        $data['descripcion'] = $data['observaciones'];
        $data['userID'] = $_SESSION['userID'];
        $data['cta1'] = 2;
        $data['cta2'] = $data['tipo_cuenta'];
        if($transaccionDB->addTransaccionEfectivo($data))
        {
            $transaccionDB->updateStatusDepositos(array($id_deposito),1);
        }
        else
        {
            return false;
        }
    }
    return $id_deposito;
}
