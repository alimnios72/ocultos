<?php
include('main.php');
//Para las hojas de estilo y javascript
$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css','jquery-ui.css','jquery.jqplot.min.css');
$js=array('jquery.js','switcher.js','toggle.js','jquery-ui.js','ui.core.js','ui.datepicker-es.js',
		   'reportes.js','jquery.tablesorter.min.js','jquery.jqplot.min.js');
$topmenu['repor'] = 'id="menu-active"';
$leftmenu = array('Total prestado por fecha'=>'prestado', 'Total prestado por persona'=>'porpersona', 
				  'Total inter�s por fecha'=>'totinteres', 'Totales por mes' => 'pormes', 
				  'Cartera Vencida'=>array('Por parcialidades vencidas'=>'parcialidadesV',
				  							'Por d�as vencidos'=>'diasV'),
				  'Desglose por dep�sito'=>'pordeposito',
				  'Total devengado'=>'devengado',
				  'Gestiones realizadas'=>'gestiones',
				  'Recuperado por persona'=>'recporpersona');

//Identificamos cual va a ser el contenido a mostrar de la seccion
$content = isset($_GET['content'])? $_GET['content'] : false;
switch($content)
{
    case 'prestado':
        $section = RUTA_SEC.'reportes/prestado.php';
        break;
    case 'totinteres':
        $section = RUTA_SEC.'reportes/totinteres.php';
       	break;
    case 'pormes':
       	$section = RUTA_SEC.'reportes/pormes.php';
       	break;
    case 'parcialidadesV':
       	$section = RUTA_SEC.'reportes/carteravencida.php';
       	$tipoCartera = 'parcialidades';
       	break;
    case 'diasV':
       	$section = RUTA_SEC.'reportes/carteravencida.php';
       	$tipoCartera = 'dias';
       	break;
    case 'gestiones':
       	$section = RUTA_SEC.'reportes/gestiones.php';
       	break;
    case 'pordeposito':
       	$section = RUTA_SEC.'reportes/pordeposito.php';
       	break;
    case 'devengado':
       	$section = RUTA_SEC.'reportes/devengado.php';
       	break;
    case 'porpersona':
       	$section = RUTA_SEC.'reportes/porpersona.php';
       	break;
    case 'recporpersona':
       	$section = RUTA_SEC.'reportes/recporpersona.php';
       	break;
    default:
    	$section = RUTA_TPL.'home.tpl.php';
    	$rutaTemplate = 'reportes/';
    	$template = 'home.tpl.php';
        break;
}

include(RUTA_TPL.'header.tpl.php');
include($section);
include(RUTA_TPL.'footer.tpl.php');
?>
