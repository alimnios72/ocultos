<?php
include('main.php');
//Para las hojas de estilo y javascript
$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css','jquery-ui.css');
$js=array('jquery.js','switcher.js','toggle.js','jquery-ui.js','ui.core.js','cobranza.js','ui.datepicker-es.js');
$topmenu['cobra'] = 'id="menu-active"';
if($_SESSION['priv'] == 'juridico'){
	$leftmenu = array('Lista de pagos'=>'listado','Gestionar cobranza'=>'gestion');
}
else{
	$leftmenu = array('Asignación de pagos' => 'asignacion', 'Conciliación de pagos'=>'conciliacion','Condonar mora'=>'pagoMora','Lista de pagos'=>'listado',
                  'Gestionar cobranza'=>'gestion','Alerta de pago'=>'alertapago','Anticipo crédito'=>'anticipo');
}

//Identificamos cual va a ser el contenido a mostrar de la seccion
$content = isset($_GET['content'])? $_GET['content'] : false;
switch($content)
{
    case 'conciliacion':
        $section = RUTA_SEC.'cobranza/conciliacion.php';
        break;
    case 'aplicacion':
        $section = RUTA_SEC.'cobranza/aplicacion.php';
        break;
    case 'balance':
        $section = RUTA_SEC.'cobranza/balance.php';
        break;
    case 'pagoMora':
        $section = RUTA_SEC.'cobranza/pagoMora.php';
        break;
    case 'listado':
       	$section = RUTA_SEC.'cobranza/listado.php';
        break;
    case 'gestion':
    	$section = RUTA_SEC.'cobranza/gestion.php';
    	break;
    case 'alertapago':
    	$section = RUTA_SEC.'cobranza/alertaPago.php';
    	break;
    case 'anticipo':
    	$section = RUTA_SEC.'cobranza/anticipo.php';
    	break;
    case 'asignacion':
        $section = RUTA_SEC.'cobranza/asignacion.php';
        break;
    default:
        $section = RUTA_TPL.'home.tpl.php';
        $rutaTemplate = '';
        $template = 'principal.tpl.php';
        break;
}

include(RUTA_TPL.'header.tpl.php');
include($section);
include(RUTA_TPL.'footer.tpl.php');
?>