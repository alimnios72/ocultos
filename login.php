<?php
$loginPage=true;
include('main.php');
//Para las hojas de estilo y javascript
$css=array('login.css');
$js=array('jquery.js');
if(isset($_GET['msg']) && is_numeric($_GET['msg']))
	{
		switch($_GET['msg'])
		{
			case 1:
			$msg = "Llena ambos campos.";
			break;
			
			case 2:
			$msg = "Datos incorrectos.";
			break;
		}
}
if(isset($_POST['login']))
{
	if((!$_POST['username']) || (!$_POST['password']))
        {
		header('location:login.php?msg=1');// show error
		exit;
	}

	if($adminLogin->doLogin($_POST['username'],$_POST['password']))
		header('location:index.php');
	else
	{
		header('location:login.php?msg=2');
		exit;
	}
}

include(RUTA_TPL.'login.tpl.php');
?>	
