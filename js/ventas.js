$(function() {
	//Configuramos ajax para que las peticiones al servidor las haga con la codificaci�n correcta
	$.ajaxSetup({contentType: 'application/x-www-form-urlencoded;charset=windows-1252'});
	//($("#tipo").val() == "0") ? $("#tasa").val("70") : $("#tasa").show("58");
    ($("#tipo").val() == "0") ? $("#opcGpo").hide() : $("#opcGpo").show();
    ($("#edocivil").val() == "Casado" || $("#edocivil").val() == "Union Libre") ? $("#conyuge").show() : $("#conyuge").hide();
    ($('#siPC').is(':checked')) ? $("#pagando_cred").show() : $("#pagando_cred").hide();
    ($('#siPA').is(':checked')) ? $("#pension_alim").show() : $("#pension_alim").hide();
    //Para el calendario de las fechas
    //$("#fecha").datepicker({dateFormat: "yy-mm-dd"});
    $("#nacimiento").datepicker({yearRange: "-70:-18", dateFormat: "yy-mm-dd",
    							changeMonth: true, changeYear: true, defaultDate:"-70y-m-d"});
    //LLena el combo de delegaciones a partir de la selecci�n de un estado
    $("#estado").change(function(){
    	$("#deleg").load('procesos/funcionesAjax.php', {accion: 'delegaciones', idEstado: $("#estado").val()});
    });
    //Cambia algunos parametros dependiendo del tipo de credito
    $("#tipo").change(function(){
	if($(this).val() == 1){
	    $("#opcGpo").show();
	    $("#tasa").val("58");
	}
	else{
	    $("#opcGpo").hide();
	    $("#tasa").val("70");
	}
    });
    //Calcula la edad de la persona con la fecha introducida
    $("#nacimiento").change(function(){
		$.post('procesos/funcionesAjax.php', {accion: 'getEdad', fecha: $(this).val()},
		       function(data){
			    $("#edad").val(data);
		       });
    });
    //Muestra la pregunta acerca del trabajo del conyuge en caso que sea casada o en uni�n libre
    $("#edocivil").change(function(){
	if($(this).val() == "Casado" || $(this).val() == "Union Libre")
	    $("#conyuge").show();
	else
	    $("#conyuge").hide();
    });
    //Muestra u oculta algunos campos de la informaci�n econ�mica.
    $("input:radio[name='pag_cred']").change(function(){
	if($('#siPC').is(':checked'))
	    $("#pagando_cred").show();
	else
	    $("#pagando_cred").hide();
    });
     $("input:radio[name='pen_alim']").change(function(){
	if($('#siPA').is(':checked'))
	    $("#pension_alim").show();
	else
	    $("#pension_alim").hide();
    });
    $("input:radio[name='repre']").change(function(){
    	$.post('procesos/funcionesAjax.php', {accion: 'repre', num: $(this).val()},
 		       function(data){
 			    	alert("Representante asignado");
 		       });
    });
    //Ver resumen del cr�dito para aprobaci�n
    $("a[id^='credito_']").click(function(){
    	var id=$(this).attr('id');
    	var field=id.split(/_/);
    	$("#id_credito").val(field[1]);
    	$("input[name='creditos'][value='"+field[1]+"']").attr('checked',true);
    	$("#resumenCred").load('procesos/funcionesAjax.php', {accion:'resumenCred', id:field[1], tipo:field[2]});
    });
    //Borramos la informaci�n que esta guardada en sessi�n
    $("#reset").click(function(){
    	window.location.href = "ventas.php?content=nuevo&reset=1";
    });
    //Borrar integrantes para renovaciones
    $("a[id^='delInt']").live("click",function(){
    	var id=$(this).attr('id');
    	var field=id.split(/_/);
    	var acreditado = field[1];
    	$("#integrantes_table").load('procesos/funcionesAjax.php', {accion:'deleteIntegrante', acreditado:acreditado});
    });
    //Actualizo el monto del acreditado
    $("input[id^='montoInt_']").live("change",function(){
    	var id=$(this).attr('id');
    	var field=id.split(/_/);
    	var acreditado = field[1];
    	var monto = $(this).val();
    	$("#integrantes_table").load('procesos/funcionesAjax.php', {accion:'montoIntegrante', acreditado:acreditado, monto:monto});
    });
    //Guardo los selected checkbox para integrantes
    $("input[id^='chInt_']").live("change",function(){
    	var id=$(this).attr('id');
    	var field=id.split(/_/);
    	var acreditado = field[1];
    	var checked =  $(this).is(':checked');
    	$("#integrantes_table").load('procesos/funcionesAjax.php', {accion:'checkboxIntegrante', acreditado:acreditado, checked:checked});
    });
    
    //Para verificar si la persona ya existe cuando se trata de agregar un nuevo cr�dito
    $("input.solicitud").change(function(){
    	if(($("#nombres").val() != "" && $("#nacimiento").val() != "") && ($("#apellidoP").val() != "" || $("#apellidoM").val() != "")){
    		$.post('procesos/funcionesAjax.php', 
    				{accion: 'buscaPersona', 
    				 nombres: $("#nombres").val(), 
    				 apellidoP: $("#apellidoP").val(),
    				 apellidoM: $("#apellidoM").val(),
    				 nacimiento: $("#nacimiento").val(),
    				 acred: $("#num").val(),
    				 viejo: $("#viejo").val(),
    				 renovacion: $("#renovacion").val(),
    				 tipo: $("#button").attr('name')},
    	 		  function(data){
    					 if(data.deudor && $("#viejo").val() != 1){
    						 if(data.tipo == 1)
    							 alert("La persona ya tiene un cr�dito "+data.status+".");
    						 else
    							 alert("La persona es obligado solidaro de un cr�dito "+data.status+".");
    						 $("#nombres").val("");
    						 $("#apellidoP").val("");
    						 $("#apellidoM").val("");
    						 $("#nacimiento").val("");
    						 $("#edad").val("");
    						 popitup('administracion.php?content=perfil&persona='+data.idPersona, 'Perfil');
    					 }
    					 else{
	    					 alert("La persona ya existe, se cargar� su informaci�n");
	    					 if( $("#button").attr('name') == 'acreditado'){
	    						 if($("#viejo").val() == 1)
	    							 window.location.href = "ventas.php?content=viejo&editar=acreditado&num="+$("#num").val();
	    						 else if($("#renovacion").val() == 1)
	    							 window.location.href = "ventas.php?content=renovacion&accion=agregarP&editar=acreditado";
	    						 else
	    							 window.location.href = "ventas.php?content=nuevo&editar=acreditado&num="+$("#num").val();
	    					 }
	    					 else{
	    						 if($("#viejo").val() == 1)
	    							 window.location.href = "ventas.php?content=viejo&editar=solidario";
	    						 else if($("#renovacion").val() == 1)
	    							 window.location.href = "ventas.php?content=renovacion&accion=agregarP&editar=solidario";
	    						 else
	    							 window.location.href = "ventas.php?content=nuevo&editar=solidario";
	    					 }
    					 }
    	 		      }, "json");
    	}
    });
});
function popitup(url,name) {
	newwindow=window.open(url, name,'scrollbars=1, toolbar=0, status=0, width='+screen.width+', height='+screen.height);
	if (window.focus) {newwindow.focus()}
	return false;
}