$(function() {
	$("#fecha").live("click",function(){
		$(this).datepicker({showOn:'focus', dateFormat: 'yy-mm-dd'}).focus();
	});
	$("#fechaExacta").datepicker({dateFormat: "yy-mm-dd"});
	$("#fecha1").datepicker({dateFormat: "yy-mm-dd"});
    $("#fecha2").datepicker({dateFormat: "yy-mm-dd"});
    //Para borrar los campos de filtro de cheque hacemos un redireccionamiento
    $("#cleanResults").click(function(){
    	window.location = "cobranza.php?content=conciliacion";
    });
    //Paginador por d�a lista de amortizaciones
    $("a.pagination").live("click", function(){
    	var date = $(this).attr('id');
    	$("#listaAmort").load('procesos/funcionesAjax.php',{accion: 'pagAmortizaciones', date:date, tipo:'fecha'});
    });
    //Para buscar por expediente en la conciliaci�n de pagos
    $("#searchExp").click(function(){
    	var exp = $("#expediente").val();
    	$("#listaAmort").load('procesos/funcionesAjax.php',{accion: 'pagAmortizaciones', expediente:exp, tipo:'expediente'});
    });
    $("#buscarPersona").autocomplete({
		source: "procesos/buscarPersona.php",
		minLength: 2,
		select: function(event, ui ){
		    var id = ui.item.id_persona;
		    $("#idPersona").val(id);
		    $("#datosCliente").load('procesos/funcionesAjax.php',{accion:'datosC',id:id},function(){
		    	$("#historial").load('procesos/funcionesAjax.php',{accion:'historial',id:id});
		    });
		    $("#edoCta").html('');
		}
	});
    //Confirmar el anticipar cr�dito
    $("#dialog-confirm").dialog({
		autoOpen: false,
        resizable: false,
        height:250,
        width: 400,
        modal: true,
        buttons: {
            "OK": function() {
            	$.post('procesos/funcionesAjax.php',{accion:'validateUser',
            										username:$("#username").val(),
            										userpass:$("#userpass").val()})
            	.done(function(data){
            		ans = JSON.parse(data);
            		if(ans.response)
            			$('#formAnticipo').submit();
            		else
            			$(this).dialog( "close" );
            	});
            }
        }
    });
    $("#negativo").click(function(){
    	window.location ="cobranza.php?content=anticipo";
    });
    $("#anticiparCred").click(function(){
    	var mitadCred = $("#mitadCred").val();
    	if(parseInt(mitadCred) == 1)
    		$("#dialog-confirm").dialog("open");
    	else
    		$('#formAnticipo').submit();
    	return false; 
    });
    /*
    //Para buscar por expediente en la aplicaci�n de pagos a mora
    $("#busqExp").click(function(){
    	var exp = $("#expediente").val();
    	$("#credMora").load('procesos/funcionesAjax.php',{accion: 'pagaMora', expediente:exp});
    });
  //Para buscar por expediente en el anticipo del cr�dito
    $("#busqExp1").click(function(){
    	var exp = $("#expediente").val();
    	$("#credAnticipo").load('procesos/funcionesAjax.php',{accion: 'anticipoCred', expediente:exp});
    });*/
    //Prevengo que pase a la aplicaci�n de pago si no ha seleccionado nada
    $("#elegirPago").click(function(){
    	if($("input[name='idCredito']").is(':checked'))
    		$("#aplica").submit();
    	else
    		alert("Selecciona un pago");
    });
    $('.noEnterSubmit').keypress(function(e){
        if ( e.which == 13 ) 
        	e.preventDefault();
    });
});