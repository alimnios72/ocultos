var mes=new Array();
mes[0]="Enero";
mes[1]="Febrero";
mes[2]="Marzo";
mes[3]="Abril";
mes[4]="Mayo";
mes[5]="Junio";
mes[6]="Julio";
mes[7]="Agosto";
mes[8]="Septiembre";
mes[9]="Octubre";
mes[10]="Noviembre";
mes[11]="Diciembre";

$(function() {
    $("#dialog-form").dialog({
    	autoOpen: false
    });
    
    $(".pagination").live("click", function(){
    	var idSucursal = $("#sucursal").val();
    	$("#weekCalendar").load('procesos/funcionesAjax.php', {accion: 'loadCitas', fecha: $(this).attr('id'), idSucursal: idSucursal});
    });
    
    $("td[id^='cell']").live("click", function(){
    	if($("#id_credito").val() !=''){
	    	var id=$(this).attr('id');
	    	var field=id.split(/_/);
	    	var inicio = new Date(dateFromString(field[1], field[2]));
	    	var fin = new Date(inicio);
	    	fin.setMinutes(inicio.getMinutes() + 30);
	    	var fecha = inicio.getFullYear()+'-'+parseInt(inicio.getMonth()+1)+'-'+inicio.getDate();
	    	var fechaText = inicio.getDate()+' de '+mes[inicio.getMonth()]+' de '+inicio.getFullYear();
	    	var citaIni = formatDate(inicio.getHours(),inicio.getMinutes());
	    	var citaFin = formatDate(fin.getHours(),fin.getMinutes());
	    	$("#fecha").val(fecha);
	    	$("#citaI").val(citaIni);
	    	$("#citaF").val(citaFin);
	    	$("#fechaText").html(fechaText);
	    	$("#citaInicio").html(citaIni);
	    	$("#citaFin").html(citaFin);
	    	$("#dialog-form").dialog("open");
    	}
    	else
    		alert("Primero seleccione un cr�dito para poder hacer la cita");
    });
    
    $("#cancel").click(function(){
    	$("#dialog-form").dialog("close");
    	return false; 
    });
    
    $("#accept").click(function(){
    	var fecha = $("#fecha").val();
    	var idCredito = $("#id_credito").val();
    	var horaIni = $("#citaI").val();
    	var horaFin = $("#citaF").val();
    	var idSucursal = $("#sucursal").val();
    	var dataString = 'accion:addCita, fecha:'+fecha+', idC:'+idCredito+', hIni:'+horaIni+', hFin:'+horaFin;
    	$.post('procesos/funcionesAjax.php',  
    		  {accion: 'addCita',
    		   idC: idCredito,
    		   fecha: fecha,
    		   hIni: horaIni,
    		   hFin: horaFin,
    		   sucursal: idSucursal },
    		  function(){
    			  $("#dialog-form").dialog("close");
    			  $("#weekCalendar").load('procesos/funcionesAjax.php', {accion: 'loadCitas', idSucursal: idSucursal});
    			  alert('Cita agendada correctamente');
    		  });
    	return false; 
    });
    //Cuando se cambia de sucursal para ver las citas 
    $("#sucursal").live("change",function(){
    	var idSucursal = $(this).val();
    	$("#weekCalendar").load('procesos/funcionesAjax.php', {accion: 'loadCitas', idSucursal: idSucursal});
    });
    
});

function formatDate(hours, minutes){
	var hours = hours.toString();
	var minutes = minutes.toString();
	hours = (hours.length < 2) ? '0'+hours : hours;
	minutes = (minutes.length < 2) ? minutes+'0' : minutes;
	return hours+':'+minutes;
}

function dateFromString(date, hour){
	date = date.split(/-/);
	var year = date[0];
	var month = date[1];
	var day = date[2];
	hour = hour.split(/:/);
	var h = hour[0];
	var m = hour[1];
	var result = new Date(year, month-1, day, h, m);
	return result;
}