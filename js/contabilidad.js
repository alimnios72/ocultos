$(function() {
	//Agrega o modifica cheques por acreditado
	$("a[id^='idC_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		$("input[name='idC'][value='"+field[1]+"']").attr('checked',true);
		$("#disposciones").load('procesos/funcionesAjax.php',{accion:'configCheques', id:field[1]});
	});
	//Actualiza las disposiciones sobre un crédito
    $("#updateDisposiciones").live("click", function(){
    	var idC = $("#idC").val();
    	var ids = $("input[name='ids[]']").map(function() { return $(this).val(); }).get();
    	var montos =  $("input[name='montos[]']").map(function() { return $(this).val(); }).get();
    	var cheques =  $("input[name='cheque[]']").map(function() { return $(this).val(); }).get();
    	var bancos =  $("select[name='banco[]'] option:selected").map(function() { return $(this).val(); }).get();
    	alert("Información guardada");
    	$("#disposciones").load('procesos/funcionesAjax.php',{accion:'configCheques', update:true, ids:ids, montos:montos, 
    															id:idC, cheques:cheques, bancos:bancos});
    });
    //Para la fecha en la cual se agregara el depósito
    $("#fecha").datepicker({dateFormat: "yy-mm-dd"});
    $("#fecha1").datepicker({dateFormat: "yy-mm-dd", onSelect: function(){
    	if($("#fecha2").val() == "" || $("#fecha2").val() == null)
    		$("#fecha2").val($(this).val());
    }});
    $("#fecha2").datepicker({dateFormat: "yy-mm-dd"});
    //Para impresión de cheques y polizas
    $(".printChPol").click(function(){
    	var tipo = $(this).attr('name');
    	var banco = $("#bancoImp").val();
    	var noCh = $("#noCheques").val();
    	var ini = $("#chInicial").val();
    	var fin = $("#chFin").val();
    	var get = 'tipo='+tipo+'&banco='+banco+'&noCh='+noCh+'&ini='+ini+'&fin='+fin;
    	popitup('procesos/chequesPolizas.php?'+get)
    });
    //Paginador por mes en la lista de depósitos
    $("a.pagination").live("click", function(){
    	var date = $(this).attr('name');
    	var page = $(this).attr('id');
    	$("#listaDep").load('procesos/funcionesAjax.php',{accion: 'pagDepositos', date:date, page:page});
    });
    //Borra depósitos del sistema
    $("a[id^='deleteDep_']").live("click", function(){
    	var date = $('#date').val();
    	var id = $(this).attr('id');
    	var field = id.split(/_/);
    	var idDep = field[1];
    	$("#listaDep").load('procesos/funcionesAjax.php',{accion: 'borraDep', date:date, idDep:idDep});
    	alert("Borrado");
    });
    //Para borrar los campos de filtro de cheque hacemos un redireccionamiento
    $("#cleanResults").click(function(){
    	window.location = "contabilidad.php?content=gestionCh"
    });
    //Para escoger fecha de cobro en los cheques
    $('input.fechaCobro').live('click', function() {
        $(this).datepicker({showOn:'focus', dateFormat: 'yy-mm-dd'}).focus();
    });
    //Actualiza la información como status y fecha de cobro de un cheque
    $("a[id^='save_']").live("click", function(){
    	var id = $(this).attr('id');
    	var field = id.split(/_/);
    	var idDisp = field[1];
    	var banco = $("#banco").val();
    	var status = $("#status").val();
    	var fecha1 = $("#fecha1").val();
    	var fecha2 = $("#fecha2").val();//Borra depósitos del sistema
    	var noCheque = $("#noCheque_"+idDisp).val();
        $("a[id^='deleteDep_']").live("click", function(){
        	var id = $(this).attr('id');
        	var field = id.split(/_/);
        	var idDep = field[1];
        	$("#listaDep").load('procesos/funcionesAjax.php',{accion: 'pagDepositos', date:date, page:page});
        	alert(idDep);
        });
    	var Cstatus = $("#changeStatus_"+idDisp).val();
    	var fechaCobro = $("#fechaCobro_"+idDisp).val();
    	$("#gestionCh").load('procesos/funcionesAjax.php',{accion: 'cobraCheques', action:'update', idDisp:idDisp, banco:banco, status:status,
    													fecha1:fecha1, fecha2:fecha2, noCheque:noCheque, Cstatus:Cstatus, fechaCobro:fechaCobro});
    	alert("Información guardada");
    });
    //Agrega un cheque cancelado al sistema
    $("#cancelCheque").live("click", function(){
    	var banco = $("#banco").val();
    	var status = $("#status").val();
    	var fecha1 = $("#fecha1").val();
    	var fecha2 = $("#fecha2").val();
    	var cNoCh = $("#cancelNoCh").val();
    	var cBanco = $("#cancelBanco").val();
    	var cMotivo = $("#cancelMotivo").val();
    	$("#gestionCh").load('procesos/funcionesAjax.php',{accion: 'cobraCheques', action:'cancel', banco:banco, fecha1:fecha1,
														 fecha2:fecha2, cNoCh:cNoCh, cBanco:cBanco, cMotivo:cMotivo});
    	alert("Cheque cancelado");
    });
});

function popitup(url) {
	newwindow=window.open(url,'Documentación','height=600,width=800,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}