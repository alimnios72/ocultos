$(function() {
	//Configuramos ajax para que las peticiones al servidor las haga con la codificaci�n correcta
	$.ajaxSetup({contentType: 'application/x-www-form-urlencoded;charset=windows-1252'});
	//Muestra delegaciones por estado
	$("#estado").change(function(){
	    	$("#deleg").load('procesos/funcionesAjax.php', {accion: 'delegaciones', idEstado: $("#estado").val()});
	});
	//Para la modificaci�n del nombre de la zona.
	$("#estado_z").change(function(){
    	$("#delegZ").load('procesos/funcionesAjax.php', {accion: 'delegaciones', nombre:'deleg_z',
    		idEstado: $("#estado_z").val()});
	});
	$("#deleg_z").live('change',function(){
    	$("#zonas").load('procesos/funcionesAjax.php', {accion: 'editZonas', update:false, 
    		idDel: $("#deleg_z").val()});
	});
	$("a[id^='saveZona_']").live("click",function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var idDel = $("#deleg_z").val();
		var zona = $("#zona_"+field[1]).val();
		$("#zonas").load('procesos/funcionesAjax.php',{accion:'editZonas', update:true, 
				idDel:idDel, id_zona:field[1], nombre_zona:zona});
		alert("Guardado con �xito");
	});
	
	//Cuando cambia el combo de estado se cargan todas las delegaciones que pertenecen a �ste.
	$('#estado_edit').change(function(){
		$("#delegaciones").load('procesos/funcionesAjax.php',{accion:'editDelegaciones', update:false, 
							id_edo:$(this).val()});
	});
	//Modifica los datos de una delegaci�n en espec�fico
	$("a[id^='saveDel_']").live("click",function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var id_edo = $("#estado_edit").val();
		var delegacion = $("#del_"+field[1]).val();
		$("#delegaciones").load('procesos/funcionesAjax.php',{accion:'editDelegaciones', update:true, 
			id_edo:id_edo, id_del:field[1], nombre_del:delegacion});
		alert("Guardado con �xito");
	});
	//Actualiza las tasas de inter�s entre activas y no activas
	$("a[id^='saveTasaOrd_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var tasa = field[1].replace('.','\\.');
		var activo = $('#tasaOrd_'+tasa).attr('checked')?true:false;
		$.post('procesos/funcionesAjax.php', {accion: 'updateTasa', tipo:'tasaOrd', tasa:tasa, activo:activo});
		alert("Cambiado");
	});
	$("a[id^='saveTasaMor_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var tasa = field[1];
		var activo = $("#tasaMor_"+field[1]).attr('checked')?true:false;
		if(activo)
			$.post('procesos/funcionesAjax.php', {accion: 'updateTasa', tipo:'tasaMor', tasa:tasa, activo:activo});
	});
	//Actualiza plazos
	$("a[id^='savePlazo_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var plazo = field[1].replace('.','\\.');
		var activo = $('#plazo_'+plazo).attr('checked')?true:false;
		$.post('procesos/funcionesAjax.php', {accion: 'updatePlazo', plazo:plazo, activo:activo});
		alert("Cambiado");
	});
	//Actualiza el monto del acreditado o lo borra del cr�dito
	$("a[id^='saveAcred_']").live('click',function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		var monto=$("#cantidad_"+field[1]).val();
		$.post('procesos/funcionesAjax.php', {accion: 'updateAcred', tipo:'monto', idAcred:field[1], monto:monto,
			 idCred:$("#id_credito").val()});
		alert('Cambiado');
	});
	$("a[id^='deleteAcred_']").live('click',function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		$("#acreditados").load('procesos/funcionesAjax.php', {accion: 'updateAcred', tipo:'borrar', 
							idAcred:field[1], idCred:$("#id_credito").val()});
		alert('Borrado');
	});
	
	$("#nacimiento").datepicker({yearRange: "-70:-18", dateFormat: "yy-mm-dd",changeMonth: true, changeYear: true});
	$("#buscarPersona").autocomplete({
		source: "procesos/buscarPersona.php",
		minLength: 2,
		select: function(event, ui ){
		    var id = ui.item.id_persona;
		    $("#idPersona").val(id);
		}
	});
	//Para confirmar si se desea borrar pagos de un cr�dito
	$("#dialog-confirm").dialog({
		autoOpen: false,
        resizable: false,
        height:165,
        modal: true,
        buttons: {
            "Aceptar": function() {
            	var idC = $("#id_credito").val();
            	window.location.href = "sistema.php?content=editar&idCredito="+idC+"&borrar=1";
            },
            "Cancelar": function() {
                $(this).dialog( "close" );
            }
        }
    });
	//Para confirmar si se desea borrar todo el cr�dito
	$("#dialog-confirm-cred").dialog({
		autoOpen: false,
        resizable: false,
        height:165,
        modal: true,
        buttons: {
            "Aceptar": function() {
            	var idC = $("#id_credito").val();
            	window.location.href = "sistema.php?content=editar&idCredito="+idC+"&borrarCred=1";
            },
            "Cancelar": function() {
                $(this).dialog( "close" );
            }
        }
    });
	
	$("#borrarPagos").click(function(){
    	$("#dialog-confirm").dialog("open");
    	return false; 
    });
	$("#borrarCredito").click(function(){
    	$("#dialog-confirm-cred").dialog("open");
    	return false; 
    });
	//Para los d�as inhabiles
	$("#diainhabil").datepicker({dateFormat: "yy-mm-dd"});
	
});