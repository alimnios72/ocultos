$(function() {
	//Configuramos ajax para que las peticiones al servidor las haga con la codificaci�n correcta
	$.ajaxSetup({contentType: 'application/x-www-form-urlencoded;charset=windows-1252'});
	$("#buscarPersona").autocomplete({
		source: "procesos/buscarPersona.php",
		minLength: 2,
		select: function(event, ui ){
		    var id = ui.item.id_persona;
		    $("#idPersona").val(id);
		    $("#datosCliente").load('procesos/funcionesAjax.php',{accion:'datosC',id:id},function(){
		    	$("#historial").load('procesos/funcionesAjax.php',{accion:'historial',id:id});
		    });
		    $("#edoCta").html('');
		}
	});
	//Configuraci�n de cr�dito para la impresi�n
	$("a[id^='idC_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		$("input[name='idC'][value='"+field[1]+"']").attr('checked',true);
		$("#id_credito").val(field[1]);
		$("#configCredito").load('procesos/funcionesAjax.php',{accion:'configCred', id:field[1]});
	});
	//Para seleccionar la fecha de entrega del cr�dito
	$("#fecha_inicio").live("click",function(){
		$(this).datepicker({showOn:'focus', dateFormat: 'yy-mm-dd'}).focus();
	});
	$("#fecha_inicio").live("change",function(){
		if($(this).val() != ""){
			var date = $(this).val();
			date = date.split(/-/);
			var year = date[0];
			var month = date[1] - 1;
			var day = date[2];
	    	var fecha = new Date(year,month,day);
	    	var diaSem = fecha.getDay();
	    	var diaMes = parseInt(day, 10);
	    	$("#diaSemana").val(diaSem);
	    	$("#diaMes").val(diaMes);
		}
	});
	$("#nacimiento").datepicker({yearRange: "-70:-18", dateFormat: "yy-mm-dd",changeMonth: true, changeYear: true});
	//No permito que seleccionen dia de la semana y dia del mes al mismo tiempo
    $("#esSemana").live("change",function(){
		if($("#esSemana").is(':checked'))
		    $("#esMes").removeAttr("checked");
    });
    $("#esMes").live("change", function(){
		if($("#esMes").is(':checked'))
		    $("#esSemana").removeAttr("checked");
    });
    //Actualiza los detalles del credito
    $("#updateCred").live("click", function(){
    	var id = $("#id_credito").val();
    	var expediente = $("#expediente").val();
    	var digVer = $("#digVer").val();
    	var fecha = $("#fecha_inicio").val();
    	var esSemana = $("#esSemana").is(':checked') ? true : false;
    	var esMes = $("#esMes").is(':checked') ? true : false;
    	var diaSemana = $("#diaSemana").val();
    	var diaMes = $("#diaMes").val();
    	var banco = $("#bancoD").val();
    	alert("Informaci�n guardada");
    	$("#configCredito").load('procesos/funcionesAjax.php',{accion:'configCred', update:true, id:id, fecha:fecha, esSemana:esSemana,
    															esMes:esMes, diaSemana:diaSemana, diaMes:diaMes, cta_deposito: banco, 
    															expediente: expediente, digVer: digVer});
    });
  	//Para mostrar la tabla de amortizacion de un credito
	$("a[id^='idCAmt_']").click(function(){
		var id=$(this).attr('id');
		var field=id.split(/_/);
		$("#amortCredito").load('procesos/funcionesAjax.php',{accion:'amortCred', id:field[1]});
	});
    //Para borrar los campos de filtro de b�squeda por cr�dito
    $("#cleanResults").click(function(){
    	window.location = "administracion.php?content=buscar";
    });
    //Carga combos de delegaciones
    $("#estado").change(function(){
    	$("#deleg").load('procesos/funcionesAjax.php',{accion:'delegaciones', idEstado:$("#estado").val()});
    });
    $("#estado_negocio").change(function(){
    	$("#deleg_neg").load('procesos/funcionesAjax.php',{accion:'delegaciones', idEstado:$("#estado_negocio").val(), nombre:'delegacion_negocio'});
    });
    //Para los campos ocultos de la solicitud
    ($("#edocivil").val() == "Casado" || $("#edocivil").val() == "Union Libre") ? $("#conyuge").show() : $("#conyuge").hide();
    ($('#siPC').is(':checked')) ? $("#pagando_cred").show() : $("#pagando_cred").hide();
    ($('#siPA').is(':checked')) ? $("#pension_alim").show() : $("#pension_alim").hide();
    $("#edocivil").change(function(){
		if($(this).val() == "Casado" || $(this).val() == "Union Libre")
		    $("#conyuge").show();
		else
		    $("#conyuge").hide();
    });
    $("input:radio[name='pag_credito']").change(function(){
		if($('#siPC').is(':checked'))
		    $("#pagando_cred").show();
		else
		    $("#pagando_cred").hide();
    });
     $("input:radio[name='pension_alim']").change(function(){
		if($('#siPA').is(':checked'))
		    $("#pension_alim").show();
		else
		    $("#pension_alim").hide();
    });
    //Para ver el estado de cuenta en PDF y poderlo imprimir
    $("a[id^='edoCta_']").click(function(){
    	var id = $(this).attr('id');
		var field = id.split(/_/);
    	popitup('procesos/edoCta.php?idC='+field[1]);	
    });
    //Para ver el estado de cuenta de mora en PDF y poderlo imprimir
    $("a[id^='edoCtaMora_']").click(function(){
    	var id = $(this).attr('id');
		var field = id.split(/_/);
    	popitup('procesos/edoCtaMora.php?idC='+field[1]);	
    });
    $("#tabs").tabs();
     
});
function popitup(url) {
	newwindow=window.open(url,'Documentaci�n','height=600,width=800,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
