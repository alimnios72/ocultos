$(function() {
	$("#fecha1").datepicker({dateFormat: "yy-mm-dd"});
    $("#fecha2").datepicker({dateFormat: "yy-mm-dd"});
    $("#carteraVencida").tablesorter();
    $("a[id^='a_']").click(function(){
    	var id=$(this).attr('id');
		var field=id.split(/_/);
		var div= "#div_"+field[1];
		if($(div).css('display') == "none")
			$(div).css('display','block');
		else
			$(div).css('display','none');
    });
    $("a[id^='aU_']").click(function(){
    	var id=$(this).attr('id');
		var field=id.split(/_/);
		var div= "#divU_"+field[1];
		if($(div).css('display') == "none")
			$(div).css('display','block');
		else
			$(div).css('display','none');
    });
    $("#tabs").tabs();
    $("#showSearch").click(function(){
    	if($("#buscarDiv").css('display') == "none")
    		$("#buscarDiv").css('display','block');
		else
			$("#buscarDiv").css('display','none');
    });
    $("#usuario").autocomplete({
        minLength: 2,
        source: "procesos/buscarAdministrador.php",
        select: function(event, ui){
        	$("#userID").val(ui.item.id);
        }
    });
    $("#persona").change(function(){
    	if(this.checked)
    		$("#tiempo").css("display","block");
    	else
    		$("#tiempo").css("display","none");
    });
    
    //Gr�fica totales por mes
    var grupos, indivs, meses;
    $.ajax({
        url:  "procesos/graficas.php",
        type: "POST",
        data: {reporte: 'Totalxtipo', anio: 2012},
        async: false,
        dataType: 'json',
        success: function(data){
            grupos = data.grupos;
			indivs = data.indivs;
			meses = data.meses
			
        }
    });
    var graf = $.jqplot('chartdiv',  [grupos, indivs],{
    	title: 'Total prestado por tipo (2012)',
    	seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            rendererOptions: {fillToZero: true},
        },
        series:[
                {label:'Grupos'},
                {label:'Individuales'}
            ],
        legend: {
                show: true,
                placement: 'outsideGrid'
                
            },
        axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: meses
                },
                yaxis: {
                    pad: 1.05,
                    tickOptions: {formatString: "$%'\''10d\n"}
                }
            }
    });
});
