<?php
include('main.php');
//Para las hojas de estilo y javascript
$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css','jquery-ui.css');
$js=array('jquery.js','switcher.js','toggle.js','jquery-ui.js','ui.core.js','contabilidad.js','ui.datepicker-es.js');
$topmenu['conta'] = 'id="menu-active"';
$leftmenu = array('Depositos'=>'depositos','Cheques'=>array('Impresi�n'=>'impresionCh','Gesti�n'=>'gestionCh'), 
				'Referencia bancaria'=>'refBanc','Egresos'=>'egresos','Flujo de efectivo'=>'fjoefec');

//Identificamos cual va a ser el contenido a mostrar de la seccion
$content = isset($_GET['content'])? $_GET['content'] : false;
switch($content)
{
    case 'depositos':
        $section = RUTA_SEC.'contabilidad/depositos.php';
        break;
    case 'deposito':
    	$section = RUTA_SEC.'contabilidad/deposito.php';
    	break;
    case 'impresionCh':
        $section = RUTA_SEC.'contabilidad/impresionCh.php';
        break;
    case 'gestionCh':
        $section = RUTA_SEC.'contabilidad/gestionCh.php';
        break;
    case 'refBanc':
        $section = RUTA_SEC.'contabilidad/refBanc.php';
        break;
    case 'egresos':
    	$section = RUTA_SEC.'contabilidad/egresos.php';
    	break;
    case 'fjoefec':
    	$section = RUTA_SEC.'contabilidad/fjoefec.php';
    	break;
    default:
        $section = RUTA_TPL.'home.tpl.php';
        $rutaTemplate = '';
        $template = 'principal.tpl.php';
        break;
}
include(RUTA_TPL.'header.tpl.php');
include($section);
include(RUTA_TPL.'footer.tpl.php');
?>	
