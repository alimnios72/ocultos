<?php
include('main.php');
	
	//Para las hojas de estilo y javascript
	$css=array('reset.css','main.css','2col.css','1col.css','main-ie6.css','style.css','jquery-ui.css');
	$js=array('jquery.js','switcher.js','toggle.js','jquery-ui.js','ui.core.js','ui.datepicker-es.js','sistema.js');
	$topmenu['sist'] = 'id="menu-active"';
	$leftmenu = array('Variables'=>array('Tasas de inter�s'=>'tasas', 'Plazos' => 'plazos', 'Zonas'=>'zonas','Delegaciones/Municipios'=>'delegaciones',
					'D�as Inhabiles Bancarios'=>'diasInhabiles','Alerta monto pago'=>'alertapago'),
					  'Usuarios del sistema'=>array('Alta/Edici�n de usuario'=>'usuarios','Permisos'=>'permisos'),
					  'Cr�ditos'=>array('Modificar cr�dito'=>'buscar'),);
	
	//Algunos arreglos de configuraci�n
	$productoCred = array(array('id'=>0,'txt'=>'CrediMarchante'),
						  array('id'=>1,'txt'=>'CrediN�mina'),
						  array('id'=>2,'txt'=>'CrediHipoteca'));
	$statusCred = array(array('id'=>0,'txt'=>'Preaprobado'),
						array('id'=>1,'txt'=>'Aprobado'),
						array('id'=>2,'txt'=>'Activo'),
						array('id'=>4,'txt'=>'Concluido'));
	$periodoCred = array(array('val'=>'Semanal'),array('val'=>'Quincenal'),array('val'=>'Mensual'),array('val'=>'Bimestral'),array('val'=>'Trimestral'),array('val'=>'Quinquenal'),array('val'=>'Semestral'),array('val'=>'Anual'));
	
	
	//Identificamos cual va a ser el contenido a mostrar de la seccion
	$content = isset($_GET['content'])? $_GET['content'] : false;
	switch($content)
	{
		case 'delegaciones':
			$section = RUTA_SEC.'sistema/delegaciones.php';
			break;
		case 'zonas':
			$section = RUTA_SEC.'sistema/zonas.php';
			break;
		case 'tasas':
			$section = RUTA_SEC.'sistema/tasasInteres.php';
			break;
		case 'plazos':
			$section = RUTA_SEC.'sistema/plazos.php';
			break;
		case 'buscar':
			$section = RUTA_SEC.'sistema/buscarCredito.php';
			break;
		case 'editar':
			$section = RUTA_SEC.'sistema/editarCredito.php';
			break;
		case 'usuarios':
			$section = RUTA_SEC.'sistema/nuevoUsuario.php';
			break;
		case 'permisos':
			$section = RUTA_SEC.'sistema/permisos.php';
			break;
		case 'diasInhabiles':
			$section = RUTA_SEC.'sistema/diasInhabiles.php';
			break;
		case 'alertapago':
			$section = RUTA_SEC.'sistema/alertapago.php';
			break;
		default:
			$section = RUTA_TPL.'home.tpl.php';
			$rutaTemplate = '';
			$template = 'principal.tpl.php';
			break;
	}
	
	include(RUTA_TPL.'header.tpl.php');
	include($section);
	include(RUTA_TPL.'footer.tpl.php');
?>
